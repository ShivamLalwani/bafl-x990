package cn.access.group.android_all_banks_pos.repository.dao;



import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * on 8/29/2019.
 * DAO contains the methods to query transaction detail table
 */
@Dao
public interface ReversalDao {

//    @Query("SELECT * FROM transactiondetail ORDER BY txnDate DESC,txnTime DESC")
//    List<TransactionDetail> getAllTransactions();
//
//    @Query("SELECT * FROM transactiondetail ORDER BY txnDate DESC,txnTime DESC")
//    Flowable<List<TransactionDetail>>getAllTransactionsLive();
//
//    @Query("SELECT * FROM transactiondetail where invoiceNo LIKE  :invoiceNo")
//    Maybe<TransactionDetail> findByInvoiceNo(String invoiceNo);
//
//    @Query("SELECT * FROM transactiondetail ORDER BY tdid DESC Limit 1")
//    Maybe<TransactionDetail> lastReceipt();

//    @Query("SELECT * FROM reversal where status LIKE :status")
//    Maybe<Reversal> findByStatus(String status);

    @Query("SELECT * FROM reversal LIMIT 1 ")
    Maybe<Reversal> findReversal();

    @Insert
    void saveSettlement(List<TransactionDetail> transactionDetail);

//    @Query("SELECT * FROM transactiondetail where authorizationIdentificationResponseCode LIKE  :authorizationIdentificationResponseCode")
//    LiveData<TransactionDetail> findByAuthId(String authorizationIdentificationResponseCode);

//    @Query("SELECT * FROM transactiondetail where status LIKE  :status")
//    Maybe <List<TransactionDetail>> findByStatusList(String status);
//
//
//    @Update(onConflict = OnConflictStrategy.REPLACE)
//    void updateTransaction(TransactionDetail transactionDetail);
//
//    @Query("UPDATE transactiondetail SET txnType =:txnType, cancelTxnType=:cancelTxnType,processingCode=:proc WHERE tdid =:tdid")
//    void updateStatus(String txnType, String cancelTxnType,String proc, int tdid);
//
//    @Query("SELECT txnType as txnType ,COUNT(txnType) as count, SUM(amount) as totalAmount from transactiondetail GROUP BY txnType")
//    Maybe<List<CountedTransactionItem>> transactionSumCount();
//
//    @Query("SELECT txnType as txnType ,COUNT(txnType) as count, SUM(amount) as totalAmount from transactiondetail GROUP BY txnType")
//    Maybe<List<CountedTransactionItem>> transactionCount();
//
//    @Query("SELECT txnType as txnType, cardScheme as cardScheme ,COUNT(txnType) as count, SUM(amount) as totalAmount from transactiondetail GROUP BY cardScheme,txnType")
//    Maybe<List<CountedTransactionItem>> transactionCardSchemeCount();

    @Insert
    void insertAll(TransactionDetail... transactionDetails);

    @Insert
    void insertTransaction(Reversal transactionDetail);


    @Delete
    void delete(Reversal transactionDetail);

    @Query("DELETE FROM Reversal")
    void deleteReversal();

    @Query("DELETE FROM Reversal")
    void nukeTable();

}
