package cn.access.group.android_all_banks_pos.Utilities;

import java.util.HashMap;

/**
 *  Message Format Specifications for POS Devices
 *  Version 1.0
 *  Appendix A - Error Codes
 *  Following are the Error Codes which will be used in POS Transactions.
 * on 9/26/2019.
 */
public class ErrorMap {


    public HashMap<String,String> errorCodes= new HashMap<>();

    public ErrorMap() {

        errorCodes.put("0","Approved");

        errorCodes.put("1","Refer To Card Issuer");

        errorCodes.put("2","Refer To Card Issuer Special");

        errorCodes.put("3","Invalid Merchant");

        errorCodes.put("4","Pick Up");

        errorCodes.put("5","Do Not Honour");

        errorCodes.put("6","Error");

        errorCodes.put("7","Pick Up Special");

        errorCodes.put("8","Honour With ID");

        errorCodes.put("9","Request In Progress");

        errorCodes.put("10","Approved Partial");

        errorCodes.put("11","Approved VIP");

        errorCodes.put("12","Invalid Tran");

        errorCodes.put("13","Invalid Amount");

        errorCodes.put("14","Invalid Card Number");

        errorCodes.put("15","No Such Issuer");

        errorCodes.put("16","Approved Update Track 3");

        errorCodes.put("17","Customer Cancellation");

        errorCodes.put("19","Re-enter Transaction");

        errorCodes.put("20","Invalid response");

        errorCodes.put("21","No Action Taken");

        errorCodes.put("22","Suspected Malfunction");

        errorCodes.put("23","Unacceptable Tran Fee");

        errorCodes.put("24","File Update Not Supported");

        errorCodes.put("25","Unable To Locate Record");

        errorCodes.put("26","Duplicate Record");

        errorCodes.put("27","File Update Edit Error");

        errorCodes.put("28","File Update File Locked");

        errorCodes.put("29","File Update Failed");

        errorCodes.put("30","Format Error");

        errorCodes.put("31","Bank Not Supported");

        errorCodes.put("33","Expired Card Pick Up");

        errorCodes.put("34","Suspected Fraud Pick Up");

        errorCodes.put("35","Contact Acquirer Pick Up");

        errorCodes.put("36","Restricted Card Pick Up");

        errorCodes.put("37","Call Acquirer Security Pick Up");

        errorCodes.put("38","PIN Tries Exceeded Pick Up");

        errorCodes.put("39","No Credit Account");

        errorCodes.put("40","Function Not Supported");

        errorCodes.put("41","Lost Card");
        errorCodes.put("43","Stolen Card");
        errorCodes.put("51","Not Sufficient Funds");
        errorCodes.put("54","Expired Card");
        errorCodes.put("55","Incorrect PIN");
        errorCodes.put("56","No Card Record");
        errorCodes.put("57","Tran Not Permitted Cardholder");
        errorCodes.put("58","Tran Not Permitted Terminal");
        errorCodes.put("59","Suspected Fraud Declined");
        errorCodes.put("60","Contact Acquirer");
        errorCodes.put("61","Exceeds Withdrawal Limit");
        errorCodes.put("62","Restricted Card");
        errorCodes.put("63","Security Violation");
        errorCodes.put("65","Exceeds Withdrawal Frequency");
        errorCodes.put("66","Call Acquirer Security");
        errorCodes.put("68","Response Received Too Late");
        errorCodes.put("75","PIN Tries Exceeded");
        errorCodes.put("81","PIN Cryptographic Error Found");
        errorCodes.put("82","Negative CAM, dCVV, iCVV, or CVV results");
        errorCodes.put("88","Crypto Failed");
        errorCodes.put("90","Cut-off In Progress");
        errorCodes.put("91","Issuer Or Switch Inoperative");
        errorCodes.put("93","Violation Of Law");
        errorCodes.put("95","Reconcile Error");
        errorCodes.put("96","Reconcile Error if batch no. not found/match");




        ////////////////////////////////////////////
//        errorCodes.put("01","Pending");
//        errorCodes.put("00","Processed OK");
//        errorCodes.put("02","Account Inactive");
//        errorCodes.put("03","Accumulator error");
//        errorCodes.put("04","Adjust Not Allowed");
//        errorCodes.put("05","Already Used Cheque");
//        errorCodes.put("06","Already Voided");
//        errorCodes.put("07","Batch clear failed");
//        errorCodes.put("08","Batch full");
//        errorCodes.put("09","Batch Not Empty");
//        errorCodes.put("10","Call Cleared");
//        errorCodes.put("11","Card Expired");
//        errorCodes.put("12","Card Not Supported");
//        errorCodes.put("13","Card Read Error/ No Track on Card");
//        errorCodes.put("14","Carrier Lost");
//        errorCodes.put("15","Carrier Time-out");
//        errorCodes.put("16","Checksum Error");
//        errorCodes.put("17","Comms Error");
//        errorCodes.put("18","Database Error");
//        errorCodes.put("19","Download Failed");
//        errorCodes.put("20","Edit Not Allowed");
//        errorCodes.put("21","Error reading card device");
//        errorCodes.put("22","Exceed Adj Max");
//        errorCodes.put("23","Exceeded Cycle Transaction Limit");
//        errorCodes.put("24","Exec nn");
//        errorCodes.put("25","Expired Card");
//        errorCodes.put("26","Fatal Memory Error");
//        errorCodes.put("27","Field Error");
//        errorCodes.put("28","Field not found");
//        errorCodes.put("29","File Error Page");
//        errorCodes.put("30","Host Comms Down");
//        errorCodes.put("31","Host link fail");
//        errorCodes.put("32","Host Not Processing");
//        errorCodes.put("33","Host Reject");
//        errorCodes.put("34","Hot Card");
//        errorCodes.put("35","HSM Timeout");
//        errorCodes.put("36","Invalid Account");
//        errorCodes.put("37","Invalid Account Status");
//        errorCodes.put("38","Invalid amount");
//        errorCodes.put("39","Invalid card length");
//        errorCodes.put("40","Invalid Card Record");
//       errorCodes.put("41","Invalid Host Mode");
//        errorCodes.put("42","Invalid Cheque Number");
//        errorCodes.put("43","Invalid host type");
//        errorCodes.put("44","Invalid invoice number");
//        errorCodes.put("45","Invalid LUHN Check");
//        errorCodes.put("46","Invalid Password");
//        errorCodes.put("47","Invalid PIN");
//        errorCodes.put("48","Invalid User Id");
//        errorCodes.put("49","Invalid STAN");
//        errorCodes.put("50","Invalid Terminal Id");
//        errorCodes.put("51","Invalid Transaction Code");
//        errorCodes.put("52","ISDN Port Error");
//        errorCodes.put("53","ISDN Comms Error");
//        errorCodes.put("54","Limit Exceed");
//        errorCodes.put("55","Link Close Fail");
//        errorCodes.put("56","Low Balance");
//        errorCodes.put("57","Manual Not Allowed");
//        errorCodes.put("58","Memory Error");
//        errorCodes.put("59","Message Format Error");
//        errorCodes.put("60","No Answer");
//        errorCodes.put("61","No Host Response");
//        errorCodes.put("62","No Line");
//        errorCodes.put("63","No Manual Entry");
//        errorCodes.put("64","No Match Found");
//        errorCodes.put("65","Out Of Range");
//        errorCodes.put("66","PIN Change Reject");
//        errorCodes.put("67","PIN Mismatch");
//        errorCodes.put("68","PIN Retries Exceeded");
//        errorCodes.put("69","Printer Error");
//        errorCodes.put("70","Record Not Found");
//        errorCodes.put("71","Reprint info load fail");
//        errorCodes.put("72","Reprint info save fail");
//        errorCodes.put("73","Reversal file error");
//        errorCodes.put("74","Settlement Failed");
//        errorCodes.put("75","STAN");
//        errorCodes.put("76","Table Init Error");
//        errorCodes.put("77","Time out");
//        errorCodes.put("78","Tip Error");
//        errorCodes.put("79","Track not found");
//        errorCodes.put("80","Trans rejected");
//        errorCodes.put("81","Trans Cancelled");
//        errorCodes.put("82","Transaction failed");
//        errorCodes.put("83","Transaction Not Allowed");
//        errorCodes.put("84","Transaction Rejected");
//        errorCodes.put("85","Transaction Reversed");
//        errorCodes.put("86","Unknown Auth Mode");
//        errorCodes.put("87","Unknown Error");
//        errorCodes.put("88","Void Not Allowed");
//        errorCodes.put("89","Warm Card");
//        errorCodes.put("B0","Internal Error");

        //errorCodes.put("00",	"Successful approval/completion or that VIP PIN verification is valid");//is success
        errorCodes.put("01",	"Refer to card issuer");
        errorCodes.put("02",	"Refer to card issuer, special condition");
        errorCodes.put("03",	"Invalid merchant or service provider");
        errorCodes.put("04",	"Pickup");
        errorCodes.put("05",	"Do not honor");
        errorCodes.put("06",	"General error");
        errorCodes.put("07",	"Pickup card, special condition (other than lost/stolen card)");
        errorCodes.put("08",	"Honor with identification");
        errorCodes.put("09",	"Request in progress");
        errorCodes.put("10",	"Partial approval");
        errorCodes.put("11",	"VIP approval");
        errorCodes.put("12",	"Invalid transaction");
        errorCodes.put("13",	"Invalid amount (currency conversion field overflow) or amount exceeds maximum for card program");
        errorCodes.put("14",	"Invalid account number (no such number)");
        errorCodes.put("15",	"No such issuer");
        errorCodes.put("16",	"Insufficient funds");
        errorCodes.put("17",	"Customer cancellation");
        errorCodes.put("19",    "Re-enter transaction");
        errorCodes.put("20",	"Invalid response");
        errorCodes.put("21",	"No action taken (unable to back out prior transaction)");
        errorCodes.put("22",	"Suspected Malfunction");
        errorCodes.put("25",	"Unable to locate record in file, or account number is missing from the inquiry");
        errorCodes.put("28",	"File is temporarily unavailable");
        errorCodes.put("30",	"Format error");
        errorCodes.put("41",	"Merchant should retain card (card reported lost)");
        errorCodes.put("43",	"Merchant should retain card (card reported stolen)");
        errorCodes.put("51",	"Insufficient funds");
        errorCodes.put("52",	"No checking account");
        errorCodes.put("53",	"No savings account");
        errorCodes.put("54",	"Expired card");
        errorCodes.put("55",	"Incorrect PIN");
        errorCodes.put("57",	"Transaction not permitted to cardholder");
        errorCodes.put("58",	"Transaction not allowed at terminal");
        errorCodes.put("59",	"Suspected fraud");
        errorCodes.put("61",	"Activity amount limit exceeded");
        errorCodes.put("62",	"Restricted card (for example, in country exclusion table)");
        errorCodes.put("63",	"Security violation");
        errorCodes.put("65",	"Activity count limit exceeded");
        errorCodes.put("68",	"Response received too late");
        errorCodes.put("75",	"Allowable number of PIN-entry tries exceeded");
        errorCodes.put("76",	"Unable to locate previous message (no match on retrieval reference number)");
        errorCodes.put("77",	"Previous message located for a repeat or reversal, but repeat or reversal data are inconsistent with original message");
        errorCodes.put("78",	"’Blocked, first used’—The transaction is from a new cardholder, and the card has not been properly unblocked.");
        errorCodes.put("80",	"Visa transactions: credit issuer unavailable. Private label and check acceptance: Invalid date");
        errorCodes.put("81",	"PIN cryptographic error found (error found by VIC security module during PIN decryption)");
        errorCodes.put("82",	"Negative CAM, dCVV, iCVV, or CVV results");
        errorCodes.put("83",	"Unable to verify PIN");
        errorCodes.put("85",	"No reason to decline a request for account number verification, address verification, CVV2 verification; or a credit voucher or merchandise return");
        errorCodes.put("91",	"Issuer unavailable or switch inoperative (STIP not applicable or available for this transaction)");
        errorCodes.put("92",	"Destination cannot be found for routing");
        errorCodes.put("93",	"Transaction cannot be completed, violation of law");
        errorCodes.put("94",	"Duplicate transmission");
        errorCodes.put("95",	"Reconcile error");
        errorCodes.put("96",	"System malfunction, System malfunction or certain field error conditions");






    }



}
