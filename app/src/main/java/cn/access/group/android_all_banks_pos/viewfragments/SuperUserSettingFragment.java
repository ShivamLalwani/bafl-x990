package cn.access.group.android_all_banks_pos.viewfragments;


import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.system_service.aidl.ISystemManager;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLEMENT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLETIME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CHECK_EXPIRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_ETHERNET;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_GPRS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CURRENCY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.EMV_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FALLBACK_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_3;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_4;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HIGH_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_ICC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_MAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LOW_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MAINTENANCE_THRESHOLD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANAGER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY_ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_NAME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MOD10;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MSGHEADER_LENHEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.NII;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PIN_BYPASS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SSL_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SUPER_USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SYSTEM_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_THRESHOLD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TOPUP_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.USER_PASSWORD_CHANGE;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author muhammad.humayun
 * on 9/19/2019.
 */
@SuppressLint("ValidFragment")
public class SuperUserSettingFragment extends Fragment {


    String TAG="SuperUserSettingFragment";
    @BindView(R.id.terminal_id_number)
    TextView terminalIdNumber;
    @BindView(R.id.merchant_id_number)
    TextView merchantIdNumber;
    @BindView(R.id.merchant_name)
    TextView merchantName;
    @BindView(R.id.com_Ethernet)
    TextView comEthernet;
    @BindView(R.id.com_GPRS)
    TextView comGPRS;
    @BindView(R.id.market_Seg)
    TextView marketSeg;
    @BindView(R.id.tip_Thershold)
    TextView tipThreshold;
    @BindView(R.id.fallback_Enable)
    TextView fallbackEnable;
    @BindView(R.id.msg_Header_Len_Hex)
    TextView msgHeaderLenHex;
    @BindView(R.id.ssl_Enable)
    TextView sslEnable;
    @BindView(R.id.topup_Enable)
    TextView topUpEnable;
    @BindView(R.id.manual_Entry)
    TextView manualEntry;
    @BindView(R.id.auto_Settlement)
    TextView autoSettlement;
    @BindView(R.id.emv_logs_enable)
    TextView emvLogsEnable;
    @BindView(R.id.auto_Settle_Time)
    TextView autoSettleTime;
    @BindView(R.id.pin_ByPass)
    TextView pinByPass;
    @BindView(R.id.mod10)
    TextView mod10;
    @BindView(R.id.check_Expiry)
    TextView checkExpiry;
    @BindView(R.id.manual_Entry_issuer)
    TextView manualEntryIssuer;
    @BindView(R.id.last4digit_Mag)
    TextView last4DigitMag;
    @BindView(R.id.last4digit_Icc)
    TextView last4DigitIcc;
    @BindView(R.id.acquirer_Number)
    TextView acquirer;
    @BindView(R.id.low_BIN)
    TextView lowBIN;
    @BindView(R.id.high_BIN)
    TextView highBIN;
    @BindView(R.id.issuer_Number)
    TextView issuer;
    @BindView(R.id.currency)
    TextView currency;
    @BindView(R.id.user_Password_change)
    TextView userPassword;
    @BindView(R.id.manager_Password_change)
    TextView managerPassword;
    @BindView(R.id.system_User_Password_change)
    TextView systemuserPassword;
    @BindView(R.id.super_User_Password_change)
    TextView superuserPassword;
    @BindView(R.id.header_Line_1)
    TextView headerLine1;
    @BindView(R.id.header_Line_2)
    TextView headerLine2;
    @BindView(R.id.header_Line_3)
    TextView headerLine3;
    @BindView(R.id.header_Line_4)
    TextView headerLine4;
    @BindView(R.id.footer_Line_1)
    TextView footerLine1;
    @BindView(R.id.footer_Line_2)
    TextView footerLine2;
    @BindView(R.id.maintenance_threshold_tv)
    TextView maintenanceThresholdTv;

//    @BindView(R.id.tmsUpdate)
//    Button tmsUpdate;
    private WeakReference<Context>  mContext;


    //    @BindView(R.id.host_ip_number)
//    TextView hostIpNumber;
//    @BindView(R.id.host_port_number)
//    TextView hostPortNumber;
    @BindView(R.id.nii_number)
    TextView niiNumber;
    @BindView(R.id.tipText)
    TextView tipText;
    @BindView(R.id.tipToggle)
    SwitchCompat tipToggle;
    @BindView(R.id.SSLToggle)
    SwitchCompat SSLToggle;
    @BindView(R.id.FallBackToggle)
    SwitchCompat FallBackToggle;
    @BindView(R.id.ManualEntryToggle)
    SwitchCompat ManualEntryToggle;
    @BindView(R.id.AutoSettlementToggle)
    SwitchCompat AutoSettlementToggle;
    @BindView(R.id.EmvLogsToggle)
    SwitchCompat EmvLogsToggle;
    @BindView(R.id.AutoSettlementTimeToggle)
    SwitchCompat AutoSettlementTimeToggle;
    @BindView(R.id.market_Seg_ll)
    LinearLayout market_Seg_ll;
    @BindView(R.id.terminal_id_number_ll)
    LinearLayout terminal_id_number_ll;
    @BindView(R.id.merchant_id_number_ll)
    LinearLayout merchant_id_number_ll;
    @BindView(R.id.tip_Thershold_ll)
    LinearLayout tip_Thershold_ll;
    @BindView(R.id.nii_number_ll)
    LinearLayout nii_number_ll;
    @BindView(R.id.currency_ll)
    LinearLayout currency_ll;
    @BindView(R.id.user_Password_change_ll)
    LinearLayout user_Password_change_ll;
    @BindView(R.id.manager_Password_change_ll)
    LinearLayout manager_Password_change_ll;
    @BindView(R.id.super_User_Password_change_ll)
    LinearLayout super_User_Password_change_ll;
    @BindView(R.id.system_User_Password_change_ll)
    LinearLayout system_User_Password_change_ll;
    @BindView(R.id.merchant_name_ll)
    LinearLayout merchant_name_ll;
    @BindView(R.id.header_Line_1_ll)
    LinearLayout header_Line_1_ll;
    @BindView(R.id.header_Line_2_ll)
    LinearLayout header_Line_2_ll;
    @BindView(R.id.header_Line_3_ll)
    LinearLayout header_Line_3_ll;
    @BindView(R.id.header_Line_4_ll)
    LinearLayout header_Line_4_ll;

    @BindView(R.id.footer_Line_1_ll)
    LinearLayout footer_Line_1_ll;
    @BindView(R.id.footer_Line_2_ll)
    LinearLayout footer_Line_2_ll;
    @BindView(R.id.auto_Settle_Time_ll)
    LinearLayout auto_Settle_Time_ll;
    TextView superUserSetting;
    List<TransactionDetail> transactionDetailsList;
    @BindView(R.id.maintenance_threshold_ll)
    LinearLayout maintenance_threshold_ll;
    private ScheduledExecutorService scheduledExecutorService;

    Unbinder unbinder;
    String newText;
    boolean Validated = false;
    ISystemManager iSystemManager;
    IEMV iemv;
    String tipEnableText = "";

    String sslText = "";
    private  final ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            IDeviceService idevice = IDeviceService.Stub.asInterface(service);
            try {
                iemv = idevice.getEMV();
                //Toast.makeText(mContext, "bind service success", Toast.LENGTH_SHORT).show();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @SuppressLint("ValidFragment")
    public SuperUserSettingFragment() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public SuperUserSettingFragment(ISystemManager iSystemManager) {
        // Required empty public constructor
        this.iSystemManager = iSystemManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_superuser_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        // hostInformation = MultiHostsConfig.get(DashboardContainer.index);
        tipToggle = view.findViewById(R.id.tipToggle);
        marketSeg = view.findViewById(R.id.market_Seg);
        try {
            transactionDetailsList = ((DashboardContainer)mContext.get()).getAllBatchTransactions();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG("TAG",SharedPref.read("TIP_ENABLED", ""));

//        if(SharedPref.read("TIP_ENABLED", "").equals("Y")){
//            tipToggle.setChecked(true);
//        }
//        else {
//            tipToggle.setChecked(false);
//        }


        Utility.DEBUG_LOG("showtiptext oncreate","TIP_ENABLED:"+TIP_ENABLED);
        superUserSetting = (TextView) view.findViewById(R.id.superUserSetting);
        isNetworkConnected();
        terminalIdNumber.setText(TERMINAL_ID);
        merchantIdNumber.setText(MERCHANT_ID);
        merchantName.setText(MERCHANT_NAME);
        comEthernet.setText(COM_ETHERNET);
        comGPRS.setText(COM_GPRS);
        marketSeg.setText(MARKET_SEG);
        tipThreshold.setText(TIP_THRESHOLD);
        fallbackEnable.setText(FALLBACK_ENABLE);
        msgHeaderLenHex.setText(MSGHEADER_LENHEX);
        sslEnable.setText(SSL_ENABLE);
        topUpEnable.setText(TOPUP_ENABLE);
        manualEntry.setText(MANUAL_ENTRY);
        autoSettlement.setText(AUTO_SETTLEMENT);
        autoSettleTime.setText(AUTO_SETTLETIME);
        pinByPass.setText(PIN_BYPASS);
        mod10.setText(MOD10);
        checkExpiry.setText(CHECK_EXPIRY);
        manualEntryIssuer.setText(MANUAL_ENTRY_ISSUER);
        last4DigitMag.setText(LAST_4DIGIT_MAG);
        last4DigitIcc.setText(LAST_4DIGIT_ICC);
        acquirer.setText(ACQUIRER);
        lowBIN.setText(LOW_BIN);
        highBIN.setText(HIGH_BIN);
        issuer.setText(ISSUER);
        currency.setText(CURRENCY);
        userPassword.setText(USER_PASSWORD_CHANGE);
        managerPassword.setText(MANAGER_PASSWORD_CHANGE);
        systemuserPassword.setText(SYSTEM_PASSWORD_CHANGE);
        superuserPassword.setText(SUPER_USER_PASSWORD_CHANGE);
        headerLine1.setText(HEADER_LINE_1);
        headerLine2.setText(HEADER_LINE_2);
        headerLine3.setText(HEADER_LINE_3);
        headerLine4.setText(HEADER_LINE_4);
        footerLine1.setText(FOOTER_LINE_1);
        footerLine2.setText(FOOTER_LINE_2);
        Utility.DEBUG_LOG(TAG,"MAINTENANCE_THRESHOLD 1:"+MAINTENANCE_THRESHOLD);
        maintenanceThresholdTv.setText(MAINTENANCE_THRESHOLD);

        showTipText();
        //hostIpNumber.setText(hostIP);
        //hostPortNumber.setText(String.valueOf(Constants.hostPort));
        niiNumber.setText(NII);

        showSSLText();
        showAutoSettlementText();
        showAutoSettlementTimeText();
        //showEnabledLogsText();
        showFallBackText();
        showManualEntryText();
        setTipListener();
        //setEnableLogsListener();
        setSSLLogListener();
        setFallbackListener();
        setManualEntryListener();
        setAutoSettlementListener();
        setEmvLogsListener();
        setAutoSettlementTimeListener();

        superUserSetting.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
//                Toast.makeText(mcontext, "Long Clicked " ,
//                        Toast.LENGTH_SHORT).show();
                DialogUtil.showKey(Objects.requireNonNull(getActivity()));
                DialogUtil.inputDialog(mContext.get(), "Input Password", "Enter User Password", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        EditText text = Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
                        String password = text.getText().toString();
                        if (password.contentEquals(USER_PASSWORD_CHANGE)) {
                            //show confirmation
                            sweetAlertDialog.dismissWithAnimation();
                            Utility.DEBUG_LOG("Super USer Setting", "before Exit app");
                            getActivity().finish();
                            System.exit(0);
                        } else {
                            DialogUtil.errorDialog(mContext.get(), "Error!", "Wrong Password");
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    }
                }, InputType.TYPE_CLASS_NUMBER, PasswordTransformationMethod.getInstance());

                return true;    // <- set to true
            }
        });

        return view;
    }

    public void isNetworkConnected() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isWifiConn = false;
        boolean isMobileConn = false;
        for (Network network : connMgr.getAllNetworks()) {
            NetworkInfo networkInfo = connMgr.getNetworkInfo(network);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                isWifiConn = networkInfo.isConnected();
            }
            if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                isMobileConn = networkInfo.isConnected();
            }
        }
        if (isWifiConn == true) {
            SharedPref.write("COM_ETHERNET", "Connected");
            COM_ETHERNET = SharedPref.read("COM_ETHERNET", "");
        } else {
            SharedPref.write("COM_ETHERNET", "Not connected");
            COM_ETHERNET = SharedPref.read("COM_ETHERNET", "");
        }
        if (isMobileConn == true) {
            SharedPref.write("COM_GPRS", "Connected");
            COM_GPRS = SharedPref.read("COM_GPRS", "");
        } else {
            SharedPref.write("COM_GPRS", "Not connected");
            COM_GPRS = SharedPref.read("COM_GPRS", "");
        }
        Utility.DEBUG_LOG("Wifi connected: ", String.valueOf(isWifiConn));
        Utility.DEBUG_LOG("Mobile connected: ", String.valueOf(isMobileConn));
    }

//    private void showEnabledLogsText() {
//        if (SharedPref.read("LOGS_ENABLED","").equalsIgnoreCase("Y")){
//            logsEnableText="Logs Enabled";
//            logsenabled.setText(logsEnableText);
//            EnableLogsToggle.setChecked(true);
//        }
//        else if (SharedPref.read("LOGS_ENABLED","").equalsIgnoreCase("N")){
//            logsEnableText="Logs Disabled";
//            logsenabled.setText(logsEnableText);
//            EnableLogsToggle.setChecked(false);
//        }
//    }

    private void showTipText() {
        if (TIP_ENABLED.equals("Y")) {
            tipEnableText = "Tip Enabled";
            tipText.setText(tipEnableText);
            tipToggle. setChecked(true);
            //tipToggle.getTextOn() ;
            Utility.DEBUG_LOG("showtiptext","TIP_ENABLED:"+TIP_ENABLED);
            Utility.DEBUG_LOG("getTextOn","  tipToggle.getTextOn():"+  tipToggle.getTextOn());
        } else{
            tipEnableText = "Tip Disabled";
            tipText.setText(tipEnableText);
            tipToggle.setChecked(false);
            Utility.DEBUG_LOG("showtiptext","TIP_ENABLED:"+TIP_ENABLED);
            Utility.DEBUG_LOG("getTextOn","  tipToggle.getTextOff():"+  tipToggle.getTextOff());
            //tipToggle.setEnabled(false);
        }
//        if(TIP_ENABLED.equals("Y") && tipEnableText.equals("Tip Enabled")){
//            tipToggle.setChecked(true);
//        }else{
//            tipToggle.setChecked(false);
//        }

    }

    private void showSSLText() {
        if (SharedPref.read("SSL_ENABLE", "").equalsIgnoreCase("Y")) {
            sslText = "SSL Enabled";
            sslEnable.setText(sslText);
            SSLToggle.setChecked(true);
        } else if (SharedPref.read("SSL_ENABLE", "").equalsIgnoreCase("N")) {
            sslText = "SSL Disabled";
            sslEnable.setText(sslText);
            SSLToggle.setChecked(false);
        }
    }

    private void showFallBackText() {
        if (SharedPref.read("FALLBACK_ENABLE", "").equalsIgnoreCase("Y")) {

            fallbackEnable.setText("Fallback Enabled");
            FallBackToggle.setChecked(true);
        } else if (SharedPref.read("FALLBACK_ENABLE", "").equalsIgnoreCase("N")) {

            fallbackEnable.setText("Fallback Disabled");
            FallBackToggle.setChecked(false);
        }
    }

    private void showManualEntryText() {
        if (SharedPref.read("MANUAL_ENTRY", "").equalsIgnoreCase("Y")) {

            manualEntry.setText("Manual Entry Enabled");
            ManualEntryToggle.setChecked(true);
        } else if (SharedPref.read("MANUAL_ENTRY", "").equalsIgnoreCase("N")) {

            manualEntry.setText("Manual Entry Disabled");
            ManualEntryToggle.setChecked(false);
        }
    }

    private void showAutoSettlementText() {
        if (SharedPref.read("AUTO_SETTLEMENT", "").equalsIgnoreCase("Y")) {

            autoSettlement.setText("Auto Settlement Enabled");
            AutoSettlementToggle.setChecked(true);
        } else if (SharedPref.read("AUTO_SETTLEMENT", "").equalsIgnoreCase("N")) {

            autoSettlement.setText("Auto Settlement Disabled");
            AutoSettlementToggle.setChecked(false);
        }
    }

    private void showEmvLogsText() {
        if (SharedPref.read("EMV_LOGS", "").equalsIgnoreCase("Y")) {

            emvLogsEnable.setText("EMV Logs Enabled");
            EmvLogsToggle.setChecked(true);
        } else if (SharedPref.read("EMV_LOGS", "").equalsIgnoreCase("N")) {

            autoSettlement.setText("EMV Logs Disabled");
            EmvLogsToggle.setChecked(false);
        }
    }

    private void showAutoSettlementTimeText() {
        if (SharedPref.read("AUTO_SETTLETIME", "").equalsIgnoreCase("Y")) {

            autoSettleTime.setText("Auto SettlementTime Enabled");
            AutoSettlementTimeToggle.setChecked(true);
        } else if (SharedPref.read("AUTO_SETTLETIME", "").equalsIgnoreCase("N")) {

            autoSettleTime.setText("Auto SettlementTime Disabled");
            AutoSettlementTimeToggle.setChecked(false);
        }
    }

//    private void setEnableLogsListener() {
//        EnableLogsToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                String title = "";
//                if (isChecked){
//                    SharedPref.write("LOGS_ENABLED","Y");
//                    ENABLE_LOGS = SharedPref.read("LOGS_ENABLED","");
//                    showEnabledLogsText();
//                    Validated = true;
//                    title = "Logs Enabled";
//                }else {
//                    SharedPref.write("LOGS_ENABLED","N");
//                    ENABLE_LOGS = SharedPref.read("LOGS_ENABLED","");
//                    showEnabledLogsText();
//                    Validated = true;
//                    title = "Logs Disabled";
//                }
//                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
//                Repository repository = new Repository(AppDatabase.getAppDatabase(mContext.get()));
//                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
//                repository.updateTerminalConfiguration(TerminalConfig);
//                DialogUtil.successDialog(mContext.get(), title);
//            }
//        });
//    }

    private void setTipListener() {
        tipToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String title = "";
                if (isChecked) {
                    SharedPref.write("TIP_ENABLED", "Y");
                    TIP_ENABLED = SharedPref.read("TIP_ENABLED", "");
                    showTipText();
                    Validated = true;
                    title = "Tip Enabled";
                    Utility.DEBUG_LOG("tip setListener","TIP_ENABLED:"+TIP_ENABLED);
                } else {
                    SharedPref.write("TIP_ENABLED", "N");
                    TIP_ENABLED = SharedPref.read("TIP_ENABLED", "");
                    showTipText();
                    Validated = true;
                    title = "Tip Disabled";
                    Utility.DEBUG_LOG("tip setListener","TIP_ENABLED:"+TIP_ENABLED);
                }
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                DialogUtil.successDialog(mContext.get(), title);
            }
        });
    }

    private void setSSLLogListener() {
        SSLToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String title = "";
                if (isChecked) {
                    SharedPref.write("SSL_ENABLE", "Y");
                    SSL_ENABLE = SharedPref.read("SSL_ENABLE", "");
                    showSSLText();
                    Validated = true;
                    title = "SSL Enabled";
                } else {
                    SharedPref.write("SSL_ENABLE", "N");
                    SSL_ENABLE = SharedPref.read("SSL_ENABLE", "");
                    showSSLText();
                    Validated = true;
                    title = "SSL Disabled";
                }
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                DialogUtil.successDialog(mContext.get(), title);
            }
        });
    }

    private void setFallbackListener() {
        FallBackToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String title = "";
                if (isChecked) {
                    SharedPref.write("FALLBACK_ENABLE", "Y");
                    FALLBACK_ENABLE = SharedPref.read("FALLBACK_ENABLE", "");
                    showFallBackText();
                    Validated = true;
                    title = "Fallback Enabled";
                } else {
                    SharedPref.write("FALLBACK_ENABLE", "N");
                    FALLBACK_ENABLE = SharedPref.read("FALLBACK_ENABLE", "");
                    showFallBackText();
                    Validated = true;
                    title = "Fallback Disabled";
                }
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                DialogUtil.successDialog(mContext.get(), title);
            }
        });
    }

    private void setManualEntryListener() {
        ManualEntryToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String title = "";
                if (isChecked) {
                    SharedPref.write("MANUAL_ENTRY", "Y");
                    MANUAL_ENTRY = SharedPref.read("MANUAL_ENTRY", "");
                    showManualEntryText();
                    Validated = true;
                    title = "Manual Entry Enabled";
                } else {
                    SharedPref.write("MANUAL_ENTRY", "N");
                    MANUAL_ENTRY = SharedPref.read("MANUAL_ENTRY", "");
                    showManualEntryText();
                    Validated = true;
                    title = "Manual Entry  Disabled";
                }
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                DialogUtil.successDialog(mContext.get(), title);
            }
        });
    }

    private void setAutoSettlementListener() {
        AutoSettlementToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String title = "";
                if (isChecked) {
                    SharedPref.write("AUTO_SETTLEMENT", "Y");
                    AUTO_SETTLEMENT = SharedPref.read("AUTO_SETTLEMENT", "");
                    showAutoSettlementText();
                    Validated = true;
                    title = "Auto Settlement Enabled";
                } else {
                    SharedPref.write("AUTO_SETTLEMENT", "N");
                    AUTO_SETTLEMENT = SharedPref.read("AUTO_SETTLEMENT", "");
                    showAutoSettlementText();
                    Validated = true;
                    title = "Auto Settlement Disabled";
                }
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                DialogUtil.successDialog(mContext.get(), title);
            }
        });
    }

    private void setEmvLogsListener() {
        EmvLogsToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String title = "";
                if (isChecked) {
                    SharedPref.write("EMV_LOGS", "Y");
                    EMV_LOGS = SharedPref.read("EMV_LOGS", "");
                    showEmvLogsText();
                    Validated = true;
                    title = "EMV Logs Enabled";
                } else {
                    SharedPref.write("EMV_LOGS", "N");
                    EMV_LOGS = SharedPref.read("EMV_LOGS", "");
                    showEmvLogsText();
                    Validated = true;
                    title = "EMV Logs Disabled";
                }
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                DialogUtil.successDialog(mContext.get(), title);
            }
        });
    }

    private void setAutoSettlementTimeListener() {
        AutoSettlementTimeToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String title = "";
                if (isChecked) {
                    SharedPref.write("AUTO_SETTLETIME", "Y");
                    AUTO_SETTLETIME = SharedPref.read("AUTO_SETTLETIME", "");
                    showAutoSettlementTimeText();
                    Validated = true;
                    title = "Auto Settlement Enabled";
                } else {
                    SharedPref.write("AUTO_SETTLETIME", "N");
                    AUTO_SETTLETIME = SharedPref.read("AUTO_SETTLETIME", "");
                    showAutoSettlementTimeText();
                    Validated = true;
                    title = "Auto Settlement Disabled";
                }
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                DialogUtil.successDialog(mContext.get(), title);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @OnClick({R.id.terminal_id_number_ll, R.id.merchant_id_number_ll, R.id.merchant_name_ll,
            R.id.nii_number_ll, R.id.com_Ethernet, R.id.com_GPRS, R.id.market_Seg_ll,
            R.id.tip_Thershold_ll, R.id.fallback_Enable, R.id.msg_Header_Len_Hex, R.id.ssl_Enable, R.id.topup_Enable,
            R.id.manual_Entry, R.id.auto_Settle_Time_ll, R.id.auto_Settle_Time, R.id.pin_ByPass,
            R.id.mod10, R.id.check_Expiry, R.id.manual_Entry_issuer, R.id.last4digit_Mag, R.id.last4digit_Icc,
            R.id.acquirer_Number, R.id.low_BIN, R.id.high_BIN, R.id.issuer_Number, R.id.currency_ll, R.id.user_Password_change_ll,
            R.id.system_User_Password_change_ll, R.id.manager_Password_change_ll, R.id.super_User_Password_change_ll,
            R.id.header_Line_1_ll, R.id.header_Line_2_ll, R.id.header_Line_3_ll, R.id.header_Line_4_ll, R.id.footer_Line_1_ll, R.id.footer_Line_2_ll, R.id.com_Ethernet_Layout, R.id.com_GPRS_Layout, R.id.display_Layout,R.id.maintenance_threshold_ll})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.terminal_id_number_ll:
                try {
                    if (transactionDetailsList.size() != 0) {
                        DialogUtil.infoDialog(mContext.get(), "Must Settle First!");
                    } else {
                        changeInformation("Terminal ID", "Change Terminal Id", "tid", 8, InputType.TYPE_CLASS_NUMBER);
                    }
                }
                catch (Exception e){
                        e.printStackTrace();
                    }
                break;
            case R.id.merchant_id_number_ll:
                try {
                    if (transactionDetailsList.size() != 0) {
                        DialogUtil.infoDialog(mContext.get(), "Must Settle First!");
                    } else {
                        changeInformation("Merchant ID", "Change Merchant Id", "mid", 15, InputType.TYPE_CLASS_NUMBER);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }


                break;
            case R.id.merchant_name_ll:
                changeInformation("Merchant Name", "Change Merchant Name", "mname", 25, InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.com_Ethernet:
//                changeInformation("Com Ethernet","Change Com Ethernet","comethernet",0,InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.com_GPRS:
//                changeInformation("Com GPRS","Change Com GPRS","comgprs",0,InputType.TYPE_CLASS_NUMBER);
                break;
//            case R.id.enable_Logs:
//                // changeInformation("Enable Logs","Change Enable Logs","enablelogs",0,InputType.TYPE_CLASS_NUMBER);
//                break;
            case R.id.market_Seg_ll:
                try {
                    if (transactionDetailsList.size() != 0) {
                        DialogUtil.infoDialog(mContext.get(), "Must Settle First!");
                    } else {
                        //    changeInformation("Market Seg","Change Market Seg","marketseg",0,InputType.TYPE_CLASS_NUMBER);
                        openRadio("Market Seg", "Change Market Seg", "marketseg", 0);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case R.id.maintenance_threshold_ll:
                openRadio("Maintenance Threshold", "Change Maintenance Threshold", "maintenance_threshold", 0);
                break;
 
            case R.id.tip_Thershold_ll:
                changeInformation("Tip Threshold","Change Tip Threshold","tipthreshold",6,InputType.TYPE_CLASS_PHONE);
                break;
            case R.id.fallback_Enable:
                //  changeInformation("Fallback Enable","Change Fallback Enable","fallbackenable",0,InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.msg_Header_Len_Hex:
                changeInformation("Message Header Length", "Change Message Header Length", "msgheaderlenhex", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.ssl_Enable:
                // changeInformation("SSL","Change SSL","ssl",0,InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.topup_Enable:
                changeInformation("Top Up", "Change Top Up", "topup", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.manual_Entry:
                //  changeInformation("Manual Entry","Change Manual Entry","manualentry",0,InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.auto_Settlement:
                changeInformation("Auto Settlement", "Change Auto Settlement", "autosettlement", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.emv_logs_enable:
                changeInformation("EMV Logs", "Change EMV Logs", "emvLogs", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.auto_Settle_Time_ll:
                changeInformation("Auto Settle Time", "Change Auto Settle Time", "autosettletime", 4, InputType.TYPE_CLASS_NUMBER);
                break;
//            case R.id.amex_ID_ll:
//                changeInformation("Amex ID","Change Amex ID","amexid",0,InputType.TYPE_CLASS_NUMBER);
//                break;
            case R.id.pin_ByPass:
                changeInformation("Pin ByPass", "Change Pin ByPass", "pinbypass", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.mod10:
                changeInformation("MOD10", "Change MOD10", "mod10", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.check_Expiry:
                changeInformation("Check Expiry", "Change Check Expiry", "checkexpiry", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.manual_Entry_issuer:
                changeInformation("Manual Entry Issuer", "Change Manual Entry Issuer", "manualentryissuer", 0, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.last4digit_Mag:
                changeInformation("Last 4 Digit Mag", "Change Last 4 Digit Mag", "last4digitmag", 0, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.last4digit_Icc:
                changeInformation("Last 4 Digit Icc", "Change Last 4 Digit Icc", "last4digiticc", 0, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.acquirer_Number:
                changeInformation("Acquirer", "Change Acquirer", "acquirer", 0, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.low_BIN:
                changeInformation("Low Bin", "Change Low Bin", "lowbin", 0, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.high_BIN:
                changeInformation("Hign Bin", "Change Hign Bin", "highbin", 0, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.issuer_Number:
                changeInformation("Issuer", "Change Issuer", "issuer", 0, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.currency_ll:
                // changeInformation("Currency","Change Currency","currency",0,InputType.TYPE_CLASS_NUMBER);
                openRadio1("Currency", "Change Currency", "currency", 0);
                break;

            case R.id.user_Password_change_ll:
                changeInformation("User Password", "Change User Password", "userpassword", 6, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.manager_Password_change_ll:
                changeInformation("Manager Password", "Change Manager Password", "managerpassword", 6, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.system_User_Password_change_ll:
                changeInformation("System Password", "Change System Password", "systempassword", 6, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.super_User_Password_change_ll:
                changeInformation("Super User Password", "Super User Password", "superuserpassword", 6, InputType.TYPE_CLASS_NUMBER);
                break;

            case R.id.header_Line_1_ll:
                changeInformation("Header Line 1", "Change Header Line 1", "headerline1", 22, InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.header_Line_2_ll:
                changeInformation("Header Line 2", "Change Header Line 2", "headerline2", 22, InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.header_Line_3_ll:
                changeInformation("Header Line 3", "Change Header Line 3", "headerline3", 22, InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.header_Line_4_ll:
                changeInformation("Header Line 4", "Change Header Line 4", "headerline4", 22, InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.footer_Line_1_ll:
                changeInformation("Footer Line 1", "Change Footer Line 1", "footerline1", 22, InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.footer_Line_2_ll:
                changeInformation("Footer Line 2", "Change Footer Line 2", "footerline2", 22, InputType.TYPE_CLASS_TEXT);
                break;
            case R.id.com_Ethernet_Layout:
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                break;

            case R.id.com_GPRS_Layout:
                startActivity(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));
                break;

            case R.id.display_Layout:
                startActivity(new Intent(Settings.ACTION_DISPLAY_SETTINGS));
                break;


//            case R.id.tmsUpdate:
//                try {
//                    if(SharedPref.read("isSettleRequiured", "").equals("Y") || getAllTransactions().size() > 0){
//                        DialogUtil.infoDialog(mContext.get(), "Must Settle First!");
//                    }else {
//                        DashboardContainer.switchFragmentWithBackStack(new UpdateFragment(iSystemManager), "tms update");
//                    }
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//
//                break;



//            case R.id.host_ip_number:
//                changeInformation("Host IP","Change Host Ip","hip");
//                break;
//            case R.id.host_port_number:
//                changeInformation("Host Port","Change Host Port","hp");
//                break;
            case R.id.nii_number_ll:
                changeInformation("NII", "Change NII Number", "nii", 3, InputType.TYPE_CLASS_NUMBER);
                break;
//            case R.id.reset_aid_rid:
//                EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, context);
//                emvSetAidRid.setAID(3);
//                emvSetAidRid.setRID(3);
//                emvSetAidRid.setAID(1);
//                emvSetAidRid.setRID(1);
//                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Intent intent = new Intent();
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        boolean isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        mContext = new WeakReference<>(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null ) {
            getActivity().unbindService(conn);
        }
    }

    /**
     * dialog to change any of the data and rewrite to persistence
     *
     * @param title   the dialog title
     * @param content the msg to display
     * @param type    the data field to be changed
     */
    private void changeInformation(String title, String content, String type, int keyboardLength, int KeyboardType) {
        DialogUtil.inputDialogForSystemSettingFragment(mContext.get(), title, content, sweetAlertDialog -> {
            EditText text = Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
            text.setInputType(InputType.TYPE_CLASS_TEXT);
            newText = text.getText().toString();
            switch (type) {
                case "tid":
                    if (newText.length() != 8) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 8!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("TERMINAL_ID", newText);
                        TERMINAL_ID = SharedPref.read("TERMINAL_ID", "");
                        terminalIdNumber.setText(TERMINAL_ID);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "comethernet":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("COM_ETHERNET", newText);
                        COM_ETHERNET = SharedPref.read("COM_ETHERNET", "");
                        comEthernet.setText(COM_ETHERNET);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "comgprs":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("COM_GPRS", newText);
                        COM_GPRS = SharedPref.read("COM_GPRS", "");
                        comGPRS.setText(COM_GPRS);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
//                case "enablelogs":
//                    if(newText.length() == 0){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","Length must be 3!");
//                        sweetAlertDialog.dismissWithAnimation(); Validated = false;
//                    }
//                    else {
//                        SharedPref.write("ENABLE_LOGS", newText);
//                        ENABLE_LOGS = SharedPref.read("ENABLE_LOGS", "");
//                        enableLogs.setText(ENABLE_LOGS);Validated = true;
//                    }
//                    // hostInformation.terminalID=TERMINAL_ID;
//                    break;
                case "maintenance_threshold":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Invalid length!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        int maintenanceThreshold = 0;
                        maintenanceThreshold = Integer.parseInt(newText);
                        Utility.DEBUG_LOG(TAG,"maintenanceThreshold:"+maintenanceThreshold);
                        SharedPref.write("maintenanceThreshold", maintenanceThreshold);
                        maintenanceThreshold = SharedPref.read("maintenanceThreshold");
                        MAINTENANCE_THRESHOLD = Integer.toString(maintenanceThreshold);
//                        MAINTENANCE_THRESHOLD = SharedPref.read("maintenanceThreshold", "");
                        maintenanceThresholdTv.setText(MAINTENANCE_THRESHOLD);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "marketseg":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("MARKET_SEG", newText);

                        MARKET_SEG = SharedPref.read("MARKET_SEG", "");
                        marketSeg.setText(MARKET_SEG);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "tipthreshold":
                    if (Double.parseDouble(newText) > 100) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Must be less than 100!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("TIP_THRESHOLD", newText);
                        TIP_THRESHOLD = SharedPref.read("TIP_THRESHOLD", "");
                        tipThreshold.setText(TIP_THRESHOLD);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "fallbackenable":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("FALLBACK_ENABLE", newText);
                        FALLBACK_ENABLE = SharedPref.read("FALLBACK_ENABLE", "");
                        fallbackEnable.setText(FALLBACK_ENABLE);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "msgheaderlenhex":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("MSGHEADER_LENHEX", newText);
                        MSGHEADER_LENHEX = SharedPref.read("MSGHEADER_LENHEX", "");
                        msgHeaderLenHex.setText(MSGHEADER_LENHEX);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "ssl":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("SSL_ENABLE", newText);
                        SSL_ENABLE = SharedPref.read("SSL_ENABLE", "");
                        sslEnable.setText(SSL_ENABLE);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "topup":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("TOPUP_ENABLE", newText);
                        TOPUP_ENABLE = SharedPref.read("TOPUP_ENABLE", "");
                        topUpEnable.setText(TOPUP_ENABLE);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "manualentry":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("MANUAL_ENTRY", newText);
                        MANUAL_ENTRY = SharedPref.read("MANUAL_ENTRY", "");
                        manualEntry.setText(MANUAL_ENTRY);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "autosettlement":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("AUTO_SETTLEMENT", newText);
                        AUTO_SETTLEMENT = SharedPref.read("AUTO_SETTLEMENT", "");
                        autoSettlement.setText(AUTO_SETTLEMENT);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "emvLogs":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("EMV_LOGS", newText);
                        EMV_LOGS = SharedPref.read("EMV_LOGS", "");
                        emvLogsEnable.setText(EMV_LOGS);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;

                case "autosettletime":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("AUTO_SETTLETIME", newText);
                        AUTO_SETTLETIME = SharedPref.read("AUTO_SETTLETIME", "");
                        autoSettleTime.setText(AUTO_SETTLETIME);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
//                case "amexid":
//                    if(newText.length() == 0){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","Length must be 3!");
//                        sweetAlertDialog.dismissWithAnimation(); Validated = false;
//                    }
//                    else {
//                        SharedPref.write("AMEX_ID", newText);
//                        AMEX_ID = SharedPref.read("AMEX_ID", "");
//                        amexID.setText(AMEX_ID);Validated = true;
//                    }
//                    // hostInformation.terminalID=TERMINAL_ID;
//                    break;
                case "pinbypass":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("PIN_BYPASS", newText);
                        PIN_BYPASS = SharedPref.read("PIN_BYPASS", "");
                        pinByPass.setText(PIN_BYPASS);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "mod10":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("MOD10", newText);
                        MOD10 = SharedPref.read("MOD10", "");
                        mod10.setText(MOD10);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "checkexpiry":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("CHECK_EXPIRY", newText);
                        CHECK_EXPIRY = SharedPref.read("CHECK_EXPIRY", "");
                        checkExpiry.setText(CHECK_EXPIRY);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "manualentryissuer":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("MANUAL_ENTRY_ISSUER", newText);
                        MANUAL_ENTRY_ISSUER = SharedPref.read("MANUAL_ENTRY_ISSUER", "");
                        manualEntryIssuer.setText(MANUAL_ENTRY_ISSUER);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "last4digitmag":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("LAST_4DIGIT_MAG", newText);
                        LAST_4DIGIT_MAG = SharedPref.read("LAST_4DIGIT_MAG", "");
                        last4DigitMag.setText(LAST_4DIGIT_MAG);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "last4digiticc":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("LAST_4DIGIT_ICC", newText);
                        LAST_4DIGIT_ICC = SharedPref.read("LAST_4DIGIT_ICC", "");
                        last4DigitIcc.setText(LAST_4DIGIT_ICC);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "acquirer":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("ACQUIRER", newText);
                        ACQUIRER = SharedPref.read("ACQUIRER", "");
                        acquirer.setText(ACQUIRER);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "lowbin":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("LOW_BIN", newText);
                        LOW_BIN = SharedPref.read("LOW_BIN", "");
                        lowBIN.setText(LOW_BIN);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "highbin":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("HIGH_BIN", newText);
                        HIGH_BIN = SharedPref.read("HIGH_BIN", "");
                        highBIN.setText(HIGH_BIN);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "issuer":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("ISSUER", newText);
                        ISSUER = SharedPref.read("ISSUER", "");
                        issuer.setText(ISSUER);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "currency":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("CURRENCY", newText);
                        CURRENCY = SharedPref.read("CURRENCY", "");
                        currency.setText(CURRENCY);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "userpassword":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 6!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("USER_PASSWORD_CHANGE", newText);
                        USER_PASSWORD_CHANGE = SharedPref.read("USER_PASSWORD_CHANGE", "");
                        userPassword.setText(USER_PASSWORD_CHANGE);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "managerpassword":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 6!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("MANAGER_PASSWORD_CHANGE", newText);
                        MANAGER_PASSWORD_CHANGE = SharedPref.read("MANAGER_PASSWORD_CHANGE", "");
                        managerPassword.setText(MANAGER_PASSWORD_CHANGE);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "systempassword":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 6!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("SYSTEM_PASSWORD_CHANGE", newText);
                        SYSTEM_PASSWORD_CHANGE = SharedPref.read("SYSTEM_PASSWORD_CHANGE", "");
                        systemuserPassword.setText(SYSTEM_PASSWORD_CHANGE);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "superuserpassword":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 6!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("SUPER_USER_PASSWORD_CHANGE", newText);
                        SUPER_USER_PASSWORD_CHANGE = SharedPref.read("SUPER_USER_PASSWORD_CHANGE", "");
                        superuserPassword.setText(SUPER_USER_PASSWORD_CHANGE);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "headerline1":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("HEADER_LINE_1", newText);
                        HEADER_LINE_1 = SharedPref.read("HEADER_LINE_1", "");
                        headerLine1.setText(HEADER_LINE_1);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "headerline2":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("HEADER_LINE_2", newText);
                        HEADER_LINE_2 = SharedPref.read("HEADER_LINE_2", "");
                        headerLine2.setText(HEADER_LINE_2);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "headerline3":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("HEADER_LINE_3", newText);
                        HEADER_LINE_3 = SharedPref.read("HEADER_LINE_3", "");
                        headerLine3.setText(HEADER_LINE_3);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "headerline4":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("HEADER_LINE_4", newText);
                        HEADER_LINE_4 = SharedPref.read("HEADER_LINE_4", "");
                        headerLine4.setText(HEADER_LINE_4);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "footerline1":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("FOOTER_LINE_1", newText);
                        FOOTER_LINE_1 = SharedPref.read("FOOTER_LINE_1", "");
                        footerLine1.setText(FOOTER_LINE_1);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "footerline2":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("FOOTER_LINE_2", newText);
                        FOOTER_LINE_2 = SharedPref.read("FOOTER_LINE_2", "");
                        footerLine2.setText(FOOTER_LINE_2);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "mid":
                    if (newText.length() != 15) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 15, add spaces if needed!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("MERCHANT_ID", newText);
                        MERCHANT_ID = SharedPref.read("MERCHANT_ID", "");
                        merchantIdNumber.setText(MERCHANT_ID);
                        Validated = true;
                    }
                    //  hostInformation.merchantID=MERCHANT_ID;
                    break;
                case "mname":
                    if (newText.equals("")) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "cannot be empty!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("merchantName", newText);
                        MERCHANT_NAME = SharedPref.read("merchantName", "");
                        merchantName.setText(MERCHANT_NAME);
                        Validated = true;
                    }
                    // hostInformation.merchantName=MERCHANT_NAME;
                    break;
//                case "hip":
//                    if(!Patterns.IP_ADDRESS.matcher(newText).matches()){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","invalid ip!");
//                        sweetAlertDialog.dismissWithAnimation();Validated = false;
//                    }
//                    else {
//                        SharedPref.write("hostIP", newText);
//                        hostIP = SharedPref.read("hostIP", "");
//                        hostIpNumber.setText(hostIP);Validated = true;
//                    }
//                  //  hostInformation.hostAddr=hostIP;
//                    break;
//                case "hp":
//                    if(newText.length()!=4){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","Length must be 4!");
//                        sweetAlertDialog.dismissWithAnimation();Validated = false;
//                    }
//                    else {
//                        int port = Integer.valueOf(newText);
//                        SharedPref.write("hostPort", port);
//                        hostPort = SharedPref.read("hostPort");
//                        hostPortNumber.setText(String.valueOf(Constants.hostPort));Validated = true;
//                    }
//                   // hostInformation.hostPort=hostPort;
//                    break;
                case "nii":
                    if (newText.length() != 3) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("NII", newText);
                        NII = SharedPref.read("NII", "");
                        niiNumber.setText(NII);
                        Validated = true;
                    }
                    // hostInformation.nii=NII;
                    break;
                case "tip":
                    break;
            }
            //  MultiHostsConfig.update(DashboardContainer.index, hostInformation);
            if (Validated) {
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                sweetAlertDialog.dismissWithAnimation();
                DialogUtil.successDialog(mContext.get(), title + " Changed!");
            }
        }, KeyboardType, null, keyboardLength);

    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void openRadio1(String title, String content, String type, int keyboardLength) {
        DialogUtil.radioButtonForCurrency(type, mContext.get(), title, content, sweetAlertDialog -> {
            //  EditText text= Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
//            text.setInputType(InputType.TYPE_CLASS_TEXT);
            //newText =   text.getText().toString();
            switch (type) {
                case "currency":
                    if (SharedPref.read("CURRENCY", "").equalsIgnoreCase("notSelected")) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Select Currency");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        sweetAlertDialog.dismissWithAnimation();
                        CURRENCY = SharedPref.read("CURRENCY", "");
                        currency.setText(CURRENCY);
                        Validated = true;
                    }


                    break;

            }
            //  MultiHostsConfig.update(DashboardContainer.index, hostInformation);
            if (Validated) {
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                sweetAlertDialog.dismissWithAnimation();
                DialogUtil.successDialog(mContext.get(), title + " Changed!");
            }
        }, InputType.TYPE_CLASS_NUMBER, null, keyboardLength);

    }

    private void openRadio(String title, String content, String type, int keyboardLength) {
        DialogUtil.radioButtonForMarketSegAndMaintenanceThreshold(type, mContext.get(), title, content, sweetAlertDialog -> {
            //  EditText text= Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
//            text.setInputType(InputType.TYPE_CLASS_TEXT);
            //newText =   text.getText().toString();
            Utility.DEBUG_LOG(TAG,"type:"+type);
            int maintenanceThreshold = 0;
            switch (type) {
                 case "maintenance_threshold":
                    Utility.DEBUG_LOG(TAG,"before SharedPref.read(maintenanceThreshold) 1");
                    maintenanceThreshold = SharedPref.read("maintenanceThreshold");
                    Utility.DEBUG_LOG(TAG,"maintenanceThreshold:"+maintenanceThreshold);
//                    if (SharedPref.read("maintenanceThreshold", "").equalsIgnoreCase("notSelected")) {
                        if ( maintenanceThreshold < 0 || maintenanceThreshold > 300 ) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Select Maintenance Threshold");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        sweetAlertDialog.dismissWithAnimation();
                        Utility.DEBUG_LOG(TAG,"before SharedPref.read(maintenanceThreshold) 2 ");
                        maintenanceThreshold = SharedPref.read("maintenanceThreshold");
                        Utility.DEBUG_LOG(TAG,"maintenanceThreshold:"+maintenanceThreshold);
                            MAINTENANCE_THRESHOLD = Integer.toString(maintenanceThreshold);
//                        MAINTENANCE_THRESHOLD = SharedPref.read("maintenanceThreshold", "");
                        Utility.DEBUG_LOG(TAG,"MAINTENANCE_THRESHOLD 2:"+MAINTENANCE_THRESHOLD);
                        maintenanceThresholdTv.setText(MAINTENANCE_THRESHOLD);
                        Validated = true;
                    }
					break;
               case "marketseg":
                    if (SharedPref.read("MARKET_SEG", "").equalsIgnoreCase("notSelected")) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Select Market-Seg");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        sweetAlertDialog.dismissWithAnimation();
                        MARKET_SEG = SharedPref.read("MARKET_SEG", "");
                        Utility.DEBUG_LOG(TAG,"Market seg:"+MARKET_SEG);

                        marketSeg.setText(MARKET_SEG);
                        if(MARKET_SEG.equals("T")){
                            tipToggle.setEnabled(true);
                            SharedPref.write("TIP_ENABLED", "Y");
                            TIP_ENABLED = SharedPref.read("TIP_ENABLED","");
                            tipToggle.setChecked(true);

                        }
                        else{
                          //  tipToggle.setEnabled(false);
                            SharedPref.write("TIP_ENABLED", "N");
                            TIP_ENABLED = SharedPref.read("TIP_ENABLED","");
                            tipToggle.setChecked(false);

                        }
                        Validated = true;
                    }


                    break;

            }
            //  MultiHostsConfig.update(DashboardContainer.index, hostInformation);
            if (Validated) {
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                sweetAlertDialog.dismissWithAnimation();
                DialogUtil.successDialog(mContext.get(), title + " Changed!");
            }
        }, InputType.TYPE_CLASS_NUMBER, null, keyboardLength);

    }
//    private class TipCheck implements Runnable {
//        @Override
//        public void run() {
//            if(MARKET_SEG.equals("T")){
//                tipToggle.setEnabled(false);
//            }
//            else {
//                tipToggle.setEnabled(true);
//            }
//        }
//    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        Utility.DEBUG_LOG("SuperUserFragment", "onResume");
//        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
//        scheduledExecutorService.scheduleWithFixedDelay(new TipCheck(), 1, 1, TimeUnit.SECONDS);
//
//    }

}
