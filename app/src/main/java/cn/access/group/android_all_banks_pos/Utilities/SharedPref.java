package cn.access.group.android_all_banks_pos.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

/**
 * @author muhammad.humayun
 * Created 9/18/2019
 */
public class SharedPref
{
    private static SharedPreferences mSharedPref;
    private static final String ListKey = "List_Key";


    private SharedPref()
    { }

    public static void init(Context context)
    {
        if(mSharedPref == null)
            mSharedPref = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
    }

    public static String read(String key, String defValue) {
        return mSharedPref.getString(key, defValue);
    }

    public static int read(String key) {
        return mSharedPref.getInt(key, 0);
    }
    public static int read(String key, int defaultValue)
    {
        return mSharedPref.getInt(key, defaultValue);
    }

    public static void write(String key,int value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putInt(key,value);
        prefsEditor.apply();

    }

    public static void write(String key, String value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public static void clear(){
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.clear().apply();
    }

    public static void writeList(Context context, List<TransactionDetail> ts){
        Gson gson = new Gson();
        String json = new Gson().toJson(ts);
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putString("List_Key",json);
        prefsEditor.apply();
    }

    public static List<TransactionDetail> getList(Context context){
        String json = mSharedPref.getString("List_Key","");
        Gson gson = new Gson();
        Type founderListType = new TypeToken<ArrayList<TransactionDetail>>(){}.getType();

        List<TransactionDetail> founderList = gson.fromJson(json, founderListType);
        return  founderList;
    }

    public static void countedTransactionWrite(Context context, List<CountedTransactionItem> ts){
        Gson gson = new Gson();
        String json = new Gson().toJson(ts);
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putString("Counted_List_Key",json);
        prefsEditor.apply();
    }

    public static List<CountedTransactionItem> countedTransactionRead(Context context){
        String json = mSharedPref.getString("Counted_List_Key","");
        Gson gson = new Gson();
        Type founderListType = new TypeToken<ArrayList<CountedTransactionItem>>(){}.getType();

        List<CountedTransactionItem> founderList = gson.fromJson(json, founderListType);
        return  founderList;
    }




}
