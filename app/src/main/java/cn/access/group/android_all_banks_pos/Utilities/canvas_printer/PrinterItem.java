package cn.access.group.android_all_banks_pos.Utilities.canvas_printer;

/**
 * Created by Simon on 2019/2/27.
 */


import java.lang.reflect.Array;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrinterDefine;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrinterElement;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrinterItemType;

/**
 * this is for ALL items defined
 *
 *
 */
public enum PrinterItem {

    // parameters: type, description (file name for Logo), string value, integer value, print style of description, print style of value
    // print style of description, print style of value, default font size 16, alignment left
    LOGO        (PrinterItemType.LOGO_ASSETS, new PrinterElement("verifone_logo.jpg"), new PrinterElement() ),
    TITLE       (PrinterItemType.STRING, new PrinterElement("Verifone X900", 32 , PrinterDefine.PStyle_align_center), new PrinterElement()),
    SUBTITLE    (PrinterItemType.STRING, new PrinterElement("", 22, PrinterDefine.PStyle_align_right ), new PrinterElement()),
    COPY_NOTE   (PrinterItemType.STRING, new PrinterElement("", 20, PrinterDefine.PStyle_align_left ), new PrinterElement("merchant copy", 16)),
    MERCHANT_NAME(PrinterItemType.STRING, new PrinterElement(Constants.MERCHANT_NAME,24,PrinterDefine.PStyle_align_center,  PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines
    HEADER1(PrinterItemType.STRING, new PrinterElement(Constants.HEADER_LINE_1,18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    HEADER2(PrinterItemType.STRING, new PrinterElement(Constants.HEADER_LINE_2,18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    HEADER3(PrinterItemType.STRING, new PrinterElement(Constants.HEADER_LINE_3,18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    HEADER4(PrinterItemType.STRING, new PrinterElement(Constants.HEADER_LINE_4,20,PrinterDefine.PStyle_align_center,PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines
    SUMMARY_HEADING (PrinterItemType.STRING, new PrinterElement("",24,PrinterDefine.PStyle_align_center,PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines
    SETTLEMENT_HEADING (PrinterItemType.STRING, new PrinterElement("",24,PrinterDefine.PStyle_align_center,PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines
    SETTLEMENT_SUCCESS_HEADING (PrinterItemType.STRING, new PrinterElement("",24,PrinterDefine.PStyle_align_center,PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines

    DETAIL_REPORT_HEADING (PrinterItemType.STRING, new PrinterElement("DETAIL REPORT",24,PrinterDefine.PStyle_align_center,PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines
    DETAIL_REPORT_HEADING2 (PrinterItemType.STRING, new PrinterElement("DETAIL REPORT",24,PrinterDefine.PStyle_align_center,PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines
    DETAIL_LIST_DATE_TIME (PrinterItemType.STRING, new PrinterElement("",18), new PrinterElement("",18)), // +32 + PStyle_force_multi_lines
    DETAL_SUMMARY_HEADING (PrinterItemType.STRING, new PrinterElement("Final Summary",24,PrinterDefine.PStyle_align_center,PrinterDefine.Font_Bold), new PrinterElement()), // +32 + PStyle_force_multi_lines
    //DETAL_SUMMARY (PrinterItemType.DETAIL), // +32 + PStyle_force_multi_lines


    MERCHANT_ID (PrinterItemType.STRING, new PrinterElement("Merchant ID",18),  new PrinterElement("", 18)),
    TERMINAL_ID (PrinterItemType.STRING, new PrinterElement("Terminal ID",18),  new PrinterElement("", 18 )),
    OPERATOR_ID (PrinterItemType.STRING, new PrinterElement("Operator ID",20),  new PrinterElement( )),
    HOST        (PrinterItemType.STRING, new PrinterElement("HOST", 20),  new PrinterElement()),
    TRANS_TYPE  (PrinterItemType.STRING, new PrinterElement("", 20 ,PrinterDefine.PStyle_align_right),
            new PrinterElement("", 28, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold )),
    CARD_NO_AND_SCHEME  (PrinterItemType.STRING, new PrinterElement("", 24, PrinterDefine.Font_Bold ),  new PrinterElement("", 24) ),
    CARD_ISSUE_TRANS_TYPE  (PrinterItemType.STRING, new PrinterElement("", 22, PrinterDefine.Font_Bold ),  new PrinterElement("", 22) ),
    CARD_NO_CARD_SCHEME     (PrinterItemType.STRING, new PrinterElement("",  22),  new PrinterElement("",22, PrinterDefine.PStyle_align_center , 2, PrinterDefine.Font_Bold) , true ), //+ PStyle_align_center + PStyle_force_multi_lines),
    CARD_TYPE   (PrinterItemType.STRING, new PrinterElement("Card Type", 20),  new PrinterElement() ),
    CARD_HOLDER (PrinterItemType.STRING, new PrinterElement("Card Holder", 24 , PrinterDefine.Font_Bold),  new PrinterElement() ),
    CARD_VALID  (PrinterItemType.STRING, new PrinterElement("Card Valid", 18),  new PrinterElement() ),
    TVR  (PrinterItemType.STRING, new PrinterElement("TVR:", 18),  new PrinterElement("", 18) ),
    AID  (PrinterItemType.STRING, new PrinterElement("AID:", 18),  new PrinterElement("", 18) ),
    BATCH_NO    (PrinterItemType.STRING, new PrinterElement("", 18),  new PrinterElement("", 18) ),
    BATCH_NO_DETAIL    (PrinterItemType.STRING, new PrinterElement("", 18),  new PrinterElement() ),
    DATE_TIME   (PrinterItemType.STRING, new PrinterElement("", 18),  new PrinterElement( "", 18)),
    REFER_NO    (PrinterItemType.STRING, new PrinterElement("RRN:", 18),  new PrinterElement("", 18) ),
    TRACK_NO    (PrinterItemType.STRING, new PrinterElement("TRACE #", 20),  new PrinterElement() ),
    AUTH_NO     (PrinterItemType.STRING, new PrinterElement("AUTH NO:", 18),  new PrinterElement("", 18) ),
    CURRENCY (PrinterItemType.THREE_COLUMN, new PrinterElement("",22,PrinterDefine.PStyle_align_center,  PrinterDefine.Font_Bold), new PrinterElement(),false),
    //DETAIL_LIST_3 (PrinterItemType.DETAIL, new PrinterElement("MASOOD",22), new PrinterElement("MASOOD",22),true ),
    //AMOUNT      (PrinterItemType.STRING, new PrinterElement("AMOUNT", 22, PrinterDefine.Font_Bold),  new PrinterElement("", 22,PrinterDefine.PStyle_align_right,2, PrinterDefine.Font_Bold), false),
    AMOUNT      (PrinterItemType.STRING, new PrinterElement("AMOUNT","PKR", 22, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 22, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),
    TOTAL_AMOUNT      (PrinterItemType.STRING, new PrinterElement("TOTAL AMOUNT","PKR", 22, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 22, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),
    TIP_AMOUNT      (PrinterItemType.STRING, new PrinterElement("TIP AMOUNT","PKR", 22, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 22, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),
    SUMMARY_HEADING2      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 22, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 22, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),
    //FINA_SUMMARY_HEADING2      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 22, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 22, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),
    DETAIL_LIST_1      (PrinterItemType.STRING, new PrinterElement("","", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    DETAIL_LIST_2      (PrinterItemType.STRING, new PrinterElement("","", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    DETAIL_LIST_3      (PrinterItemType.DETAIL_AMOUNT, new PrinterElement("AMOUNT","PKR", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("val", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
   // DETAIL_LIST_3 = new

    //DETAIL_LIST_3      (PrinterItemType.DETAIL, new PrinterElement("","", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),

   TOTAL_SALETYPE      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
   TOTAL_CASHOUT      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
   TOTAL_REDEEM      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
   TOTAL_VOID     (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),


    VISA_SALETYPE      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    VISA_CASHOUT      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    VISA_REDEEM      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    VISA_VOID     (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),

    UPI_SALETYPE      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    UPI_CASHOUT      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    UPI_REDEEM      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),

    MASTER_SALETYPE      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    MASTER_CASHOUT      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    MASTER_REDEEM      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),

    PAYPAK_SALETYPE      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    PAYPAK_CASHOUT      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    PAYPAK_REDEEM      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),


    JCB_SALETYPE      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    JCB_CASHOUT      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    JCB_REDEEM      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),

    AMEX_SALETYPE      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    AMEX_CASHOUT      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    AMEX_REDEEM      (PrinterItemType.STRING, new PrinterElement("CARD","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_default),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_default), false),
    TOTAL_TIP      (PrinterItemType.STRING, new PrinterElement("TOTAL SALES","COUNT", 22, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 22, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),
    SUMMARY_TOTAL_SALE      (PrinterItemType.STRING, new PrinterElement("TOTAL SALES","COUNT", 22, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 22, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),
    SUMMARY_TOTAL_TIP_SALE      (PrinterItemType.STRING, new PrinterElement("Including TIP","COUNT", 18, PrinterDefine.PStyle_align_left , PrinterDefine.Font_Bold),  new PrinterElement("AMOUNT", 18, PrinterDefine.PStyle_align_right , PrinterDefine.Font_Bold), false),


    SIGN     (PrinterItemType.STRING, new PrinterElement("SIGN:", 18),  new PrinterElement("______________________________", 18) ),
    SIGN_CASH_ADV1     (PrinterItemType.STRING, new PrinterElement("ID/Passport:", 18  ,PrinterDefine.PStyle_align_left),  new PrinterElement("______________________________", 18) ),
    SIGN_CASH_ADV2     (PrinterItemType.STRING, new PrinterElement("4 Preprinted digits:", 18,PrinterDefine.PStyle_align_left),  new PrinterElement("______________________________", 18) ),
    SIGN_CASH_ADV3     (PrinterItemType.STRING, new PrinterElement("Bank sign:", 18,PrinterDefine.PStyle_align_left),  new PrinterElement("______________________________", 18) ),
    CARD_AUTH_METHOD    (PrinterItemType.STRING, new PrinterElement("APPROVED", 20,PrinterDefine.PStyle_align_center),  new PrinterElement("", 18) ),
    FOOTER1(PrinterItemType.STRING, new PrinterElement("I agree to pay the above final",18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    FOOTER2(PrinterItemType.STRING, new PrinterElement("amount according to the card/",18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    FOOTER3(PrinterItemType.STRING, new PrinterElement("merchant issuer agreement.",18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    COPY (PrinterItemType.STRING, new PrinterElement("MERCHANT COPY",20,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    TERMINAL_SERIAL (PrinterItemType.STRING, new PrinterElement("Terminal Serial # "+Constants.TERMINAL_SERIAL,18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines
    DUPLICATE (PrinterItemType.STRING, new PrinterElement("DUPLICATE",18,PrinterDefine.PStyle_align_center), new PrinterElement()), // +32 + PStyle_force_multi_lines

    BALANCE     (PrinterItemType.STRING, new PrinterElement("Bal. Points:", 20, PrinterDefine.Font_Bold),  new PrinterElement("", 20, PrinterDefine.Font_Bold) ),
    ORBIT_REDEEM     (PrinterItemType.STRING, new PrinterElement("Orbit Redeemed:", 20, PrinterDefine.Font_Bold),  new PrinterElement("", 20, PrinterDefine.Font_Bold) ),
    ORBIT_AVAILABLE     (PrinterItemType.STRING, new PrinterElement("Orbit Available:", 20, PrinterDefine.Font_Bold),  new PrinterElement("", 20, PrinterDefine.Font_Bold) ),
    REFERENCE_KEY     (PrinterItemType.STRING, new PrinterElement("Reference Key:", 20, PrinterDefine.Font_Bold),  new PrinterElement("", 20, PrinterDefine.Font_Bold) ),
    TENURE     (PrinterItemType.STRING, new PrinterElement("Tenure:", 20, PrinterDefine.Font_Bold),  new PrinterElement("", 20, PrinterDefine.Font_Bold) ),
    INSTALLMENT     (PrinterItemType.STRING, new PrinterElement("Installments:", 20, PrinterDefine.Font_Bold),  new PrinterElement("", 20, PrinterDefine.Font_Bold) ),
    //TIP         (PrinterItemType.STRING, new PrinterElement("TIP"),  new PrinterElement() ),
    TOTAL       (PrinterItemType.STRING, new PrinterElement("TOTAL"),  new PrinterElement() ),
    REFERENCE   (PrinterItemType.STRING, new PrinterElement("REFERENCE",20),  new PrinterElement() ),
    //E_SIGN        (PrinterItemType.IMG_BCD, new PrinterElement("SIGN",20),  new PrinterElement() ),
    RE_PRINT_NOTE(PrinterItemType.STRING, new PrinterElement("RE-PRINT",20 , PrinterDefine.PStyle_align_center),  new PrinterElement() ),
    TC   (PrinterItemType.STRING, new PrinterElement("TC",20),  new PrinterElement() ),

    COMMENT_1   (PrinterItemType.STRING,1, new PrinterElement("", 16, PrinterDefine.PStyle_align_center),  new PrinterElement() ),
    COMMENT_2   (PrinterItemType.STRING,1, new PrinterElement("", 16, PrinterDefine.PStyle_align_center),  new PrinterElement() ),
    COMMENT_3   (PrinterItemType.STRING,1, new PrinterElement("", 16, PrinterDefine.PStyle_align_center),  new PrinterElement() ),

    FLEXIBLE_1  (PrinterItemType.STRING, new PrinterElement(),  new PrinterElement() ),
    FLEXIBLE_2  (PrinterItemType.STRING, new PrinterElement(),  new PrinterElement() ),
    FLEXIBLE_3  (PrinterItemType.STRING, new PrinterElement(),  new PrinterElement() ),
    FLEXIBLE_4  (PrinterItemType.STRING, new PrinterElement(),  new PrinterElement() ),
    FLEXIBLE_5  (PrinterItemType.STRING, new PrinterElement(),  new PrinterElement() ),

//    GUIDE1        (PrinterItemType.LOGO_ASSETS, new PrinterElement("guide/2-main.png"), new PrinterElement() ),
//    GUIDE2        (PrinterItemType.LOGO_ASSETS, new PrinterElement("guide/2-main.png"), new PrinterElement() ),
//    GUIDE3        (PrinterItemType.LOGO_ASSETS, new PrinterElement("guide/2-main.png"), new PrinterElement() ),
//    GUIDE4        (PrinterItemType.LOGO_ASSETS, new PrinterElement("guide/2-main.png"), new PrinterElement() ),

    BARCODE_1     (PrinterItemType.BARCODE, new PrinterElement("Barcode for refund", 48, PrinterDefine.PStyle_align_center), new PrinterElement("123456789", 32, PrinterDefine.PStyle_align_center ) ),
    BARCODE_2     (PrinterItemType.BARCODE, new PrinterElement("Barcode for refund", 48, PrinterDefine.PStyle_align_center), new PrinterElement("123456789", 32, PrinterDefine.PStyle_align_center ) ),

    QRCODE_1     (PrinterItemType.QRCODE, new PrinterElement("123456789", 180, PrinterDefine.PStyle_align_center), new PrinterElement("123456789", 180, PrinterDefine.PStyle_align_center) ),
    QRCODE_2     (PrinterItemType.QRCODE, new PrinterElement("123456789", 180, PrinterDefine.PStyle_align_center), new PrinterElement("123456789", 180, PrinterDefine.PStyle_align_center) ),
   // CUT_BREAK         (PrinterItemType.STRING, 0, new PrinterElement("----------x----------x----------"),  new PrinterElement() ),

   // CUT         (PrinterItemType.STRING, 0, new PrinterElement("----------x----------x----------"),  new PrinterElement() ),
    LINE        (PrinterItemType.LINE, new PrinterElement("",2),  new PrinterElement() ),
    FEED        (PrinterItemType.FEED, new PrinterElement("",2),  new PrinterElement() ), // pixel for feed

    FEED_LINE   (PrinterItemType.FEED, 1,  new PrinterElement("",20),  new PrinterElement()),
     FEED_LINE_1  (PrinterItemType.FEED, 1,  new PrinterElement("",18),  new PrinterElement()); // pixel for feed


        //PrinterItemType[] creatures = new PrinterItemType[]{PrinterItemType.FEED};





    public PrinterItemType type;
    public PrinterElement title;
    public PrinterElement value;
    public PrinterElement value1;
    /**
     * 1 for paper, 2 for show, 3 for paper and show as the default
     */
    public int printerMode;

    public boolean isForceMultiLines;

    private PrinterItemType df_type;
    private PrinterElement df_title;
    private PrinterElement df_value;
    private PrinterElement df_value1;
    private boolean df_isForceMultiLines;

    private void set(PrinterItemType type, PrinterElement title, PrinterElement value, boolean isForceMultiLines, int printerMode){
        if( title.style == -1 ){
            title.style = PrinterDefine.PStyle_align_left;
        }
        if( value.style == -1 ){
            value.style = PrinterDefine.PStyle_align_right;
        }

        this.df_type = type;
        this.df_title = title;
        this.df_value = value;
        this.df_isForceMultiLines = isForceMultiLines ;

        this.type = df_type;
        this.title = df_title;
        this.value = df_value;
        this.isForceMultiLines = this.df_isForceMultiLines;

        this.printerMode = printerMode;
    }

    private void set(PrinterItemType type, PrinterElement title, PrinterElement value, PrinterElement value1){
        if( title.style == -1 ){
            title.style = PrinterDefine.PStyle_align_left;
        }
        if( value.style == -1 ){
            value.style = PrinterDefine.PStyle_align_right;
        }

        this.df_type = type;
        this.df_title = title;
        this.df_value = value;
        this.df_value1 = value1;
        this.df_isForceMultiLines = isForceMultiLines ;

        this.type = df_type;
        this.title = df_title;
        this.value = df_value;
        this.value1 = df_value1;
        this.isForceMultiLines = this.df_isForceMultiLines;


        this.printerMode = printerMode;
    }

    PrinterItem(PrinterItemType type, PrinterElement title, PrinterElement value){
        set(type, title, value, false , 3);
    }

    PrinterItem(PrinterItemType type, PrinterElement title, PrinterElement value, boolean isForceMultiLines ){
        set(type, title,value, isForceMultiLines , 3);
    }
    PrinterItem(PrinterItemType type, PrinterElement title, PrinterElement value, PrinterElement value1, boolean isForceMultiLines ){
        set(type, title,value, value1  );
    }
    PrinterItem(PrinterItemType type, int printerMode, PrinterElement title, PrinterElement value){
        set(type, title, value, false , printerMode);
    }

    public void copy( PrinterItem printerItem ){
        this.title = new PrinterElement( printerItem.title);
        this.value = new PrinterElement( printerItem.value);
        this.value1 = new PrinterElement( printerItem.value1);
        this.type = printerItem.type;
        this.isForceMultiLines = printerItem.isForceMultiLines;
    }

    public void restore(){
        type = df_type;
        title = df_title;
        value = df_value;
        value1 = df_value1;
        isForceMultiLines = df_isForceMultiLines;
    }


}
