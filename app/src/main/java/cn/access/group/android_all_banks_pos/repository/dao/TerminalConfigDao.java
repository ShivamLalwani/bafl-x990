package cn.access.group.android_all_banks_pos.repository.dao;

        import android.arch.persistence.room.Dao;
        import android.arch.persistence.room.Insert;
        import android.arch.persistence.room.OnConflictStrategy;
        import android.arch.persistence.room.Query;
        import android.arch.persistence.room.Update;

        import java.util.List;

        import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;

/**
 * on 10/29/2019.
 */
@Dao
public interface TerminalConfigDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConfig(TerminalConfig terminalConfig);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertManyConfig(List<TerminalConfig> terminalConfig);

    @Query("SELECT * FROM TerminalConfig")
    List<TerminalConfig> terminalConfiguration();

    @Query("SELECT * FROM terminalconfig WHERE tcid LIKE  :tcid ")
    TerminalConfig terminalConfigurationByID(int tcid);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateConfiguration(TerminalConfig terminalConfig);

    @Query("UPDATE TerminalConfig SET batchNo=:batchNo WHERE tcid = :tcid")
    void updateBatch(String batchNo, int tcid);

    @Query("UPDATE TerminalConfig SET SETTLECOUNTER=:SETTLECOUNTER WHERE tcid = :tcid")
    void updateSettleCounter(int SETTLECOUNTER , int tcid);

    @Query("UPDATE TerminalConfig SET tipNumAdjust=:TIPNUMADJUST WHERE tcid = :tcid")
    void updateTipNumAdjust(int TIPNUMADJUST , int tcid);

    @Query("DELETE FROM terminalconfig")
    void nukeTable();
}
