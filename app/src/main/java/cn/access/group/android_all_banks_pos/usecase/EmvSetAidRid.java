package cn.access.group.android_all_banks_pos.usecase;

import android.content.Context;
import android.nfc.Tag;
import android.os.RemoteException;
import android.util.Log;

import com.vfi.smartpos.deviceservice.aidl.DRLData;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.basic.EMVParamKey;
import cn.access.group.android_all_banks_pos.caseA.EMVParamAppCaseA;
import cn.access.group.android_all_banks_pos.caseA.EMVParamKeyCaseA;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;

/**
 * Created by Simon on 2018/9/7.
 *
 * @ brief Demo source of set AID and RID
 */
public class EmvSetAidRid {
    private static final String TAG = "EMV-SetAidRid";

    private IEMV ipboc;

    private WeakReference<Context> mContext;

    public EmvSetAidRid(IEMV ipboc, Context context) {
        this.ipboc = ipboc;
        this.mContext = new WeakReference<>(context) ;
    }

    /**
     * @ brief set, update the AID
     * <p>
     * In this demo, there're 2 way to set the AID
     * 1#, set each tag & value
     * 2#, set one tlv string
     * in the EMVParamAppCaseA, you can reset the tag or value in EMVParamAppCaseA.append
     * \code{.java}
     * \end code
     * @ version
     * @see EMVParamAppCaseA
     */
    public void setAID(int type) {
        boolean isSuccess;
        //+ taha 10-03-2021 set to 1 to enable RMB: 0156, else 0 for PKR:0586 currency
        SharedPref.write("isRmbCurrency",0);
        SharedPref.write("printEmvLogs",0);
        SharedPref.write("printIsoLogs",0);

        int isRmbCurrency = SharedPref.read("isRmbCurrency");
        Utility.DEBUG_LOG(TAG,"isRmbCurrency:"+isRmbCurrency);

        Repository repository = new Repository(mContext.get());
        TerminalConfig terminalConfig = repository.getTerminalConfigurationFromUi();
        Utility.DEBUG_LOG(TAG,"terminalConfig.getCvmLimitUpi:"+terminalConfig.getCvmLimitUpi());

        String cvmLimitVisaHex = DataFormatterUtil.decimalToBcdAmount( terminalConfig.getCvmLimitVisa()  );
        String cvmLimitMcHex = DataFormatterUtil.decimalToBcdAmount( terminalConfig.getCvmLimitMc()  );
        String cvmLimitUpiHex = DataFormatterUtil.decimalToBcdAmount( terminalConfig.getCvmLimitUpi()  );
        String cvmLimitPaypakHex = DataFormatterUtil.decimalToBcdAmount( terminalConfig.getCvmLimitPaypak()  );
        String cvmLimitJcbHex = DataFormatterUtil.decimalToBcdAmount( terminalConfig.getCvmLimitJcb()  );
        String cvmLimitAmexHex = DataFormatterUtil.decimalToBcdAmount( terminalConfig.getCvmLimitAmex()  );

        Utility.DEBUG_LOG(TAG,"cvmLimitVisaHex:"+cvmLimitVisaHex);
        Utility.DEBUG_LOG(TAG,"cvmLimitMcHex:"+cvmLimitMcHex);
        Utility.DEBUG_LOG(TAG,"cvmLimitUpiHex:"+cvmLimitUpiHex);
        Utility.DEBUG_LOG(TAG,"cvmLimitPaypakHex:"+cvmLimitPaypakHex);
        Utility.DEBUG_LOG(TAG,"cvmLimitJcbHex:"+cvmLimitJcbHex);
        Utility.DEBUG_LOG(TAG,"cvmLimitAmexHex:"+cvmLimitAmexHex);

        byte[][] drlId          = new byte[3][2];//default FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
//        byte[][] drlId          = new byte[3][16];//default FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
        byte[][] drlFloorLimits = new byte[3][6];
        byte[][] drlTransLimits = new byte[3][6];
        byte[][] drlCvmLimits   = new byte[3][6];

        drlId[0] = new byte[]{ (byte)0x9f, (byte)0x70 };//9f70 for amex
        drlId[1] = new byte[]{ (byte)0x9f, (byte)0x70 };//9f70 for amex
        drlId[2] = new byte[]{ (byte)0x9f, (byte)0x70 };//9f70 for amex

//        drlId[0] = new byte[]{ (byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF };//9f70 for amex
//        drlId[1] = new byte[]{ (byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF };//9f70 for amex
//        drlId[2] = new byte[]{ (byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF };//9f70 for amex

        drlFloorLimits[0] = new byte[]{00, 00, 00, 00, (byte)00, 00};//000000000000
        drlFloorLimits[1] = new byte[]{00, 00, 00, 00, (byte)0x04, 00};//000000000000
        drlFloorLimits[2] = new byte[]{00, 00, 00, 00, (byte)0x02, 00};//000000000000

        drlTransLimits[0] = new byte[]{(byte)00, (byte)0x09, (byte)0x99, (byte)0x99, (byte)0x99, (byte)0x99};//000999999999
        drlTransLimits[1] = new byte[]{(byte)00, (byte)0x09, (byte)0x99, (byte)0x99, (byte)0x99, (byte)0x99};//000999999999
        drlTransLimits[2] = new byte[]{(byte)00, (byte)0x09, (byte)0x99, (byte)0x99, (byte)0x99, (byte)0x99};//000999999999

        drlCvmLimits[0] = new byte[]{00, 00, 00, (byte)0x03, 00, 00};//000000000200
        drlCvmLimits[1] = new byte[]{00, 00, 00, (byte)0x03, 00, 00};//000000000200
        drlCvmLimits[2] = new byte[]{00, 00, 00, (byte)0x03, 00, 00};//000000000200

        DRLData drlData1 = new DRLData(drlId[0], drlFloorLimits[0], drlTransLimits[0], drlCvmLimits[0]);
        DRLData drlData2 = new DRLData(drlId[1], drlFloorLimits[1], drlTransLimits[1], drlCvmLimits[1]);
        DRLData drlData3 = new DRLData(drlId[2], drlFloorLimits[2], drlTransLimits[2], drlCvmLimits[2]);

        byte[][] drlIdVisa          = new byte[4][2];//default FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
        byte[][] drlFloorLimitsVisa = new byte[4][6];
        byte[][] drlTransLimitsVisa = new byte[4][6];
        byte[][] drlCvmLimitsVisa   = new byte[4][6];

        drlIdVisa[0] = new byte[]{ (byte)0x9F, (byte)0x5A };//9F5A for visa
        drlIdVisa[1] = new byte[]{ (byte)0x9F, (byte)0x5A };//9F5A for visa
        drlIdVisa[2] = new byte[]{ (byte)0x9F, (byte)0x5A };//9F5A for visa
        drlIdVisa[3] = new byte[]{ (byte)0x9F, (byte)0x5A };//9F5A for visa

        drlFloorLimitsVisa[0] = new byte[]{00, 00, 00, 00, (byte)00, 00};//000000000000
        drlFloorLimitsVisa[1] = new byte[]{00, 00, 00, 00, (byte)00, 00};//000000000000
        drlFloorLimitsVisa[2] = new byte[]{00, 00, 00, 00, (byte)00, 00};//000000000000
        drlFloorLimitsVisa[3] = new byte[]{00, 00, 00, 00, (byte)00, 00};//000000000000

        drlTransLimitsVisa[0] = new byte[]{(byte)00, (byte)0x09, (byte)0x99, (byte)0x99, (byte)0x99, (byte)0x99};//000999999999
        drlTransLimitsVisa[1] = new byte[]{(byte)00, (byte)0x09, (byte)0x99, (byte)0x99, (byte)0x99, (byte)0x99};//000999999999
        drlTransLimitsVisa[2] = new byte[]{(byte)00, (byte)0x09, (byte)0x99, (byte)0x99, (byte)0x99, (byte)0x99};//000999999999
        drlTransLimitsVisa[3] = new byte[]{(byte)00, (byte)0x09, (byte)0x99, (byte)0x99, (byte)0x99, (byte)0x99};//000999999999

        drlCvmLimitsVisa[0] = new byte[]{00, 00, 00, (byte)0x15, 00, 00};//000000000200
        drlCvmLimitsVisa[1] = new byte[]{00, 00, 00, (byte)0x15, 00, 00};//000000000200
        drlCvmLimitsVisa[2] = new byte[]{00, 00, 00, (byte)0x15, 00, 00};//000000000200
        drlCvmLimitsVisa[3] = new byte[]{00, 00, 00, (byte)0x15, 00, 00};//000000000200

        DRLData drlData1Visa = new DRLData(drlIdVisa[0], drlFloorLimitsVisa[0], drlTransLimitsVisa[0], drlCvmLimitsVisa[0]);
        DRLData drlData2Visa = new DRLData(drlIdVisa[1], drlFloorLimitsVisa[1], drlTransLimitsVisa[1], drlCvmLimitsVisa[1]);
        DRLData drlData3Visa = new DRLData(drlIdVisa[2], drlFloorLimitsVisa[2], drlTransLimitsVisa[2], drlCvmLimitsVisa[2]);
        DRLData drlData4Visa = new DRLData(drlIdVisa[3], drlFloorLimitsVisa[3], drlTransLimitsVisa[3], drlCvmLimitsVisa[3]);


        if (type == ConstIPBOC.updateAID.operation.clear) {
            // clear all AID
            isSuccess = false;
            try {
                isSuccess = ipboc.updateAID(3, 1, null);
                Utility.DEBUG_LOG(TAG, "Clear AID (smart AID):" + isSuccess);
                isSuccess = ipboc.updateAID(3, 2, null);
                Utility.DEBUG_LOG(TAG, "Clear AID (CTLS):" + isSuccess);
                isSuccess = ipboc.updateVisaAPID(2, drlData1);
                Utility.DEBUG_LOG(TAG, "Clear DRL 1:" + isSuccess);
                isSuccess = ipboc.updateVisaAPID(2, drlData2);
                Utility.DEBUG_LOG(TAG, "Clear DRL 2:" + isSuccess);
                isSuccess = ipboc.updateVisaAPID(2, drlData3);
                Utility.DEBUG_LOG(TAG, "Clear DRL 3:" + isSuccess);

                isSuccess = ipboc.updateVisaAPID(2, drlData1Visa);
                Utility.DEBUG_LOG(TAG, "Clear DRL 1 Visa:" + isSuccess);
                isSuccess = ipboc.updateVisaAPID(2, drlData2Visa);
                Utility.DEBUG_LOG(TAG, "Clear DRL 1 Visa:" + isSuccess);
                isSuccess = ipboc.updateVisaAPID(2, drlData3Visa);
                Utility.DEBUG_LOG(TAG, "Clear DRL 1 Visa:" + isSuccess);
                isSuccess = ipboc.updateVisaAPID(2, drlData4Visa);
                Utility.DEBUG_LOG(TAG, "Clear DRL 1 Visa:" + isSuccess);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return;
        }
        // append a AID

        boolean rv = false;
        try {
            rv = ipboc.updateVisaAPID(1,drlData1 );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG(TAG,"rv 1:"+rv);
        try {
            rv = ipboc.updateVisaAPID(1,drlData2 );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG(TAG,"rv 2:"+rv);
        try {
            rv = ipboc.updateVisaAPID(1,drlData3 );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG(TAG,"rv 3:"+rv);


        try {
            rv = ipboc.updateVisaAPID(1,drlData1Visa );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG(TAG,"rv 1 visa:"+rv);
        try {
            rv = ipboc.updateVisaAPID(1,drlData2Visa );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG(TAG,"rv 2 visa:"+rv);
        try {
            rv = ipboc.updateVisaAPID(1,drlData3Visa );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG(TAG,"rv 3 visa:"+rv);
        try {
            rv = ipboc.updateVisaAPID(1,drlData4Visa );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Utility.DEBUG_LOG(TAG,"rv 4 visa:"+rv);



        final int MAX_AID = 50;
        // 1# way of setting the AID
        // set each Tag & Value
        EMVParamAppCaseA[] emvParamAppUMSList = new EMVParamAppCaseA[MAX_AID];
//        emvParamAppUMSList[0] = new EMVParamAppCaseA();
//        emvParamAppUMSList[0].setComment("PBOC credit or debit");
//        emvParamAppUMSList[0].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[0].aidType = ConstIPBOC.updateAID.aidType.smart_card;
////                "9F0608A000000333010106DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000001DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000000100000DF1906000000100000DF2006000999999999DF2106000000100000"
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_AID_9F06 , "A000000333010106");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[0].append(0x9F08, "0020");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800" );
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "D84004F800" );
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[0].append(0xDF18, "01");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_ECTransLimit_9F7B, "000000100000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000100000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000000000");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[0].getTlvString());
//
//        emvParamAppUMSList[1] = new EMVParamAppCaseA();
//        emvParamAppUMSList[1].setComment("Visa Plus");
//        emvParamAppUMSList[1].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[1].aidType = ConstIPBOC.updateAID.aidType.contactless;
////                "9F0607A0000000038010DF2006000000050000DF14039F3704DF1B060000000000005F2A020156DF1701019F090201409F1B04000000009F5A07A0000000038010DF2106000000000000DF160101DF0406000000000000DF19060000000000009F3303E0F9C8DF1A060000000000009F4005FF00F0A001DF0101009F6604260000809F350122DF180101DF1205D84004F8009F1A020156DF150400000000DF1105D84004A800DF13050010000000"
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_AID_9F06	, "A0000000038010" );  // 	9F06	07	A0000000038010
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000000050000" );  // 	DF20	06	000000050000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" );  // 	DF14	03	9F3704
//        emvParamAppUMSList[1].append( 0xDF1B	, "000000000000" );  // 	DF1B	06	000000000000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "01" );  // 	DF17	01	01
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "0140" );  // 	9F09	02	0140
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "00000000" );  // 	9F1B	04	00000000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000000000" );  // 	DF21	06	000000000000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "01" );  // 	DF16	01	01
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );  // 	DF04	06	000000000000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );  // 	DF19	06	000000000000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F9C8
//        emvParamAppUMSList[1].append( 0xDF1A	, "000000000000" );  // 	DF1A	06	000000000000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "FF00F0A001" );  // 	9F40	05	FF00F0A001
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_ASI_DF01	, "00" );  // 	DF01	01	00
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );  // 	9F35	01	22
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG__DF18	, "01" );  // 	DF18	01	01
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "D84004F800" );  // 	DF12	05	D84004F800
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );  // 	DF15	04	00000000
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "D84004A800" );  // 	DF11	05	D84004A800
//        emvParamAppUMSList[1].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0010000000" );  // 	DF13	05	0010000000
//
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[1].getTlvString());
//
//        //UnionPay Debit
//        emvParamAppUMSList[2] = new EMVParamAppCaseA();
//        emvParamAppUMSList[2].setComment("UnionPay Debit");
//        emvParamAppUMSList[2].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[2].aidType = ConstIPBOC.updateAID.aidType.smart_card;
////                "9F0608A000000333010106DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000001DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000000100000DF1906000000100000DF2006000999999999DF2106000000100000"
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_AID_9F06 , "A000000333010101");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800" );
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800" );
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[2].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[2].append(0xDF18, "01");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[2].getTlvString());
//        //UnionPay Credit
//        emvParamAppUMSList[3] = new EMVParamAppCaseA();
//        emvParamAppUMSList[3].setComment("UnionPay Credit");
//        emvParamAppUMSList[3].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[3].aidType = ConstIPBOC.updateAID.aidType.smart_card;
////                "9F0608A000000333010106DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000001DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000000100000DF1906000000100000DF2006000999999999DF2106000000100000"
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_AID_9F06 , "A000000333010102");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800" );
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800" );
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[3].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[3].append(0xDF18, "01");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[3].getTlvString());   //UNION PAY AID
//
//
//        //UnionPay Quasi Credit
//        emvParamAppUMSList[4] = new EMVParamAppCaseA();
//        emvParamAppUMSList[4].setComment("UnionPay Quasi Credit");
//        emvParamAppUMSList[4].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[4].aidType = ConstIPBOC.updateAID.aidType.smart_card;
////                "9F0608A000000333010106DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000001DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000000100000DF1906000000100000DF2006000999999999DF2106000000100000"
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_AID_9F06 , "A000000333010103");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800" );
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800" );
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[4].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[4].append(0xDF18, "01");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[4].getTlvString());
//
//        //////////////5////////////// union pay ctls
//        emvParamAppUMSList[5] = new EMVParamAppCaseA();
//        emvParamAppUMSList[5].setComment("Union Pay CTLS");
//        emvParamAppUMSList[5].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[5].aidType = ConstIPBOC.updateAID.aidType.contactless;
////                "9F0607A0000000038010DF2006000000050000DF14039F3704DF1B060000000000005F2A020586DF1701019F090201409F1B04000000009F5A07A0000000038010DF2106000000000000DF160101DF0406000000000000DF19060000000000009F3303E0F9C8DF1A060000000000009F4005FF00F0A001DF0101009F6604260000809F350122DF180101DF1205D84004F8009F1A020586DF150400000000DF1105D84004A800DF13050010000000"
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_AID_9F06 , "A000000333010101");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "0030" );  // 	9F09	0030
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "0000000000" );
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "0000000000" );
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_Threshold_DF15, "000000250000");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[5].append(0xDF18, "01");
//        ///////////???????????????///////////////
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000000050000" );  // 	DF20	06	000000050000
//        emvParamAppUMSList[5].append( 0xDF1B	, "000000000000" );  // 	DF1B	06	000000000000
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG__9F5A	, "A000000333010101" );  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000000000" );  // 	DF21	06	000000000000
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );  // 	DF04	06	000000000000
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );  // 	DF19	06	000000000000
//        emvParamAppUMSList[5].append( 0xDF1A	, "000000000000" );  // 	DF1A	06	000000000000
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "FF00F0A001" );  // 	9F40	05	FF00F0A001
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );  // 	9F35	01	22
//        emvParamAppUMSList[5].append(EMVParamAppCaseA.TAG__DF18	, "01" );  // 	DF18	01	01
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[5].getTlvString());
//
//
//
//        //UnionPay Quasi Credit ctls
//        emvParamAppUMSList[6] = new EMVParamAppCaseA();
//        emvParamAppUMSList[6].setComment("UnionPay Quasi Credit CTLS");
//        emvParamAppUMSList[6].flagAppendRemoveClear = ConstIPBOC.updateAID.operation.append;
//        emvParamAppUMSList[6].aidType = ConstIPBOC.updateAID.aidType.contactless;
////                "9F0608A000000333010106DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000001DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000000100000DF1906000000100000DF2006000999999999DF2106000000100000"
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_AID_9F06 , "A000000333010103");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800" );
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800" );
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[6].append(0xDF18, "01");
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000000050000" );  // 	DF20	06	000000050000
//        emvParamAppUMSList[6].append( 0xDF1B	, "000000000000" );  // 	DF1B	06	000000000000
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG__9F5A	, "A000000333010103" );  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000000000" );  // 	DF21	06	000000000000
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );  // 	DF04	06	000000000000
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );  // 	DF19	06	000000000000
//        emvParamAppUMSList[6].append( 0xDF1A	, "000000000000" );  // 	DF1A	06	000000000000
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "FF00F0A001" );  // 	9F40	05	FF00F0A001
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );  // 	9F35	01	22
//        emvParamAppUMSList[6].append(EMVParamAppCaseA.TAG__DF18	, "01" );  // 	DF18	01	01
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[6].getTlvString());
//
//        //UnionPay Debit ctls
//        emvParamAppUMSList[7] = new EMVParamAppCaseA();
//        emvParamAppUMSList[7].setComment("UnionPay Debit CTLS");
//        emvParamAppUMSList[7].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[7].aidType = ConstIPBOC.updateAID.aidType.contactless;
////                "9F0608A000000333010106DF0101009F08020020DF1105D84000A800DF1205D84004F800DF130500100000009F1B0400000001DF150400000000DF160199DF170199DF14039F3704DF1801019F7B06000000100000DF1906000000100000DF2006000999999999DF2106000000100000"
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_AID_9F06 , "A000000333010102");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800" );
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800" );
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[7].append(0xDF18, "01");
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000000050000" );  // 	DF20	06	000000050000
//        emvParamAppUMSList[7].append( 0xDF1B	, "000000000000" );  // 	DF1B	06	000000000000
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG__9F5A	, "A000000333010102" );  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000000000" );  // 	DF21	06	000000000000
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );  // 	DF04	06	000000000000
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );  // 	DF19	06	000000000000
//        emvParamAppUMSList[7].append( 0xDF1A	, "000000000000" );  // 	DF1A	06	000000000000
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "FF00F0A001" );  // 	9F40	05	FF00F0A001
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );  // 	9F35	01	22
//        emvParamAppUMSList[7].append(EMVParamAppCaseA.TAG__DF18	, "01" );  // 	DF18	01	01
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[7].getTlvString());
//
//
//        //MASTER CARD AIDS CARDS...
//
//        // // MasterCard(debit card)_	30 60
//        emvParamAppUMSList[8] = new EMVParamAppCaseA();
//        emvParamAppUMSList[8].setComment("MasterCard debit card");
//        emvParamAppUMSList[8].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[8].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000000043060");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A812" );
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "D84000F800" );
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        //emvParamAppUMSList[8].append(0xDF18, "01");
//        //emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_DefaultTDOL_97_Optional,"9F1A0295059A039C01");
//        emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//       // emvParamAppUMSList[8].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[8].getTlvString());
//
//
//
//        //MasterCard credit or debit	10 10
//        emvParamAppUMSList[9] = new EMVParamAppCaseA();
//        emvParamAppUMSList[9].setComment("MasterCard credit or debit");
//        emvParamAppUMSList[9].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[9].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000000041010");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A812" );
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "D84000F800" );
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//       // emvParamAppUMSList[9].append(0xDF18, "01");
//       // emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_DefaultTDOL_97_Optional,"9F1A0295059A039C01");
//        emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//       // emvParamAppUMSList[9].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[9].getTlvString());
//
//
//        // MasterCard
//        emvParamAppUMSList[10] = new EMVParamAppCaseA();
//        emvParamAppUMSList[10].setComment("MasterCard");
//        emvParamAppUMSList[10].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[10].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000000044010");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A812" );
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "D84000F800" );
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        //emvParamAppUMSList[10].append(0xDF18, "01");
//       // emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_DefaultTDOL_97_Optional,"9F1A0295059A039C01");
//        emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//        //emvParamAppUMSList[10].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[10].getTlvString());
//
//      //MasterCard
//        emvParamAppUMSList[11] = new EMVParamAppCaseA();
//        emvParamAppUMSList[11] = new EMVParamAppCaseA();
//        emvParamAppUMSList[11].setComment("MasterCard");
//        emvParamAppUMSList[11].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[11].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000000046000");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A812" );
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "D84000F800" );
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        //emvParamAppUMSList[11].append(0xDF18, "01");
//        //emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_DefaultTDOL_97_Optional,"9F1A0295059A039C01");
//        emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//        //emvParamAppUMSList[11].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[11].getTlvString());
//    /*    DF01(001): 00
//        DF19(006): 00 00 00 00 00 00
//        9F7B(006): 00 00 00 00 00 0*/
//
//        //paypak
//        emvParamAppUMSList[12] = new EMVParamAppCaseA();
//        emvParamAppUMSList[12].setComment("paypak credit or debit");
//        emvParamAppUMSList[12].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[12].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000007361010");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "BC78BCA800" );
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "BC78BCF800" );
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        emvParamAppUMSList[12].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000000000");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[12].getTlvString());
//
//
//        //paypak
//        emvParamAppUMSList[13] = new EMVParamAppCaseA();
//        emvParamAppUMSList[13].setComment("paypak credit or debit");
//        emvParamAppUMSList[13].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[13].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000007362010");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "BC78BCA800" );
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "BC78BCF800" );
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        emvParamAppUMSList[13].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000000000");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[13].getTlvString());
//
//
//
//        //Paypak
//        emvParamAppUMSList[14] = new EMVParamAppCaseA();
//        emvParamAppUMSList[14].setComment("paypak credit or debit");
//        emvParamAppUMSList[14].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[14].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000007363010");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "BC78BCA800" );
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "BC78BCF800" );
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        emvParamAppUMSList[14].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000000000");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[14].getTlvString());
//
//
//      /*  // Visa credit or debit	10 10
//        "9F0607A0000000031010" +
//                "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF120500000000009F1B0400000000DF160101DF150400000000DF1105C000000000DF19060000000000009F7B06000000000000DF13050000000000",
//*/
//        emvParamAppUMSList[15] = new EMVParamAppCaseA();
//        emvParamAppUMSList[15].setComment("Visa Plus");
//        emvParamAppUMSList[15].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[15].aidType = ConstIPBOC.updateAID.aidType.contactless;
////                "9F0607A0000000038010DF2006000000050000DF14039F3704DF1B060000000000005F2A020156DF1701019F090201409F1B04000000009F5A07A0000000038010DF2106000000000000DF160101DF0406000000000000DF19060000000000009F3303E0F9C8DF1A060000000000009F4005FF00F0A001DF0101009F6604260000809F350122DF180101DF1205D84004F8009F1A020156DF150400000000DF1105D84004A800DF13050010000000"
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_AID_9F06	, "A0000000031010" );  // 	9F06	07	A0000000038010
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000900000000" );  // 	DF20	06	000000050000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" );  // 	DF14	03	9F3704
//        emvParamAppUMSList[15].append( 0xDF1B	, "000000000000" );  // 	DF1B	06	000000000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );  // 	5F2A	02	0586
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "01" );  // 	DF17	01	01
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "0140" );  // 	9F09	02	0140
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "00000000" );  // 	9F1B	04	00000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000031010" );  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000000000" );  // 	DF21	06	000000000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "01" );  // 	DF16	01	01
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );  // 	DF04	06	000000000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );  // 	DF19	06	000000000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F9C8
//        emvParamAppUMSList[15].append( 0xDF1A	, "000000000000" );  // 	DF1A	06	000000000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "FF00F0A001" );  // 	9F40	05	FF00F0A001
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_ASI_DF01	, "00" );  // 	DF01	01	00
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG__9F66	, "26000080" );  // 	9F66	04	26000080
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );  // 	9F35	01	22
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG__DF18	, "01" );  // 	DF18	01	01
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "D84004F800" );  // 	DF12	05	D84004F800
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );  // 	DF15	04	00000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "D84004A800" );  // 	DF11	05	D84004A800
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0010000000" );  // 	DF13	05	0010000000
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        emvParamAppUMSList[15].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000000000");
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[15].getTlvString());
//
//
//        //VISA
//        emvParamAppUMSList[16] = new EMVParamAppCaseA();
//        emvParamAppUMSList[16].setComment("VISA credit or debit");
//        emvParamAppUMSList[16].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[16].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_AID_9F06 , "A0000000031010");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C" );  // 	9F09	0030
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "30");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000000000");

        //Shivam added Aid for certification 02-Feb-2021
        //FOR SMART CARD

        int index=0;
        //VISA
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("Visa Credit Debit");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000031010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(0xDF1A, "000000012000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6F00F0F001");
        emvParamAppUMSList[index].append(0xDF20, "009999999999");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "32004000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
        emvParamAppUMSList[index].append(0x9F08, "0140");
        emvParamAppUMSList[index].append(0x9F08, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;

        //VISA
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("Visa Electron");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000032010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;

        //VISA
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("VPAY");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000032020");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //Mastercard
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("Mastercard");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000041010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0002");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "FE50BC2000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "FE50BCF800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");  // 	9F1A	02	0586 //new 0586
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1D_TermRiskManagementData, "6CE8800000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //Maestro
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("Maestro");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000043060");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0002");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "FE50BC2000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "FE50BCF800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //AMEX
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("AMEX");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A00000002501");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC50FC9800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DE00FC9800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //UnionPay Debit
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("UnionPay Debit");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000333010101");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0030");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "F000F0A001");  // 	9F40	05	F000F0A001
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //UnionPay Credit
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("UnionPay Credit");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000333010102");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0030");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "F000F0A001");  // 	9F40	05	F000F0A001
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //UnionPay Quasi Credit
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("UnionPay Quasi Credit");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000333010103");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0030");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "F000F0A001");  // 	9F40	05	F000F0A001
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //PayPak
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("PayPak PrePaid");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000007361010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "BC78BCA800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "BC78BCF800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //PayPak
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("PayPak Debit");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000007362010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "BC78BCA800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "BC78BCF800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //PayPak
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("PayPak Credit");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000007363010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "BC78BCA800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "BC78BCF800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //JCB
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("JCB");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000651010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0200");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "FC6024A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "FC60ACF800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00050000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E040C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //Shivam added Aid for certification 02-Feb-2021
        //FOR CTLS

        //Mastercard
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("Mastercard");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000041010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000"); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0002000000000000000000000000000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0002");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
        // emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitMcHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLS_CDCVM_DF54, "000000050000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E040C8");
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6000F0A001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B6000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "FC509C8800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "FC509C8800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1D_TermRiskManagementData, "6CE8800000000000");
        //shivam adding more tags here
        emvParamAppUMSList[index].append(0xDF811B, "30");
        emvParamAppUMSList[index].append(0xDF8132, "0014");
        emvParamAppUMSList[index].append(0xDF8133, "0032");
        emvParamAppUMSList[index].append(0xDF8134, "0012");
        emvParamAppUMSList[index].append(0xDF8135, "0018");
        emvParamAppUMSList[index].append(0xDF8136, "012C");
        emvParamAppUMSList[index].append(0xDF8137, "32");

        emvParamAppUMSList[index].append(0xDFAB02, "01");
        emvParamAppUMSList[index].append(0xDFAB03, "0C02000000");
        emvParamAppUMSList[index].append(0xDFAB05, "2120110000000000");
        emvParamAppUMSList[index].append(0xDFAB06, "86065931");
        emvParamAppUMSList[index].append(0xDFAB07, "301A128D");
        emvParamAppUMSList[index].append(0xDFAB08, "0A");

        emvParamAppUMSList[index].append(0xDF811E, "60");
        emvParamAppUMSList[index].append(0xDF812C, "08");
        emvParamAppUMSList[index].append(0xDF8118, "60");
        emvParamAppUMSList[index].append(0xDF8119, "08");
        emvParamAppUMSList[index].append(0xDF811F, "68");
        emvParamAppUMSList[index].append(0xDF8117, "E0");

        Utility.DEBUG_LOG(TAG+"MC ctls", emvParamAppUMSList[index].getTlvString());
        index++;


        //VISA
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("VISA");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000031010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000"); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "008D000000000000000000000000000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
        // emvParamAppUMSList413].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitVisaHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E040C8");
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6000F0A001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "36000080");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");

        emvParamAppUMSList[index].append(0xDFAB02, "01");

        Utility.DEBUG_LOG(TAG + "visa ctls", emvParamAppUMSList[index].getTlvString());
        index++;


        //VISA
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("Visa Electron");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000032010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000"); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "008D000000000000000000000000000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
        // emvParamAppUMSList513].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitVisaHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6000F0A001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "36000080");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
        emvParamAppUMSList[index].append(0xDFAB02, "01");

        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;

        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("Union Pay CTLS");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000333010101");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0030");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "000000250000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
        emvParamAppUMSList[index].append(0xDF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000");  // 	DF20	06	000000050000
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");  // 	DF1B	06	000000000000
        if ( isRmbCurrency == 1)
        {
            Utility.DEBUG_LOG(TAG,"Setting currency code to RMB");
            emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0156");  // 	5F2A	02	0586
        }
        else
        {
            Utility.DEBUG_LOG(TAG,"Setting currency code to PKR");
            emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");  // 	5F2A	02	0586
        }
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F5A, "A000000333010101");  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");  // 	DF21	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitUpiHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");  // 	DF04	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");  // 	DF19	06	000000000000
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");  // 	DF1A	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "FF00F0A001");  // 	9F40	05	FF00F0A001
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "26000080");  // 	9F66	04	26000080
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");  // 	9F35	01	22
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");  // 	DF18	01	01
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        SharedPref.write("CvmLimitUpi","300.00");
        index++;

        //UnionPay Debit ctls
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("UnionPay Debit CTLS");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000333010102");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "008C");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
        emvParamAppUMSList[index].append(0xDF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000");  // 	DF20	06	000000050000
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");  // 	DF1B	06	000000000000
        if ( isRmbCurrency == 1)
        {
            Utility.DEBUG_LOG(TAG,"Setting currency code to RMB");
            emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0156");  // 	5F2A	02	0586
        }
        else
        {
            Utility.DEBUG_LOG(TAG,"Setting currency code to PKR");
            emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");  // 	5F2A	02	0586
        }
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F5A, "A000000333010102");  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");  // 	DF21	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitUpiHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");  // 	DF04	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");  // 	DF19	06	000000000000
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");  // 	DF1A	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "FF00F0A001");  // 	9F40	05	FF00F0A001
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "26000080");  // 	9F66	04	26000080
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");  // 	9F35	01	22
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");  // 	DF18	01	01
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //UnionPay Quasi Credit ctls
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("UnionPay Quasi Credit CTLS");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateAID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000333010103");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "008C");  // 	9F09	0030
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "D84000A800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "99");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "99");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
        emvParamAppUMSList[index].append(0xDF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000");  // 	DF20	06	000000050000
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");  // 	DF1B	06	000000000000
        if ( isRmbCurrency == 1)
        {
            Utility.DEBUG_LOG(TAG,"Setting currency code to RMB");
            emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0156");  // 	5F2A	02	0586
        }
        else
        {
            Utility.DEBUG_LOG(TAG,"Setting currency code to PKR");
            emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");  // 	5F2A	02	0586
        }
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F5A, "A000000333010103");  // 	9F5A	07	A0000000038010
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");  // 	DF21	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitUpiHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");  // 	DF04	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");  // 	DF19	06	000000000000
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");  // 	DF1A	06	000000000000
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "FF00F0A001");  // 	9F40	05	FF00F0A001
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "26000080");  // 	9F66	04	26000080
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");  // 	9F35	01	22
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");  // 	DF18	01	01
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


//        //Union Pay
//        emvParamAppUMSList[16] = new EMVParamAppCaseA();
//        emvParamAppUMSList[16].setComment("Union Pay");
//        emvParamAppUMSList[16].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[16].aidType = ConstIPBOC.updateAID.aidType.contactless;
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_AID_9F06	, "A000000333010102" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000999999999" ); // not found in v200t
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" ); // not found in v200t
//        emvParamAppUMSList[16].append( 0xDF1B	, "000000000000" );// not found in v200t
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "00" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C000000000000000000000000000000000000" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "000000000000" );
//       // emvParamAppUMSList613].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000000000" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "00" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );
//        emvParamAppUMSList[16].append( 0xDF1A	, "000000000000" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "F000B0A001" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_ASI_DF01	, "01" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG__9F66	, "B6000000" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG__DF18	, "01" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "DC4004F800" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "D84000A800" );
//        emvParamAppUMSList[16].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0000000000" );
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[16].getTlvString());


//        //Union Pay
//        emvParamAppUMSList[17] = new EMVParamAppCaseA();
//        emvParamAppUMSList[17].setComment("Union Pay");
//        emvParamAppUMSList[17].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[17].aidType = ConstIPBOC.updateAID.aidType.contactless;
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_AID_9F06	, "A000000333010103" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000999999999" ); // not found in v200t
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" ); // not found in v200t
//        emvParamAppUMSList[17].append( 0xDF1B	, "000000000000" );// not found in v200t
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "00" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "008C000000000000000000000000000000000000" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "000000000000" );
//       // emvParamAppUMSList713].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000000000" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "00" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );
//        emvParamAppUMSList[17].append( 0xDF1A	, "000000000000" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "F000B0A001" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_ASI_DF01	, "01" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG__9F66	, "B6000000" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG__DF18	, "01" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "DC4004F800" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "D84000A800" );
//        emvParamAppUMSList[17].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0000000000" );
//
//        Utility.DEBUG_LOG(TAG, emvParamAppUMSList[17].getTlvString());


//        //PAYPAK
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("PAYPAK");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000007361010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000"); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t

        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
        // emvParamAppUMSList813].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitPaypakHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "7000B0A001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B6000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "B470008800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "B470008800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
      //  emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B4E0");
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;
//
////
////
////        //PAYPAK
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("PAYPAK");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000007362010");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999"); // not found in v200t
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
//        // emvParamAppUMSList913].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
////        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitPaypakHex);
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
//        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "7000B0A001");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B6000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "B470008800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "B470008800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        //Gemalto
//        emvParamAppUMSList[index].append(0x9F1C, "3030303030303030");
//        emvParamAppUMSList[index].append(0x9F1A, "0586");
//        emvParamAppUMSList[index].append(0x9F35, "22");
//        emvParamAppUMSList[index].append(0x9F33, "E0F8C8");
//        emvParamAppUMSList[index].append(0x9F40, "7000B0A001");
//        emvParamAppUMSList[index].append(0x9F15, "5999");
//        emvParamAppUMSList[index].append(0x9F01, "000000000000");
////        emvParamAppUMSList[index].append(0xDFAB31	, "0000000000" );
////        emvParamAppUMSList[index].append(0xDFAB40	, "000000000000" );
////        emvParamAppUMSList[index].append(0xDFAB41	, "009999999999" );
////        emvParamAppUMSList[index].append(0xDFAB42	, "000000150000" );
////        emvParamAppUMSList[index].append(0xDFAB43	, "BC78BCA800" );
////        emvParamAppUMSList[index].append(0xDFAB44	, "0000000000" );
////        emvParamAppUMSList[index].append(0xDFAB45	, "BC78BCF800" );
////        emvParamAppUMSList[index].append(0xDFAB4F	, "36006043F9" );
////        emvParamAppUMSList[index].append(0xDFAB51	, "9F3704" );
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;


//
//
//        //PAYPAK
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("PAYPAK");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000007362010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000"); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
        // emvParamAppUMSList913].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitPaypakHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "7000B0A001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B6000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "B470008800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "B470008800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        //Gemalto
        emvParamAppUMSList[index].append(0x9F1C, "3030303030303030");
        emvParamAppUMSList[index].append(0x9F1A, "0586");
        emvParamAppUMSList[index].append(0x9F35, "22");
        emvParamAppUMSList[index].append(0x9F33, "E0F8C8");
        emvParamAppUMSList[index].append(0x9F40, "7000B0A001");
        emvParamAppUMSList[index].append(0x9F15, "5999");
        emvParamAppUMSList[index].append(0x9F01, "000000000000");
       // emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B4E0");
//        emvParamAppUMSList[index].append(0xDFAB31	, "0000000000" );
//        emvParamAppUMSList[index].append(0xDFAB40	, "000000000000" );
//        emvParamAppUMSList[index].append(0xDFAB41	, "009999999999" );
//        emvParamAppUMSList[index].append(0xDFAB42	, "000000150000" );
//        emvParamAppUMSList[index].append(0xDFAB43	, "BC78BCA800" );
//        emvParamAppUMSList[index].append(0xDFAB44	, "0000000000" );
//        emvParamAppUMSList[index].append(0xDFAB45	, "BC78BCF800" );
//        emvParamAppUMSList[index].append(0xDFAB4F	, "36006043F9" );
//        emvParamAppUMSList[index].append(0xDFAB51	, "9F3704" );
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;

        //
//
//        //PAYPAK
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("PAYPAK");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000007363010");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000005000000"); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
        // emvParamAppUMSList913].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, cvmLimitPaypakHex);
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "7000B0A001");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "01");  //checked
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B6000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "B470008800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0000000000");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "B470008800");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
        //Gemalto
        emvParamAppUMSList[index].append(0x9F1C, "3030303030303030");
        emvParamAppUMSList[index].append(0x9F1A, "0586");
        emvParamAppUMSList[index].append(0x9F35, "22");
        emvParamAppUMSList[index].append(0x9F33, "E0F8C8");
        emvParamAppUMSList[index].append(0x9F40, "7000B0A001");
        emvParamAppUMSList[index].append(0x9F15, "5999");
        emvParamAppUMSList[index].append(0x9F01, "000000000000");
       // emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B4E0");
//        emvParamAppUMSList[index].append(0xDFAB31	, "0000000000" );
//        emvParamAppUMSList[index].append(0xDFAB40	, "000000000000" );
//        emvParamAppUMSList[index].append(0xDFAB41	, "009999999999" );
//        emvParamAppUMSList[index].append(0xDFAB42	, "000000150000" );
//        emvParamAppUMSList[index].append(0xDFAB43	, "BC78BCA800" );
//        emvParamAppUMSList[index].append(0xDFAB44	, "0000000000" );
//        emvParamAppUMSList[index].append(0xDFAB45	, "BC78BCF800" );
//        emvParamAppUMSList[index].append(0xDFAB4F	, "36006043F9" );
//        emvParamAppUMSList[index].append(0xDFAB51	, "9F3704" );
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;

//
//
//        //AMEX
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("AMEX CTLS 1");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06	, "A00000002501" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000005000000" ); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" ); // not found in v200t
        emvParamAppUMSList[index].append( 0xDF1B	, "000000000000" );// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "0001000000000000000000000000000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "000000001200" );
       // emvParamAppUMSLis2013].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, cvmLimitAmexHex );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );
        emvParamAppUMSList[index].append( 0xDF1A	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "6000F0A001" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66	, "B6000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18	, "01" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "DE00FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "DC50FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0010000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6D_ContactlessReaderCapabilities	, "C8" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "D8F04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "DCF04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB40_ContactlessFloorLimit	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB41_ContactlessTransactionLimit	, "000005000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB42_ContactlessCVMRequiredLimit	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB52_UnpredictableNumberRange	, "3C" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB30_TecSupport	, "03" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F1C_TermIdent	, "3132333435363738" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F15_MerchantCategoryCode	, "5999" );
        emvParamAppUMSList[index].append(0xDFAB02, "01");
        emvParamAppUMSList[index].append(0xDFAB03, "0C02000000");
        emvParamAppUMSList[index].append(0xDFAB05, "2120110000000000");
        emvParamAppUMSList[index].append(0xDFAB08, "0A");
//
        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //        //AMEX
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("AMEX CTLS 2");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06	, "A000000025010402" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000005000000" ); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" ); // not found in v200t
        emvParamAppUMSList[index].append( 0xDF1B	, "000000000000" );// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "0001000000000000000000000000000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "000000001200" );
        // emvParamAppUMSLis2013].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, cvmLimitAmexHex );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );
        emvParamAppUMSList[index].append( 0xDF1A	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "6000F0A001" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01	, "01" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66	, "B6000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18	, "01" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "DE00FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "DC50FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0010000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6D_ContactlessReaderCapabilities	, "C8" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "D8F04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "DCF04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB40_ContactlessFloorLimit	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB41_ContactlessTransactionLimit	, "000005000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB42_ContactlessCVMRequiredLimit	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB52_UnpredictableNumberRange	, "3C" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB30_TecSupport	, "03" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F1C_TermIdent	, "3132333435363738" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F15_MerchantCategoryCode	, "5999" );
        emvParamAppUMSList[index].append(0xDFAB02, "01");
        emvParamAppUMSList[index].append(0xDFAB03, "0C02000000");
        emvParamAppUMSList[index].append(0xDFAB05, "2120110000000000");
        emvParamAppUMSList[index].append(0xDFAB08, "0A");


        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;

        //        //AMEX
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("AMEX CTLS 2");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06	, "A000000025010403" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000005000000" ); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" ); // not found in v200t
        emvParamAppUMSList[index].append( 0xDF1B	, "000000000000" );// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "0001000000000000000000000000000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "000000001200" );
        // emvParamAppUMSLis2013].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, cvmLimitAmexHex );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );
        emvParamAppUMSList[index].append( 0xDF1A	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "6000F0A001" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01	, "01" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66	, "B6000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18	, "01" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "DE00FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "DC50FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0010000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6D_ContactlessReaderCapabilities	, "C8" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "D8F04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "DCF04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB40_ContactlessFloorLimit	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB41_ContactlessTransactionLimit	, "000005000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB42_ContactlessCVMRequiredLimit	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB52_UnpredictableNumberRange	, "3C" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB30_TecSupport	, "03" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F1C_TermIdent	, "3132333435363738" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F15_MerchantCategoryCode	, "5999" );
        emvParamAppUMSList[index].append(0xDFAB02, "01");
        emvParamAppUMSList[index].append(0xDFAB03, "0C02000000");
        emvParamAppUMSList[index].append(0xDFAB05, "2120110000000000");
        emvParamAppUMSList[index].append(0xDFAB08, "0A");


        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;


        //        //AMEX
        emvParamAppUMSList[index] = new EMVParamAppCaseA();
        emvParamAppUMSList[index].setComment("AMEX CTLS 3");
        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06	, "A000000025010901" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20	, "000005000000" ); // not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14	, "9F3704" ); // not found in v200t
        emvParamAppUMSList[index].append( 0xDF1B	, "000000000000" );// not found in v200t
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09	, "0001000000000000000000000000000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B	, "000000001200" );
        // emvParamAppUMSLis2013].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21	, cvmLimitAmexHex );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16	, "00" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33	, "E0F8C8" );
        emvParamAppUMSList[index].append( 0xDF1A	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40	, "6000F0A001" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01	, "01" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66	, "B6000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35	, "22" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18	, "01" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12	, "DE00FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11	, "DC50FC9800" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13	, "0010000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A	, "0586" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15	, "00000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6D_ContactlessReaderCapabilities	, "C8" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "D8F04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F6E_TerminalTransactionCapabilities	, "DCF04000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB40_ContactlessFloorLimit	, "000000000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB41_ContactlessTransactionLimit	, "000005000000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB42_ContactlessCVMRequiredLimit	, "000000030000" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB52_UnpredictableNumberRange	, "3C" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DFAB30_TecSupport	, "03" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F1C_TermIdent	, "3132333435363738" );
        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_9F15_MerchantCategoryCode	, "5999" );
        emvParamAppUMSList[index].append(0xDFAB02, "01");
        emvParamAppUMSList[index].append(0xDFAB03, "0C02000000");
        emvParamAppUMSList[index].append(0xDFAB05, "2120110000000000");
        emvParamAppUMSList[index].append(0xDFAB08, "0A");


        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
        index++;

//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("Visa Credit Debit");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000003101001");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(0xDF1A, "000000012000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6F00F0F001");
//        emvParamAppUMSList[index].append(0xDF20, "009999999999");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "32004000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000050000");
//        emvParamAppUMSList[index].append(0x9F08, "0140");
//        emvParamAppUMSList[index].append(0x9F08, "000000000000");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
//
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("Visa Credit Debit");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000003101002");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(0xDF1A, "000000012000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6F00F0F001");
//        emvParamAppUMSList[index].append(0xDF20, "009999999999");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "32004000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000050000");
//        emvParamAppUMSList[index].append(0x9F08, "0140");
//        emvParamAppUMSList[index].append(0x9F08, "000000000000");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;

//
        isSuccess = false;
        Utility.DEBUG_LOG(TAG,"Final AID index:"+index);
        try {
            for (int i = 0; i < index; i++) {
                Utility.DEBUG_LOG(TAG,"i:"+i);
                isSuccess = ipboc.updateAID(emvParamAppUMSList[i].flagAppendRemoveClear, emvParamAppUMSList[i].aidType, emvParamAppUMSList[i].getTlvString());
                Utility.DEBUG_LOG(TAG, "isSuccess:" + isSuccess);

                if (isSuccess) {
                    Utility.DEBUG_LOG(TAG, "updateAID success:" + emvParamAppUMSList[i].getComment());
                } else {
                    Utility.DEBUG_LOG(TAG, "updateAID false:" + emvParamAppUMSList[i].getComment());
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
//        // 2# way of setting the AID
//        // given the AID string to set. You can change the EMVParamAppCaseA to check each Tag & Value, and modify the tag or value if need
//        // hardcoding the AID string
//        String[] AID_SmartCard = {
//                // Visa credit or debit	10 10
//                "9F3303E0F8C8DF1A06000000012000" +
//                        "9F0607A0000000031010" +
//                        "9F40056F00F0F001DF010100DF2006009999999999DF14039F37049F660432004000DF1701015F2A020586DF1B060000000000009F350122DF1205FC60ACF8009F1B04000000009F1A020586DF2106009999999999DF160199DF150400049444DF1105FC6024A8009F08020140DF1906000000000000DF13050010000000",
//                // Visa credit or debit	10 10
//                "9F0607A0000000031010" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF120500000000009F1B0400000000DF160101DF150400000000DF1105C000000000DF19060000000000009F7B06000000000000DF13050000000000",
//                // Visa credit or debit	10 10
//                "9F0608A000000003101001" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
//                // Visa credit or debit	10 10
//                "9F0608A000000003101002" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
//                // Visa Electron	20 10
//                "9F0607A0000000032010" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
//                // MasterCard credit or debit	10 10
//                "9F0607A0000000041010" +
//                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000001000DF13050000400000",
//                // MasterCard(debit card)_	30 60
//                "9F0607A0000000043060" +
//                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000000000DF13050000400000",
//                // MasterCard
//                "9F0607A0000000044010" +
//                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000000000DF13050000400000",
//                // MasterCard
//                "9F0607A0000000046000" +
//                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000001000DF13050000400000",
//
//                /*   // PBOC
//                   "9F0608A000000333010103" +
//                           "DF2006009999999999DF010100DF14039F3704DF1701209F09020020DF180101DF1205DC4004F8009F1B0400000064DF160150DF150400000028DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
//                   "9F0608A000000333010101" +
//                           "DF2006009999999999DF010100DF14039F3704DF1701999F09020020DF180101DF1205DC4004F8009F1B0400000064DF160199DF150400000000DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
//                   "9F0608A000000333010106" +
//                           "DF2006009999999999DF010100DF14039F3704DF1701999F09020020DF180101DF1205DC4004F8009F1B0400000064DF160199DF150400000000DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
//                   "9F0608A000000333010102" +
//                           "DF2006009999999999DF010100DF14039F3704DF1701209F09020020DF180101DF1205DC4004F8009F1B0400000064DF160150DF150400000000DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
//              */     //
//                "9F0607A0000001523010" +
//                        "DF2006009999999999DF010100DF140111DF1701019F09020001DF180101DF1205D84004F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
//                // JCB
//                "9F0607A0000000651010" +
//                        "DF2006009999999999DF010100DF14039F3704DF1701019F09020001DF180101DF1205FC60ACF8009F1B0400000000DF160101DF150400000000DF1105FC60242800DF19060000000000009F7B06000000000000DF13050010000000",
//                // American Express
//                "9F0606F00000002501" +
//                        "DF2006009999999999DF010100DF14039F3704DF1701009F09020001DF180100DF1205CC000000009F1B0400000000DF160101DF150400000000DF1105CC00000000DF19060000000000009F7B06000000100000DF13050000000000",
//                "9F0606A00000002501" +
//                        "DF2006009999999999DF010100DF14039F3704DF1701999F09020020DF180101DF1205CC000080009F1B0400000000DF160199DF150400000000DF1105CC00000000DF19060000000000009F7B06000010000000DF13050000000000",
//                /* "9F0607A0000003241010" +
//
//                        "DF2006009999999999DF010100DF14039F3704DF1701999F09020001DF180101DF1205FCE09CF8009F1B0400000000DF160199DF150400000000DF1105DC00002000DF19060000000000009F7B06000000000000DF13050010000000",
//              */   "9F0605A000000000" +
//                "DF2006009999999999DF010100DF140111DF1701019F09020001DF180100DF120500000000009F1B0400000064DF160101DF150400000001DF11050000000000DF19060000000000009F7B06000000000100DF13050000000000",
//
//        };
//        String[] AID_CTLS_Card = {
//                "9F3303E0F8C8DF1A06000000012000" +
//                        "9F0607A0000000031010" +
//                        "9F40056F00F0F001DF010100DF2006009999999999DF14039F37049F660432004000DF1701015F2A020586DF1B060000000000009F350122DF1205FC60ACF8009F1B04000000009F1A020586DF2106009999999999DF160199DF150400049444DF1105FC6024A8009F08020140DF1906000000000000DF13050010000000",
//                // Visa credit or debit	10 10
//                "9F0607A0000000031010" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF120500000000009F1B0400000000DF160101DF150400000000DF1105C000000000DF19060000000000009F7B06000000000000DF13050000000000",
//                // Visa credit or debit	10 10
//                "9F0608A000000003101001" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
//                // Visa credit or debit	10 10
//                "9F0608A000000003101002" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
//                // Visa Electron	20 10
//                "9F0607A0000000032010" +
//                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
//
//// masood commented this on 1-9-2020
//
//                // Visa, Plus	80 10
////                "9F0607A0000000038010" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601005F2A020586DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A020586DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
////
////                // Visa credit or debit	10 10
////                "DF1A06000000012000" +
////                        "9F0607A0000000031010" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
////                // Visa credit or debit	10 10
////                "DF1A06000000012000" +
////                        "9F0608A000000003101001" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
////                "DF1A06000000012000" +
////
////                        "9F0608A000000003101002" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
////                // JCB
////                "9F0607A0000000651010" +
////                        "9F1A020586DF0306009999999999DF2006000999999999DF010100DF14039F3704DF170100DF180101DF1205FFFFFFFFFF9F1B04000020009F150400000000DF2106000000100000DF150400000000DF160100DF110590400080009F0802008CDF19060000001000009F7B06000000000000DF13050000000000",
////                // MasterCard credit or debit	10 10
//                // + masood 01-09-2020 commented for testing
//                "9F0607A0000000041010" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
//
//                "9F0607A0000000043060" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
//                // MasterCard
//                "9F0607A0000000044010" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
//                // MasterCard
//                "9F0607A0000000046000" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
//                //
//                "9F0607A0000001523010" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020001DF180101DF1205D84004F8009F1B04000000009F1A020586DF2106000000000100DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
//                /*  // PBOC
//                  "9F0608A000000333010102" +
//                          "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205DC4004F8009F1B04000000649F1A020586DF2106000000100000DF160199DF150400000000DF1105DC4000A800DF0406000000000000DF1906000000000000DF13050010000000",
//                  "9F0608A000000333010101" +
//                          "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205DC4004F8009F1B04000000649F1A020586DF2106000000100000DF160199DF150400000000DF1105DC4000A800DF0406000000000000DF1906000000000000DF13050010000000",
//                  "9F0608A000000333010106" +
//                          "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205DC4004F8009F1B04000000649F1A020586DF2106000000100000DF160199DF150400000000DF1105DC4000A800DF0406000000000000DF1906000000000000DF13050010000000",
//            */      // American Express
//                "9F0606F00000002501" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601225F2A020586DF1701009F09020001DF180100DF1205CC000000009F1B04000000009F1A020586DF2106000000100000DF160101DF150400000000DF1105CC00000000DF0406000000000501DF1906000000000000DF13050000000000",
//                "9F0606A00000002501" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205C4000000009F1B04000000009F1A020586DF2106000010000000DF160199DF150400000000DF1105C400000000DF0406000000000000DF1906000000000000DF13050000000000",
//                "9F0607A0000003241010" +
//                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020001DF180101DF1205FCE09CF8009F1B04000000009F1A020586DF2106000000000000DF160199DF150400000000DF1105DC00002000DF0406000000000000DF1906000000000000DF13050010000000",
//                "DF1A06000000012000" +
//                        "9F0607A00000000" +
//                        "32010DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
//
//        };
//        String[] aidList = AID_SmartCard;
//        int aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        for (int i = 0; i < 2; i++) {
//            for (String aid : aidList) {
//                isSuccess = false;
//                if (aid.length() == 0) {
//                    continue;
//                }
//                try {
//                    EMVParamAppCaseA emvParamAppUMS = new EMVParamAppCaseA();
//
//                    emvParamAppUMS.setFlagAppendRemoveClear(ConstIPBOC.updateAID.operation.append);
//                    emvParamAppUMS.setAidType(aidType);
//                    emvParamAppUMS.append(aid);
//                    isSuccess = ipboc.updateAID(emvParamAppUMS.getFlagAppendRemoveClear(), emvParamAppUMS.getAidType(), emvParamAppUMS.getTlvString());
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//                Utility.DEBUG_LOG(TAG, "" + isSuccess);
//
//                if (isSuccess) {
//                    Utility.DEBUG_LOG(TAG, "update AID success");
//                } else {
//                    Utility.DEBUG_LOG(TAG, "updateAID false");
//                }
//            }
//            aidList = AID_CTLS_Card;
//            aidType = ConstIPBOC.updateAID.aidType.contactless;
//        }
    }

//    public void setAID_temp(int type)
//    {
//
//        boolean isSuccess;
//        if (type == ConstIPBOC.updateAID.operation.clear) {
//            // clear all AID
//            isSuccess = false;
//            try {
//                isSuccess = ipboc.updateAID(3, 1, null);
//                Utility.DEBUG_LOG(TAG, "Clear AID (smart AID):" + isSuccess);
//                isSuccess = ipboc.updateAID(3, 2, null);
//                Utility.DEBUG_LOG(TAG, "Clear AID (CTLS):" + isSuccess);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//            return;
//        }
//        // append a AID
//
//        final int MAX_AID = 7;
//        // 1# way of setting the AID
//        // set each Tag & Value
//        EMVParamAppCaseA[] emvParamAppUMSList = new EMVParamAppCaseA[MAX_AID];
//
//        int index=0;
//        //VISA
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("Visa Credit Debit");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000031010");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(0xDF1A, "000000012000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6F00F0F001");
//        emvParamAppUMSList[index].append(0xDF20, "009999999999");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "32004000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(0x9F08, "0140");
//        emvParamAppUMSList[index].append(0x9F08, "000000000000");
////        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
////        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
//        //VISA
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("Visa Electron");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000032010");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
////        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
////        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
//        //VISA
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("VPAY");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000032020");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "10");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
////        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
////        emvParamAppUMSList[0].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
//        //VISA
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("VISA");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000031010");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999"); // not found in v200t
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "008D000000000000000000000000000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
//        // emvParamAppUMSList413].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
//        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6000F0A001");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B6000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
//
//        //VISA
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("Visa Electron");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.contactless;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A0000000032010");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSTransLimit_DF20, "000999999999"); // not found in v200t
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704"); // not found in v200t
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");// not found in v200t
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "008D000000000000000000000000000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "000000000000");
//        // emvParamAppUMSList513].append(EMVParamAppCaseA.TAG__9F5A	, "A0000000038010" );
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSCVMLimit_DF21, "000000030000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF04, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CTLSFloorLimit_DF19, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");
//        emvParamAppUMSList[index].append(0xDF1A, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6000F0A001");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "B6000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__DF18, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("Visa Credit Debit");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000003101001");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(0xDF1A, "000000012000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6F00F0F001");
//        emvParamAppUMSList[index].append(0xDF20, "009999999999");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "32004000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(0x9F08, "0140");
//        emvParamAppUMSList[index].append(0x9F08, "000000000000");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
//
//        emvParamAppUMSList[index] = new EMVParamAppCaseA();
//        emvParamAppUMSList[index].setComment("Visa Credit Debit");
//        emvParamAppUMSList[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamAppUMSList[index].aidType = ConstIPBOC.updateAID.aidType.smart_card;
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AID_9F06, "A000000003101002");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_ASI_DF01, "00");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F1C_TermIdent, "3132333435363738");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_VerNum_9F09, "0096");  // 	9F09	0030
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Default_DF11, "DC4000A800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Online_DF12, "DC4004F800");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TAC_Denial_DF13, "0010000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_FloorLimit_9F1B, "00000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_Threshold_DF15, "00000000");
//        emvParamAppUMSList[index].append(0xDF1A, "000000012000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_MaxTargetPercentage_DF16, "25");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_TargetPercentage_DF17, "01");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_DefaultDDOL_DF14, "9F3704");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermCap_9F33, "E0F8C8");  // 	9F33	03	E0F8C8
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CountryCodeTerm_9F1A, "0586");  // 	9F1A	02	0586 //new 0586
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTermAddCap_9F40, "6F00F0F001");
//        emvParamAppUMSList[index].append(0xDF20, "009999999999");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG__9F66, "32004000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_CurrencyCodeTerm_5F2A, "0586");
//        emvParamAppUMSList[index].append(0xDF1B, "000000000000");
//        emvParamAppUMSList[index].append(EMVParamAppCaseA.TAG_AppTerminalType_9F35, "22");
//        emvParamAppUMSList[index].append(0x9F08, "0140");
//        emvParamAppUMSList[index].append(0x9F08, "000000000000");
//        Utility.DEBUG_LOG(TAG, "Setting for: " + emvParamAppUMSList[index].getComment() + "; value: " + emvParamAppUMSList[index].getTlvString());
//        index++;
//
////
//        isSuccess = false;
//        try {
//            for (int i = 0; i < MAX_AID; i++) {
//                Utility.DEBUG_LOG(TAG,"current index");
//                isSuccess = ipboc.updateAID(emvParamAppUMSList[i].flagAppendRemoveClear, emvParamAppUMSList[i].aidType, emvParamAppUMSList[i].getTlvString());
//                Utility.DEBUG_LOG(TAG, "" + isSuccess);
//
//                if (isSuccess) {
//                    Utility.DEBUG_LOG(TAG, "updateAID success:" + emvParamAppUMSList[i].getComment());
//                } else {
//                    Utility.DEBUG_LOG(TAG, "updateAID false:" + emvParamAppUMSList[i].getComment());
//                }
//            }
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
////        // 2# way of setting the AID
////        // given the AID string to set. You can change the EMVParamAppCaseA to check each Tag & Value, and modify the tag or value if need
////        // hardcoding the AID string
////        String[] AID_SmartCard = {
////                // Visa credit or debit	10 10
////                "9F3303E0F8C8DF1A06000000012000" +
////                        "9F0607A0000000031010" +
////                        "9F40056F00F0F001DF010100DF2006009999999999DF14039F37049F660432004000DF1701015F2A020586DF1B060000000000009F350122DF1205FC60ACF8009F1B04000000009F1A020586DF2106009999999999DF160199DF150400049444DF1105FC6024A8009F08020140DF1906000000000000DF13050010000000",
////                // Visa credit or debit	10 10
////                "9F0607A0000000031010" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF120500000000009F1B0400000000DF160101DF150400000000DF1105C000000000DF19060000000000009F7B06000000000000DF13050000000000",
////                // Visa credit or debit	10 10
////                "9F0608A000000003101001" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
////                // Visa credit or debit	10 10
////                "9F0608A000000003101002" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
////                // Visa Electron	20 10
////                "9F0607A0000000032010" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
////                // MasterCard credit or debit	10 10
////                "9F0607A0000000041010" +
////                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000001000DF13050000400000",
////                // MasterCard(debit card)_	30 60
////                "9F0607A0000000043060" +
////                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000000000DF13050000400000",
////                // MasterCard
////                "9F0607A0000000044010" +
////                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000000000DF13050000400000",
////                // MasterCard
////                "9F0607A0000000046000" +
////                        "DF2006009999999999DF010100DF140111DF1701019F09020004DF180101DF1205F85080F8009F1B0400000000DF160101DF150400000000DF1105FC5080A000DF19060000000000009F7B06000000001000DF13050000400000",
////
////                /*   // PBOC
////                   "9F0608A000000333010103" +
////                           "DF2006009999999999DF010100DF14039F3704DF1701209F09020020DF180101DF1205DC4004F8009F1B0400000064DF160150DF150400000028DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
////                   "9F0608A000000333010101" +
////                           "DF2006009999999999DF010100DF14039F3704DF1701999F09020020DF180101DF1205DC4004F8009F1B0400000064DF160199DF150400000000DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
////                   "9F0608A000000333010106" +
////                           "DF2006009999999999DF010100DF14039F3704DF1701999F09020020DF180101DF1205DC4004F8009F1B0400000064DF160199DF150400000000DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
////                   "9F0608A000000333010102" +
////                           "DF2006009999999999DF010100DF14039F3704DF1701209F09020020DF180101DF1205DC4004F8009F1B0400000064DF160150DF150400000000DF1105DC4000A800DF19060000000000009F7B06000000100000DF13050010000000",
////              */     //
////                "9F0607A0000001523010" +
////                        "DF2006009999999999DF010100DF140111DF1701019F09020001DF180101DF1205D84004F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
////                // JCB
////                "9F0607A0000000651010" +
////                        "DF2006009999999999DF010100DF14039F3704DF1701019F09020001DF180101DF1205FC60ACF8009F1B0400000000DF160101DF150400000000DF1105FC60242800DF19060000000000009F7B06000000000000DF13050010000000",
////                // American Express
////                "9F0606F00000002501" +
////                        "DF2006009999999999DF010100DF14039F3704DF1701009F09020001DF180100DF1205CC000000009F1B0400000000DF160101DF150400000000DF1105CC00000000DF19060000000000009F7B06000000100000DF13050000000000",
////                "9F0606A00000002501" +
////                        "DF2006009999999999DF010100DF14039F3704DF1701999F09020020DF180101DF1205CC000080009F1B0400000000DF160199DF150400000000DF1105CC00000000DF19060000000000009F7B06000010000000DF13050000000000",
////                /* "9F0607A0000003241010" +
////
////                        "DF2006009999999999DF010100DF14039F3704DF1701999F09020001DF180101DF1205FCE09CF8009F1B0400000000DF160199DF150400000000DF1105DC00002000DF19060000000000009F7B06000000000000DF13050010000000",
////              */   "9F0605A000000000" +
////                "DF2006009999999999DF010100DF140111DF1701019F09020001DF180100DF120500000000009F1B0400000064DF160101DF150400000001DF11050000000000DF19060000000000009F7B06000000000100DF13050000000000",
////
////        };
////        String[] AID_CTLS_Card = {
////                "9F3303E0F8C8DF1A06000000012000" +
////                        "9F0607A0000000031010" +
////                        "9F40056F00F0F001DF010100DF2006009999999999DF14039F37049F660432004000DF1701015F2A020586DF1B060000000000009F350122DF1205FC60ACF8009F1B04000000009F1A020586DF2106009999999999DF160199DF150400049444DF1105FC6024A8009F08020140DF1906000000000000DF13050010000000",
////                // Visa credit or debit	10 10
////                "9F0607A0000000031010" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF120500000000009F1B0400000000DF160101DF150400000000DF1105C000000000DF19060000000000009F7B06000000000000DF13050000000000",
////                // Visa credit or debit	10 10
////                "9F0608A000000003101001" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
////                // Visa credit or debit	10 10
////                "9F0608A000000003101002" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
////                // Visa Electron	20 10
////                "9F0607A0000000032010" +
////                        "9F1A020586DF2006009999999999DF010100DF140111DF1701019F09020140DF180101DF1205D84000F8009F1B0400000000DF160101DF150400000000DF1105D84004A800DF19060000000000009F7B06000000000000DF13050010000000",
////
////// masood commented this on 1-9-2020
////
////                // Visa, Plus	80 10
//////                "9F0607A0000000038010" +
//////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601005F2A020586DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A020586DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
//////
//////                // Visa credit or debit	10 10
//////                "DF1A06000000012000" +
//////                        "9F0607A0000000031010" +
//////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
//////                // Visa credit or debit	10 10
//////                "DF1A06000000012000" +
//////                        "9F0608A000000003101001" +
//////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
//////                "DF1A06000000012000" +
//////
//////                        "9F0608A000000003101002" +
//////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
//////                // JCB
//////                "9F0607A0000000651010" +
//////                        "9F1A020586DF0306009999999999DF2006000999999999DF010100DF14039F3704DF170100DF180101DF1205FFFFFFFFFF9F1B04000020009F150400000000DF2106000000100000DF150400000000DF160100DF110590400080009F0802008CDF19060000001000009F7B06000000000000DF13050000000000",
//////                // MasterCard credit or debit	10 10
////                // + masood 01-09-2020 commented for testing
////                "9F0607A0000000041010" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
////
////                "9F0607A0000000043060" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
////                // MasterCard
////                "9F0607A0000000044010" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
////                // MasterCard
////                "9F0607A0000000046000" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020004DF180101DF1205A0109C98009F1B043B9ACA009F1A020586DF2106000000001000DF160101DF150400000000DF1105A4109C0000DF0406000000000000DF1906000000000000DF13055C40000000",
////                //
////                "9F0607A0000001523010" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6604260000805F2A020586DF1701019F09020001DF180101DF1205D84004F8009F1B04000000009F1A020586DF2106000000000100DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
////                /*  // PBOC
////                  "9F0608A000000333010102" +
////                          "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205DC4004F8009F1B04000000649F1A020586DF2106000000100000DF160199DF150400000000DF1105DC4000A800DF0406000000000000DF1906000000000000DF13050010000000",
////                  "9F0608A000000333010101" +
////                          "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205DC4004F8009F1B04000000649F1A020586DF2106000000100000DF160199DF150400000000DF1105DC4000A800DF0406000000000000DF1906000000000000DF13050010000000",
////                  "9F0608A000000333010106" +
////                          "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205DC4004F8009F1B04000000649F1A020586DF2106000000100000DF160199DF150400000000DF1105DC4000A800DF0406000000000000DF1906000000000000DF13050010000000",
////            */      // American Express
////                "9F0606F00000002501" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601225F2A020586DF1701009F09020001DF180100DF1205CC000000009F1B04000000009F1A020586DF2106000000100000DF160101DF150400000000DF1105CC00000000DF0406000000000501DF1906000000000000DF13050000000000",
////                "9F0606A00000002501" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020020DF180101DF1205C4000000009F1B04000000009F1A020586DF2106000010000000DF160199DF150400000000DF1105C400000000DF0406000000000000DF1906000000000000DF13050000000000",
////                "9F0607A0000003241010" +
////                        "DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1701999F09020001DF180101DF1205FCE09CF8009F1B04000000009F1A020586DF2106000000000000DF160199DF150400000000DF1105DC00002000DF0406000000000000DF1906000000000000DF13050010000000",
////                "DF1A06000000012000" +
////                        "9F0607A00000000" +
////                        "32010DF0306009999999999DF2006009999999999DF010100DF14039F37049F6601265F2A020586DF1B06000000000000DF1701019F09020140DF180101DF1205D84004F8009F1B04000000009F1A0205869F5A054001560156DF2106000000000000DF160101DF150400000000DF1105D84004A800DF0406000000000000DF1906000000000000DF13050010000000",
////
////        };
////        String[] aidList = AID_SmartCard;
////        int aidType = ConstIPBOC.updateAID.aidType.smart_card;
////        for (int i = 0; i < 2; i++) {
////            for (String aid : aidList) {
////                isSuccess = false;
////                if (aid.length() == 0) {
////                    continue;
////                }
////                try {
////                    EMVParamAppCaseA emvParamAppUMS = new EMVParamAppCaseA();
////
////                    emvParamAppUMS.setFlagAppendRemoveClear(ConstIPBOC.updateAID.operation.append);
////                    emvParamAppUMS.setAidType(aidType);
////                    emvParamAppUMS.append(aid);
////                    isSuccess = ipboc.updateAID(emvParamAppUMS.getFlagAppendRemoveClear(), emvParamAppUMS.getAidType(), emvParamAppUMS.getTlvString());
////                } catch (RemoteException e) {
////                    e.printStackTrace();
////                }
////                Utility.DEBUG_LOG(TAG, "" + isSuccess);
////
////                if (isSuccess) {
////                    Utility.DEBUG_LOG(TAG, "update AID success");
////                } else {
////                    Utility.DEBUG_LOG(TAG, "updateAID false");
////                }
////            }
////            aidList = AID_CTLS_Card;
////            aidType = ConstIPBOC.updateAID.aidType.contactless;
////        }
//
//    }

    /**
     * @brief set, update the RID
     * <p>
     * In this demo, there're 2 way to set the AID
     * 1#, set each tag & value
     * 2#, set one tlv string
     * in the EMVParamKeyCaseA.append, you can reset the Tag or Value
     * \code{.java}
     * \end code
     * @ version 1.0
     * @see EMVParamKeyCaseA
     */
    public void setRID(int type) {

        boolean isSuccess;
        if (type == 3) {
            // clear RID
            isSuccess = false;
            try {
                isSuccess = ipboc.updateRID(3, null);
                Utility.DEBUG_LOG(TAG, "Clear RID :" + isSuccess);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return;
        }

        final int MAX_RID = 150;
        int index = 0;

        // append RID
        // way 1#, set each tag & value
        EMVParamKeyCaseA[] emvParamKeyUMS = new EMVParamKeyCaseA[MAX_RID];


        //+ Shivam 08-04-2021 production keys
        if(Constants.keysType.equals("T")) {


            Utility.DEBUG_LOG(TAG,"Test keys");
            // + taha 26-02-2021 below are test UPI keys
            ////union pay rid
            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "union pay credit";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "0A");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B2AB1B6E9AC55A75ADFD5BBC34490E53C4C3381F34E60E7FAC21CC2B26DD34462B64A6FAE2495ED1DD383B8138BEA100FF9B7A111817E7B9869A9742B19E5C9DAC56F8B8827F11B05A08ECCF9E8D5E85B0F7CFA644EFF3E9B796688F38E006DEB21E101C01028903A06023AC5AAB8635F8E307A53AC742BDCE6A283F585F48EF");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "C88BE6B2417C4F941C9371EA35A377158767E4E3");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "union pay credit";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "08");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B61645EDFD5498FB246444037A0FA18C0F101EBD8EFA54573CE6E6A7FBF63ED21D66340852B0211CF5EEF6A1CD989F66AF21A8EB19DBD8DBC3706D135363A0D683D046304F5A836BC1BC632821AFE7A2F75DA3C50AC74C545A754562204137169663CFCC0B06E67E2109EBA41BC67FF20CC8AC80D7B6EE1A95465B3B2657533EA56D92D539E5064360EA4850FED2D1BF");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "EE23B616C95C02652AD18860E48787C079E8E85A");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "union pay credit";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "09");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "EB374DFC5A96B71D2863875EDA2EAFB96B1B439D3ECE0B1826A2672EEEFA7990286776F8BD989A15141A75C384DFC14FEF9243AAB32707659BE9E4797A247C2F0B6D99372F384AF62FE23BC54BCDC57A9ACD1D5585C303F201EF4E8B806AFB809DB1A3DB1CD112AC884F164A67B99C7D6E5A8A6DF1D3CAE6D7ED3D5BE725B2DE4ADE23FA679BF4EB15A93D8A6E29C7FFA1A70DE2E54F593D908A3BF9EBBD760BBFDC8DB8B54497E6C5BE0E4A4DAC29E5");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A075306EAB0045BAF72CDD33B3B678779DE1F527");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "PBOC credit or debit";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "0B");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "CF9FDF46B356378E9AF311B0F981B21A1F22F250FB11F55C958709E3C7241918293483289EAE688A094C02C344E2999F315A72841F489E24B1BA0056CFAB3B479D0E826452375DCDBB67E97EC2AA66F4601D774FEAEF775ACCC621BFEB65FB0053FC5F392AA5E1D4C41A4DE9FFDFDF1327C4BB874F1F63A599EE3902FE95E729FD78D4234DC7E6CF1ABABAA3F6DB29B7F05D1D901D2E76A606A8CBFFFFECBD918FA2D278BDB43B0434F5D45134BE1C2781D157D501FF43E5F1C470967CD57CE53B64D82974C8275937C5D8502A1252A8A5D6088A259B694F98648D9AF2CB0EFD9D943C69F896D49FA39702162ACB5AF29B90BADE005BC157");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "BD331F9996A490B33C13441066A09AD3FEB5F66C");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;
            // - taha 26-02-2021test UPI keys

            //+ taha 16-03-2021 below are test AMEX keys
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 1";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"10" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"CF98DFEDB3D3727965EE7797723355E0751C81D2D3DF4D18EBAB9FB9D49F38C8C4A826B99DC9DEA3F01043D4BF22AC3550E2962A59639B1332156422F788B9C16D40135EFD1BA94147750575E636B6EBC618734C91C1D1BF3EDC2A46A43901668E0FFC136774080E888044F6A1E65DC9AAA8928DACBEB0DB55EA3514686C6A732CEF55EE27CF877F110652694A0E3484C855D882AE191674E25C296205BBB599455176FDD7BBC549F27BA5FE35336F7E29E68D783973199436633C67EE5A680F05160ED12D1665EC83D1997F10FD05BBDBF9433E8F797AEE3E9F02A34228ACE927ABE62B8B9281AD08D3DF5C7379685045D7BA5FCDE58637");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"C729CF2FD262394ABC4CC173506502446AA9B9FD");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 2";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"0E" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323137");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"AA94A8C6DAD24F9BA56A27C09B01020819568B81A026BE9FD0A3416CA9A71166ED5084ED91CED47DD457DB7E6CBCD53E560BC5DF48ABC380993B6D549F5196CFA77DFB20A0296188E969A2772E8C4141665F8BB2516BA2C7B5FC91F8DA04E8D512EB0F6411516FB86FC021CE7E969DA94D33937909A53A57F907C40C22009DA7532CB3BE509AE173B39AD6A01BA5BB85");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"A7266ABAE64B42A3668851191D49856E17F8FBCD");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 3";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"0F" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"C8D5AC27A5E1FB89978C7C6479AF993AB3800EB243996FBB2AE26B67B23AC482C4B746005A51AFA7D2D83E894F591A2357B30F85B85627FF15DA12290F70F05766552BA11AD34B7109FA49DE29DCB0109670875A17EA95549E92347B948AA1F045756DE56B707E3863E59A6CBE99C1272EF65FB66CBB4CFF070F36029DD76218B21242645B51CA752AF37E70BE1A84FF31079DC0048E928883EC4FADD497A719385C2BBBEBC5A66AA5E5655D18034EC5");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"A73472B3AB557493A9BC2179CC8014053B12BAB4");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 1";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"10" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"CF98DFEDB3D3727965EE7797723355E0751C81D2D3DF4D18EBAB9FB9D49F38C8C4A826B99DC9DEA3F01043D4BF22AC3550E2962A59639B1332156422F788B9C16D40135EFD1BA94147750575E636B6EBC618734C91C1D1BF3EDC2A46A43901668E0FFC136774080E888044F6A1E65DC9AAA8928DACBEB0DB55EA3514686C6A732CEF55EE27CF877F110652694A0E3484C855D882AE191674E25C296205BBB599455176FDD7BBC549F27BA5FE35336F7E29E68D783973199436633C67EE5A680F05160ED12D1665EC83D1997F10FD05BBDBF9433E8F797AEE3E9F02A34228ACE927ABE62B8B9281AD08D3DF5C7379685045D7BA5FCDE58637");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"C729CF2FD262394ABC4CC173506502446AA9B9FD");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 2";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"0E" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"AA94A8C6DAD24F9BA56A27C09B01020819568B81A026BE9FD0A3416CA9A71166ED5084ED91CED47DD457DB7E6CBCD53E560BC5DF48ABC380993B6D549F5196CFA77DFB20A0296188E969A2772E8C4141665F8BB2516BA2C7B5FC91F8DA04E8D512EB0F6411516FB86FC021CE7E969DA94D33937909A53A57F907C40C22009DA7532CB3BE509AE173B39AD6A01BA5BB85");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"A7266ABAE64B42A3668851191D49856E17F8FBCD");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 3";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"0F" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"C8D5AC27A5E1FB89978C7C6479AF993AB3800EB243996FBB2AE26B67B23AC482C4B746005A51AFA7D2D83E894F591A2357B30F85B85627FF15DA12290F70F05766552BA11AD34B7109FA49DE29DCB0109670875A17EA95549E92347B948AA1F045756DE56B707E3863E59A6CBE99C1272EF65FB66CBB4CFF070F36029DD76218B21242645B51CA752AF37E70BE1A84FF31079DC0048E928883EC4FADD497A719385C2BBBEBC5A66AA5E5655D18034EC5");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"A73472B3AB557493A9BC2179CC8014053B12BAB4");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 4";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"01" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"AFAD7010F884E2824650F764D47D7951A16EED6DBB881F384DEDB6702E0FB55C0FBEF945A2017705E5286FA249A591E194BDCD74B21720B44CE986F144237A25F95789F38B47EA957F9ADB2372F6D5D41340A147EAC2AF324E8358AE1120EF3F");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"ACFE734CF09A84C7BF025F0FFC6FA8CA25A22869");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 5";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"02" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"AF4B8D230FDFCB1538E975795A1DB40C396A5359FAA31AE095CB522A5C82E7FFFB252860EC2833EC3D4A665F133DD934EE1148D81E2B7E03F92995DDF7EB7C90A75AB98E69C92EC91A533B21E1C4918B43AFED5780DE13A32BBD37EBC384FA3DD1A453E327C56024DACAEA74AA052C4D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"33F5B0344943048237EC89B275A95569718AEE20");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 6";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"03" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"B0C2C6E2A6386933CD17C239496BF48C57E389164F2A96BFF133439AE8A77B20498BD4DC6959AB0C2D05D0723AF3668901937B674E5A2FA92DDD5E78EA9D75D79620173CC269B35F463B3D4AAFF2794F92E6C7A3FB95325D8AB95960C3066BE548087BCB6CE12688144A8B4A66228AE4659C634C99E36011584C095082A3A3E3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"8708A3E3BBC1BB0BE73EBD8D19D4E5D20166BF6C");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 7";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"04" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"D0F543F03F2517133EF2BA4A1104486758630DCFE3A883C77B4E4844E39A9BD6360D23E6644E1E071F196DDF2E4A68B4A3D93D14268D7240F6A14F0D714C17827D279D192E88931AF7300727AE9DA80A3F0E366AEBA61778171737989E1EE309");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"FDD7139EC7E0C33167FD61AD3CADBD68D66E91C5");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 8";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"64" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"B0DD551047DAFCD10D9A5E33CF47A9333E3B24EC57E8F066A72DED60E881A8AD42777C67ADDF0708042AB943601EE60248540B67E0637018EEB3911AE9C873DAD66CB40BC8F4DC77EB2595252B61C21518F79B706AAC29E7D3FD4D259DB72B6E6D446DD60386DB40F5FDB076D80374C993B4BB2D1DB977C3870897F9DFA454F5");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"792B121D86D0F3A99582DB06974481F3B2E18454");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 9";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"65" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"E53EB41F839DDFB474F272CD0CBE373D5468EB3F50F39C95BDF4D39FA82B98DABC9476B6EA350C0DCE1CD92075D8C44D1E57283190F96B3537D9E632C461815EBD2BAF36891DF6BFB1D30FA0B752C43DCA0257D35DFF4CCFC98F84198D5152EC61D7B5F74BD09383BD0E2AA42298FFB02F0D79ADB70D72243EE537F75536A8A8DF962582E9E6812F3A0BE02A4365400D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"894C5D08D4EA28BB79DC46CEAD998B877322F416");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 10";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"66" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"BD1478877B9333612D257D9E3C9C23503E28336B723C71F47C25836670395360F53C106FD74DEEEA291259C001AFBE7B4A83654F6E2D9E8148E2CB1D9223AC5903DA18B433F8E3529227505DE84748F241F7BFCD2146E5E9A8C5D2A06D19097087A069F9AE3D610C7C8E1214481A4F27025A1A2EDB8A9CDAFA445690511DB805");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"F367CB70F9C9B67B580F533819E302BAC0330090");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 11";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"67" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"C687ADCCF3D57D3360B174E471EDA693AA555DFDC6C8CD394C74BA25CCDF8EABFD1F1CEADFBE2280C9E81F7A058998DC22B7F22576FE84713D0BDD3D34CFCD12FCD0D26901BA74103D075C664DABCCAF57BF789494051C5EC303A2E1D784306D3DB3EB665CD360A558F40B7C05C919B2F0282FE1ED9BF6261AA814648FBC263B14214491DE426D242D65CD1FFF0FBE4D4DAFF5CFACB2ADC7131C9B147EE791956551076270696B75FD97373F1FD7804F");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"52A2907300C8445BF54B970C894691FEADF2D28E");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 12";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"68" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"F4D198F2F0CF140E4D2D81B765EB4E24CED4C0834822769854D0E97E8066CBE465029B3F410E350F6296381A253BE71A4BBABBD516625DAE67D073D00113AAB9EA4DCECA29F3BB7A5D46C0D8B983E2482C2AD759735A5AB9AAAEFB31D3E718B8CA66C019ECA0A8BE312E243EB47A62300620BD51CF169A9194C17A42E51B34D83775A98E80B2D66F4F98084A448FE0507EA27C905AEE72B62A8A29438B6A4480FFF72F93280432A55FDD648AD93D82B9ECF01275C0914BAD8EB3AAF46B129F8749FEA425A2DCDD7E813A08FC0CA7841EDD49985CD8BC6D5D56F17AB9C67CEC50BA422440563ECCE21699E435C8682B6266393672C693D8B7");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"415E5FE9EC966C835FBB3E6F766A9B1A4B8674C3");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"97" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"E178FFE834B4B767AF3C9A511F973D8E8505C5FCB2D3768075AB7CC946A955789955879AAF737407151521996DFA43C58E6B130EB1D863B85DC9FFB4050947A2676AA6A061A4A7AE1EDB0E36A697E87E037517EB8923136875BA2CA1087CBA7EC7653E5E28A0C261A033AF27E3A67B64BBA26956307EC47E674E3F8B722B3AE0498DB16C7985310D9F3D117300D32B09");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"EBDA522B631B3EB4F4CBFC0679C450139D2B69CD");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 14";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"98" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"D31A7094FB221CBA6660FB975AAFEA80DB7BB7EAFD7351E748827AB62D4AEECCFC1787FD47A04699A02DB00D7C382E80E804B35C59434C602389D691B9CCD51ED06BE67A276119C4C10E2E40FC4EDDF9DF39B9B0BDEE8D076E2A012E8A292AF8EFE18553470639C1A032252E0E5748B25A3F9BA4CFCEE073038B061837F2AC1B04C279640F5BD110A9DC665ED2FA6828BD5D0FE810A892DEE6B0E74CE8863BDE08FD5FD61A0F11FA0D14978D8CED7DD3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"D4DBA428CF11D45BAEB0A35CAEA8007AD8BA8D71");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 15";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"99" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"E1740074229FA0D228A9623581D7A322903FB89BA7686712E601FA8AB24A9789186F15B70CCBBE7421B1CB110D45361688135FFD0DB15A3F516BB291D4A123EBF5A06FBF7E1EE6311B737DABB289570A7959D532B25F1DA6758C84DDCCADC049BC764C05391ABD2CADEFFA7E242D5DD06E56001F0E68151E3388074BD9330D6AFA57CBF33946F531E51E0D4902EE235C756A905FB733940E6EC897B4944A5EDC765705E2ACF76C78EAD78DD9B066DF0B2C88750B8AEE00C9B4D4091FA7338449DA92DBFC908FA0781C0128C492DB993C88BA8BB7CADFE238D477F2517E0E7E3D2B11796A0318CE2AD4DA1DB8E54AB0D94F109DB9CAEEFBEF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"F0885777642C96BB24441FA057AD9A3490763BD2");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 16";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"C1" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"E69E319C34D1B4FB43AED4BD8BBA6F7A8B763F2F6EE5DDF7C92579A984F89C4A9C15B27037764C58AC7E45EFBC34E138E56BA38F76E803129A8DDEB5E1CC8C6B30CF634A9C9C1224BF1F0A9A18D79ED41EBCF1BE78087AE8B7D2F896B1DE8B7E784161A138A0F2169AD33E146D1B16AB595F9D7D98BE671062D217F44EB68C68640C7D57465A063F6BAC776D3E2DAC61");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"DC79D6B5FC879362299BC5A637DAD2E0D99656B8");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 17";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"C2" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"B875002F38BA26D61167C5D440367604AD38DF2E93D8EE8DA0E8D9C0CF4CC5788D11DEA689E5F41D23A3DA3E0B1FA5875AE25620F5A6BCCEE098C1B35C691889D7D0EF670EB8312E7123FCC5DC7D2F0719CC80E1A93017F944D097330EDF945762FEE62B7B0BA0348228DBF38D4216E5A67A7EF74F5D3111C44AA31320F623CB3C53E60966D6920067C9E082B746117E48E4F00E110950CA54DA3E38E5453BD5544E3A6760E3A6A42766AD2284E0C9AF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"8E748296359A7428F536ADDA8E2C037E2B697EF6");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 18";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"C3" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"B93182ABE343DFBF388C71C4D6747DCDEC60367FE63CFAA942D7D323E688D0832836548BF0EDFF1EDEEB882C75099FF81A93FA525C32425B36023EA02A8899B9BF7D7934E86F997891823006CEAA93091A73C1FDE18ABD4F87A22308640C064C8C027685F1B2DB7B741B67AB0DE05E870481C5F972508C17F57E4F833D63220F6EA2CFBB878728AA5887DE407D10C6B8F58D46779ECEC1E2155487D52C78A5C03897F2BB580E0A2BBDE8EA2E1C18F6AAF3EB3D04C3477DEAB88F150C8810FD1EF8EB0596866336FE2C1FBC6BEC22B4FE5D885647726DB59709A505F75C49E0D8D71BF51E4181212BE2142AB2A1E8C0D3B7136CD7B7708E4D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"12F1790CB0273DC73C6E70784BC24C12E8DB71F6");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 19";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"C7" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"CD237E34E0299DE48F1A2C94F478FE972896011E1CA6AB462B68FE0F6109C9A97C2DBEEA65932CDE0625138B9F162B92979DAAB019D3B5561D31EB2D4F09F12F927EA8F740CE0E87154965505E2272F69042B15D57CCC7F771919123978283B3CCE524D9715207BF5F5AD369102176F0F7A78A6DEB2BFF0EDCE165F3B14F14D0035B2756861FE03C43396ED002C894A3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"6221E0C726BAC8F8AC25F8F93B811D1FFD4C131C");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "AMEX capk test 20";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"C8" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"BF0CFCED708FB6B048E3014336EA24AA007D7967B8AA4E613D26D015C4FE7805D9DB131CED0D2A8ED504C3B5CCD48C33199E5A5BF644DA043B54DBF60276F05B1750FAB39098C7511D04BABC649482DDCF7CC42C8C435BAB8DD0EB1A620C31111D1AAAF9AF6571EEBD4CF5A08496D57E7ABDBB5180E0A42DA869AB95FB620EFF2641C3702AF3BE0B0C138EAEF202E21D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"33BD7A059FAB094939B90A8F35845C9DC779BD50");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "AMEX capk test 21";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "C9");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230333131323233");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B362DB5733C15B8797B8ECEE55CB1A371F760E0BEDD3715BB270424FD4EA26062C38C3F4AAA3732A83D36EA8E9602F6683EECC6BAFF63DD2D49014BDE4D6D603CD744206B05B4BAD0C64C63AB3976B5C8CAAF8539549F5921C0B700D5B0F83C4E7E946068BAAAB5463544DB18C63801118F2182EFCC8A1E85E53C2A7AE839A5C6A3CABE73762B70D170AB64AFC6CA482944902611FB0061E09A67ACB77E493D998A0CCF93D81A4F6C0DC6B7DF22E62DB");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "8E8DFF443D78CD91DE88821D70C98F0638E51E49");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "AMEX capk test 22";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "CA");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230333131323233");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323531323030");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "C23ECBD7119F479C2EE546C123A585D697A7D10B55C2D28BEF0D299C01DC65420A03FE5227ECDECB8025FBC86EEBC1935298C1753AB849936749719591758C315FA150400789BB14FADD6EAE2AD617DA38163199D1BAD5D3F8F6A7A20AEF420ADFE2404D30B219359C6A4952565CCCA6F11EC5BE564B49B0EA5BF5B3DC8C5C6401208D0029C3957A8C5922CBDE39D3A564C6DEBB6BD2AEF91FC27BB3D3892BEB9646DCE2E1EF8581EFFA712158AAEC541C0BBB4B3E279D7DA54E45A0ACC3570E712C9F7CDF985CFAFD382AE13A3B214A9E8E1E71AB1EA707895112ABC3A97D0FCB0AE2EE5C85492B6CFD54885CDD6337E895CC70FB3255E3");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "6BDA32B1AA171444C7E8F88075A74FBFE845765F");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            //- taha 16-03-2021 below are test AMEX keys

//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "union pay credit";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22,"0B" );
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05 ,"3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07,"01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02,"CF9FDF46B356378E9AF311B0F981B21A1F22F250FB11F55C958709E3C7241918293483289EAE688A094C02C344E2999F315A72841F489E24B1BA0056CFAB3B479D0E826452375DCDBB67E97EC2AA66F4601D774FEAEF775ACCC621BFEB65FB0053FC5F392AA5E1D4C41A4DE9FFDFDF1327C4BB874F1F63A599EE3902FE95E729FD78D4234DC7E6CF1ABABAA3F6DB29B7F05D1D901D2E76A606A8CBFFFFECBD918FA2D278BDB43B0434F5D45134BE1C2781D157D501FF43E5F1C470967CD57CE53B64D82974C8275937C5D8502A1252A8A5D6088A259B694F98648D9AF2CB0EFD9D943C69F896D49FA39702162ACB5AF29B90BADE005BC157");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04,"03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03,"BD331F9996A490B33C13441066A09AD3FEB5F66C");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;

//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "C3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B93182ABE343DFBF388C71C4D6747DCDEC60367FE63CFAA942D7D323E688D0832836548BF0EDFF1EDEEB882C75099FF81A93FA525C32425B36023EA02A8899B9BF7D7934E86F997891823006CEAA93091A73C1FDE18ABD4F87A22308640C064C8C027685F1B2DB7B741B67AB0DE05E870481C5F972508C17F57E4F833D63220F6EA2CFBB878728AA5887DE407D10C6B8F58D46779ECEC1E2155487D52C78A5C03897F2BB580E0A2BBDE8EA2E1C18F6AAF3EB3D04C3477DEAB88F150C8810FD1EF8EB0596866336FE2C1FBC6BEC22B4FE5D885647726DB59709A505F75C49E0D8D71BF51E4181212BE2142AB2A1E8C0D3B7136CD7B7708E4D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "12F1790CB0273DC73C6E70784BC24C12E8DB71F6");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "C7");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "CD237E34E0299DE48F1A2C94F478FE972896011E1CA6AB462B68FE0F6109C9A97C2DBEEA65932CDE0625138B9F162B92979DAAB019D3B5561D31EB2D4F09F12F927EA8F740CE0E87154965505E2272F69042B15D57CCC7F771919123978283B3CCE524D9715207BF5F5AD369102176F0F7A78A6DEB2BFF0EDCE165F3B14F14D0035B2756861FE03C43396ED002C894A3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "6221E0C726BAC8F8AC25F8F93B811D1FFD4C131C");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "C8");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "BF0CFCED708FB6B048E3014336EA24AA007D7967B8AA4E613D26D015C4FE7805D9DB131CED0D2A8ED504C3B5CCD48C33199E5A5BF644DA043B54DBF60276F05B1750FAB39098C7511D04BABC649482DDCF7CC42C8C435BAB8DD0EB1A620C31111D1AAAF9AF6571EEBD4CF5A08496D57E7ABDBB5180E0A42DA869AB95FB620EFF2641C3702AF3BE0B0C138EAEF202E21D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "33BD7A059FAB094939B90A8F35845C9DC779BD50");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "C9");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B362DB5733C15B8797B8ECEE55CB1A371F760E0BEDD3715BB270424FD4EA26062C38C3F4AAA3732A83D36EA8E9602F6683EECC6BAFF63DD2D49014BDE4D6D603CD744206B05B4BAD0C64C63AB3976B5C8CAAF8539549F5921C0B700D5B0F83C4E7E946068BAAAB5463544DB18C63801118F2182EFCC8A1E85E53C2A7AE839A5C6A3CABE73762B70D170AB64AFC6CA482944902611FB0061E09A67ACB77E493D998A0CCF93D81A4F6C0DC6B7DF22E62DB");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "8E8DFF443D78CD91DE88821D70C98F0638E51E49");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//

//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "CA");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "C23ECBD7119F479C2EE546C123A585D697A7D10B55C2D28BEF0D299C01DC65420A03FE5227ECDECB8025FBC86EEBC1935298C1753AB849936749719591758C315FA150400789BB14FADD6EAE2AD617DA38163199D1BAD5D3F8F6A7A20AEF420ADFE2404D30B219359C6A4952565CCCA6F11EC5BE564B49B0EA5BF5B3DC8C5C6401208D0029C3957A8C5922CBDE39D3A564C6DEBB6BD2AEF91FC27BB3D3892BEB9646DCE2E1EF8581EFFA712158AAEC541C0BBB4B3E279D7DA54E45A0ACC3570E712C9F7CDF985CFAFD382AE13A3B214A9E8E1E71AB1EA707895112ABC3A97D0FCB0AE2EE5C85492B6CFD54885CDD6337E895CC70FB3255E3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "6BDA32B1AA171444C7E8F88075A74FBFE845765F");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;

//        emvParamKeyUMS[index] = new EMVParamKeyCaseA(); //fails...
//        emvParamKeyUMS[index].comment = "master card";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B855CC64313AF99C453D181642EE7DD21A67D0FF50C61FE213BCDC18AFBCD07722EFDD2594EFDC227DA3DA23ADCC90E3FA907453ACC954C47323BEDCF8D4862C457D25F47B16D7C3502BE081913E5B0482D838484065DA5F6659E00A9E5D570ADA1EC6AF8C57960075119581FC81468D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "8BB99ADDF7B560110955014505FB6B5F8308CE27");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 2"; //fails
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "07");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B8DAB883EF1B5763E4F861F9EA3044B276635D402F3AE5E6B6C0547E368E79A36366DAC5609B6EC486DA1A8D2002CA4F4EFC2CB0EC1573A0B0917969EB60645BAEDF11C050C5D07FED817D11E84A174859A0DAE7F7935F109229C0AC4EE5BFB3D65533A679F0486C5AEFCC937379833BEC45D79DCF97B5228B1910FA03765331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "4896DD5A9EB5B11B9DCCA6DFE336C009F69F509A");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 3";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "F3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "98F0C770F23864C2E766DF02D1E833DFF4FFE92D696E1642F0A88C5694C6479D16DB1537BFE29E4FDC6E6E8AFD1B0EB7EA0124723C333179BF19E93F10658B2F776E829E87DAEDA9C94A8B3382199A350C077977C97AFF08FD11310AC950A72C3CA5002EF513FCCC286E646E3C5387535D509514B3B326E1234F9CB48C36DDD44B416D23654034A66F403BA511C5EFA3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A69AC7603DAF566E972DEDC2CB433E07E8B01A9A");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 4";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "F8");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A1F5E1C9BD8650BD43AB6EE56B891EF7459C0A24FA84F9127D1A6C79D4930F6DB1852E2510F18B61CD354DB83A356BD190B88AB8DF04284D02A4204A7B6CB7C5551977A9B36379CA3DE1A08E69F301C95CC1C20506959275F41723DD5D2925290579E5A95B0DF6323FC8E9273D6F849198C4996209166D9BFC973C361CC826E1");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "F06ECC6D2AAEBF259B7E755A38D9A9B24E2FF3DD");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA(); //fails...
//        emvParamKeyUMS[index].comment = "master card 5";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FB");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A9548DFB398B48123FAF41E6CFA4AE1E2352B518AB4BCEFECDB0B3EDEC090287D88B12259F361C1CC088E5F066494417E8EE8BBF8991E2B32FF16F994697842B3D6CB37A2BB5742A440B6356C62AA33DB3C455E59EDDF7864701D03A5B83EE9E9BD83AB93302AC2DFE63E66120B051CF081F56326A71303D952BB336FF12610D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "02");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "6C7289632919ABEE6E1163D7E6BF693FD88EBD35");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA(); //fails...
//        emvParamKeyUMS[index].comment = "master card 6";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FC");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B37BFD2A9674AD6221C1A001081C62653DC280B0A9BD052C677C913CE7A0D902E77B12F4D4D79037B1E9B923A8BB3FAC3C612045BB3914F8DF41E9A1B61BFA5B41705A691D09CE6F530FE48B30240D98F4E692FFD6AADB87243BA8597AB237586ECF258F4148751BE5DA5A3BE6CC34BD");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "02");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "7FB377EEBBCF7E3A6D04015D10E1BDCB15E21B80");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA(); //fails
//        emvParamKeyUMS[index].comment = "master card 7";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FD");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B3572BA49AE4C7B7A0019E5189E142CFCDED9498DDB5F0470567AB0BA713B8DA226424622955B54B937ABFEFAAD97919E377621E22196ABC1419D5ADC123484209EA7CB7029E66A0D54C5B45C8AD615AEDB6AE9E0A2F75310EA8961287241245");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "02");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "23CF0D702E0AEFE518E4FA6B836D3CD45B8AAA71");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 8";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FE");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A653EAC1C0F786C8724F737F172997D63D1C3251C44402049B865BAE877D0F398CBFBE8A6035E24AFA086BEFDE9351E54B95708EE672F0968BCD50DCE40F783322B2ABA04EF137EF18ABF03C7DBC5813AEAEF3AA7797BA15DF7D5BA1CBAF7FD520B5A482D8D3FEE105077871113E23A49AF3926554A70FE10ED728CF793B62A1");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "9A295B05FB390EF7923F57618A9FDA2941FC34E0");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 9";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B855CC64313AF99C453D181642EE7DD21A67D0FF50C61FE213BCDC18AFBCD07722EFDD2594EFDC227DA3DA23ADCC90E3FA907453ACC954C47323BEDCF8D4862C457D25F47B16D7C3502BE081913E5B0482D838484065DA5F6659E00A9E5D570ADA1EC6AF8C57960075119581FC81468D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "B4E769CECF7AAC4783F305E0B110602A07A6355B");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 10";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "05");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B8048ABC30C90D976336543E3FD7091C8FE4800DF820ED55E7E94813ED00555B573FECA3D84AF6131A651D66CFF4284FB13B635EDD0EE40176D8BF04B7FD1C7BACF9AC7327DFAA8AA72D10DB3B8E70B2DDD811CB4196525EA386ACC33C0D9D4575916469C4E4F53E8E1C912CC618CB22DDE7C3568E90022E6BBA770202E4522A2DD623D180E215BD1D1507FE3DC90CA310D27B3EFCCD8F83DE3052CAD1E48938C68D095AAC91B5F37E28BB49EC7ED597");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "EBFA0D5D06D8CE702DA3EAE890701D45E274C845");
//        //emvParamKeyUMS[index].append(0xBF01,"31");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 11";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "F1");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A0DCF4BDE19C3546B4B6F0414D174DDE294AABBB828C5A834D73AAE27C99B0B053A90278007239B6459FF0BBCD7B4B9C6C50AC02CE91368DA1BD21AAEADBC65347337D89B68F5C99A09D05BE02DD1F8C5BA20E2F13FB2A27C41D3F85CAD5CF6668E75851EC66EDBF98851FD4E42C44C1D59F5984703B27D5B9F21B8FA0D93279FBBF69E090642909C9EA27F898959541AA6757F5F624104F6E1D3A9532F2A6E51515AEAD1B43B3D7835088A2FAFA7BE7");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "D8E68DA167AB5A85D8C3D55ECB9B0517A1A5B4BB");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 12";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "04");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A6DA428387A502D7DDFB7A74D3F412BE762627197B25435B7A81716A700157DDD06F7CC99D6CA28C2470527E2C03616B9C59217357C2674F583B3BA5C7DCF2838692D023E3562420B4615C439CA97C44DC9A249CFCE7B3BFB22F68228C3AF13329AA4A613CF8DD853502373D62E49AB256D2BC17120E54AEDCED6D96A4287ACC5C04677D4A5A320DB8BEE2F775E5FEC5");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "381A035DA58B482EE2AF75F4C3F2CA469BA4AA6C");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "EF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A191CB87473F29349B5D60A88B3EAEE0973AA6F1A082F358D849FDDFF9C091F899EDA9792CAF09EF28F5D22404B88A2293EEBBC1949C43BEA4D60CFD879A1539544E09E0F09F60F065B2BF2A13ECC705F3D468B9D33AE77AD9D3F19CA40F23DCF5EB7C04DC8F69EBA565B1EBCB4686CD274785530FF6F6E9EE43AA43FDB02CE00DAEC15C7B8FD6A9B394BABA419D3F6DC85E16569BE8E76989688EFEA2DF22FF7D35C043338DEAA982A02B866DE5328519EBBCD6F03CDD686673847F84DB651AB86C28CF1462562C577B853564A290C8556D818531268D25CC98A4CC6A0BDFFFDA2DCCA3A94C998559E307FDDF915006D9A987B07DDAEB3B");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "21766EBB0EE122AFB65D7845B73DB46BAB65427A");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());


            //Shivam added RID keys 29 January 2021


//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "04");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D0F543F03F2517133EF2BA4A1104486758630DCFE3A883C77B4E4844E39A9BD6360D23E6644E1E071F196DDF2E4A68B4A3D93D14268D7240F6A14F0D714C17827D279D192E88931AF7300727AE9DA80A3F0E366AEBA61778171737989E1EE309");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "FDD7139EC7E0C33167FD61AD3CADBD68D66E91C5");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "64");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B0DD551047DAFCD10D9A5E33CF47A9333E3B24EC57E8F066A72DED60E881A8AD42777C67ADDF0708042AB943601EE60248540B67E0637018EEB3911AE9C873DAD66CB40BC8F4DC77EB2595252B61C21518F79B706AAC29E7D3FD4D259DB72B6E6D446DD60386DB40F5FDB076D80374C993B4BB2D1DB977C3870897F9DFA454F5");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "792B121D86D0F3A99582DB06974481F3B2E18454");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "65");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "E53EB41F839DDFB474F272CD0CBE373D5468EB3F50F39C95BDF4D39FA82B98DABC9476B6EA350C0DCE1CD92075D8C44D1E57283190F96B3537D9E632C461815EBD2BAF36891DF6BFB1D30FA0B752C43DCA0257D35DFF4CCFC98F84198D5152EC61D7B5F74BD09383BD0E2AA42298FFB02F0D79ADB70D72243EE537F75536A8A8DF962582E9E6812F3A0BE02A4365400D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "894C5D08D4EA28BB79DC46CEAD998B877322F416");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "66");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "BD1478877B9333612D257D9E3C9C23503E28336B723C71F47C25836670395360F53C106FD74DEEEA291259C001AFBE7B4A83654F6E2D9E8148E2CB1D9223AC5903DA18B433F8E3529227505DE84748F241F7BFCD2146E5E9A8C5D2A06D19097087A069F9AE3D610C7C8E1214481A4F27025A1A2EDB8A9CDAFA445690511DB805");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "F367CB70F9C9B67B580F533819E302BAC0330090");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "67");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "C687ADCCF3D57D3360B174E471EDA693AA555DFDC6C8CD394C74BA25CCDF8EABFD1F1CEADFBE2280C9E81F7A058998DC22B7F22576FE84713D0BDD3D34CFCD12FCD0D26901BA74103D075C664DABCCAF57BF789494051C5EC303A2E1D784306D3DB3EB665CD360A558F40B7C05C919B2F0282FE1ED9BF6261AA814648FBC263B14214491DE426D242D65CD1FFF0FBE4D4DAFF5CFACB2ADC7131C9B147EE791956551076270696B75FD97373F1FD7804F");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "52A2907300C8445BF54B970C894691FEADF2D28E");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "68");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "F4D198F2F0CF140E4D2D81B765EB4E24CED4C0834822769854D0E97E8066CBE465029B3F410E350F6296381A253BE71A4BBABBD516625DAE67D073D00113AAB9EA4DCECA29F3BB7A5D46C0D8B983E2482C2AD759735A5AB9AAAEFB31D3E718B8CA66C019ECA0A8BE312E243EB47A62300620BD51CF169A9194C17A42E51B34D83775A98E80B2D66F4F98084A448FE0507EA27C905AEE72B62A8A29438B6A4480FFF72F93280432A55FDD648AD93D82B9ECF01275C0914BAD8EB3AAF46B129F8749FEA425A2DCDD7E813A08FC0CA7841EDD49985CD8BC6D5D56F17AB9C67CEC50BA422440563ECCE21699E435C8682B6266393672C693D8B7");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "415E5FE9EC966C835FBB3E6F766A9B1A4B8674C3");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "97");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "E178FFE834B4B767AF3C9A511F973D8E8505C5FCB2D3768075AB7CC946A955789955879AAF737407151521996DFA43C58E6B130EB1D863B85DC9FFB4050947A2676AA6A061A4A7AE1EDB0E36A697E87E037517EB8923136875BA2CA1087CBA7EC7653E5E28A0C261A033AF27E3A67B64BBA26956307EC47E674E3F8B722B3AE0498DB16C7985310D9F3D117300D32B09");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "EBDA522B631B3EB4F4CBFC0679C450139D2B69CD");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "98");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D31A7094FB221CBA6660FB975AAFEA80DB7BB7EAFD7351E748827AB62D4AEECCFC1787FD47A04699A02DB00D7C382E80E804B35C59434C602389D691B9CCD51ED06BE67A276119C4C10E2E40FC4EDDF9DF39B9B0BDEE8D076E2A012E8A292AF8EFE18553470639C1A032252E0E5748B25A3F9BA4CFCEE073038B061837F2AC1B04C279640F5BD110A9DC665ED2FA6828BD5D0FE810A892DEE6B0E74CE8863BDE08FD5FD61A0F11FA0D14978D8CED7DD3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "D4DBA428CF11D45BAEB0A35CAEA8007AD8BA8D71");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "99");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "E1740074229FA0D228A9623581D7A322903FB89BA7686712E601FA8AB24A9789186F15B70CCBBE7421B1CB110D45361688135FFD0DB15A3F516BB291D4A123EBF5A06FBF7E1EE6311B737DABB289570A7959D532B25F1DA6758C84DDCCADC049BC764C05391ABD2CADEFFA7E242D5DD06E56001F0E68151E3388074BD9330D6AFA57CBF33946F531E51E0D4902EE235C756A905FB733940E6EC897B4944A5EDC765705E2ACF76C78EAD78DD9B066DF0B2C88750B8AEE00C9B4D4091FA7338449DA92DBFC908FA0781C0128C492DB993C88BA8BB7CADFE238D477F2517E0E7E3D2B11796A0318CE2AD4DA1DB8E54AB0D94F109DB9CAEEFBEF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "F0885777642C96BB24441FA057AD9A3490763BD2");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "C1");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "E69E319C34D1B4FB43AED4BD8BBA6F7A8B763F2F6EE5DDF7C92579A984F89C4A9C15B27037764C58AC7E45EFBC34E138E56BA38F76E803129A8DDEB5E1CC8C6B30CF634A9C9C1224BF1F0A9A18D79ED41EBCF1BE78087AE8B7D2F896B1DE8B7E784161A138A0F2169AD33E146D1B16AB595F9D7D98BE671062D217F44EB68C68640C7D57465A063F6BAC776D3E2DAC61");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "DC79D6B5FC879362299BC5A637DAD2E0D99656B8");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "C2");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B875002F38BA26D61167C5D440367604AD38DF2E93D8EE8DA0E8D9C0CF4CC5788D11DEA689E5F41D23A3DA3E0B1FA5875AE25620F5A6BCCEE098C1B35C691889D7D0EF670EB8312E7123FCC5DC7D2F0719CC80E1A93017F944D097330EDF945762FEE62B7B0BA0348228DBF38D4216E5A67A7EF74F5D3111C44AA31320F623CB3C53E60966D6920067C9E082B746117E48E4F00E110950CA54DA3E38E5453BD5544E3A6760E3A6A42766AD2284E0C9AF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "8E748296359A7428F536ADDA8E2C037E2B697EF6");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "02");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AF4B8D230FDFCB1538E975795A1DB40C396A5359FAA31AE095CB522A5C82E7FFFB252860EC2833EC3D4A665F133DD934EE1148D81E2B7E03F92995DDF7EB7C90A75AB98E69C92EC91A533B21E1C4918B43AFED5780DE13A32BBD37EBC384FA3DD1A453E327C56024DACAEA74AA052C4D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "33F5B0344943048237EC89B275A95569718AEE20");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B0C2C6E2A6386933CD17C239496BF48C57E389164F2A96BFF133439AE8A77B20498BD4DC6959AB0C2D05D0723AF3668901937B674E5A2FA92DDD5E78EA9D75D79620173CC269B35F463B3D4AAFF2794F92E6C7A3FB95325D8AB95960C3066BE548087BCB6CE12688144A8B4A66228AE4659C634C99E36011584C095082A3A3E3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "8708A3E3BBC1BB0BE73EBD8D19D4E5D20166BF6C");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FA");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A90FCD55AA2D5D9963E35ED0F440177699832F49C6BAB15CDAE5794BE93F934D4462D5D12762E48C38BA83D8445DEAA74195A301A102B2F114EADA0D180EE5E7A5C73E0C4E11F67A43DDAB5D55683B1474CC0627F44B8D3088A492FFAADAD4F42422D0E7013536C3C49AD3D0FAE96459B0F6B1B6056538A3D6D44640F94467B108867DEC40FAAECD740C00E2B7A8852D");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "5BED4068D96EA16D2D77E03D6036FC7A160EA99C");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "98");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "CA026E52A695E72BD30AF928196EEDC9FAF4A619F2492E3FB31169789C276FFBB7D43116647BA9E0D106A3542E3965292CF77823DD34CA8EEC7DE367E08070895077C7EFAD939924CB187067DBF92CB1E785917BD38BACE0C194CA12DF0CE5B7A50275AC61BE7C3B436887CA98C9FD39");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "E7AC9AA8EED1B5FF1BD532CF1489A3E5557572C1");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "99");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AB79FCC9520896967E776E64444E5DCDD6E13611874F3985722520425295EEA4BD0C2781DE7F31CD3D041F565F747306EED62954B17EDABA3A6C5B85A1DE1BEB9A34141AF38FCF8279C9DEA0D5A6710D08DB4124F041945587E20359BAB47B7575AD94262D4B25F264AF33DEDCF28E09615E937DE32EDC03C54445FE7E382777");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "4ABFFD6B1C51212D05552E431C5B17007D2F5E6D");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            //index 95 is present as test capk
            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "visa";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "95");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "BE9E1FA5E9A803852999C4AB432DB28600DCD9DAB76DFAAA47355A0FE37B1508AC6BF38860D3C6C2E5B12A3CAAF2A7005A7241EBAA7771112C74CF9A0634652FBCA0E5980C54A64761EA101A114E0F0B5572ADD57D010B7C9C887E104CA4EE1272DA66D997B9A90B5A6D624AB6C57E73C8F919000EB5F684898EF8C3DBEFB330C62660BED88EA78E909AFF05F6DA627B");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "EE1511CEC71020A9B90443B37B1D5F6E703030F6");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            //index 92 is present as test capk
            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "visa";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "92");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "996AF56F569187D09293C14810450ED8EE3357397B18A2458EFAA92DA3B6DF6514EC060195318FD43BE9B8F0CC669E3F844057CBDDF8BDA191BB64473BC8DC9A730DB8F6B4EDE3924186FFD9B8C7735789C23A36BA0B8AF65372EB57EA5D89E7D14E9C7B6B557460F10885DA16AC923F15AF3758F0F03EBD3C5C2C949CBA306DB44E6A2C076C5F67E281D7EF56785DC4D75945E491F01918800A9E2DC66F60080566CE0DAF8D17EAD46AD8E30A247C9F");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "429C954A3859CEF91295F663C963E582ED6EB253");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            //index 94 is present as test capk
            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "visa";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "94");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "ACD2B12302EE644F3F835ABD1FC7A6F62CCE48FFEC622AA8EF062BEF6FB8BA8BC68BBF6AB5870EED579BC3973E121303D34841A796D6DCBC41DBF9E52C4609795C0CCF7EE86FA1D5CB041071ED2C51D2202F63F1156C58A92D38BC60BDF424E1776E2BC9648078A03B36FB554375FC53D57C73F5160EA59F3AFC5398EC7B67758D65C9BFF7828B6B82D4BE124A416AB7301914311EA462C19F771F31B3B57336000DFF732D3B83DE07052D730354D297BEC72871DCCF0E193F171ABA27EE464C6A97690943D59BDABB2A27EB71CEEBDAFA1176046478FD62FEC452D5CA393296530AA3F41927ADFE434A2DF2AE3054F8840657A26E0FC617");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "C4A3C43CCF87327D136B804160E47D43B60E6E0F");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "visa";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B3E5E667506C47CAAFB12A2633819350846697DD65A796E5CE77C57C626A66F70BB630911612AD2832909B8062291BECA46CD33B66A6F9C9D48CED8B4FC8561C8A1D8FB15862C9EB60178DEA2BE1F82236FFCFF4F3843C272179DCDD384D541053DA6A6A0D3CE48FDC2DC4E3E0EEE15F");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "FE70AB3B4D5A1B9924228ADF8027C758483A8B7E");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "05");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D0135CE8A4436C7F9D5CC66547E30EA402F98105B71722E24BC08DCC80AB7E71EC23B8CE6A1DC6AC2A8CF55543D74A8AE7B388F9B174B7F0D756C22CBB5974F9016A56B601CCA64C71F04B78E86C501B193A5556D5389ECE4DEA258AB97F52A3");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "86DF041E7995023552A79E2623E49180C0CD957A");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "90");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "C26B3CB3833E42D8270DC10C8999B2DA18106838650DA0DBF154EFD51100AD144741B2A87D6881F8630E3348DEA3F78038E9B21A697EB2A6716D32CBF26086F1");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "B3AE2BC3CAFC05EEEFAA46A2A47ED51DE679F823");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "97");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AF0754EAED977043AB6F41D6312AB1E22A6809175BEB28E70D5F99B2DF18CAE73519341BBBD327D0B8BE9D4D0E15F07D36EA3E3A05C892F5B19A3E9D3413B0D97E7AD10A5F5DE8E38860C0AD004B1E06F4040C295ACB457A788551B6127C0B29");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "8001CA76C1203955E2C62841CD6F201087E564BF");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "96");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B74586D19A207BE6627C5B0AAFBC44A2ECF5A2942D3A26CE19C4FFAEEE920521868922E893E7838225A3947A2614796FB2C0628CE8C11E3825A56D3B1BBAEF783A5C6A81F36F8625395126FA983C5216D3166D48ACDE8A431212FF763A7F79D9EDB7FED76B485DE45BEB829A3D4730848A366D3324C3027032FF8D16A1E44D8D");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "7616E9AC8BE014AF88CA11A8FB17967B7394030E");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "B0");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D0F543F03F2517133EF2BA4A1104486758630DCFE3A883C77B4E4844E39A9BD6360D23E6644E1E071F196DDF2E4A68B4A3D93D14268D7240F6A14F0D714C17827D279D192E88931AF7300727AE9DA80A3F0E366AEBA61778171737989E1EE309");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "B1");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B74586D19A207BE6627C5B0AAFBC44A2ECF5A2942D3A26CE19C4FFAEEE920521868922E893E7838225A3947A2614796FB2C0628CE8C11E3825A56D3B1BBAEF783A5C6A81F36F8625395126FA983C5216D3166D48ACDE8A431212FF763A7F79D9EDB7FED76B485DE45BEB829A3D4730848A366D3324C3027032FF8D16A1E44D8D");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FF");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AF0754EAED977043AB6F41D6312AB1E22A6809175BEB28E70D5F99B2DF18CAE73519341BBBD327D0B8BE9D4D0E15F07D36EA3E3A05C892F5B19A3E9D3413B0D97E7AD10A5F5DE8E38860C0AD004B1E06F4040C295ACB457A788551B6127C0B29");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "89");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "E5E195705CE61A0672B8367E7A51713927A04289EA308328FAD28071ECEAE889B3C4F29AC3BDE46772B00D42FD05F27228820F2693990F81B0F6928E240D957EC4484354CD5E5CA9092B444741A0394D3476651232474A9B87A961DA8DD96D90F036E9B3C52FB09766BDA4D6BC3BDADBC89122B74068F8FA04026C5FA8EF398BC3AB3992A87F6A785CC779BA99F170956623D67A18EB8324263D626BE85BFF77B8B981C0A3F7849C4F3D8E20542955D19128198547B47AE34DF67F28BE433F33");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "7170850B97F83952045CF9CA8B7612DFEB69E9EF");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "53");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "BCD83721BE52CCCC4B6457321F22A7DC769F54EB8025913BE804D9EABBFA19B3D7C5D3CA658D768CAF57067EEC83C7E6E9F81D0586703ED9DDDADD20675D63424980B10EB364E81EB37DB40ED100344C928886FF4CCC37203EE6106D5B59D1AC102E2CD2D7AC17F4D96C398E5FD993ECB4FFDF79B17547FF9FA2AA8EEFD6CBDA124CBB17A0F8528146387135E226B005A474B9062FF264D2FF8EFA36814AA2950065B1B04C0A1AE9B2F69D4A4AA979D6CE95FEE9485ED0A03AEE9BD953E81CFD1EF6E814DFD3C2CE37AEFA38C1F9877371E91D6A5EB59FDEDF75D3325FA3CA66CDFBA0E57146CC789818FF06BE5FCC50ABD362AE4B80996D");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A84A53964513A5D9363B4BA13AF5D43B83A83CE7");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "51");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "DB5FA29D1FDA8C1634B04DCCFF148ABEE63C772035C79851D3512107586E02A917F7C7E885E7C4A7D529710A145334CE67DC412CB1597B77AA2543B98D19CF2CB80C522BDBEA0F1B113FA2C86216C8C610A2D58F29CF3355CEB1BD3EF410D1EDD1F7AE0F16897979DE28C6EF293E0A19282BD1D793F1331523FC71A228800468C01A3653D14C6B4851A5C029478E757F");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "969299D792D3CC08AD28F2D544CEE3309DADF1B9");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card 13";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "50");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D11197590057B84196C2F4D11A8F3C05408F422A35D702F90106EA5B019BB28AE607AA9CDEBCD0D81A38D48C7EBB0062D287369EC0C42124246AC30D80CD602AB7238D51084DED4698162C59D25EAC1E66255B4DB2352526EF0982C3B8AD3D1CCE85B01DB5788E75E09F44BE7361366DEF9D1E1317B05E5D0FF5290F88A0DB47");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "010001");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "B769775668CACB5D22A647D1D993141EDAB7237B");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AFAD7010F884E2824650F764D47D7951A16EED6DBB881F384DEDB6702E0FB55C0FBEF945A2017705E5286FA249A591E194BDCD74B21720B44CE986F144237A25F95789F38B47EA957F9ADB2372F6D5D41340A147EAC2AF324E8358AE1120EF3F");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "ACFE734CF09A84C7BF025F0FFC6FA8CA25A22869");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card new 1";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FA");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A90FCD55AA2D5D9963E35ED0F440177699832F49C6BAB15CDAE5794BE93F934D4462D5D12762E48C38BA83D8445DEAA74195A301A102B2F114EADA0D180EE5E7A5C73E0C4E11F67A43DDAB5D55683B1474CC0627F44B8D3088A492FFAADAD4F42422D0E7013536C3C49AD3D0FAE96459B0F6B1B6056538A3D6D44640F94467B108867DEC40FAAECD740C00E2B7A8852D");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "5BED4068D96EA16D2D77E03D6036FC7A160EA99C");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card new 2";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "EF");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A191CB87473F29349B5D60A88B3EAEE0973AA6F1A082F358D849FDDFF9C091F899EDA9792CAF09EF28F5D22404B88A2293EEBBC1949C43BEA4D60CFD879A1539544E09E0F09F60F065B2BF2A13ECC705F3D468B9D33AE77AD9D3F19CA40F23DCF5EB7C04DC8F69EBA565B1EBCB4686CD274785530FF6F6E9EE43AA43FDB02CE00DAEC15C7B8FD6A9B394BABA419D3F6DC85E16569BE8E76989688EFEA2DF22FF7D35C043338DEAA982A02B866DE5328519EBBCD6F03CDD686673847F84DB651AB86C28CF1462562C577B853564A290C8556D818531268D25CC98A4CC6A0BDFFFDA2DCCA3A94C998559E307FDDF915006D9A987B07DDAEB3B");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "21766EBB0EE122AFB65D7845B73DB46BAB65427A");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card new 3";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FE");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A653EAC1C0F786C8724F737F172997D63D1C3251C44402049B865BAE877D0F398CBFBE8A6035E24AFA086BEFDE9351E54B95708EE672F0968BCD50DCE40F783322B2ABA04EF137EF18ABF03C7DBC5813AEAEF3AA7797BA15DF7D5BA1CBAF7FD520B5A482D8D3FEE105077871113E23A49AF3926554A70FE10ED728CF793B62A1");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "9A295B05FB390EF7923F57618A9FDA2941FC34E0");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card new 4";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "F8");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A1F5E1C9BD8650BD43AB6EE56B891EF7459C0A24FA84F9127D1A6C79D4930F6DB1852E2510F18B61CD354DB83A356BD190B88AB8DF04284D02A4204A7B6CB7C5551977A9B36379CA3DE1A08E69F301C95CC1C20506959275F41723DD5D2925290579E5A95B0DF6323FC8E9273D6F849198C4996209166D9BFC973C361CC826E1");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "F06ECC6D2AAEBF259B7E755A38D9A9B24E2FF3DD");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card new 5";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "F3");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "98F0C770F23864C2E766DF02D1E833DFF4FFE92D696E1642F0A88C5694C6479D16DB1537BFE29E4FDC6E6E8AFD1B0EB7EA0124723C333179BF19E93F10658B2F776E829E87DAEDA9C94A8B3382199A350C077977C97AFF08FD11310AC950A72C3CA5002EF513FCCC286E646E3C5387535D509514B3B326E1234F9CB48C36DDD44B416D23654034A66F403BA511C5EFA3");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A69AC7603DAF566E972DEDC2CB433E07E8B01A9A");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "master card new 6";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "F1");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A0DCF4BDE19C3546B4B6F0414D174DDE294AABBB828C5A834D73AAE27C99B0B053A90278007239B6459FF0BBCD7B4B9C6C50AC02CE91368DA1BD21AAEADBC65347337D89B68F5C99A09D05BE02DD1F8C5BA20E2F13FB2A27C41D3F85CAD5CF6668E75851EC66EDBF98851FD4E42C44C1D59F5984703B27D5B9F21B8FA0D93279FBBF69E090642909C9EA27F898959541AA6757F5F624104F6E1D3A9532F2A6E51515AEAD1B43B3D7835088A2FAFA7BE7");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "D8E68DA167AB5A85D8C3D55ECB9B0517A1A5B4BB");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            //+ taha 18-03-2021 adding JCB test+live keys
            int jcbKeyCounter = 1;
            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "08");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B74670DAD1DC8983652000E5A7F2F8B35DFD083EE593E5BA895C95729F2BADE9C8ABF3DD9CE240C451C6CEFFC768D83CBAC76ABB8FEA58F013C647007CFF7617BAC2AE3981816F25CC7E5238EF34C4F02D0B01C24F80C2C65E7E7743A4FA8E23206A23ECE290C26EA56DB085C5C5EAE26292451FC8292F9957BE8FF20FAD53E5");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "DD36D5896228C8C4900742F107E2F91FE50BC7EE");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "10");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "99B63464EE0B4957E4FD23BF923D12B61469B8FFF8814346B2ED6A780F8988EA9CF0433BC1E655F05EFA66D0C98098F25B659D7A25B8478A36E489760D071F54CDF7416948ED733D816349DA2AADDA227EE45936203CBF628CD033AABA5E5A6E4AE37FBACB4611B4113ED427529C636F6C3304F8ABDD6D9AD660516AE87F7F2DDF1D2FA44C164727E56BBC9BA23C0285");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "C75E5210CBE6E8F0594A0F1911B07418CADB5BAB");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "11");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A2583AA40746E3A63C22478F576D1EFC5FB046135A6FC739E82B55035F71B09BEB566EDB9968DD649B94B6DEDC033899884E908C27BE1CD291E5436F762553297763DAA3B890D778C0F01E3344CECDFB3BA70D7E055B8C760D0179A403D6B55F2B3B083912B183ADB7927441BED3395A199EEFE0DEBD1F5FC3264033DA856F4A8B93916885BD42F9C1F456AAB8CFA83AC574833EB5E87BB9D4C006A4B5346BD9E17E139AB6552D9C58BC041195336485");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "D9FD62C9DD4E6DE7741E9A17FB1FF2C5DB948BCB");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "13");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A3270868367E6E29349FC2743EE545AC53BD3029782488997650108524FD051E3B6EACA6A9A6C1441D28889A5F46413C8F62F3645AAEB30A1521EEF41FD4F3445BFA1AB29F9AC1A74D9A16B93293296CB09162B149BAC22F88AD8F322D684D6B49A12413FC1B6AC70EDEDB18EC1585519A89B50B3D03E14063C2CA58B7C2BA7FB22799A33BCDE6AFCBEB4A7D64911D08D18C47F9BD14A9FAD8805A15DE5A38945A97919B7AB88EFA11A88C0CD92C6EE7DC352AB0746ABF13585913C8A4E04464B77909C6BD94341A8976C4769EA6C0D30A60F4EE8FA19E767B170DF4FA80312DBA61DB645D5D1560873E2674E1F620083F30180BD96CA589");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "54CFAE617150DFA09D3F901C9123524523EBEDF3");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "EC");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A9EDFDC58029A7EC003D13F22F6AED5622786D45F7C36516A3DBFE4D75BFCE00F4CF656670CD07A66A99A7CD35D2F5228CB2D794B95C4930FDDAD17F8C9293164AFEC876D5644DD31ABFE86B7AA512C58D5C71310FB36E8D7CCFF4C958669C0042DFF048F52E412B530C3BB77555B6F9B35E2C0F1B17A6180D03D94914B4970A42309F259DB37EC77FF6BA04BACF6B17FF7B10C1A04272D08C043A1C8E8951681DE41BE30F4E42D3ED3FE3328BD4C6327B19D110A2E85D9DC4C34225A2F0CA7684FF5C05C1F01135FC51D7331E3A413AED0942C8BBDB975104E171B08EE7C2B388EC4EA493BE5FCB0C416DF2A9DBBCDFA5D12344EC30576B");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A15945946935956845ADB8ABE73E5B0BEEF76ECB");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "EB");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A9A696A7E3C6AC1421E4DE8936EFB6D66CE960EAFA6EA5E03D066F296284A61DA2890A6D0D869BA8C9A9E01E4EFF3BBE331ECE2CE1429C066DD885781E2ADABC86CFFD76854FB9F1BCDAEDCE6B54727D9C2C01C9642E9CD1BFC4CF24A6A2E49C1541B02EBC0534744481CC8922073A21F8E0D72BC8998BD529C698567A87F16450DFB969961A4186AB4CC648A4D41B149E3DD21393A993833D5EEEEBFCBCE0C777B52447ED3B816E3A2984930C07C021");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A19CAF700603E37BC7C2E5C420A11A56D1A8BAC3");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "EA");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A9796C29E39C2D44FDDAE7EE1341DA5461DCE4DCB31438D583B2BC0845B64AA37D055B190D7F5152E5057A5FB9CD27634EAC4003A2803C804E22D492738A164369A17F265F8016C622DA0631494F03B2DA4D5E7D13F7082F9BD8A7393B119AC70A39E861B645B1FBF29BA9CC1B8A5A97B5A8444DB0FCA5BC511E68E7B01D7ADCB8E46D9648A995E256F7715251B431B3");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "88F6D04BED35D6B43E697D765AE3293F649A961F");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "0F");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "9EFBADDE4071D4EF98C969EB32AF854864602E515D6501FDE576B310964A4F7C2CE842ABEFAFC5DC9E26A619BCF2614FE07375B9249BEFA09CFEE70232E75FFD647571280C76FFCA87511AD255B98A6B577591AF01D003BD6BF7E1FCE4DFD20D0D0297ED5ECA25DE261F37EFE9E175FB5F12D2503D8CFB060A63138511FE0E125CF3A643AFD7D66DCF9682BD246DDEA1");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "2A1B82DE00F5F0C401760ADF528228D3EDE0F403");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "09");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B72A8FEF5B27F2B550398FDCC256F714BAD497FF56094B7408328CB626AA6F0E6A9DF8388EB9887BC930170BCC1213E90FC070D52C8DCD0FF9E10FAD36801FE93FC998A721705091F18BC7C98241CADC15A2B9DA7FB963142C0AB640D5D0135E77EBAE95AF1B4FEFADCF9C012366BDDA0455C1564A68810D7127676D493890BD");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "4410C6D51C2F83ADFD92528FA6E38A32DF048D0A");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "12");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "ADF05CD4C5B490B087C3467B0F3043750438848461288BFEFD6198DD576DC3AD7A7CFA07DBA128C247A8EAB30DC3A30B02FCD7F1C8167965463626FEFF8AB1AA61A4B9AEF09EE12B009842A1ABA01ADB4A2B170668781EC92B60F605FD12B2B2A6F1FE734BE510F60DC5D189E401451B62B4E06851EC20EBFF4522AACC2E9CDC89BC5D8CDE5D633CFD77220FF6BBD4A9B441473CC3C6FEFC8D13E57C3DE97E1269FA19F655215B23563ED1D1860D8681");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "874B379B7F607DC1CAF87A19E400B6A9E25163E8");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "DA");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "DC7449EC24944EA4C09EF37656F5390594DE4F1686AFC4B1C21C893F5F3AEFC5A8109E06A52389C0414E7DFBC44293B04D5F4E85528FA85F1A9706BAAAA034E8B44111C043B1CC95309C6946225971D4B158909F4438726812777FAC4D06879A7AA41089F0DD2C27B3EAA23A8D02E2A9A9B8EDAD0CA32AE91B383740CF50E5E5");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "5C037E1815E38B79D4D5C15FF90CE4E3F2070863");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;

            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB key #" + jcbKeyCounter++;
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "14");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AEED55B9EE00E1ECEB045F61D2DA9A66AB637B43FB5CDBDB22A2FBB25BE061E937E38244EE5132F530144A3F268907D8FD648863F5A96FED7E42089E93457ADC0E1BC89C58A0DB72675FBC47FEE9FF33C16ADE6D341936B06B6A6F5EF6F66A4EDD981DF75DA8399C3053F430ECA342437C23AF423A211AC9F58EAF09B0F837DE9D86C7109DB1646561AA5AF0289AF5514AC64BC2D9D36A179BB8A7971E2BFA03A9E4B847FD3D63524D43A0E8003547B94A8A75E519DF3177D0A60BC0B4BAB1EA59A2CBB4D2D62354E926E9C7D3BE4181E81BA60F8285A896D17DA8C3242481B6C405769A39D547C74ED9FF95A70A796046B5EFF36682DC29");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "C0D15F6CD957E491DB56DCDD1CA87A03EBE06B7B");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;
        }
        else if(Constants.keysType.equals("P"))
        {

            Utility.DEBUG_LOG(TAG,"Production keys");
            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "Visa Key # 1";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "08");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323431323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D9FD6ED75D51D0E30664BD157023EAA1FFA871E4DA65672B863D255E81E137A51DE4F72BCC9E44ACE12127F87E263D3AF9DD9CF35CA4A7B01E907000BA85D24954C2FCA3074825DDD4C0C8F186CB020F683E02F2DEAD3969133F06F7845166ACEB57CA0FC2603445469811D293BFEFBAFAB57631B3DD91E796BF850A25012F1AE38F05AA5C4D6D03B1DC2E568612785938BBC9B3CD3A910C1DA55A5A9218ACE0F7A21287752682F15832A678D6E1ED0B");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "20D213126955DE205ADC2FD2822BD22DE21CF9A8");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "Visa Key # 2";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "09");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230333031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "9D912248DE0A4E39C1A7DDE3F6D2588992C1A4095AFBD1824D1BA74847F2BC4926D2EFD904B4B54954CD189A54C5D1179654F8F9B0D2AB5F0357EB642FEDA95D3912C6576945FAB897E7062CAA44A4AA06B8FE6E3DBA18AF6AE3738E30429EE9BE03427C9D64F695FA8CAB4BFE376853EA34AD1D76BFCAD15908C077FFE6DC5521ECEF5D278A96E26F57359FFAEDA19434B937F1AD999DC5C41EB11935B44C18100E857F431A4A5A6BB65114F174C2D7B59FDF237D6BB1DD0916E644D709DED56481477C75D95CDD68254615F7740EC07F330AC5D67BCD75BF23D28A140826C026DBDE971A37CD3EF9B8DF644AC385010501EFC6509D7A41");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "1FF80A40173F52D7D27E0F26A146A1C8CCB29046");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "Master Key # 1";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "05");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323431323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B8048ABC30C90D976336543E3FD7091C8FE4800DF820ED55E7E94813ED00555B573FECA3D84AF6131A651D66CFF4284FB13B635EDD0EE40176D8BF04B7FD1C7BACF9AC7327DFAA8AA72D10DB3B8E70B2DDD811CB4196525EA386ACC33C0D9D4575916469C4E4F53E8E1C912CC618CB22DDE7C3568E90022E6BBA770202E4522A2DD623D180E215BD1D1507FE3DC90CA310D27B3EFCCD8F83DE3052CAD1E48938C68D095AAC91B5F37E28BB49EC7ED597");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "EBFA0D5D06D8CE702DA3EAE890701D45E274C845");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;



            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "Master Key # 2";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000004");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "06");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323531323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "CB26FC830B43785B2BCE37C81ED334622F9622F4C89AAE641046B2353433883F307FB7C974162DA72F7A4EC75D9D657336865B8D3023D3D645667625C9A07A6B7A137CF0C64198AE38FC238006FB2603F41F4F3BB9DA1347270F2F5D8C606E420958C5F7D50A71DE30142F70DE468889B5E3A08695B938A50FC980393A9CBCE44AD2D64F630BB33AD3F5F5FD495D31F37818C1D94071342E07F1BEC2194F6035BA5DED3936500EB82DFDA6E8AFB655B1EF3D0D7EBF86B66DD9F29F6B1D324FE8B26CE38AB2013DD13F611E7A594D675C4432350EA244CC34F3873CBA06592987A1D7E852ADC22EF5A2EE28132031E48F74037E3B34AB747F");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "F910A1504D5FFB793D94F3B500765E1ABCAD72D9");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "UPI Key # 1";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "02");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323131323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "A3767ABD1B6AA69D7F3FBF28C092DE9ED1E658BA5F0909AF7A1CCD907373B7210FDEB16287BA8E78E1529F443976FD27F991EC67D95E5F4E96B127CAB2396A94D6E45CDA44CA4C4867570D6B07542F8D4BF9FF97975DB9891515E66F525D2B3CBEB6D662BFB6C3F338E93B02142BFC44173A3764C56AADD202075B26DC2F9F7D7AE74BD7D00FD05EE430032663D27A57");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "03BB335A8549A03B87AB089D006F60852E4B8060");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;



            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "UPI Key # 2";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323431323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B0627DEE87864F9C18C13B9A1F025448BF13C58380C91F4CEBA9F9BCB214FF8414E9B59D6ABA10F941C7331768F47B2127907D857FA39AAF8CE02045DD01619D689EE731C551159BE7EB2D51A372FF56B556E5CB2FDE36E23073A44CA215D6C26CA68847B388E39520E0026E62294B557D6470440CA0AEFC9438C923AEC9B2098D6D3A1AF5E8B1DE36F4B53040109D89B77CAFAF70C26C601ABDF59EEC0FDC8A99089140CD2E817E335175B03B7AA33D");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "87F0CD7C0E86F38F89A66F8C47071A8B88586F26");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;



            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "UPI Key # 3";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000333");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "04");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "BC853E6B5365E89E7EE9317C94B02D0ABB0DBD91C05A224A2554AA29ED9FCB9D86EB9CCBB322A57811F86188AAC7351C72BD9EF196C5A01ACEF7A4EB0D2AD63D9E6AC2E7836547CB1595C68BCBAFD0F6728760F3A7CA7B97301B7E0220184EFC4F653008D93CE098C0D93B45201096D1ADFF4CF1F9FC02AF759DA27CD6DFD6D789B099F16F378B6100334E63F3D35F3251A5EC78693731F5233519CDB380F5AB8C0F02728E91D469ABD0EAE0D93B1CC66CE127B29C7D77441A49D09FCA5D6D9762FC74C31BB506C8BAE3C79AD6C2578775B95956B5370D1D0519E37906B384736233251E8F09AD79DFBE2C6ABFADAC8E4D8624318C27DAF1");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "F527081CF371DD7E1FD4FA414A665036E0F5E6E5");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "Amex Key # 1";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "0F");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323431323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "C8D5AC27A5E1FB89978C7C6479AF993AB3800EB243996FBB2AE26B67B23AC482C4B746005A51AFA7D2D83E894F591A2357B30F85B85627FF15DA12290F70F05766552BA11AD34B7109FA49DE29DCB0109670875A17EA95549E92347B948AA1F045756DE56B707E3863E59A6CBE99C1272EF65FB66CBB4CFF070F36029DD76218B21242645B51CA752AF37E70BE1A84FF31079DC0048E928883EC4FADD497A719385C2BBBEBC5A66AA5E5655D18034EC5");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A73472B3AB557493A9BC2179CC8014053B12BAB4");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;



            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "Amex Key # 2";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "10");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "CF98DFEDB3D3727965EE7797723355E0751C81D2D3DF4D18EBAB9FB9D49F38C8C4A826B99DC9DEA3F01043D4BF22AC3550E2962A59639B1332156422F788B9C16D40135EFD1BA94147750575E636B6EBC618734C91C1D1BF3EDC2A46A43901668E0FFC136774080E888044F6A1E65DC9AAA8928DACBEB0DB55EA3514686C6A732CEF55EE27CF877F110652694A0E3484C855D882AE191674E25C296205BBB599455176FDD7BBC549F27BA5FE35336F7E29E68D783973199436633C67EE5A680F05160ED12D1665EC83D1997F10FD05BBDBF9433E8F797AEE3E9F02A34228ACE927ABE62B8B9281AD08D3DF5C7379685045D7BA5FCDE58637");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "C729CF2FD262394ABC4CC173506502446AA9B9FD");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "Amex Key # 3";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000025");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "0E");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AA94A8C6DAD24F9BA56A27C09B01020819568B81A026BE9FD0A3416CA9A71166ED5084ED91CED47DD457DB7E6CBCD53E560BC5DF48ABC380993B6D549F5196CFA77DFB20A0296188E969A2772E8C4141665F8BB2516BA2C7B5FC91F8DA04E8D512EB0F6411516FB86FC021CE7E969DA94D33937909A53A57F907C40C22009DA7532CB3BE509AE173B39AD6A01BA5BB85");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A7266ABAE64B42A3668851191D49856E17F8FBCD");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB Key # 1";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "12");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "ADF05CD4C5B490B087C3467B0F3043750438848461288BFEFD6198DD576DC3AD7A7CFA07DBA128C247A8EAB30DC3A30B02FCD7F1C8167965463626FEFF8AB1AA61A4B9AEF09EE12B009842A1ABA01ADB4A2B170668781EC92B60F605FD12B2B2A6F1FE734BE510F60DC5D189E401451B62B4E06851EC20EBFF4522AACC2E9CDC89BC5D8CDE5D633CFD77220FF6BBD4A9B441473CC3C6FEFC8D13E57C3DE97E1269FA19F655215B23563ED1D1860D8681");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "874B379B7F607DC1CAF87A19E400B6A9E25163E8");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;


            emvParamKeyUMS[index] = new EMVParamKeyCaseA();
            emvParamKeyUMS[index].comment = "JCB Key # 2";
            emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
            emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000065");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "14");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230353031323331");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AEED55B9EE00E1ECEB045F61D2DA9A66AB637B43FB5CDBDB22A2FBB25BE061E937E38244EE5132F530144A3F268907D8FD648863F5A96FED7E42089E93457ADC0E1BC89C58A0DB72675FBC47FEE9FF33C16ADE6D341936B06B6A6F5EF6F66A4EDD981DF75DA8399C3053F430ECA342437C23AF423A211AC9F58EAF09B0F837DE9D86C7109DB1646561AA5AF0289AF5514AC64BC2D9D36A179BB8A7971E2BFA03A9E4B847FD3D63524D43A0E8003547B94A8A75E519DF3177D0A60BC0B4BAB1EA59A2CBB4D2D62354E926E9C7D3BE4181E81BA60F8285A896D17DA8C3242481B6C405769A39D547C74ED9FF95A70A796046B5EFF36682DC29");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
            emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "C0D15F6CD957E491DB56DCDD1CA87A03EBE06B7B");
            Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
            index++;
        }
        //- Shivam 08-04-2021 production keys

        Utility.DEBUG_LOG(TAG,"final CAPK index:" + index);

        try {
            for (int i = 0; i < index; i++) {
                Utility.DEBUG_LOG(TAG,"i:"+i);
                Utility.DEBUG_LOG(TAG,"capk: final TLV string: " + emvParamKeyUMS[i].getTlvString());
                boolean bRet = ipboc.updateRID(emvParamKeyUMS[i].flagAppendRemoveClear, emvParamKeyUMS[i].getTlvString());
                Utility.DEBUG_LOG(TAG, "bRet:" + bRet);
                if (bRet) {
                    Utility.DEBUG_LOG(TAG, "update RID success1 for " + emvParamKeyUMS[i].getComment() + ";" + emvParamKeyUMS[i].getTlvString());
                } else {
                    Utility.DEBUG_LOG(TAG, "update RID fails1 for " + emvParamKeyUMS[i].getComment() + ";" + emvParamKeyUMS[i].getTlvString());
                }
            }

        } catch (RemoteException e) {
            // Utility.DEBUG_LOG( TAG, "update RID for " + emvParamKeyUMS[i].comment + ", exception!");
            e.printStackTrace();
        }
        // way 2#, set one tlv string
        // hardcoding some rid
//        String[] ridList = new String[]{
//                // VISA
//          "9F0605A000000003"+
//                  "9F220199"+
//                  "DF050420291231"+
//                  "DF028180AB79FCC9520896967E776E64444E5DCDD6E13611874F3985722520425295EEA4BD0C2781DE7F31CD3D041F565F747306EED62954B17EDABA3A6C5B85A1DE1BEB9A34141AF38FCF8279C9DEA0D5A6710D08DB4124F041945587E20359BAB47B7575AD94262D4B25F264AF33DEDCF28E09615E937DE32EDC03C54445FE7E382777" +
//                  "DF040103" +
//                  "DF03144ABFFD6B1C51212D05552E431C5B17007D2F5E6D" +
//                  "BF010131" +
//                  "DF070101",
//          "9F0605A000000003" +
//                  "9F220195DF050420291231DF028190BE9E1FA5E9A803852999C4AB432DB28600DCD9DAB76DFAAA47355A0FE37B1508AC6BF38860D3C6C2E5B12A3CAAF2A7005A7241EBAA7771112C74CF9A0634652FBCA0E5980C54A64761EA101A114E0F0B5572ADD57D010B7C9C887E104CA4EE1272DA66D997B9A90B5A6D624AB6C57E73C8F919000EB5F684898EF8C3DBEFB330C62660BED88EA78E909AFF05F6DA627BDF040103DF0314EE1511CEC71020A9B90443B37B1D5F6E703030F6BF010131DF070101",
//          "9F0605A000000003" +
//                  "9F220192DF050420291231DF0281B0996AF56F569187D09293C14810450ED8EE3357397B18A2458EFAA92DA3B6DF6514EC060195318FD43BE9B8F0CC669E3F844057CBDDF8BDA191BB64473BC8DC9A730DB8F6B4EDE3924186FFD9B8C7735789C23A36BA0B8AF65372EB57EA5D89E7D14E9C7B6B557460F10885DA16AC923F15AF3758F0F03EBD3C5C2C949CBA306DB44E6A2C076C5F67E281D7EF56785DC4D75945E491F01918800A9E2DC66F60080566CE0DAF8D17EAD46AD8E30A247C9FDF040103DF0314429C954A3859CEF91295F663C963E582ED6EB253BF010131DF070101",
//          "9F0605A000000003" +
//                  "9F220107DF050420291231DF028190A89F25A56FA6DA258C8CA8B40427D927B4A1EB4D7EA326BBB12F97DED70AE5E4480FC9C5E8A972177110A1CC318D06D2F8F5C4844AC5FA79A4DC470BB11ED635699C17081B90F1B984F12E92C1C529276D8AF8EC7F28492097D8CD5BECEA16FE4088F6CFAB4A1B42328A1B996F9278B0B7E3311CA5EF856C2F888474B83612A82E4E00D0CD4069A6783140433D50725FDF040103DF0314B4BC56CC4E88324932CBC643D6898F6FE593B172BF010131DF070101",
//          "9F0605A000000003" +
//                  "9F220108DF050420291231DF0281B0D9FD6ED75D51D0E30664BD157023EAA1FFA871E4DA65672B863D255E81E137A51DE4F72BCC9E44ACE12127F87E263D3AF9DD9CF35CA4A7B01E907000BA85D24954C2FCA3074825DDD4C0C8F186CB020F683E02F2DEAD3969133F06F7845166ACEB57CA0FC2603445469811D293BFEFBAFAB57631B3DD91E796BF850A25012F1AE38F05AA5C4D6D03B1DC2E568612785938BBC9B3CD3A910C1DA55A5A9218ACE0F7A21287752682F15832A678D6E1ED0BDF040103DF031420D213126955DE205ADC2FD2822BD22DE21CF9A8BF010131DF070101",
//          "9F0605A000000003" +
//                  "9F220109DF050420291231DF0281F89D912248DE0A4E39C1A7DDE3F6D2588992C1A4095AFBD1824D1BA74847F2BC4926D2EFD904B4B54954CD189A54C5D1179654F8F9B0D2AB5F0357EB642FEDA95D3912C6576945FAB897E7062CAA44A4AA06B8FE6E3DBA18AF6AE3738E30429EE9BE03427C9D64F695FA8CAB4BFE376853EA34AD1D76BFCAD15908C077FFE6DC5521ECEF5D278A96E26F57359FFAEDA19434B937F1AD999DC5C41EB11935B44C18100E857F431A4A5A6BB65114F174C2D7B59FDF237D6BB1DD0916E644D709DED56481477C75D95CDD68254615F7740EC07F330AC5D67BCD75BF23D28A140826C026DBDE971A37CD3EF9B8DF644AC385010501EFC6509D7A41DF040103DF03141FF80A40173F52D7D27E0F26A146A1C8CCB29046BF010131DF070101",
//          "9F0605A000000003" +
//                  "9F220101DF050420291231DF028180C696034213D7D8546984579D1D0F0EA519CFF8DEFFC429354CF3A871A6F7183F1228DA5C7470C055387100CB935A712C4E2864DF5D64BA93FE7E63E71F25B1E5F5298575EBE1C63AA617706917911DC2A75AC28B251C7EF40F2365912490B939BCA2124A30A28F54402C34AECA331AB67E1E79B285DD5771B5D9FF79EA630B75DF040103DF0314D34A6A776011C7E7CE3AEC5F03AD2F8CFC5503CCBF010131DF070101",
//          "9F0605A000000003" +
//                  "9F220103DF050420291231DF0270B3E5E667506C47CAAFB12A2633819350846697DD65A796E5CE77C57C626A66F70BB630911612AD2832909B8062291BECA46CD33B66A6F9C9D48CED8B4FC8561C8A1D8FB15862C9EB60178DEA2BE1F82236FFCFF4F3843C272179DCDD384D541053DA6A6A0D3CE48FDC2DC4E3E0EEE15FDF040103DF0314FE70AB3B4D5A1B9924228ADF8027C758483A8B7EBF010131DF070101",
//
//          // MasterCard
//          "9F0605A000000004" +
//                  "9F2201EFDF050420291231DF0281F8A191CB87473F29349B5D60A88B3EAEE0973AA6F1A082F358D849FDDFF9C091F899EDA9792CAF09EF28F5D22404B88A2293EEBBC1949C43BEA4D60CFD879A1539544E09E0F09F60F065B2BF2A13ECC705F3D468B9D33AE77AD9D3F19CA40F23DCF5EB7C04DC8F69EBA565B1EBCB4686CD274785530FF6F6E9EE43AA43FDB02CE00DAEC15C7B8FD6A9B394BABA419D3F6DC85E16569BE8E76989688EFEA2DF22FF7D35C043338DEAA982A02B866DE5328519EBBCD6F03CDD686673847F84DB651AB86C28CF1462562C577B853564A290C8556D818531268D25CC98A4CC6A0BDFFFDA2DCCA3A94C998559E307FDDF915006D9A987B07DDAEB3BDF040103DF031421766EBB0EE122AFB65D7845B73DB46BAB65427ABF010131DF070101",
//          "9F0605A000000004" +
//                  "9F2201FADF050420291231DF028190A90FCD55AA2D5D9963E35ED0F440177699832F49C6BAB15CDAE5794BE93F934D4462D5D12762E48C38BA83D8445DEAA74195A301A102B2F114EADA0D180EE5E7A5C73E0C4E11F67A43DDAB5D55683B1474CC0627F44B8D3088A492FFAADAD4F42422D0E7013536C3C49AD3D0FAE96459B0F6B1B6056538A3D6D44640F94467B108867DEC40FAAECD740C00E2B7A8852DDF040103DF03142CFBB82409ED86A31973B0E0CEEA381BC43C8097BF010131DF070101",
//          "9F0605A000000004" +
//                  "9F220104DF050420291231DF028190A6DA428387A502D7DDFB7A74D3F412BE762627197B25435B7A81716A700157DDD06F7CC99D6CA28C2470527E2C03616B9C59217357C2674F583B3BA5C7DCF2838692D023E3562420B4615C439CA97C44DC9A249CFCE7B3BFB22F68228C3AF13329AA4A613CF8DD853502373D62E49AB256D2BC17120E54AEDCED6D96A4287ACC5C04677D4A5A320DB8BEE2F775E5FEC5DF040103DF0314381A035DA58B482EE2AF75F4C3F2CA469BA4AA6CBF010131DF070101",
//          "9F0605A000000004" +
//                  "9F220105DF050420291231DF0281B0B8048ABC30C90D976336543E3FD7091C8FE4800DF820ED55E7E94813ED00555B573FECA3D84AF6131A651D66CFF4284FB13B635EDD0EE40176D8BF04B7FD1C7BACF9AC7327DFAA8AA72D10DB3B8E70B2DDD811CB4196525EA386ACC33C0D9D4575916469C4E4F53E8E1C912CC618CB22DDE7C3568E90022E6BBA770202E4522A2DD623D180E215BD1D1507FE3DC90CA310D27B3EFCCD8F83DE3052CAD1E48938C68D095AAC91B5F37E28BB49EC7ED597DF040103DF0314EBFA0D5D06D8CE702DA3EAE890701D45E274C845BF010131DF070101",
//          "9F0605A000000004" +
//                  "9F220106DF050420291231DF0281F8CB26FC830B43785B2BCE37C81ED334622F9622F4C89AAE641046B2353433883F307FB7C974162DA72F7A4EC75D9D657336865B8D3023D3D645667625C9A07A6B7A137CF0C64198AE38FC238006FB2603F41F4F3BB9DA1347270F2F5D8C606E420958C5F7D50A71DE30142F70DE468889B5E3A08695B938A50FC980393A9CBCE44AD2D64F630BB33AD3F5F5FD495D31F37818C1D94071342E07F1BEC2194F6035BA5DED3936500EB82DFDA6E8AFB655B1EF3D0D7EBF86B66DD9F29F6B1D324FE8B26CE38AB2013DD13F611E7A594D675C4432350EA244CC34F3873CBA06592987A1D7E852ADC22EF5A2EE28132031E48F74037E3B34AB747FDF040103DF0314F910A1504D5FFB793D94F3B500765E1ABCAD72D9BF010131DF070101",
//          "9F0605A000000004" +
//                  "9F2201F1DF050420231231DF0281B0A0DCF4BDE19C3546B4B6F0414D174DDE294AABBB828C5A834D73AAE27C99B0B053A90278007239B6459FF0BBCD7B4B9C6C50AC02CE91368DA1BD21AAEADBC65347337D89B68F5C99A09D05BE02DD1F8C5BA20E2F13FB2A27C41D3F85CAD5CF6668E75851EC66EDBF98851FD4E42C44C1D59F5984703B27D5B9F21B8FA0D93279FBBF69E090642909C9EA27F898959541AA6757F5F624104F6E1D3A9532F2A6E51515AEAD1B43B3D7835088A2FAFA7BE7DF040103DF0314D8E68DA167AB5A85D8C3D55ECB9B0517A1A5B4BBBF010131DF070101",
//          "9F0605A000000004" +
//                  "9F220103DF050420291231DF028180C2490747FE17EB0584C88D47B1602704150ADC88C5B998BD59CE043EDEBF0FFEE3093AC7956AD3B6AD4554C6DE19A178D6DA295BE15D5220645E3C8131666FA4BE5B84FE131EA44B039307638B9E74A8C42564F892A64DF1CB15712B736E3374F1BBB6819371602D8970E97B900793C7C2A89A4A1649A59BE680574DD0B60145DF040103DF03145ADDF21D09278661141179CBEFF272EA384B13BBBF010131DF070101",
//          "9F0605A000000004" +
//                  "9F220109DF050420291231DF028180C132F436477A59302E885646102D913EC86A95DD5D0A56F625F472B67F52179BC8BD258A7CD43EF1720AC0065519E3FFCECC26F978EDF9FB8C6ECDF145FDCC697D6B72562FA2E0418B2B80A038D0DC3B769EB027484087CCE6652488D2B3816742AC9C2355B17411C47EACDD7467566B302F512806E331FAD964BF000169F641DF040103DF0300BF010131DF070101",
//
//
//          // JCB
//          "9F0605A000000065" +
//                  "9F22010FDF050420291231DF0281909EFBADDE4071D4EF98C969EB32AF854864602E515D6501FDE576B310964A4F7C2CE842ABEFAFC5DC9E26A619BCF2614FE07375B9249BEFA09CFEE70232E75FFD647571280C76FFCA87511AD255B98A6B577591AF01D003BD6BF7E1FCE4DFD20D0D0297ED5ECA25DE261F37EFE9E175FB5F12D2503D8CFB060A63138511FE0E125CF3A643AFD7D66DCF9682BD246DDEA1DF040103DF03142A1B82DE00F5F0C401760ADF528228D3EDE0F403BF010131DF070101",
//          "9F0605A000000065" +
//                  "9F220113DF050420250101DF0281F8A3270868367E6E29349FC2743EE545AC53BD3029782488997650108524FD051E3B6EACA6A9A6C1441D28889A5F46413C8F62F3645AAEB30A1521EEF41FD4F3445BFA1AB29F9AC1A74D9A16B93293296CB09162B149BAC22F88AD8F322D684D6B49A12413FC1B6AC70EDEDB18EC1585519A89B50B3D03E14063C2CA58B7C2BA7FB22799A33BCDE6AFCBEB4A7D64911D08D18C47F9BD14A9FAD8805A15DE5A38945A97919B7AB88EFA11A88C0CD92C6EE7DC352AB0746ABF13585913C8A4E04464B77909C6BD94341A8976C4769EA6C0D30A60F4EE8FA19E767B170DF4FA80312DBA61DB645D5D1560873E2674E1F620083F30180BD96CA589DF040103DF031454CFAE617150DFA09D3F901C9123524523EBEDF3BF010131DF070101",
//          //
//          "9F0605A000000152" +//fails...
//                  "9F22015CDF050420291231DF0281B0833F275FCF5CA4CB6F1BF880E54DCFEB721A316692CAFEB28B698CAECAFA2B2D2AD8517B1EFB59DDEFC39F9C3B33DDEE40E7A63C03E90A4DD261BC0F28B42EA6E7A1F307178E2D63FA1649155C3A5F926B4C7D7C258BCA98EF90C7F4117C205E8E32C45D10E3D494059D2F2933891B979CE4A831B301B0550CDAE9B67064B31D8B481B85A5B046BE8FFA7BDB58DC0D7032525297F26FF619AF7F15BCEC0C92BCDCBC4FB207D115AA65CD04C1CF982191DF040103DF0314C165C48EB36DDF969DDC0B326312AFE2F6B52713DF070101",
//          "9F0605A000000152" +
//                  "9F22015BDF050420291231DF028190D3F45D065D4D900F68B2129AFA38F549AB9AE4619E5545814E468F382049A0B9776620DA60D62537F0705A2C926DBEAD4CA7CB43F0F0DD809584E9F7EFBDA3778747BC9E25C5606526FAB5E491646D4DD28278691C25956C8FED5E452F2442E25EDC6B0C1AA4B2E9EC4AD9B25A1B836295B823EDDC5EB6E1E0A3F41B28DB8C3B7E3E9B5979CD7E079EF024095A1D19DDDF040103DF03140000000000000000000000000000000000000000BF010131DF070101",
//          "9F0605A000000152" +
//                  "9F22015DDF050420241231DF0281F8AD938EA9888E5155F8CD272749172B3A8C504C17460EFA0BED7CBC5FD32C4A80FD810312281B5A35562800CDC325358A9639C501A537B7AE43DF263E6D232B811ACDB6DDE979D55D6C911173483993A423A0A5B1E1A70237885A241B8EEBB5571E2D32B41F9CC5514DF83F0D69270E109AF1422F985A52CCE04F3DF269B795155A68AD2D6B660DDCD759F0A5DA7B64104D22C2771ECE7A5FFD40C774E441379D1132FAF04CDF55B9504C6DCE9F61776D81C7C45F19B9EFB3749AC7D486A5AD2E781FA9D082FB2677665B99FA5F1553135A1FD2A2A9FBF625CA84A7D736521431178F13100A2516F9A43CE095B032B886C7A6AB126E203BE7DF040103DF0314B51EC5F7DE9BB6D8BCE8FB5F69BA57A04221F39BBF010131DF070101",
//          /*   // PBOC
//             "9F0605A000000333" +
//                     "9F220108DF050420291231DF028190B61645EDFD5498FB246444037A0FA18C0F101EBD8EFA54573CE6E6A7FBF63ED21D66340852B0211CF5EEF6A1CD989F66AF21A8EB19DBD8DBC3706D135363A0D683D046304F5A836BC1BC632821AFE7A2F75DA3C50AC74C545A754562204137169663CFCC0B06E67E2109EBA41BC67FF20CC8AC80D7B6EE1A95465B3B2657533EA56D92D539E5064360EA4850FED2D1BFDF040103DF0314EE23B616C95C02652AD18860E48787C079E8E85ABF010131DF070101",
//             "9F0605A000000333" +
//                     "9F220109DF050420291231DF0281B0EB374DFC5A96B71D2863875EDA2EAFB96B1B439D3ECE0B1826A2672EEEFA7990286776F8BD989A15141A75C384DFC14FEF9243AAB32707659BE9E4797A247C2F0B6D99372F384AF62FE23BC54BCDC57A9ACD1D5585C303F201EF4E8B806AFB809DB1A3DB1CD112AC884F164A67B99C7D6E5A8A6DF1D3CAE6D7ED3D5BE725B2DE4ADE23FA679BF4EB15A93D8A6E29C7FFA1A70DE2E54F593D908A3BF9EBBD760BBFDC8DB8B54497E6C5BE0E4A4DAC29E5DF040103DF0314A075306EAB0045BAF72CDD33B3B678779DE1F527BF010131DF070101",
//             // PBOC credit or debit
//             "9F0605A000000333" +
//                     "9F22010BDF050420291231DF0281F8CF9FDF46B356378E9AF311B0F981B21A1F22F250FB11F55C958709E3C7241918293483289EAE688A094C02C344E2999F315A72841F489E24B1BA0056CFAB3B479D0E826452375DCDBB67E97EC2AA66F4601D774FEAEF775ACCC621BFEB65FB0053FC5F392AA5E1D4C41A4DE9FFDFDF1327C4BB874F1F63A599EE3902FE95E729FD78D4234DC7E6CF1ABABAA3F6DB29B7F05D1D901D2E76A606A8CBFFFFECBD918FA2D278BDB43B0434F5D45134BE1C2781D157D501FF43E5F1C470967CD57CE53B64D82974C8275937C5D8502A1252A8A5D6088A259B694F98648D9AF2CB0EFD9D943C69F896D49FA39702162ACB5AF29B90BADE005BC157DF040103DF03140000000000000000000000000000000000000000BF010131DF070101",
//             "9F0605A000000333" +
//                     "9F220184DF050420291231DF0281B0F9EA5503CFE43038596C720645A94E0154793DE73AE5A935D1FB9D0FE77286B61261E3BB1D3DFEC547449992E2037C01FF4EFB88DA8A82F30FEA3198D5D16754247A1626E9CFFB4CD9E31399990E43FCA77C744A93685A260A20E6A607F3EE3FAE2ABBE99678C9F19DFD2D8EA76789239D13369D7D2D56AF3F2793068950B5BD808C462571662D4364B30A2582959DB238333BADACB442F9516B5C336C8A613FE014B7D773581AE10FDF7BDB2669012DDF040103DF03144D4E6D415F2CF8C394D40C49FB2459110578CF22BF010131DF070101",
//             "9F0605A000000333" +
//                     "9F220180DF050420291231DF0281809DD730669F27892944A68C0C62344C0E8EC57C2AA78004B014C26A0B0F3B517A0B60D355DFBC8929BBC59CEBCD0CCA13CDAA0C94E91C84A26E7DBE6B58595C4EFF2D717CB9EB965C15D287AF60AC28D06BC51282BC4A518B0EA3ABA9343F1778545FFB49EE840BBCEA457DBAABBFD755BA0F943A08A59CFFB6066B4084767599DF0403010001DF0314A35663495B5702D2ADE19104ADB8EE07F97E8B2ABF010131DF070101",
//             "9F0605A000000333" +
//                     "9F220183DF050420291231DF028190E46C9D054471D24A3DAEEA13875ECFB92C34D309106092E6AF57BD612C18E4E2BB3FBBC9E14F86D8660A065848B821347D04521578D4B789FD57231185DF92F45C5733C7912C291D7B13E649B094B33B1B75151C0E4E71E45CCDFD5217DC9F3EF39C3D324CA460DDC40C45CC27B2E421A2B409A47FAAEFD65F8A7F58A269B38CFD9C18210856A493A6624141677F5E95DF040103DF03141CC9BA05BC70F3D049F817404051122E35AC9683BF010131DF070101",
//             "9F0605A000000333" +
//                     "9F220185DF050420291231DF0281F8CD026B3E11A7234EFC24FB5976D9F51F7188A1598861AA8A6CA8D9A55300C6E6C39ED97E128973306E7D15DF603823A2C0C2E4C01C5AC0D4E71127DFEC69F2B17DAB12F2E8A84CD30AFC791AE71CD6D69D1B7E7648B2F0BB2140791C585E9CAC6642230B13C81A66E52E927681594EC08CFB30E10658F4199B8BF48B55F140925DEEEF4341E2C6C91E039944A5C44DD72379C2227F02105F462C0E977A2E79D2841143941EB4B4BC1ADAC274E3B0129DE7FDCC77C75BBC29A2861DCE7F748EBEE1E69339348667B729C2900EC6A6D43881622555FA8F8B85E18BD2B8B6F56EBD47643181FF7039D883CB5D723D9DEBD073A5A0CD7B980F0DDF040103DF031496C22F92B7644934F03B4065F1C37BC9DBEA45B0BF010131DF070101",
//        };

//        for (String rid : ridList) {
//
//            try {
//                if (rid.length() == 0) {
//                    continue;
//                }
//                EMVParamAppCaseA emvParamAppUMS = new EMVParamAppCaseA();
//                emvParamAppUMS.append(rid);
//                boolean bRet = ipboc.updateRID(ConstIPBOC.updateRID.operation.append, rid);
//
//                if (bRet) {
//                    Utility.DEBUG_LOG(TAG, "update RID success ");
//                } else {
//                    Utility.DEBUG_LOG(TAG, "update RID fails " + rid);
//                }
//
//            } catch (RemoteException e) {
//                Utility.DEBUG_LOG(TAG, "update RID exception!");
//                e.printStackTrace();
//            }
//        }
    }

//    public void setRID_temp(int type)
//    {
//        boolean isSuccess;
//        int index=0;
//        if (type == 3) {
//            // clear RID
//            isSuccess = false;
//            try {
//                isSuccess = ipboc.updateRID(3, null);
//                Utility.DEBUG_LOG(TAG, "Clear RID :" + isSuccess);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
//            return;
//        }
//
//
//        // append RID
//        // way 1#, set each tag & value
//        EMVParamKeyCaseA[] emvParamKeyUMS = new EMVParamKeyCaseA[index];
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "98");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "CA026E52A695E72BD30AF928196EEDC9FAF4A619F2492E3FB31169789C276FFBB7D43116647BA9E0D106A3542E3965292CF77823DD34CA8EEC7DE367E08070895077C7EFAD939924CB187067DBF92CB1E785917BD38BACE0C194CA12DF0CE5B7A50275AC61BE7C3B436887CA98C9FD39");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "E7AC9AA8EED1B5FF1BD532CF1489A3E5557572C1");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "99");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AB79FCC9520896967E776E64444E5DCDD6E13611874F3985722520425295EEA4BD0C2781DE7F31CD3D041F565F747306EED62954B17EDABA3A6C5B85A1DE1BEB9A34141AF38FCF8279C9DEA0D5A6710D08DB4124F041945587E20359BAB47B7575AD94262D4B25F264AF33DEDCF28E09615E937DE32EDC03C54445FE7E382777");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "4ABFFD6B1C51212D05552E431C5B17007D2F5E6D");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "95");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "BE9E1FA5E9A803852999C4AB432DB28600DCD9DAB76DFAAA47355A0FE37B1508AC6BF38860D3C6C2E5B12A3CAAF2A7005A7241EBAA7771112C74CF9A0634652FBCA0E5980C54A64761EA101A114E0F0B5572ADD57D010B7C9C887E104CA4EE1272DA66D997B9A90B5A6D624AB6C57E73C8F919000EB5F684898EF8C3DBEFB330C62660BED88EA78E909AFF05F6DA627B");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "EE1511CEC71020A9B90443B37B1D5F6E703030F6");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "92");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "996AF56F569187D09293C14810450ED8EE3357397B18A2458EFAA92DA3B6DF6514EC060195318FD43BE9B8F0CC669E3F844057CBDDF8BDA191BB64473BC8DC9A730DB8F6B4EDE3924186FFD9B8C7735789C23A36BA0B8AF65372EB57EA5D89E7D14E9C7B6B557460F10885DA16AC923F15AF3758F0F03EBD3C5C2C949CBA306DB44E6A2C076C5F67E281D7EF56785DC4D75945E491F01918800A9E2DC66F60080566CE0DAF8D17EAD46AD8E30A247C9F");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "429C954A3859CEF91295F663C963E582ED6EB253");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "94");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "ACD2B12302EE644F3F835ABD1FC7A6F62CCE48FFEC622AA8EF062BEF6FB8BA8BC68BBF6AB5870EED579BC3973E121303D34841A796D6DCBC41DBF9E52C4609795C0CCF7EE86FA1D5CB041071ED2C51D2202F63F1156C58A92D38BC60BDF424E1776E2BC9648078A03B36FB554375FC53D57C73F5160EA59F3AFC5398EC7B67758D65C9BFF7828B6B82D4BE124A416AB7301914311EA462C19F771F31B3B57336000DFF732D3B83DE07052D730354D297BEC72871DCCF0E193F171ABA27EE464C6A97690943D59BDABB2A27EB71CEEBDAFA1176046478FD62FEC452D5CA393296530AA3F41927ADFE434A2DF2AE3054F8840657A26E0FC617");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "C4A3C43CCF87327D136B804160E47D43B60E6E0F");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B3E5E667506C47CAAFB12A2633819350846697DD65A796E5CE77C57C626A66F70BB630911612AD2832909B8062291BECA46CD33B66A6F9C9D48CED8B4FC8561C8A1D8FB15862C9EB60178DEA2BE1F82236FFCFF4F3843C272179DCDD384D541053DA6A6A0D3CE48FDC2DC4E3E0EEE15F");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "FE70AB3B4D5A1B9924228ADF8027C758483A8B7E");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "05");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D0135CE8A4436C7F9D5CC66547E30EA402F98105B71722E24BC08DCC80AB7E71EC23B8CE6A1DC6AC2A8CF55543D74A8AE7B388F9B174B7F0D756C22CBB5974F9016A56B601CCA64C71F04B78E86C501B193A5556D5389ECE4DEA258AB97F52A3");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "86DF041E7995023552A79E2623E49180C0CD957A");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "90");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "C26B3CB3833E42D8270DC10C8999B2DA18106838650DA0DBF154EFD51100AD144741B2A87D6881F8630E3348DEA3F78038E9B21A697EB2A6716D32CBF26086F1");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "B3AE2BC3CAFC05EEEFAA46A2A47ED51DE679F823");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "97");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AF0754EAED977043AB6F41D6312AB1E22A6809175BEB28E70D5F99B2DF18CAE73519341BBBD327D0B8BE9D4D0E15F07D36EA3E3A05C892F5B19A3E9D3413B0D97E7AD10A5F5DE8E38860C0AD004B1E06F4040C295ACB457A788551B6127C0B29");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "8001CA76C1203955E2C62841CD6F201087E564BF");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "96");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B74586D19A207BE6627C5B0AAFBC44A2ECF5A2942D3A26CE19C4FFAEEE920521868922E893E7838225A3947A2614796FB2C0628CE8C11E3825A56D3B1BBAEF783A5C6A81F36F8625395126FA983C5216D3166D48ACDE8A431212FF763A7F79D9EDB7FED76B485DE45BEB829A3D4730848A366D3324C3027032FF8D16A1E44D8D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "7616E9AC8BE014AF88CA11A8FB17967B7394030E");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "B0");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D0F543F03F2517133EF2BA4A1104486758630DCFE3A883C77B4E4844E39A9BD6360D23E6644E1E071F196DDF2E4A68B4A3D93D14268D7240F6A14F0D714C17827D279D192E88931AF7300727AE9DA80A3F0E366AEBA61778171737989E1EE309");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "B1");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "B74586D19A207BE6627C5B0AAFBC44A2ECF5A2942D3A26CE19C4FFAEEE920521868922E893E7838225A3947A2614796FB2C0628CE8C11E3825A56D3B1BBAEF783A5C6A81F36F8625395126FA983C5216D3166D48ACDE8A431212FF763A7F79D9EDB7FED76B485DE45BEB829A3D4730848A366D3324C3027032FF8D16A1E44D8D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "FF");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "AF0754EAED977043AB6F41D6312AB1E22A6809175BEB28E70D5F99B2DF18CAE73519341BBBD327D0B8BE9D4D0E15F07D36EA3E3A05C892F5B19A3E9D3413B0D97E7AD10A5F5DE8E38860C0AD004B1E06F4040C295ACB457A788551B6127C0B29");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "89");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "E5E195705CE61A0672B8367E7A51713927A04289EA308328FAD28071ECEAE889B3C4F29AC3BDE46772B00D42FD05F27228820F2693990F81B0F6928E240D957EC4484354CD5E5CA9092B444741A0394D3476651232474A9B87A961DA8DD96D90F036E9B3C52FB09766BDA4D6BC3BDADBC89122B74068F8FA04026C5FA8EF398BC3AB3992A87F6A785CC779BA99F170956623D67A18EB8324263D626BE85BFF77B8B981C0A3F7849C4F3D8E20542955D19128198547B47AE34DF67F28BE433F33");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "7170850B97F83952045CF9CA8B7612DFEB69E9EF");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "53");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "BCD83721BE52CCCC4B6457321F22A7DC769F54EB8025913BE804D9EABBFA19B3D7C5D3CA658D768CAF57067EEC83C7E6E9F81D0586703ED9DDDADD20675D63424980B10EB364E81EB37DB40ED100344C928886FF4CCC37203EE6106D5B59D1AC102E2CD2D7AC17F4D96C398E5FD993ECB4FFDF79B17547FF9FA2AA8EEFD6CBDA124CBB17A0F8528146387135E226B005A474B9062FF264D2FF8EFA36814AA2950065B1B04C0A1AE9B2F69D4A4AA979D6CE95FEE9485ED0A03AEE9BD953E81CFD1EF6E814DFD3C2CE37AEFA38C1F9877371E91D6A5EB59FDEDF75D3325FA3CA66CDFBA0E57146CC789818FF06BE5FCC50ABD362AE4B80996D");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "A84A53964513A5D9363B4BA13AF5D43B83A83CE7");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "51");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "DB5FA29D1FDA8C1634B04DCCFF148ABEE63C772035C79851D3512107586E02A917F7C7E885E7C4A7D529710A145334CE67DC412CB1597B77AA2543B98D19CF2CB80C522BDBEA0F1B113FA2C86216C8C610A2D58F29CF3355CEB1BD3EF410D1EDD1F7AE0F16897979DE28C6EF293E0A19282BD1D793F1331523FC71A228800468C01A3653D14C6B4851A5C029478E757F");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "03");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "969299D792D3CC08AD28F2D544CEE3309DADF1B9");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        emvParamKeyUMS[index] = new EMVParamKeyCaseA();
//        emvParamKeyUMS[index].comment = "master card 13";
//        emvParamKeyUMS[index].flagAppendRemoveClear = ConstIPBOC.updateRID.operation.append;
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_RID_9F06, "A000000003");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Index_9F22, "50");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_ExpiryDate_DF05, "3230323931323331");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Algorithm_DF07, "01");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_KEY_DF02, "D11197590057B84196C2F4D11A8F3C05408F422A35D702F90106EA5B019BB28AE607AA9CDEBCD0D81A38D48C7EBB0062D287369EC0C42124246AC30D80CD602AB7238D51084DED4698162C59D25EAC1E66255B4DB2352526EF0982C3B8AD3D1CCE85B01DB5788E75E09F44BE7361366DEF9D1E1317B05E5D0FF5290F88A0DB47");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Exponent_DF04, "010001");
//        emvParamKeyUMS[index].append(EMVParamKey.TAG_Hash_DF03, "B769775668CACB5D22A647D1D993141EDAB7237B");
//        Utility.DEBUG_LOG(TAG, emvParamKeyUMS[index].getTlvString());
//        index++;
//
//
//        try {
//            for (int i = 0; i < index; i++) {
////                for (int i = 0; i < 37; i++) {
//                boolean bRet = ipboc.updateRID(emvParamKeyUMS[i].flagAppendRemoveClear, emvParamKeyUMS[i].getTlvString());
//                if (bRet) {
//                    Utility.DEBUG_LOG(TAG, "update RID success2 for " + emvParamKeyUMS[i].getComment() + emvParamKeyUMS[i].getTlvString());
//                } else {
//                    Utility.DEBUG_LOG(TAG, "update RID fails2 for " + emvParamKeyUMS[i].getComment() + emvParamKeyUMS[i].getTlvString());
//                }
//            }
//
//        } catch (RemoteException e) {
//            // Utility.DEBUG_LOG( TAG, "update RID for " + emvParamKeyUMS[i].comment + ", exception!");
//            e.printStackTrace();
//        }
//        // way 2#, set one tlv string
//        // hardcoding some rid
////        String[] ridList = new String[]{
////                // VISA
////          "9F0605A000000003"+
////                  "9F220199"+
////                  "DF050420291231"+
////                  "DF028180AB79FCC9520896967E776E64444E5DCDD6E13611874F3985722520425295EEA4BD0C2781DE7F31CD3D041F565F747306EED62954B17EDABA3A6C5B85A1DE1BEB9A34141AF38FCF8279C9DEA0D5A6710D08DB4124F041945587E20359BAB47B7575AD94262D4B25F264AF33DEDCF28E09615E937DE32EDC03C54445FE7E382777" +
////                  "DF040103" +
////                  "DF03144ABFFD6B1C51212D05552E431C5B17007D2F5E6D" +
////                  "BF010131" +
////                  "DF070101",
////          "9F0605A000000003" +
////                  "9F220195DF050420291231DF028190BE9E1FA5E9A803852999C4AB432DB28600DCD9DAB76DFAAA47355A0FE37B1508AC6BF38860D3C6C2E5B12A3CAAF2A7005A7241EBAA7771112C74CF9A0634652FBCA0E5980C54A64761EA101A114E0F0B5572ADD57D010B7C9C887E104CA4EE1272DA66D997B9A90B5A6D624AB6C57E73C8F919000EB5F684898EF8C3DBEFB330C62660BED88EA78E909AFF05F6DA627BDF040103DF0314EE1511CEC71020A9B90443B37B1D5F6E703030F6BF010131DF070101",
////          "9F0605A000000003" +
////                  "9F220192DF050420291231DF0281B0996AF56F569187D09293C14810450ED8EE3357397B18A2458EFAA92DA3B6DF6514EC060195318FD43BE9B8F0CC669E3F844057CBDDF8BDA191BB64473BC8DC9A730DB8F6B4EDE3924186FFD9B8C7735789C23A36BA0B8AF65372EB57EA5D89E7D14E9C7B6B557460F10885DA16AC923F15AF3758F0F03EBD3C5C2C949CBA306DB44E6A2C076C5F67E281D7EF56785DC4D75945E491F01918800A9E2DC66F60080566CE0DAF8D17EAD46AD8E30A247C9FDF040103DF0314429C954A3859CEF91295F663C963E582ED6EB253BF010131DF070101",
////          "9F0605A000000003" +
////                  "9F220107DF050420291231DF028190A89F25A56FA6DA258C8CA8B40427D927B4A1EB4D7EA326BBB12F97DED70AE5E4480FC9C5E8A972177110A1CC318D06D2F8F5C4844AC5FA79A4DC470BB11ED635699C17081B90F1B984F12E92C1C529276D8AF8EC7F28492097D8CD5BECEA16FE4088F6CFAB4A1B42328A1B996F9278B0B7E3311CA5EF856C2F888474B83612A82E4E00D0CD4069A6783140433D50725FDF040103DF0314B4BC56CC4E88324932CBC643D6898F6FE593B172BF010131DF070101",
////          "9F0605A000000003" +
////                  "9F220108DF050420291231DF0281B0D9FD6ED75D51D0E30664BD157023EAA1FFA871E4DA65672B863D255E81E137A51DE4F72BCC9E44ACE12127F87E263D3AF9DD9CF35CA4A7B01E907000BA85D24954C2FCA3074825DDD4C0C8F186CB020F683E02F2DEAD3969133F06F7845166ACEB57CA0FC2603445469811D293BFEFBAFAB57631B3DD91E796BF850A25012F1AE38F05AA5C4D6D03B1DC2E568612785938BBC9B3CD3A910C1DA55A5A9218ACE0F7A21287752682F15832A678D6E1ED0BDF040103DF031420D213126955DE205ADC2FD2822BD22DE21CF9A8BF010131DF070101",
////          "9F0605A000000003" +
////                  "9F220109DF050420291231DF0281F89D912248DE0A4E39C1A7DDE3F6D2588992C1A4095AFBD1824D1BA74847F2BC4926D2EFD904B4B54954CD189A54C5D1179654F8F9B0D2AB5F0357EB642FEDA95D3912C6576945FAB897E7062CAA44A4AA06B8FE6E3DBA18AF6AE3738E30429EE9BE03427C9D64F695FA8CAB4BFE376853EA34AD1D76BFCAD15908C077FFE6DC5521ECEF5D278A96E26F57359FFAEDA19434B937F1AD999DC5C41EB11935B44C18100E857F431A4A5A6BB65114F174C2D7B59FDF237D6BB1DD0916E644D709DED56481477C75D95CDD68254615F7740EC07F330AC5D67BCD75BF23D28A140826C026DBDE971A37CD3EF9B8DF644AC385010501EFC6509D7A41DF040103DF03141FF80A40173F52D7D27E0F26A146A1C8CCB29046BF010131DF070101",
////          "9F0605A000000003" +
////                  "9F220101DF050420291231DF028180C696034213D7D8546984579D1D0F0EA519CFF8DEFFC429354CF3A871A6F7183F1228DA5C7470C055387100CB935A712C4E2864DF5D64BA93FE7E63E71F25B1E5F5298575EBE1C63AA617706917911DC2A75AC28B251C7EF40F2365912490B939BCA2124A30A28F54402C34AECA331AB67E1E79B285DD5771B5D9FF79EA630B75DF040103DF0314D34A6A776011C7E7CE3AEC5F03AD2F8CFC5503CCBF010131DF070101",
////          "9F0605A000000003" +
////                  "9F220103DF050420291231DF0270B3E5E667506C47CAAFB12A2633819350846697DD65A796E5CE77C57C626A66F70BB630911612AD2832909B8062291BECA46CD33B66A6F9C9D48CED8B4FC8561C8A1D8FB15862C9EB60178DEA2BE1F82236FFCFF4F3843C272179DCDD384D541053DA6A6A0D3CE48FDC2DC4E3E0EEE15FDF040103DF0314FE70AB3B4D5A1B9924228ADF8027C758483A8B7EBF010131DF070101",
////
////          // MasterCard
////          "9F0605A000000004" +
////                  "9F2201EFDF050420291231DF0281F8A191CB87473F29349B5D60A88B3EAEE0973AA6F1A082F358D849FDDFF9C091F899EDA9792CAF09EF28F5D22404B88A2293EEBBC1949C43BEA4D60CFD879A1539544E09E0F09F60F065B2BF2A13ECC705F3D468B9D33AE77AD9D3F19CA40F23DCF5EB7C04DC8F69EBA565B1EBCB4686CD274785530FF6F6E9EE43AA43FDB02CE00DAEC15C7B8FD6A9B394BABA419D3F6DC85E16569BE8E76989688EFEA2DF22FF7D35C043338DEAA982A02B866DE5328519EBBCD6F03CDD686673847F84DB651AB86C28CF1462562C577B853564A290C8556D818531268D25CC98A4CC6A0BDFFFDA2DCCA3A94C998559E307FDDF915006D9A987B07DDAEB3BDF040103DF031421766EBB0EE122AFB65D7845B73DB46BAB65427ABF010131DF070101",
////          "9F0605A000000004" +
////                  "9F2201FADF050420291231DF028190A90FCD55AA2D5D9963E35ED0F440177699832F49C6BAB15CDAE5794BE93F934D4462D5D12762E48C38BA83D8445DEAA74195A301A102B2F114EADA0D180EE5E7A5C73E0C4E11F67A43DDAB5D55683B1474CC0627F44B8D3088A492FFAADAD4F42422D0E7013536C3C49AD3D0FAE96459B0F6B1B6056538A3D6D44640F94467B108867DEC40FAAECD740C00E2B7A8852DDF040103DF03142CFBB82409ED86A31973B0E0CEEA381BC43C8097BF010131DF070101",
////          "9F0605A000000004" +
////                  "9F220104DF050420291231DF028190A6DA428387A502D7DDFB7A74D3F412BE762627197B25435B7A81716A700157DDD06F7CC99D6CA28C2470527E2C03616B9C59217357C2674F583B3BA5C7DCF2838692D023E3562420B4615C439CA97C44DC9A249CFCE7B3BFB22F68228C3AF13329AA4A613CF8DD853502373D62E49AB256D2BC17120E54AEDCED6D96A4287ACC5C04677D4A5A320DB8BEE2F775E5FEC5DF040103DF0314381A035DA58B482EE2AF75F4C3F2CA469BA4AA6CBF010131DF070101",
////          "9F0605A000000004" +
////                  "9F220105DF050420291231DF0281B0B8048ABC30C90D976336543E3FD7091C8FE4800DF820ED55E7E94813ED00555B573FECA3D84AF6131A651D66CFF4284FB13B635EDD0EE40176D8BF04B7FD1C7BACF9AC7327DFAA8AA72D10DB3B8E70B2DDD811CB4196525EA386ACC33C0D9D4575916469C4E4F53E8E1C912CC618CB22DDE7C3568E90022E6BBA770202E4522A2DD623D180E215BD1D1507FE3DC90CA310D27B3EFCCD8F83DE3052CAD1E48938C68D095AAC91B5F37E28BB49EC7ED597DF040103DF0314EBFA0D5D06D8CE702DA3EAE890701D45E274C845BF010131DF070101",
////          "9F0605A000000004" +
////                  "9F220106DF050420291231DF0281F8CB26FC830B43785B2BCE37C81ED334622F9622F4C89AAE641046B2353433883F307FB7C974162DA72F7A4EC75D9D657336865B8D3023D3D645667625C9A07A6B7A137CF0C64198AE38FC238006FB2603F41F4F3BB9DA1347270F2F5D8C606E420958C5F7D50A71DE30142F70DE468889B5E3A08695B938A50FC980393A9CBCE44AD2D64F630BB33AD3F5F5FD495D31F37818C1D94071342E07F1BEC2194F6035BA5DED3936500EB82DFDA6E8AFB655B1EF3D0D7EBF86B66DD9F29F6B1D324FE8B26CE38AB2013DD13F611E7A594D675C4432350EA244CC34F3873CBA06592987A1D7E852ADC22EF5A2EE28132031E48F74037E3B34AB747FDF040103DF0314F910A1504D5FFB793D94F3B500765E1ABCAD72D9BF010131DF070101",
////          "9F0605A000000004" +
////                  "9F2201F1DF050420231231DF0281B0A0DCF4BDE19C3546B4B6F0414D174DDE294AABBB828C5A834D73AAE27C99B0B053A90278007239B6459FF0BBCD7B4B9C6C50AC02CE91368DA1BD21AAEADBC65347337D89B68F5C99A09D05BE02DD1F8C5BA20E2F13FB2A27C41D3F85CAD5CF6668E75851EC66EDBF98851FD4E42C44C1D59F5984703B27D5B9F21B8FA0D93279FBBF69E090642909C9EA27F898959541AA6757F5F624104F6E1D3A9532F2A6E51515AEAD1B43B3D7835088A2FAFA7BE7DF040103DF0314D8E68DA167AB5A85D8C3D55ECB9B0517A1A5B4BBBF010131DF070101",
////          "9F0605A000000004" +
////                  "9F220103DF050420291231DF028180C2490747FE17EB0584C88D47B1602704150ADC88C5B998BD59CE043EDEBF0FFEE3093AC7956AD3B6AD4554C6DE19A178D6DA295BE15D5220645E3C8131666FA4BE5B84FE131EA44B039307638B9E74A8C42564F892A64DF1CB15712B736E3374F1BBB6819371602D8970E97B900793C7C2A89A4A1649A59BE680574DD0B60145DF040103DF03145ADDF21D09278661141179CBEFF272EA384B13BBBF010131DF070101",
////          "9F0605A000000004" +
////                  "9F220109DF050420291231DF028180C132F436477A59302E885646102D913EC86A95DD5D0A56F625F472B67F52179BC8BD258A7CD43EF1720AC0065519E3FFCECC26F978EDF9FB8C6ECDF145FDCC697D6B72562FA2E0418B2B80A038D0DC3B769EB027484087CCE6652488D2B3816742AC9C2355B17411C47EACDD7467566B302F512806E331FAD964BF000169F641DF040103DF0300BF010131DF070101",
////
////
////          // JCB
////          "9F0605A000000065" +
////                  "9F22010FDF050420291231DF0281909EFBADDE4071D4EF98C969EB32AF854864602E515D6501FDE576B310964A4F7C2CE842ABEFAFC5DC9E26A619BCF2614FE07375B9249BEFA09CFEE70232E75FFD647571280C76FFCA87511AD255B98A6B577591AF01D003BD6BF7E1FCE4DFD20D0D0297ED5ECA25DE261F37EFE9E175FB5F12D2503D8CFB060A63138511FE0E125CF3A643AFD7D66DCF9682BD246DDEA1DF040103DF03142A1B82DE00F5F0C401760ADF528228D3EDE0F403BF010131DF070101",
////          "9F0605A000000065" +
////                  "9F220113DF050420250101DF0281F8A3270868367E6E29349FC2743EE545AC53BD3029782488997650108524FD051E3B6EACA6A9A6C1441D28889A5F46413C8F62F3645AAEB30A1521EEF41FD4F3445BFA1AB29F9AC1A74D9A16B93293296CB09162B149BAC22F88AD8F322D684D6B49A12413FC1B6AC70EDEDB18EC1585519A89B50B3D03E14063C2CA58B7C2BA7FB22799A33BCDE6AFCBEB4A7D64911D08D18C47F9BD14A9FAD8805A15DE5A38945A97919B7AB88EFA11A88C0CD92C6EE7DC352AB0746ABF13585913C8A4E04464B77909C6BD94341A8976C4769EA6C0D30A60F4EE8FA19E767B170DF4FA80312DBA61DB645D5D1560873E2674E1F620083F30180BD96CA589DF040103DF031454CFAE617150DFA09D3F901C9123524523EBEDF3BF010131DF070101",
////          //
////          "9F0605A000000152" +//fails...
////                  "9F22015CDF050420291231DF0281B0833F275FCF5CA4CB6F1BF880E54DCFEB721A316692CAFEB28B698CAECAFA2B2D2AD8517B1EFB59DDEFC39F9C3B33DDEE40E7A63C03E90A4DD261BC0F28B42EA6E7A1F307178E2D63FA1649155C3A5F926B4C7D7C258BCA98EF90C7F4117C205E8E32C45D10E3D494059D2F2933891B979CE4A831B301B0550CDAE9B67064B31D8B481B85A5B046BE8FFA7BDB58DC0D7032525297F26FF619AF7F15BCEC0C92BCDCBC4FB207D115AA65CD04C1CF982191DF040103DF0314C165C48EB36DDF969DDC0B326312AFE2F6B52713DF070101",
////          "9F0605A000000152" +
////                  "9F22015BDF050420291231DF028190D3F45D065D4D900F68B2129AFA38F549AB9AE4619E5545814E468F382049A0B9776620DA60D62537F0705A2C926DBEAD4CA7CB43F0F0DD809584E9F7EFBDA3778747BC9E25C5606526FAB5E491646D4DD28278691C25956C8FED5E452F2442E25EDC6B0C1AA4B2E9EC4AD9B25A1B836295B823EDDC5EB6E1E0A3F41B28DB8C3B7E3E9B5979CD7E079EF024095A1D19DDDF040103DF03140000000000000000000000000000000000000000BF010131DF070101",
////          "9F0605A000000152" +
////                  "9F22015DDF050420241231DF0281F8AD938EA9888E5155F8CD272749172B3A8C504C17460EFA0BED7CBC5FD32C4A80FD810312281B5A35562800CDC325358A9639C501A537B7AE43DF263E6D232B811ACDB6DDE979D55D6C911173483993A423A0A5B1E1A70237885A241B8EEBB5571E2D32B41F9CC5514DF83F0D69270E109AF1422F985A52CCE04F3DF269B795155A68AD2D6B660DDCD759F0A5DA7B64104D22C2771ECE7A5FFD40C774E441379D1132FAF04CDF55B9504C6DCE9F61776D81C7C45F19B9EFB3749AC7D486A5AD2E781FA9D082FB2677665B99FA5F1553135A1FD2A2A9FBF625CA84A7D736521431178F13100A2516F9A43CE095B032B886C7A6AB126E203BE7DF040103DF0314B51EC5F7DE9BB6D8BCE8FB5F69BA57A04221F39BBF010131DF070101",
////          /*   // PBOC
////             "9F0605A000000333" +
////                     "9F220108DF050420291231DF028190B61645EDFD5498FB246444037A0FA18C0F101EBD8EFA54573CE6E6A7FBF63ED21D66340852B0211CF5EEF6A1CD989F66AF21A8EB19DBD8DBC3706D135363A0D683D046304F5A836BC1BC632821AFE7A2F75DA3C50AC74C545A754562204137169663CFCC0B06E67E2109EBA41BC67FF20CC8AC80D7B6EE1A95465B3B2657533EA56D92D539E5064360EA4850FED2D1BFDF040103DF0314EE23B616C95C02652AD18860E48787C079E8E85ABF010131DF070101",
////             "9F0605A000000333" +
////                     "9F220109DF050420291231DF0281B0EB374DFC5A96B71D2863875EDA2EAFB96B1B439D3ECE0B1826A2672EEEFA7990286776F8BD989A15141A75C384DFC14FEF9243AAB32707659BE9E4797A247C2F0B6D99372F384AF62FE23BC54BCDC57A9ACD1D5585C303F201EF4E8B806AFB809DB1A3DB1CD112AC884F164A67B99C7D6E5A8A6DF1D3CAE6D7ED3D5BE725B2DE4ADE23FA679BF4EB15A93D8A6E29C7FFA1A70DE2E54F593D908A3BF9EBBD760BBFDC8DB8B54497E6C5BE0E4A4DAC29E5DF040103DF0314A075306EAB0045BAF72CDD33B3B678779DE1F527BF010131DF070101",
////             // PBOC credit or debit
////             "9F0605A000000333" +
////                     "9F22010BDF050420291231DF0281F8CF9FDF46B356378E9AF311B0F981B21A1F22F250FB11F55C958709E3C7241918293483289EAE688A094C02C344E2999F315A72841F489E24B1BA0056CFAB3B479D0E826452375DCDBB67E97EC2AA66F4601D774FEAEF775ACCC621BFEB65FB0053FC5F392AA5E1D4C41A4DE9FFDFDF1327C4BB874F1F63A599EE3902FE95E729FD78D4234DC7E6CF1ABABAA3F6DB29B7F05D1D901D2E76A606A8CBFFFFECBD918FA2D278BDB43B0434F5D45134BE1C2781D157D501FF43E5F1C470967CD57CE53B64D82974C8275937C5D8502A1252A8A5D6088A259B694F98648D9AF2CB0EFD9D943C69F896D49FA39702162ACB5AF29B90BADE005BC157DF040103DF03140000000000000000000000000000000000000000BF010131DF070101",
////             "9F0605A000000333" +
////                     "9F220184DF050420291231DF0281B0F9EA5503CFE43038596C720645A94E0154793DE73AE5A935D1FB9D0FE77286B61261E3BB1D3DFEC547449992E2037C01FF4EFB88DA8A82F30FEA3198D5D16754247A1626E9CFFB4CD9E31399990E43FCA77C744A93685A260A20E6A607F3EE3FAE2ABBE99678C9F19DFD2D8EA76789239D13369D7D2D56AF3F2793068950B5BD808C462571662D4364B30A2582959DB238333BADACB442F9516B5C336C8A613FE014B7D773581AE10FDF7BDB2669012DDF040103DF03144D4E6D415F2CF8C394D40C49FB2459110578CF22BF010131DF070101",
////             "9F0605A000000333" +
////                     "9F220180DF050420291231DF0281809DD730669F27892944A68C0C62344C0E8EC57C2AA78004B014C26A0B0F3B517A0B60D355DFBC8929BBC59CEBCD0CCA13CDAA0C94E91C84A26E7DBE6B58595C4EFF2D717CB9EB965C15D287AF60AC28D06BC51282BC4A518B0EA3ABA9343F1778545FFB49EE840BBCEA457DBAABBFD755BA0F943A08A59CFFB6066B4084767599DF0403010001DF0314A35663495B5702D2ADE19104ADB8EE07F97E8B2ABF010131DF070101",
////             "9F0605A000000333" +
////                     "9F220183DF050420291231DF028190E46C9D054471D24A3DAEEA13875ECFB92C34D309106092E6AF57BD612C18E4E2BB3FBBC9E14F86D8660A065848B821347D04521578D4B789FD57231185DF92F45C5733C7912C291D7B13E649B094B33B1B75151C0E4E71E45CCDFD5217DC9F3EF39C3D324CA460DDC40C45CC27B2E421A2B409A47FAAEFD65F8A7F58A269B38CFD9C18210856A493A6624141677F5E95DF040103DF03141CC9BA05BC70F3D049F817404051122E35AC9683BF010131DF070101",
////             "9F0605A000000333" +
////                     "9F220185DF050420291231DF0281F8CD026B3E11A7234EFC24FB5976D9F51F7188A1598861AA8A6CA8D9A55300C6E6C39ED97E128973306E7D15DF603823A2C0C2E4C01C5AC0D4E71127DFEC69F2B17DAB12F2E8A84CD30AFC791AE71CD6D69D1B7E7648B2F0BB2140791C585E9CAC6642230B13C81A66E52E927681594EC08CFB30E10658F4199B8BF48B55F140925DEEEF4341E2C6C91E039944A5C44DD72379C2227F02105F462C0E977A2E79D2841143941EB4B4BC1ADAC274E3B0129DE7FDCC77C75BBC29A2861DCE7F748EBEE1E69339348667B729C2900EC6A6D43881622555FA8F8B85E18BD2B8B6F56EBD47643181FF7039D883CB5D723D9DEBD073A5A0CD7B980F0DDF040103DF031496C22F92B7644934F03B4065F1C37BC9DBEA45B0BF010131DF070101",
////        };
//
////        for (String rid : ridList) {
////
////            try {
////                if (rid.length() == 0) {
////                    continue;
////                }
////                EMVParamAppCaseA emvParamAppUMS = new EMVParamAppCaseA();
////                emvParamAppUMS.append(rid);
////                boolean bRet = ipboc.updateRID(ConstIPBOC.updateRID.operation.append, rid);
////
////                if (bRet) {
////                    Utility.DEBUG_LOG(TAG, "update RID success ");
////                } else {
////                    Utility.DEBUG_LOG(TAG, "update RID fails " + rid);
////                }
////
////            } catch (RemoteException e) {
////                Utility.DEBUG_LOG(TAG, "update RID exception!");
////                e.printStackTrace();
////            }
////        }
//    }
}


