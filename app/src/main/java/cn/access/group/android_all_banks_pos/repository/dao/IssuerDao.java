package cn.access.group.android_all_banks_pos.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.Issuer;

/**
 * Created by Syed Masood Azeem on 12-21-2020.
 */
@Dao
public interface IssuerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIssuerConfig(List<Issuer> issuerList);
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertManyConfig(List<TerminalConfig> terminalConfig);
//
    @Query("SELECT * FROM Issuer ")
    List<Issuer> issuerGetConfig();
//
//    @Query("SELECT * FROM terminalconfig WHERE tcid LIKE  :tcid ")
//    TerminalConfig terminalConfigurationByID(int tcid);
//
    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateIssuerConfig(Issuer issuer);
//
    @Query("DELETE FROM Issuer")
    void nukeTable();
}
