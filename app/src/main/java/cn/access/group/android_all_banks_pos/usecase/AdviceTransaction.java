package cn.access.group.android_all_banks_pos.usecase;


import android.annotation.SuppressLint;
import android.util.SparseArray;
import android.util.Log;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;

public class AdviceTransaction
{
    private static final String TAG = "AdviceTransaction";
    private List<TransactionDetail> transactionsToUploadAdvice;
    private List<SparseArray<String>> data8583 = new ArrayList<>();
    AppDatabase appDatabase;

    @SuppressLint("CheckResult")
    public AdviceTransaction(AppDatabase appDatabase)
    {
        Utility.DEBUG_LOG(TAG,"+ AdviceTransaction +");
        this.appDatabase = appDatabase;
        appDatabase.transactionDetailDao().findAllAdviceTransactions()
//                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactionDetailList -> {
                    transactionsToUploadAdvice=transactionDetailList;
                },throwable ->{
                    Utility.DEBUG_LOG(TAG,"some error");
                    Utility.DEBUG_LOG(TAG,"transactionsToUploadAdvice:"+transactionsToUploadAdvice);
                }// there was an error
                );

    }


    public List<SparseArray<String>> getAdviceTransactionsList(){

        SparseArray<String> data8583_u_purchase = new SparseArray<>();
        for(int i=0;i<transactionsToUploadAdvice.size();i++) {
            data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.ADVICE_MESSAGE_TYPE);
            data8583_u_purchase.put(ISO8583u.F_AccountNumber_02, transactionsToUploadAdvice.get(i).getCardNo());
            data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03,  "020000");
            String amount = "";
            if(transactionsToUploadAdvice.get(i).getTxnType().equals("VOID") && transactionsToUploadAdvice.get(i).getCancelTxnType().equals("ADJUST")) {
                amount = "000000000000";
            }
            else {
                amount = String.format(Locale.getDefault(), "%012d", (int) (Double.parseDouble(transactionsToUploadAdvice.get(i).getAmount()) * 100));
            }
            data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
            data8583_u_purchase.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
            data8583_u_purchase.put(ISO8583u.F_TxTime_12,  transactionsToUploadAdvice.get(i).getTxnTime());
            data8583_u_purchase.put(ISO8583u.F_TxDate_13,  transactionsToUploadAdvice.get(i).getTxnDate());

            String dateOfExpiry = transactionsToUploadAdvice.get(i).getCardexpiry().replaceAll("[\\/]", "");


            String dateOfExpiry2 = dateOfExpiry.substring(2,4)  + dateOfExpiry.substring(0,2);


            data8583_u_purchase.put(ISO8583u.F_DateOfExpired_14, dateOfExpiry2);


            Utility.DEBUG_LOG("MA exp", dateOfExpiry);
            Utility.DEBUG_LOG("MA exp", dateOfExpiry2);
            //14
//            if(transactionsToUploadAdvice.get(i).getCardType().equals("chip")){
//                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
//            }
//            else {
//                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
//            }
            data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, transactionsToUploadAdvice.get(i).getPosEntryMode());
            data8583_u_purchase.put(ISO8583u.F_NII_24, Constants.NII);
            data8583_u_purchase.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
            Utility.DEBUG_LOG(TAG,"before put F_Track_2_Data_35 advice");
            data8583_u_purchase.put(ISO8583u.F_Track_2_Data_35,  transactionsToUploadAdvice.get(i).getTrack2());
           data8583_u_purchase.put(ISO8583u.F_RRN_37,  transactionsToUploadAdvice.get(i).getRrn());

            data8583_u_purchase.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38,  transactionsToUploadAdvice.get(i).getAuthorizationIdentificationResponseCode());
            data8583_u_purchase.put(ISO8583u.F_TID_41, transactionsToUploadAdvice.get(i).getTId());
            data8583_u_purchase.put(ISO8583u.F_MerchantID_42, transactionsToUploadAdvice.get(i).getMId());
//            data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, Constants.RESERVED_NATIONAL);
            //Constants cn = new Constants();
            if(TIP_ENABLED.equals("Y")){
                if(transactionsToUploadAdvice.get(i).getTxnType().equals("VOID") && transactionsToUploadAdvice.get(i).getCancelTxnType().equals("ADJUST")) {
                    data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, "000000000000");
                }else {
                    data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(transactionsToUploadAdvice.get(i).getTipAmount()) * 100)));
                }
            }
            data8583_u_purchase.put(ISO8583u.F_ServiceCode_61, Constants.FIELD_61);//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
            data8583_u_purchase.put(ISO8583u.F_KeyExchange_62, transactionsToUploadAdvice.get(i).getInvoiceNo());//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
//            if(Constants.HOST_INDEX_FROM_DB==0){
//                data8583_u_purchase.put(ISO8583u.F_KeyExchange_62,transactionsToUploadAdvice.get(i).getInvoiceNo());
//                ////////////////////////////////////////////////////////msgtype+transstan............................+12spaces...... 22total length
////                data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, "0200"+transactionsToUploadAdvice.get(i).getStan()+"            ");
//            }
            data8583.add(i, data8583_u_purchase);

        }
        return data8583;
    }

    public TransactionDetail getTopAdviceTransaction()
    {
        TransactionDetail rv;
        Utility.DEBUG_LOG(TAG,"+ getTopAdviceTransaction +");
        rv = transactionsToUploadAdvice.get(0);
        return rv;
    }
    public SparseArray<String> getTopAdviceInStringFormat(){

        Utility.DEBUG_LOG(TAG,"+ getTopAdviceInStringFormat +");
        SparseArray<String> data8583_u_purchase=null;
        if ( transactionsToUploadAdvice.size() == 0 )
        {
            Utility.DEBUG_LOG(TAG,"No record found");
            return data8583_u_purchase;
        }
        data8583_u_purchase = new SparseArray<>();
        data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.ADVICE_MESSAGE_TYPE);
        data8583_u_purchase.put(ISO8583u.F_AccountNumber_02, transactionsToUploadAdvice.get(0).getCardNo());
//        if(transactionsToUploadAdvice.get(0).getTxnType().equals("COMPLETION")){
//            data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03, "000000");
//        }else {
//            data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03, "020000");
//        }
        data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03, transactionsToUploadAdvice.get(0).getProcessingCode());
        String amount = "";

        if(transactionsToUploadAdvice.get(0).getTxnType().equals("VOID") && (transactionsToUploadAdvice.get(0).isCompletion() || transactionsToUploadAdvice.get(0).getCancelTxnType().equals("ADJUST"))) {
            amount = "000000000000";
        }
        else {
            amount = String.format(Locale.getDefault(), "%012.0f", (Double)(Double.parseDouble(transactionsToUploadAdvice.get(0).getAmount()) * 100));
        }
        Utility.DEBUG_LOG(TAG,"Amount:"+amount);
        data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
        data8583_u_purchase.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_u_purchase.put(ISO8583u.F_TxTime_12,  transactionsToUploadAdvice.get(0).getTxnTime());
        data8583_u_purchase.put(ISO8583u.F_TxDate_13,  transactionsToUploadAdvice.get(0).getTxnDate());
        String dateOfExpiry = transactionsToUploadAdvice.get(0).getCardexpiry().replaceAll("[\\/]", "");

        String dateOfExpiry2 = dateOfExpiry.substring(2,4)  + dateOfExpiry.substring(0,2);


        data8583_u_purchase.put(ISO8583u.F_DateOfExpired_14, dateOfExpiry2);


        Utility.DEBUG_LOG("MA exp", dateOfExpiry);
        Utility.DEBUG_LOG("MA exp", dateOfExpiry2);
        //14
//            if(transactionsToUploadAdvice.get(0).getCardType().equals("chip")){
//                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
//            }
//            else {
//                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
//            }
        data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, transactionsToUploadAdvice.get(0).getPosEntryMode());
        data8583_u_purchase.put(ISO8583u.F_NII_24, Constants.NII);
        data8583_u_purchase.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        Utility.DEBUG_LOG(TAG,"before put F_Track_2_Data_35 advice string");
        data8583_u_purchase.put(ISO8583u.F_Track_2_Data_35,  transactionsToUploadAdvice.get(0).getTrack2());
        data8583_u_purchase.put(ISO8583u.F_RRN_37,  transactionsToUploadAdvice.get(0).getRrn());
        Utility.DEBUG_LOG(TAG,"field-38:"+transactionsToUploadAdvice.get(0).getAuthorizationIdentificationResponseCode());
        data8583_u_purchase.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38,  transactionsToUploadAdvice.get(0).getAuthorizationIdentificationResponseCode());
        data8583_u_purchase.put(ISO8583u.F_TID_41, transactionsToUploadAdvice.get(0).getTId());
        data8583_u_purchase.put(ISO8583u.F_MerchantID_42, transactionsToUploadAdvice.get(0).getMId());
//            data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, Constants.RESERVED_NATIONAL);
        //Constants cn = new Constants();
        if(TIP_ENABLED.equals("Y")){
            if(transactionsToUploadAdvice.get(0).getTxnType().equals("VOID") && (transactionsToUploadAdvice.get(0).isCompletion() || transactionsToUploadAdvice.get(0).getCancelTxnType().equals("ADJUST"))) {
                data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, "000000000000");
            }else {
                if(transactionsToUploadAdvice.get(0).getTxnType().equals("ADJUST") || transactionsToUploadAdvice.get(0).getTxnType().equals("SALE") || (transactionsToUploadAdvice.get(0).getTxnType().equals("VOID") && transactionsToUploadAdvice.get(0).getCancelTxnType().equals("SALE"))) {
                    data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(transactionsToUploadAdvice.get(0).getTipAmount()) * 100)));
                }
            }
          //  data8583_u_purchase.put(ISO8583u.F_BalancAmount_54,String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(transactionsToUploadAdvice.get(0).getTipAmount()) * 100)));
        }

        data8583_u_purchase.put(ISO8583u.F_ServiceCode_61, Constants.FIELD_61);//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
        data8583_u_purchase.put(ISO8583u.F_KeyExchange_62, transactionsToUploadAdvice.get(0).getInvoiceNo());//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
//            if(Constants.HOST_INDEX_FROM_DB==0){
//                data8583_u_purchase.put(ISO8583u.F_KeyExchange_62,transactionsToUploadAdvice.get(0).getInvoiceNo());
//                ////////////////////////////////////////////////////////msgtype+transstan............................+12spaces...... 22total length
////                data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, "0200"+transactionsToUploadAdvice.get(0).getStan()+"            ");
//            }
        return data8583_u_purchase;
    }

    public boolean popTopAdviceTransaction()
    {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG,"+ unmarkTopAdviceTransaction +");
        if ( transactionsToUploadAdvice.size() == 0 )
        {
            Utility.DEBUG_LOG(TAG,"No record found");
//            rv = false;
        }
        else
        {
            TransactionDetail txn = transactionsToUploadAdvice.get(0);
            //+ taha 17-02-2021 do not mark off advice flag, as below behavior is not observed on V200 terminals
            txn.setIsAdvice(false);
            this.updateTransaction(appDatabase,txn);
            transactionsToUploadAdvice.remove(0);
            rv = true;
        }
        Utility.DEBUG_LOG(TAG,"return rv:"+rv);
        return rv;
    }

    public int getAdviceCount(){
        Utility.DEBUG_LOG(TAG,"+ getAdviceCount() +");
        int a =transactionsToUploadAdvice.size();
        Utility.DEBUG_LOG(TAG,"transactionsToUploadAdvice.size:"+a);
        for ( int i = 0 ; i < a;i ++)
        {
            Utility.DEBUG_LOG(TAG,"TransactionAmount:"+transactionsToUploadAdvice.get(i).getTransactionAmount());
            Utility.DEBUG_LOG(TAG,"Amount:"+transactionsToUploadAdvice.get(i).getAmount());
            Utility.DEBUG_LOG(TAG,"track2:"+transactionsToUploadAdvice.get(i).getTrack2());
        }
        return a;
    }

    @SuppressLint("CheckResult")
    private static void updateTransaction(final AppDatabase db, TransactionDetail txn) {
        Utility.DEBUG_LOG(TAG,"+ updateTransaction +");
        Utility.DEBUG_LOG(TAG,"before db.transactionDetailDao().updateTransaction(txn)");
        db.transactionDetailDao().updateTransaction(txn);
        //        Completable.fromAction(() -> db.transactionDetailDao().updateTransaction(txn))
//                .subscribeOn(Schedulers.io())
////                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(()->{
//                            Utility.DEBUG_LOG(TAG,"success");
//                        }// completed with success
//                        ,throwable ->{
//                            Utility.DEBUG_LOG(TAG,"failed");
//                        }// there was an error
//                );
    }

//
//    public TransactionDetail getFirstAdviceRecord()
//    {
//        TransactionDetail rv = null;
//        Utility.DEBUG_LOG(TAG,"+ getFirstAdviceRecord() +");
//        if ( transactionsToUploadAdvice.size() > 0 )
//        {
//            rv = new TransactionDetail();
//            rv = transactionsToUploadAdvice.get(i);
//        }
//        return rv;
//    }
}
