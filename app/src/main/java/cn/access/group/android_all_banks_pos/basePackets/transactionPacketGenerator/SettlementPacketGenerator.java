package cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator;

import android.util.SparseArray;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UPIPacketSpec;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UblPacketSpec;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;

/**
 * on 11/25/2019.
 */
public class SettlementPacketGenerator {
    private SparseArray<String> data8583_u_purchase = new SparseArray<>();

    public SettlementPacketGenerator() { }


    public SparseArray<String> getSettlementPacket(String procCode,String field63){
        SparseArray<String> data8583;
        switch (Constants.HOST_INDEX_FROM_DB) {
            /*case "alfalah":
                data8583= generatePacketAlfalah();
                break;
            case "hbl":
                data8583= generatePacketHbl();
                break;*/
            case 1:
                UPIPacketSpec upiPacketSpec= new UPIPacketSpec();
                data8583=upiPacketSpec.generateSettlementPacket(procCode,field63);
                data8583_u_purchase=data8583;
                break;
            case 0:
                UblPacketSpec ublPacketSpec = new UblPacketSpec();
                data8583= ublPacketSpec.generateSettlementPacket(procCode,field63);
                data8583_u_purchase=data8583;
                break;
        }
        data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.SETTLEMENT_MESSAGE_TYPE);
        return data8583_u_purchase;
    }

}
