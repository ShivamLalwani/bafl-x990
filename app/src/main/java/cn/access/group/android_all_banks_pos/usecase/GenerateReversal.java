package cn.access.group.android_all_banks_pos.usecase;

import android.annotation.SuppressLint;
import android.content.res.AssetManager;
import android.util.Log;
import android.util.SparseArray;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UPIPacketSpec;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UblPacketSpec;
import cn.access.group.android_all_banks_pos.repository.model.Merchant;
import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * on 10/11/2019.
 */
public class GenerateReversal {
    private Reversal transactionToReverse;
//    private TransactionDetail transactionToReverse;
    private AppDatabase appDatabase;
    private static final String TAG = "EMVDemoReversal";

    @SuppressLint("CheckResult")
    public GenerateReversal(AppDatabase appDatabase){
        Utility.DEBUG_LOG("GenerateReversal","+ GenerateReversal() +");
        this.appDatabase=appDatabase;
        appDatabase.reversalDao().findReversal()
                .subscribe(reversal -> transactionToReverse=reversal);
//        appDatabase.reversalDao().findReversal()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(reversal -> transactionToReverse=reversal);
        Utility.DEBUG_LOG(TAG,"transactionToReverse:"+transactionToReverse);
    }

    public boolean isReversalPresentInDB(){
        Utility.DEBUG_LOG(TAG,"+ isReversalPresentInDB() +");
//        appDatabase.reversalDao().findByStatus("reversal")
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(transactionDetail -> transactionToReverse=transactionDetail);
        Utility.DEBUG_LOG(TAG,"transactionToReverse:"+transactionToReverse);
        if (transactionToReverse == null )
        {
            Utility.DEBUG_LOG(TAG,"reversal not found");
            return false;
        }
        else
        {
            Utility.DEBUG_LOG(TAG,"reversal found");
            return true;
        }
//        return transactionToReverse != null;
    }
    public   SparseArray<String> generateReversalPacket(){

        SparseArray<String> data8583_u_reversal = new SparseArray<>();
        if ( transactionToReverse == null )
        {
            Utility.DEBUG_LOG(TAG,"Reversal record not found, return");
            return data8583_u_reversal;
        }
        switch (Constants.HOST_INDEX_FROM_DB) {
            /*case "alfalah":
                data8583= generatePacketAlfalah();
                break;
            case "hbl":
                data8583= generatePacketHbl();
                break;*/
            case 1:
                UPIPacketSpec upiPacketSpec = new UPIPacketSpec();
                data8583_u_reversal=upiPacketSpec.generateReversalPacket(transactionToReverse);
                break;
            case 0:
                UblPacketSpec ublPacketSpec = new UblPacketSpec();
                data8583_u_reversal= ublPacketSpec.generateReversalPacket(transactionToReverse);
                break;
        }
        return data8583_u_reversal;
    }

    @SuppressLint("CheckResult")
    public void deleteReversal(){
//        Utility.DEBUG_LOG(TAG,"GenerateReversal:deleteTransaction:transactionToReverse:"+transactionToReverse);
//        if ( transactionToReverse == null)
//        {
//            Utility.DEBUG_LOG(TAG,"Reversal not present - nothing to delete");
//        }
//        else
//        {
//            Completable.fromAction(() -> appDatabase.reversalDao().delete(transactionToReverse))
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(()->{ }// completed with success
//                            ,throwable ->{}// there was an error
//                    );
//        }
        Utility.DEBUG_LOG(TAG, "+ deleteReversal() +");
//        Completable.fromAction(() -> appDatabase.reversalDao().deleteReversal())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(()->{
//                    Utility.DEBUG_LOG(TAG,"Reversal successfully deleted");
//                        }// completed with success
//                        ,throwable ->{
//                            Utility.DEBUG_LOG(TAG,"Error deleting Reversal");
//
//                        }// there was an error
//                );

        appDatabase.reversalDao().deleteReversal();
        Utility.DEBUG_LOG(TAG,"reversal deleted");
        transactionToReverse=null;
    }

    public void deleteReversalAsync(){
        Utility.DEBUG_LOG(TAG, "+ deleteReversalAsync() +");
        Completable.fromAction(() -> appDatabase.reversalDao().deleteReversal())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                    Utility.DEBUG_LOG(TAG,"Reversal successfully deleted");
                        }// completed with success
                        ,throwable ->{
                            Utility.DEBUG_LOG(TAG,"Error deleting Reversal");

                        }// there was an error
                );
        transactionToReverse=null;
    }
    @SuppressLint("CheckResult")
    public void insertReversal(Reversal txn) {
        Utility.DEBUG_LOG(TAG,"insertReversal:txn:" + txn);
        Utility.DEBUG_LOG(TAG,"insertReversal:before setStatus" );
        txn.setStatus("reversal");
        Utility.DEBUG_LOG(TAG, "+ insertReversal() +");
        Completable.fromAction(() -> appDatabase.reversalDao().insertTransaction(txn))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"insertReversal:record added");
                            transactionToReverse = txn;
                        }// completed with success
                        ,throwable ->{
                            Utility.DEBUG_LOG(TAG,"insertReversal:record not added");
                        }// there was an error
                );
    }
    public void printReversal(ReceiptPrinter receiptPrinter, IPrinter printer, AssetManager assetManager, Merchant merchant, String cardName, String cardExpiry){
       // receiptPrinter.printSaleReceipt(printer, assetManager, merchant, cardName, "Reversal", cardExpiry, transactionToReverse, "0980980980909");
    }

    public SparseArray<String> createTagOf55()
    {
        if ( transactionToReverse == null )
        {
            SparseArray<String> rv = new SparseArray<String>();
            Utility.DEBUG_LOG(TAG,"Reversal record not found, return");
            return rv;
        }
        return transactionToReverse.getTag55();
    }

}
