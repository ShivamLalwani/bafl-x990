package cn.access.group.android_all_banks_pos.Utilities;

import android.util.Log;

import java.util.Locale;

import cn.access.group.android_all_banks_pos.repository.model.EmvTagValuePair;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

public class TagConstants {

    public static String TVR_Cons = "0000000000";
    public static String TSI= "0000";
    public static String Cryptogram= "0000000000000000"; //void 0400
    public static String AIP= "0000";
    public static String CID= "00"; //void 0400
    public static String CVM_Result= "000000";
    public static String TERM_CAP= "000000";
    public static String TERM_ADD= "0000000000";
    public static String CAP= "0000";
//    public static String TAC_Denial= "0000000000";
//    public static String TAC_Online= "0000000000";
//    public static String TAC_Default= "0000000000";
    public static String IAC_Denial= "0000000000";
    public static String IAC_Online= "0000000000";
    public static String IAC_Default= "0000000000";
    public static String TTQ= "00000000";
    public static String CTQ= "0000";

    //+ taha 08-03-2021 added cvm codes below, same as SDK docs
    public static final int NO_CVM = 0;
    public static final int CVM_PIN = 1;
    public static final int CVM_SIGN = 2;
    public static final int CVM_CDCVM = 3;

    public static void resetTagConstants()
    {
        Utility.DEBUG_LOG("TagConstants","+ resetTagConstants +");
        TVR_Cons = "0000000000";
        TSI= "0000";
        Cryptogram= "0000000000000000"; //void 0400
        AIP= "0000";
        CID= "00"; //void 0400
        CVM_Result= "000000";
        TERM_CAP= "000000";
        TERM_ADD= "0000000000";
        CAP= "0000";
        IAC_Denial= "0000000000";
        IAC_Online= "0000000000";
        IAC_Default= "0000000000";
        TTQ= "00000000";
        CTQ= "0000";
    }

}
