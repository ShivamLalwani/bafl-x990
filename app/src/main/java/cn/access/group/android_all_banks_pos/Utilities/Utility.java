package cn.access.group.android_all_banks_pos.Utilities;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.vfi.smartpos.system_service.aidl.ISystemManager;
import com.vfi.smartpos.system_service.aidl.settings.ISettingsManager;

import org.json.JSONException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import cn.access.group.android_all_banks_pos.viewfragments.UpdateFragment;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.SETTLECOUNTER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIPNUMADJUST;

/**
 * Created by Simon on 2018/8/31.
 * Utility for BCD ASCII convert and others
 * modified by muhammad.humayun on 2019/9/03.
 */
public class Utility {
    static String TAG = "Utility";

    public static void DEBUG_LOG(String TAG, String msg)
    {

        if(true) {
            Log.d(TAG, msg);
        }
//        File file = new File("/sdcard/Download/logs.txt");
//        FileWriter fr = null;
//        try {
//            fr = new FileWriter(file, true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            fr.write(TAG+"\t : \t"+msg + "\n");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            fr.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    public static void checkAutoDownload(Context context, ISystemManager systemManager) {
        Constants.SETTLECOUNTER++;

        Repository repository = new Repository(context);
        repository.updateSettleCounterRepository(SETTLECOUNTER,0);
        Utility.DEBUG_LOG("Utility", "Auto Download Counter" + Constants.SETTLECOUNTER);

        if(Constants.SETTLERANGE <= Constants.SETTLECOUNTER)
        {
            try {
                UpdateCall uc = new UpdateCall(context,systemManager);
                uc.tmsInitialization(context);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Utility.DEBUG_LOG("Utility", "Perform Download");
           // updateFragment.deleteAPK();
            //delete old profile and download profile
            //deleteJSONFile();
            //downloading new version
//            updateFragment.tmsInitialization(context);

        }
        else {
            DashboardContainer.fragmentManager.popBackStack();
            //DashboardContainer.backStack();;
        }
    }


    public static boolean checkAdjust(Context context, TransactionDetail td) {
        boolean flag = false;

        Utility.DEBUG_LOG("Utility", "Adjust Counter" + Constants.TIPNUMADJUST);

        if(td.getTipNumAdjustCounter() < TIPNUMADJUST)
        {
            int tipAdjusCounter = td.getTipNumAdjustCounter();
            tipAdjusCounter++;
         //Constants.TIPNUMADJUSTCOUNTER = td.getTipNumAdjustCounter();
            Utility.DEBUG_LOG(TAG, "when true getTipNumAdjustCounter" +  td.getTipNumAdjustCounter());
            Utility.DEBUG_LOG(TAG, "when true getInvoiceNo" +  td.getInvoiceNo());

            Utility.DEBUG_LOG(TAG, "when true TIPNUMADJUSTCounter" +  tipAdjusCounter);
            flag =true;
        }
        else {
            Utility.DEBUG_LOG(TAG, "when false TIPNUMADJUST" +  Constants.TIPNUMADJUST);
           // DashboardContainer.backStack();;
            flag = false;
        }
        return flag;
    }


    public static String byte2HexStr(byte[] var0, int offset, int length ) {
        if (var0 == null) {
            return "";
        } else {
            String var1 ;
            StringBuilder var2 = new StringBuilder("");

            for (int var3 = offset; var3 < (offset+length); ++var3) {
                var1 = Integer.toHexString(var0[var3] & 255);
                var2.append(var1.length() == 1 ? "0" + var1 : var1);
            }

            return var2.toString().toUpperCase().trim();
        }
    }
    public static String byte2HexStr(byte[] var0) {
        if (var0 == null) {
            return "";
        } else {
            String var1 ;
            StringBuilder var2 = new StringBuilder("");

            for (byte b : var0) {
                var1 = Integer.toHexString(b & 255);
                var2.append(var1.length() == 1 ? "0" + var1 : var1);
            }

            return var2.toString().toUpperCase().trim();
        }
    }

    public static byte[] hexStr2Byte(String hexString) {
//        Utility.DEBUG_LOG(TAG, "hexStr2Byte:" + hexString);
        if (hexString == null || hexString.length() == 0 ) {
            return new byte[] {0};
        }
        String hexStrTrimed = hexString.replace(" ", "");
//        Utility.DEBUG_LOG(TAG, "hexStr2Byte:" + hexStrTrimed);
        {
            String hexStr = hexStrTrimed;
            int len = hexStrTrimed.length();
            if( (len % 2 ) == 1 ){
                //modified by humayun
                hexStr ="0"+ hexStrTrimed ; //this will pad the 0 in the beginning
                //old code
              //  hexStr = hexStrTrimed+"0"; //0 pad on the end
                ++len;
            }
            char highChar, lowChar;
            int  high, low;
            byte result [] = new byte[len/2];
            String s;
            for( int i=0; i< hexStr.length(); i++ ) {
                // read 2 chars to convert to byte
//                s = hexStr.substring(i,i+2);
//                int v = Integer.parseInt(s, 16);
//
//                result[i/2] = (byte) v;
//                i++;
                // read high byte and low byte to convert
                highChar = hexStr.charAt(i);
                lowChar = hexStr.charAt(i+1);
                high = CHAR2INT(highChar);
                low = CHAR2INT(lowChar);
                result[i/2] = (byte) (high*16+low);
                i++;
            }
            return  result;

        }
    }
    public static byte[] hexStr2Byte(String hexString, int beginIndex, int length ) {
        if (hexString == null || hexString.length() == 0 ) {
            return new byte[] {0};
        }
        {
            if( length > hexString.length() ){
                length = hexString.length();
            }
            String hexStr = hexString;
            int len = length;
            if( (len % 2 ) == 1 ){
                hexStr = hexString + "0";
                ++len;
            }
            byte result [] = new byte[len/2];
            String s;
            for( int i=beginIndex; i< len; i++ ) {
                s = hexStr.substring(i,i+2);
                int v = Integer.parseInt(s, 16);

                result[i/2] = (byte) v;
                i++;
            }
            return  result;

        }
    }

    public static byte HEX2DEC( int hex ){
        return (byte)((hex/10)*16+hex%10);
    }

    public static int DEC2INT( byte dec ){
        int high = ((0x007F & dec) >> 4);
        if( 0!= (0x0080&dec) ) {
            high += 8;
        }
        return (high)*10+(dec&0x0F) ;
    }
    public static int CHAR2INT(char c) {
        if(
                (c >= '0' && c <= '9' )
            || (c == '=' )  // for track2
                ) {
            return c - '0';
        } else if(c >= 'a' && c <= 'f'){
            return c - 'a' +10;
        } else if(c >= 'A' && c <= 'F'){
            return c - 'A' +10;
        } else {
            return 0;
        }

    }
    public static byte[] hexStr2ByteField35(String hexString) {
//        Utility.DEBUG_LOG(TAG, "hexStr2Byte:" + hexString);
        if (hexString == null || hexString.length() == 0 ) {
            return new byte[] {0};
        }
        String hexStrTrimed = hexString.replace(" ", "");
//        Utility.DEBUG_LOG(TAG, "hexStr2Byte:" + hexStrTrimed);
        {
            String hexStr = hexStrTrimed;
            int len = hexStrTrimed.length();
            if( (len % 2 ) == 1 ){
                //modified by humayun
               // hexStr ="0"+ hexStrTrimed ; //this will pad the 0 in the beginning
                //old code
               hexStr = hexStrTrimed+"0"; //0 pad on the end
                ++len;
            }
            char highChar, lowChar;
            int  high, low;
            byte result [] = new byte[len/2];
            String s;
            for( int i=0; i< hexStr.length(); i++ ) {
                // read 2 chars to convert to byte
//                s = hexStr.substring(i,i+2);
//                int v = Integer.parseInt(s, 16);
//
//                result[i/2] = (byte) v;
//                i++;
                // read high byte and low byte to convert
                highChar = hexStr.charAt(i);
                lowChar = hexStr.charAt(i+1);
                high = CHAR2INT(highChar);
                low = CHAR2INT(lowChar);
                result[i/2] = (byte) (high*16+low);
                i++;
            }
            return  result;

        }
    }
    public static String byte2HexStrField35(byte[] var0) {
        Utility.DEBUG_LOG("TRK2","+ byte2HexStrField35 +");
        if (var0 == null) {
            return "";
        }
        else {
            String var1 ;
            StringBuilder var2 = new StringBuilder("");

            for (byte b : var0) {
                var1 = Integer.toHexString(b & 255);
                var2.append(var1.length() == 1 ? "0" + var1 : var1);
//                Utility.DEBUG_LOG("TRK2","var1:"+var1);
//                Utility.DEBUG_LOG("TRK2","var2:"+var2);
            }
            Utility.DEBUG_LOG("TRK2","final var2:"+var2);
            return var2.toString().toUpperCase().trim();
        }
    }

    public static String rightPadding(String word, int length, char ch) {
        return (length > word.length()) ? rightPadding(word + ch, length, ch) : word;
    }

    public static int[] addElement(int[] a, int e) {
        a  = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }

    // Get a MemoryInfo object for the device's current memory status.
    public static ActivityManager.MemoryInfo getAvailableMemory(Context context) {
        ActivityManager.MemoryInfo memoryInfo = null;
        if ( context == null ) {
            Utility.DEBUG_LOG(TAG,"Context not yet ready");
        }
        else
        {
            memoryInfo = new ActivityManager.MemoryInfo();
            ActivityManager activityManager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
            activityManager.getMemoryInfo(memoryInfo);
            Utility.DEBUG_LOG(TAG,"available memory: " + memoryInfo.availMem/(1024.0f*1024.0f) +
                    " MB; total memory: " + memoryInfo.totalMem/(1024.0f*1024.0f) +
                    " MB; low memory?: " + memoryInfo.lowMemory +
                    " ; threshold: " + memoryInfo.threshold/(1024.0f*1024.0f) +
                    " MB"
            );

        }
        return memoryInfo;
    }

    public static Boolean isPosLowOnMemory(Context context, ISystemManager systemManager)
    {
        Utility.DEBUG_LOG(TAG,"+ isPosLowOnMemory +");
        Boolean rv=false;
        ActivityManager.MemoryInfo memoryInfo = null;
        int temp = SharedPref.read("maintenanceThreshold");
        long crashThresholdCushion = temp;
        Utility.DEBUG_LOG(TAG,"crashThresholdCushion [MB]:"+crashThresholdCushion);
        if ( crashThresholdCushion == 0 )
        {
            Utility.DEBUG_LOG(TAG,"zero value disables this check return rv:" + rv);
            return rv;
        }
        crashThresholdCushion = crashThresholdCushion * (1024 * 1024);
        memoryInfo = Utility.getAvailableMemory(context);
        if ( memoryInfo == null )
        {
            Utility.DEBUG_LOG(TAG,"memoryInfo context is null, this should not happen");
        }
        else
        {
//            Utility.DEBUG_LOG(TAG,"memoryInfo.availMem:"+memoryInfo.availMem);
//            Utility.DEBUG_LOG(TAG,"memoryInfo.threshold:"+memoryInfo.threshold);
            Utility.DEBUG_LOG(TAG,"crashThresholdCushion:"+crashThresholdCushion/(1024.0f*1024.0f));
            Utility.DEBUG_LOG(TAG,"want restart if memory less than:"+( memoryInfo.threshold + crashThresholdCushion )/(1024.0f*1024.0f));
            if ( memoryInfo.lowMemory || memoryInfo.availMem <= ( memoryInfo.threshold + crashThresholdCushion ) )
            {
                Utility.DEBUG_LOG(TAG,"Device low on memory, needs restart");
                rv = true;
            }
        }
        Utility.DEBUG_LOG(TAG,"Return rv:"+rv);
        return rv;
    }

}
