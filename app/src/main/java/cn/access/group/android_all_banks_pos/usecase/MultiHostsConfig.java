package cn.access.group.android_all_banks_pos.usecase;

import android.util.Log;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.basic.HostInformation;
import cn.access.group.android_all_banks_pos.basic.MultiHosts;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;


/**
 * on 2019/11/15.
 */

public class MultiHostsConfig {
    private static String TAG = "MultiHostsConfig";

    public static MultiHosts multiHosts = null;

    public enum Category {
        UBL,
        HBL,
        BAF,
        UPI,
    }

    public static void initialize(){
        if (null != multiHosts) {
            multiHosts.removeAll();
        } else {
            multiHosts = new MultiHosts();
        }
        int index = -1;

        HostInformation hostInformation;

        // UBL index 0
        hostInformation = new HostInformation<> ( new ISO8583u(), Category.UBL, "UBL" );

        hostInformation.setMerchant("11111112", "1100000001     ", "UBL TEST ACCESS" );
        hostInformation.setHost("172.30.151.11", 5005 ,"003");
       // hostInformation.AIDList = null;
      //  hostInformation.cardBinList = null; // both the AIDList & cardBinList, so all cards can be match for this host. this should be the last ONE in the list.
       // hostInformation.setKeysIndex(10, 10, 10, 10, 1 );

        hostInformation.prn_card_mask = "*";
        hostInformation.prn_card_mask_range = new int[]{6, -5};

        index = multiHosts.append(hostInformation);
        Utility.DEBUG_LOG(TAG, "new host " + hostInformation.description +
                "setting @:" + index);

       /* // HBL
        hostInformation = new HostInformation<>(new ISO8583u(), Category.HBL,  "HBL" );
        hostInformation.setMerchant( "03040102", "0123456789ABCDE", "X990 EMV Demo" );
        hostInformation.setHost( "127.0.0.1", 5558 );
     //   hostInformation.AIDAppend("A000000003");
      //  hostInformation.CardBINAppend("439225");
     //   hostInformation.CardBINAppend("11000241");
     //   hostInformation.setKeysIndex( 20, 20, 20, 20, 2 );

        hostInformation.prn_card_mask = "#";
        hostInformation.prn_card_mask_range = new int[]{2, -5};


        index = multiHosts.append(hostInformation);
        Utility.DEBUG_LOG(TAG, "new host " + hostInformation.description +
                "setting @:" + index);*/

        // UPI CONFIG index 1
        hostInformation = new HostInformation<>(new ISO8583u(), Category.UPI,  "UPI Configuration" );
        hostInformation.setMerchant( "42017890", "200000005000510", "UPI TEST ACCESS" );
        hostInformation.setHost( "202.61.40.171", 7006 ,"444");
       // hostInformation.AIDAppend("A000000004");
     //   hostInformation.CardBINAppend("233605");
      //  hostInformation.setKeysIndex( 20, 20, 20, 20, 2 );

        hostInformation.prn_card_mask = "X";
        hostInformation.prn_card_mask_range = new int[]{4, -5};

        index = multiHosts.append(hostInformation);
        Utility.DEBUG_LOG(TAG, "new host " + hostInformation.description +
                "setting @:" + index);

    }

    public static HostInformation get(int index ){
        return multiHosts.get(index);
    }

    public static void update( int index, HostInformation hostInformation ){
        multiHosts.update(index, hostInformation);
    }

    public static HostInformation getHost(int index, String cardBin, String aid){
        return multiHosts.getHost(index, cardBin, aid);
    }
}
