package cn.access.group.android_all_banks_pos.viewfragments;


import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.DatabaseErrorHandler;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.system_service.aidl.ISystemManager;
import com.vfi.smartpos.system_service.aidl.settings.ISettingsManager;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.GetRequest;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.presenter.SettlementPresenter;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.usecase.DeleteReversal;
import cn.access.group.android_all_banks_pos.usecase.EmvSetAidRid;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AMEX_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLEMENT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLETIME;
//import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CHECK_EXPIRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_ETHERNET;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_GPRS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CURRENCY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.EMV_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ENABLE_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FALLBACK_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_3;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_4;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HIGH_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_ICC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_MAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LOW_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANAGER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY_ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_NAME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MOD10;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MSGHEADER_LENHEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.NII;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PIN_BYPASS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PRI_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SSL_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SUPER_USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SYSTEM_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_THRESHOLD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TOPUP_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostIP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostPort;

import cn.access.group.android_all_banks_pos.Utilities.Constants.*;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author muhammad.humayun
 * on 9/19/2019.
 */
public class SystemSettingFragment extends Fragment {


    @BindView(R.id.pri_medium)
    TextView priMedium;
    @BindView(R.id.pri_medium_ll)
    LinearLayout pri_medium_ll;
    @BindView(R.id.sec_medium_ll)
    LinearLayout sec_medium_ll;
    @BindView(R.id.sec_medium)
    TextView secMedium;
    @BindView(R.id.batch_number)
    TextView batchNumber;
    @BindView(R.id.host_ip_number)
    TextView hostIpNumber;
    @BindView(R.id.host_port_number)
    TextView hostPortNumber;
    @BindView(R.id.tms_ip)
    TextView tmsIp;

    @BindView(R.id.sec_ip)
    TextView secIp;
    @BindView(R.id.sec_port)
    TextView secPort;

    @BindView(R.id.batch_number_ll)
    LinearLayout batch_number_ll;

    @BindView(R.id.tms_ip_ll)
    LinearLayout tms_ip_ll;
    @BindView(R.id.host_ip_number_ll)
    LinearLayout host_ip_number_ll;
    @BindView(R.id.host_port_number_ll)
    LinearLayout host_port_number_ll;
    @BindView(R.id.sec_ip_ll)
    LinearLayout sec_ip_ll;
    @BindView(R.id.sec_port_ll)
    LinearLayout sec_port_ll;
    @BindView(R.id.clearbatchcard)
    Button clearbatchcard;
//    @BindView(R.id.profileDownload)
//    Button profileDownload;
    private WeakReference<Context>  mContext;
    ISystemManager systemManager = null;
    ISettingsManager settingsManager = null;
    private static final String ACTION = "com.vfi.smartpos.system_service";
    private static final String PACKAGE = "com.vfi.smartpos.system_service";
    private static final String CLASSNAME = "com.vfi.smartpos.system_service.SystemService";
    public static final String ACTION_X9SERVICE = "com.vfi.smartpos.device_service";
    public static final String PACKAGE_X9SERVICE = "com.vfi.smartpos.deviceservice";
    public static final String CLASSNAME_X9SERVICE = "com.verifone.smartpos.service.VerifoneDeviceService";

    Unbinder unbinder;

    String newText;
    //    Constants cn = new Constants();
    //HostInformation hostInformation;
    //TerminalConfig terminalConfig;
    boolean Validated = false;
    IEMV iemv;
    String tipEnableText = "";

    public SystemSettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_system_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        // hostInformation = MultiHostsConfig.get(DashboardContainer.index);


        priMedium.setText(PRI_MEDIUM);
        secMedium.setText(SEC_MEDIUM);
        Constants.BATCH_NO = String.format(Locale.getDefault(), "%06d", SharedPref.read("batch_no"));
        batchNumber.setText(Constants.BATCH_NO);
        tmsIp.setText(PortalUrl);
        //tmsPort.setText(TMS_PORT);
        hostIpNumber.setText(hostIP);
        hostPortNumber.setText(String.valueOf(Constants.hostPort));
        secIp.setText(SEC_IP);
        secPort.setText(String.valueOf(SEC_PORT));

        //niiNumber.setText(NII);
        // showTipText();
        //setListener();

        return view;
    }


//    private void showTipText() {
//        if (SharedPref.read("TIP_ENABLED","").equalsIgnoreCase("Y")){
//            tipEnableText="Tip Enabled";
//            tipText.setText(tipEnableText);
//            tipToggle.setChecked(true);
//        }
//        else if (SharedPref.read("TIP_ENABLED","").equalsIgnoreCase("N")){
//            tipEnableText="Tip Disabled";
//            tipText.setText(tipEnableText);
//            tipToggle.setChecked(false);
//        }
//    }

//    private void setListener() {
//        tipToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                String title = "";
//                if (isChecked){
//                    SharedPref.write("TIP_ENABLED","Y");
//                    TIP_ENABLED = SharedPref.read("TIP_ENABLED","");
//                    showTipText();
//                    Validated = true;
//                    title = "Tip Enabled";
//                }else {
//                    SharedPref.write("TIP_ENABLED","N");
//                    TIP_ENABLED = SharedPref.read("TIP_ENABLED","");
//                    showTipText();
//                    Validated = true;
//                    title = "Tip Disabled";
//                }
//                TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID,
//                        MERCHANT_NAME, hostIP, String.valueOf(hostPort), NII, TIP_ENABLED);
//                Repository repository = new Repository(AppDatabase.getAppDatabase(mContext));
//                repository.updateTerminalConfiguration(TerminalConfig);
//                DialogUtil.successDialog(mContext, title);
//            }
//        });
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressLint("CheckResult")
    @OnClick({R.id.pri_medium_ll, R.id.sec_medium_ll, R.id.batch_number_ll, R.id.host_ip_number_ll, R.id.host_port_number_ll,
             R.id.tms_ip_ll, R.id.sec_ip_ll, R.id.sec_port_ll, R.id.clearbatchcard})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.pri_medium_ll:
                openRadio("Pri Medium", "Change Pri Medium", "pm", 0);
                break;
            case R.id.sec_medium_ll:
                openRadio("Sec Medium", "Change Sec Medium", "sm", 0);
                break;
            case R.id.batch_number_ll:
                changeInformation("Batch Number", "Change Batch Number", "batch", 6, InputType.TYPE_CLASS_NUMBER);
                break;
            case R.id.host_ip_number_ll:
                changeInformation("Host IP", "Change Host Ip", "hip", 15, InputType.TYPE_CLASS_PHONE);
                break;
            case R.id.host_port_number_ll:
                changeInformation("Host Port", "Change Host Port", "hp", 4, InputType.TYPE_CLASS_PHONE);
                break;
            case R.id.tms_ip_ll:
                changeInformation("TMS IP", "Change TMS IP", "tmsip", 100, InputType.TYPE_CLASS_TEXT);
                break;
//            case R.id.tms_port_ll:
//                changeInformation("TMS Port", "Change TMS Port", "tmsport", 4, InputType.TYPE_CLASS_PHONE);
//                break;
            case R.id.sec_ip_ll:
                changeInformation("Sec IP", "Change Sec IP", "secip", 15, InputType.TYPE_CLASS_PHONE);
                break;
            case R.id.sec_port_ll:
                changeInformation("Sec Port", "Change Sec Port", "secport", 4, InputType.TYPE_CLASS_PHONE);
                break;
            case R.id.clearbatchcard:
                Utility.DEBUG_LOG("SL", "clearBatch");
                Utility.DEBUG_LOG("Shivam","Lalwani");
                DialogUtil.confirmDialog(mContext.get(),"Clear Batch",sweetAlertDialog -> {
                    sweetAlertDialog.dismissWithAnimation();
                    try {
                        Utility.DEBUG_LOG("TAG","getall transaction"+getAllBatchTransactions().size());
                        if(getAllBatchTransactions().size() != 0) {

                            new Repository(mContext.get()).deleteBatch();
                            int tempBatchNo = SharedPref.read("batch_no");
                            SharedPref.write("old_batch_no", SharedPref.read("batch_no"));
                            tempBatchNo = tempBatchNo + 1;
                            SharedPref.write("batch_no", tempBatchNo);
                            Utility.DEBUG_LOG("SystemSettingFragment", "wrote batch_no:" + tempBatchNo);
                            Repository repository = new Repository(mContext.get());
                            TerminalConfig terminalConfig = new TerminalConfig();
                            terminalConfig.setBatchNo(String.format(Locale.getDefault(), "%06d", SharedPref.read("batch_no")));
                            Utility.DEBUG_LOG("SystemSettingFragment", "batch updated in db" + terminalConfig.getBatchNo());
                            repository.updateBatchNumber(terminalConfig.getBatchNo(), 0);
                            SharedPref.write("isSettleRequiured", "N");
                        }
                        else{
                            DialogUtil.errorDialog(mContext.get(),"Error","No batch found!");
                        }
                    } catch (ExecutionException | InterruptedException e) {
                        DialogUtil.errorDialog(mContext.get(),"Error","No batch found!");
                        e.printStackTrace();
                    }
                });




                break;

//            case R.id.profileDownload:
//                try {
//                    if (SharedPref.read("isSettleRequiured", "").equals("Y") || getAllTransactions().size() > 0) {
//                        DialogUtil.infoDialog(mContext.get(), "Must Settle First!");
//                    } else {
//                        GetRequest.sendGetRequest(mContext.get(), appDatabase, systemManager);
//                        MainApplication.showProgressDialog(mContext.get(),"Please wait");
//                        final Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                // Do something after 5s = 5000ms
//                                MainApplication.hideProgressDialog();
//                               DashboardContainer.switchFragment(new HomeFragment(),"HOME FRAGMENT");
//                            }
//                        }, 5000);
//                    }
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//
////                try {
////                    systemManager.reboot();
////                } catch (RemoteException e) {
////                    e.printStackTrace();
////                }
//
//                break;
            case R.id.reset_aid_rid:
                EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, mContext.get());
                emvSetAidRid.setAID(3);
                emvSetAidRid.setRID(3);
                emvSetAidRid.setAID(1);
                emvSetAidRid.setRID(1);
                break;
        }
    }

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            IDeviceService idevice = IDeviceService.Stub.asInterface(service);
            try {
                iemv = idevice.getEMV();
                //   Toast.makeText(mContext.get(), "bind service success", Toast.LENGTH_SHORT).show();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };


    private ServiceConnection conn1 = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Utility.DEBUG_LOG("systemSetting", "system service bind success");
            systemManager = ISystemManager.Stub.asInterface(iBinder);
            try {
                settingsManager = systemManager.getSettingsManager();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
//            try {
//                systemManager.reboot();
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Utility.DEBUG_LOG("systemSetting", "system service disconnected.");
            systemManager = null;
        }
    };


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Intent intent = new Intent();
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        boolean isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        Intent intent1 = new Intent();
        intent1.setAction(ACTION_X9SERVICE);
        intent1.setPackage(PACKAGE);
        intent1.setClassName(PACKAGE, CLASSNAME);
        boolean isSucc1 = context.bindService(intent1, conn1, Context.BIND_AUTO_CREATE);

        mContext = new WeakReference<>(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null ) {
            getActivity().unbindService(conn);
            getActivity().unbindService(conn1);
        }
    }

    /**
     * dialog to change any of the data and rewrite to persistence
     *
     * @param title   the dialog title
     * @param content the msg to display
     * @param type    the data field to be changed
     */
    private void changeInformation(String title, String content, String type, int keyboardLength, int KeyboardType) {
        DialogUtil.showKey(getActivity());
//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_FORCED);
        DialogUtil.inputDialogForSystemSettingFragment(mContext.get(), title, content, sweetAlertDialog -> {
            // DialogUtil.hideKeyboard(getActivity());
            EditText text = Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
            text.setInputType(InputType.TYPE_CLASS_TEXT);
            newText = text.getText().toString();
            switch (type) {
                case "pm":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 8!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("PRI_MEDIUM", newText);
                        PRI_MEDIUM = SharedPref.read("PRI_MEDIUM", "");
                        priMedium.setText(PRI_MEDIUM);
                        Validated = true;
                    }
                    // hostInformation.terminalID=TERMINAL_ID;
                    break;
                case "sm":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 15, add spaces if needed!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("SEC_MEDIUM", newText);
                        SEC_MEDIUM = SharedPref.read("SEC_MEDIUM", "");
                        secMedium.setText(SEC_MEDIUM);
                        Validated = true;
                    }
                    //  hostInformation.merchantID=MERCHANT_ID;
                    break;
                case "batch":
                    if (newText.equals("")) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "cannot be empty!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                        //DialogUtil.hideKeyboard(getActivity());
                    } else {
                        SharedPref.write("batch_no", Integer.parseInt(newText));
                        ///BATCH_NO =  SharedPref.read("batch_no", "");
                        Constants.BATCH_NO = String.format(Locale.getDefault(), "%06d", SharedPref.read("batch_no"));
                        batchNumber.setText(BATCH_NO);
                        Validated = true;
                        //DialogUtil.hideKeyboard(getActivity());
                    }
                    // hostInformation.merchantName=MERCHANT_NAME;
                    break;
                case "hip":
                    if (!Patterns.IP_ADDRESS.matcher(newText).matches()) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "invalid ip!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("hostIP", newText);
                        hostIP = SharedPref.read("hostIP", "");
                        hostIpNumber.setText(hostIP);
                        Validated = true;
                    }
                    //  hostInformation.hostAddr=hostIP;
                    break;
                case "hp":
                    if (newText.length() != 4) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 4!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        int port = Integer.valueOf(newText);
                        SharedPref.write("hostPort", port);
                        hostPort = SharedPref.read("hostPort");
                        hostPortNumber.setText(String.valueOf(Constants.hostPort));
                        Validated = true;
                    }
                    // hostInformation.hostPort=hostPort;
                    break;
                case "tmsip":
                    if (newText.length() < 10) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "invalid ip!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                       // SharedPref.write("TMS_IP", newText);
                        //TMS_IP = SharedPref.read("TMS_IP", "");
                        SharedPref.write("PortalUrl",newText);
                        Constants.PortalUrl = SharedPref.read("PortalUrl", "");
                        tmsIp.setText(Constants.PortalUrl );
                        Validated = true;
                    }
                    // hostInformation.nii=NII;
                    break;

//                case "tmsport":
//                    if (newText.length() == 0) {
//                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
//                        sweetAlertDialog.dismissWithAnimation();
//                        Validated = false;
//                    } else {
//                        SharedPref.write("TMS_PORT", newText);
//                        TMS_PORT = SharedPref.read("TMS_PORT", "");
//                        tmsPort.setText(TMS_PORT);
//                        Validated = true;
//                    }
//                    // hostInformation.nii=NII;
//                    break;
                case "secip":
                    if (!Patterns.IP_ADDRESS.matcher(newText).matches()) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "invalid ip!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SharedPref.write("SEC_IP", newText);
                        SEC_IP = SharedPref.read("SEC_IP", "");
                        secIp.setText(SEC_IP);
                        Validated = true;
                    }
                    // hostInformation.nii=NII;
                    break;
                case "secport":
                    if (newText.length() == 0) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Length must be 3!");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        int secport = Integer.valueOf(newText);
                        SharedPref.write("SEC_PORT", secport);
                        SEC_PORT = SharedPref.read("SEC_PORT");
                        secPort.setText(String.valueOf(SEC_PORT));
                        Validated = true;
                    }
                    // hostInformation.nii=NII;
                    break;
                case "clearbatchcard":
//                case "tip":
//                    break;
            }
            //  MultiHostsConfig.update(DashboardContainer.index, hostInformation);
            if (Validated) {
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                sweetAlertDialog.dismissWithAnimation();
                DialogUtil.successDialog(mContext.get(), title + " Changed!");
            }
        }, KeyboardType, null, keyboardLength);

    }

    private void openRadio(String title, String content, String type, int keyboardLength) {

        DialogUtil.radioButton(type, mContext.get(), title, content, sweetAlertDialog -> {
            //  EditText text= Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
            //            text.setInputType(InputType.TYPE_CLASS_TEXT);
            //newText =   text.getText().toString();
            switch (type) {
                case "pm":
                    if (SharedPref.read("PRI_MEDIUM", "").equalsIgnoreCase("notSelected")) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Select Pri Medium");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {

                        PRI_MEDIUM = SharedPref.read("PRI_MEDIUM", "");
                        priMedium.setText(PRI_MEDIUM);
                        Validated = true;
                    }


                    break;
                case "sm":
                    if (SharedPref.read("SEC_MEDIUM", "").equalsIgnoreCase("notSelected")) {
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Select SEC MEDIUM");
                        sweetAlertDialog.dismissWithAnimation();
                        Validated = false;
                    } else {
                        SEC_MEDIUM = SharedPref.read("SEC_MEDIUM", "");
                        secMedium.setText(SEC_MEDIUM);
                        Validated = true;
                    }

                    break;

            }
            //  MultiHostsConfig.update(DashboardContainer.index, hostInformation);
            if (Validated) {
                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                sweetAlertDialog.dismissWithAnimation();
                DialogUtil.successDialog(mContext.get(), title + " Changed!");
            }
        }, InputType.TYPE_CLASS_NUMBER, null, keyboardLength);
    }




    public List<TransactionDetail> getAllBatchTransactions() throws ExecutionException, InterruptedException {
        return ((DashboardContainer)mContext.get()).getAllBatchTransactions();
    }


}
