package cn.access.group.android_all_banks_pos.presenter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;

import com.vfi.smartpos.deviceservice.aidl.CheckCardListener;
import com.vfi.smartpos.deviceservice.aidl.EMVHandler;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;
import com.vfi.smartpos.deviceservice.aidl.OnlineResultHandler;
import com.vfi.smartpos.deviceservice.aidl.PinInputListener;
import com.vfi.smartpos.deviceservice.aidl.PinpadKeyType;
import com.vfi.smartpos.deviceservice.constdefine.ConstCheckCardListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPinpad;
import com.vfi.smartpos.deviceservice.constdefine.ConstOnlineResultHandler;
import com.vfi.smartpos.deviceservice.constdefine.ConstPBOCHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.PrinterFonts;
import cn.access.group.android_all_banks_pos.Utilities.Comm;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.ErrorMap;
import cn.access.group.android_all_banks_pos.Utilities.PostRequest;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.TagConstants;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Validation;
import cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator.SalePackageGenerator;
import cn.access.group.android_all_banks_pos.basic.ISO8583;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.EmvTagValuePair;
import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.usecase.EmvSetAidRid;
import cn.access.group.android_all_banks_pos.usecase.GenerateReversal;
import cn.access.group.android_all_banks_pos.viewfragments.PaymentAmountFragment;
import cn.access.group.android_all_banks_pos.viewfragments.SaleIPPFragment;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CASH_ADV_MESSAGE_TYPE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CASH_ADV_PROCESSING_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CASH_OUT_MESSAGE_TYPE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CASH_OUT_PROCESSING_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FIELD_61;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HOST_INDEX_FROM_DB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.INVOICE_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_NAME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ORBIT_INQUIRY_MESSAGE_TYPE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ORBIT_INQUIRY_PROCESSING_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ORBIT_REDEEM_MESSAGE_TYPE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ORBIT_REDEEM_PROCESSING_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PREAUTH_MESSAGE_TYPE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PRE_AUTH_PROCESSING_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.REFUND_MESSAGE_TYPE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALEIPP_PROCESSING_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MESSAGE_TYPE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_PROCESSING_CODE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ecrRefNo;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.firstBootUp;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostIP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostPort;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.isEcrEnable;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.onBackward;
import static com.vfi.smartpos.deviceservice.aidl.PinpadKeyType.DUKPTKEY;
import static java.lang.Thread.sleep;

enum ReaderType {RT_Chip, RT_Mag, RT_CTLS, RT_Chip_Mag, RT_All};


/**
 * @author muhammad.humayun
 * on 8/21/2019.
 */
public class PaymentAmountPresenter {
    private static final String TAG = "PaymentAmountPresenter";

    ;
    ///Float cleanAmount;
    double cleanAmount; /// Shivam's code update on 15 Jan 2021 for Amount decimal points calculation.
    WeakReference<Context> context;
    SparseArray<String> data8583_i = null;
    String cardType = "UNKNOWN";
    String cardRanges;
    String cardholdername;
    boolean isPinInput = false;
    boolean redeemCheck = false;
    PostRequest postRequest;
    String tipAmount = "0";
    String transactionAmount = "0";
    String txnTypePresenter;
    Validation vs;

    //ubl keys...11/22/2019
  /*  private String pinKey_WorkKey = "9A75CD73C53EAD2510C4FD6A3AD95DAF"; ///neww
 //  private String pinKey_WorkKey ="1A98C8528C83E9E5C891B5043E51B045";
    private String mainKey_MasterKey = "22222222222222222222222222222222";*/// Taha's code update on Jan21 for BAF Keys'
    ISerialPort serialPort;
    String Bal = "";
    String field_63;
    String orbit_amount = "";
    String orbit_bal = "";
    String orbit_key = "";
    IDeviceInfo iDeviceInfo;
    String UpdatedYear = "";
    String saleippMonths = "";
    Double installment = 0.00;
    String last4DigitsStr = "";
    boolean emvLogsPrinted = false;
    // transPrinter; // SHIVAM UPDATE HERE FOR TEXT PRINTER
    private IEMV iemv;
    private IPinpad ipinpad;
    private IBeeper iBeeper;
    private IPrinter printer;
    private EMVHandler emvHandler;
    private PinInputListener pinInputListener;
    private PinInputListener last4DigitsListener;
    private String headerType = "";
    // keys
    private int mainKeyId = 97;
    private int workKeyId = 1;
    private int last4DigitKeyId = 2;//+ taha 23-02-2021 for compatibility sake only
    //+ taha 15-01-2021 setting BAF keys
    private String mainKey_MasterKey = "D6DF20AEAEBC70E5";
    private String pinKey_WorkKey = "AB85765997CDDA0C"; ///neww
    private String macKey = "";
    private String savedPan = "";
    private byte[] savedPinBlock = null;
    private byte[] last4Digits = null;
    String authNumber = "";
    /**
     * field-value map to save iso data
     */
    private SparseArray<String> data8583 = null;
    private SparseArray<String> tagOfF55 = null;
    private String cardName = null, cardExpiry = null;
    private String totalAmountStr, dateFromResponse, timeFromResponse, rnn;
    private String transAmount = null, transDate = null, transTime = null;
    private boolean doSecondTap = true;
    private AssetManager assetManager;
    private GenerateReversal generateReversal;
    private TransactionContract transactionContract;
    private ReceiptPrinter receiptPrinter;
    private ErrorMap errorMap;
    private PaymentContract paymentContract;
    // private static String STAN;
    private String track2, aid;
    private boolean CTLS = false;
    private boolean internet = true;
    private String from = "";
    private TerminalConfig terminalConfig;
    @SuppressLint("HandlerLeak")
    private Handler onlineResponse = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG, "before getData");
            Bundle data = msg.getData();
            String val = data.getString("value");
            Utility.DEBUG_LOG(TAG, "handle message:" + val);
        }
    };
    private ISO8583u isoResponse;
    // SSLComm sslComm = new SSLComm(context, "keystore/kclient.bks", "172.191.1.194", 5555);
    private Runnable onlineRequest = new Runnable() {
        @Override
        public void run() {

            ISO8583u iso8583u;
            generateReversal = new GenerateReversal(AppDatabase.getAppDatabase(context.get()));
            Utility.DEBUG_LOG(TAG, "+ PAP:run() +");
//            boolean reversalBeingProcessed = false;
            //sumTransactions(appDatabase);
//


            byte[] packet;
            if (generateReversal.isReversalPresentInDB()) {
                if (HOST_INDEX_FROM_DB == 0) {
                    iso8583u = new ISO8583u();
                } else {
                    iso8583u = new ISO8583u(16);
                }
                Utility.DEBUG_LOG(TAG, "before getReversalPacket");
                packet = getReversalPacket(iso8583u, generateReversal);
            } else {
                iso8583u = new ISO8583u();
                if (tagOfF55 != null) {
                    for (int i = 0; i < tagOfF55.size(); i++) {
                        int tag = tagOfF55.keyAt(i);
                        String value = tagOfF55.valueAt(i);
                        Utility.DEBUG_LOG(TAG, "tag:" + tag + ", value:" + value);
                        if (value.length() > 0) {
                            byte[] tmp = iso8583u.appendF55(tag, value);
                            if (tmp == null) {
                                Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                            } else {
                                Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                                if (Integer.toHexString(tag).equals("84")) {
                                    aid = Utility.byte2HexStr(tmp).substring(4);
                                }
                            }
                        }
                    }
                    tagOfF55 = null;

                }


                String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
                String currentDate = new SimpleDateFormat("MMdd", Locale.getDefault()).format(new Date());

                if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
                    data8583.put(ISO8583u.F_TxTime_12, currentTime);
                    data8583.put(ISO8583u.F_TxDate_13, currentDate);
                }
                data8583.put(ISO8583u.F_STAN_11, INVOICE_NO);
                Utility.DEBUG_LOG(TAG, "invoice 2" + INVOICE_NO);
                if (txnTypePresenter.equals("SALEIPP")) {

                    data8583.put(ISO8583u.F_SettlementData_63, String.valueOf(saleippMonths));
                    Utility.DEBUG_LOG(TAG,"saleippMonths:"+saleippMonths);
                }
                Utility.DEBUG_LOG(TAG, "before iso8583u.makePacket data8583");
                packet = iso8583u.makePacket(data8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
                ((DashboardContainer) context.get()).getTransactionDetail().setMId(MERCHANT_ID);
                ((DashboardContainer) context.get()).getTransactionDetail().setTId(TERMINAL_ID);
                ((DashboardContainer) context.get()).getTransactionDetail().setStan(data8583.get(ISO8583u.F_STAN_11));
                ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);
                ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(dateFromResponse);
                ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(timeFromResponse);

                if (((DashboardContainer) context.get()).getTransactionDetail().getCardNo() == null) {
                    ((DashboardContainer) context.get()).getTransactionDetail().setCardNo(savedPan);
                }
                ((DashboardContainer) context.get()).getTransactionDetail().setAmount(transAmount);
                ((DashboardContainer) context.get()).getTransactionDetail().setTxnType(txnTypePresenter);
                cardRanges = savedPan.substring(0, 6);
                cardType = DataFormatterUtil.setCardTypeScheme(Integer.parseInt(cardRanges));
                ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme2(cardType);
                if (((DashboardContainer) context.get()).getTransactionDetail().getCardScheme() == null) {
                    ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme(cardType);

                }
                ((DashboardContainer) context.get()).getTransactionDetail().setCancelTxnType("None");
                //transactionDetail.setIsVoided("No");
                ((DashboardContainer) context.get()).getTransactionDetail().setInvoiceNo(INVOICE_NO);
                if(Constants.isEcrReq && Constants.ecrType.equals("M") && isEcrEnable.equals("Y")) {
                    ((DashboardContainer) context.get()).getTransactionDetail().setEcrRefNo(ecrRefNo);
                    //Constants.isEcrReq = false;
                }
                Utility.DEBUG_LOG(TAG, "invoice 3" + INVOICE_NO);
                ((DashboardContainer) context.get()).getTransactionDetail().setBatchNo(BATCH_NO);
                //Utility.DEBUG_LOG("batch_payment",BATCH_NO);
                ((DashboardContainer) context.get()).getTransactionDetail().setTrack2(track2);
                ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);


                Utility.DEBUG_LOG(TAG, "MTI:" + txnTypePresenter);
                switch (txnTypePresenter) {
                    case "SALE":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(SALE_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(SALE_PROCESSING_CODE);
                        data8583.put(ISO8583u.F_ProcessingCode_03, SALE_PROCESSING_CODE);
                        break;

                    case "SALEIPP":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(SALE_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(SALEIPP_PROCESSING_CODE);
                        data8583.put(ISO8583u.F_ProcessingCode_03, SALEIPP_PROCESSING_CODE);

                        Utility.DEBUG_LOG("SALEIPP", String.valueOf(data8583));
                        break;

                    case "PRE AUTH":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(PREAUTH_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(PRE_AUTH_PROCESSING_CODE);
                        break;
                    case "AUTH":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(PREAUTH_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode("000000");
                        break;
                    case "REDEEM":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(ORBIT_REDEEM_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(ORBIT_REDEEM_PROCESSING_CODE);
                        redeemCheck = true;
                        break;
                    case "ORBIT INQUIRY":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(ORBIT_INQUIRY_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(ORBIT_INQUIRY_PROCESSING_CODE);
                        break;
                    case "CASH OUT":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(CASH_OUT_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(CASH_OUT_PROCESSING_CODE);
                        break;

                    case "CASH ADV":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(CASH_ADV_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(CASH_ADV_PROCESSING_CODE);
                        break;

                    case "REFUND":
                        ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(CASH_ADV_MESSAGE_TYPE);
                        ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(REFUND_MESSAGE_TYPE);
                        break;
                }

                ((DashboardContainer) context.get()).getTransactionDetail().setAcquiringInstitutionIdentificationCode(ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
                ((DashboardContainer) context.get()).getTransactionDetail().setForwardingInstitutionIdentificationCode(FORWARDING_INSTITUTION_IDENTIFICATION_CODE);


            }

            Utility.DEBUG_LOG(TAG, "before isOfflineTransaction 1");
            // SHIVAM UPDATE HERE FOR TEXT PRINTER
            if (isOfflineTransaction()) {
                Utility.DEBUG_LOG(TAG, "Offline approval...");
                Utility.DEBUG_LOG(TAG, "before transPrinter.print() 1");
                //  transPrinter = new PrintRecpSale(context);
                Utility.DEBUG_LOG(TAG, "checking redeem " + redeemCheck);
                //  transPrinter.initializeData(transactionDetail, Bal, redeemCheck, "MERCHANT COPY");
                // transPrinter.setTransactionContract(transactionContract);
                Utility.DEBUG_LOG(TAG, "before print() 1");
                // transPrinter.print();
                Utility.DEBUG_LOG(TAG, "after print() 1");
                return;
            }
// SHIVAM UPDATE HERE FOR TEXT PRINTER

            Utility.DEBUG_LOG(TAG, "Before new Comm");
            Comm comm = new Comm(hostIP, hostPort, context.get());
            Comm comm1 = new Comm(SEC_IP, SEC_PORT, context.get());

            if(Constants.isEcrReq && Constants.ecrType.equals("M"))
                ECR.ECRMsgWrite("Connecting Bank Host");

            transactionContract.showProgress("connecting");

            if (!comm.connect()) {

                if (comm1.connect()) {
                    transactionContract.hideProgress();
                    int packetLength = packet.length;
                    String packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2, Utility.byte2HexStr(packet), ' ');
                    Utility.DEBUG_LOG(TAG, packetToPrint);
                    transactionContract.showProgress("processing");
                    comm1.send(packet);
                    if(Constants.isEcrReq && Constants.ecrType.equals("M"))
                        ECR.ECRMsgWrite("Transacting");
                } else {
                    internet = false;
                    Utility.DEBUG_LOG(TAG, "Unable to establish connection with server.");
                    transactionContract.hideProgress();
                    transactionContract.transactionFailed("Error", "Unable to establish connection with server");
                    ((DashboardContainer) context.get()).getTransactionDetail().setStatus("reversal");
                    try {
                        set8A("Z3");
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
            transactionContract.hideProgress();

            //+ taha 19-01-2021 create a reversal before send/receive
            if (generateReversal.isReversalPresentInDB())//do not re-enter reversal in case reversal was being processed
            {
                Utility.DEBUG_LOG(TAG, "do not create reversal of reversal");
            } else if (IsReversalNeeded(((DashboardContainer) context.get()).getTransactionDetail())) {
//                Utility.DEBUG_LOG(TAG,"before deleteReversal - safe side");
//                generateReversal.deleteReversal();
                Reversal r = new Reversal();
                r.copy(((DashboardContainer) context.get()).getTransactionDetail());
                Utility.DEBUG_LOG(TAG, "before insertReversal1");
                generateReversal.insertReversal(r);
            } else {
                Utility.DEBUG_LOG(TAG, "reversal probably not needed like in case of balance inquiry");
            }

            //+ taha 24-03-2021
            if (internet == false) {
                Utility.DEBUG_LOG(TAG, "No internet found, return");
                return;
            }

            int packetLength = packet.length;
            String packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2, Utility.byte2HexStr(packet), ' ');
            Utility.DEBUG_LOG(TAG, "packetToPrint:" + packetToPrint);


            transactionContract.showProgress("processing");
            Utility.DEBUG_LOG(TAG, "Length of packet to send:" + String.valueOf(packet.length));
            comm.send(packet);
            if(Constants.isEcrReq && Constants.ecrType.equals("M"))
                ECR.ECRMsgWrite("Transacting");

            printIsoLogsWrapper("request:" + packetToPrint, 0);

            byte[] response = new byte[0];
            try {
                if (!comm.connect()) {
                    response = comm1.receive(1024, 40);
                } else {
                    response = comm.receive(1024, 40);
                }
            } catch (IOException e) {
                transactionContract.hideProgress();
                transactionContract.transactionFailed("Error", e.getMessage());
            }
            comm.disconnect();
            comm1.disconnect();
            transactionContract.hideProgress();
            // insertTransaction(appDatabase,transactionDetail);

            if (response == null || response.length == 0) {
                Utility.DEBUG_LOG(TAG, "response is null");
                Utility.DEBUG_LOG(TAG, "Test fails");
                Utility.DEBUG_LOG(TAG, "receive error");
                try {
                    set8A("Z3");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                isoResponse = null;
            } else {
                Utility.DEBUG_LOG(TAG, "Test return length:" + response.length);
                Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(response));
                packetLength = response.length;
                Utility.DEBUG_LOG(TAG, "packetlength:" + String.valueOf(packetLength));
                packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2, Utility.byte2HexStr(response), ' ');
                Utility.DEBUG_LOG(TAG, "packet to print:" + packetToPrint);

                printIsoLogsWrapper("response:" + packetToPrint, 0);

                // receiptPrinter.printPacket(printer,"Response  ",packetLength,packetToPrint);
                isoResponse = new ISO8583u();
                if (isoResponse.unpack(response, 2)) {
                    String message = "";
                    String s;
                    String type = "";
                    String responseCode = "";

                    s = isoResponse.getUnpack(0);
                    if (null != s) {
                        type = s;
                        message += "Message Type:";
                        message += s;
                        message += "\n";
                    }

                    s = responseCode = isoResponse.getUnpack(39);
                    if (null != s) {
                        message += "Response(39):";
                        message += s;
                        message += "\n";
                    }
                    Utility.DEBUG_LOG(TAG, "message type:" + type);
                    if (type.equals("0810")) {
                        s = isoResponse.getUnpack(62 + 200);
                        if (null != s) {
                            Utility.DEBUG_LOG(TAG, "Field62:" + s);
                            if (s.length() == 48) {
                                pinKey_WorkKey = s.substring(0, 32);
                                macKey = s.substring(32, 48);
                            } else if (s.length() == 80) {
                                pinKey_WorkKey = s.substring(0, 64);
                                macKey = s.substring(64, 80);
                            } else if (s.length() == 120) {
                                pinKey_WorkKey = s.substring(0, 64);
                                macKey = s.substring(64, 80);
                            }
                            message += "pinKey:";
                            message += pinKey_WorkKey;
                            message += "\n";
                            message += "macKey:";
                            message += macKey;
                            message += "\n";
                        }
                    } else if (type.equals("0210")) {
                        s = isoResponse.getUnpack(54);
                        if (null != s) {
                            message += "Balance(54):";
                            message += s.substring(0, 2) + "," + s.substring(2, 4) + "," + s.substring(4, 7) + "," + s.substring(7, 8);
                            message += "\n" + Integer.valueOf(s.substring(8, s.length() - 1));
                            message += "\n";
                        }
                        if (!responseCode.equals("00")) {
                            if (((DashboardContainer) context.get()).getTransactionDetail().getCardType().equals("Contactless") ||
                                    ((DashboardContainer) context.get()).getTransactionDetail().getCardType().equals("Chip")) {
                                Utility.DEBUG_LOG(TAG, "Print EMV logs only in case of ctls/chip");
                                Utility.DEBUG_LOG(TAG, "before printEmvLogsWrapper1");
                                printEmvLogsWrapper(3000);
                            } else {
                                Utility.DEBUG_LOG(TAG, "do not print EMV logs");
                            }
                        }
                    }
                    //delete reversal if we got response of reversal
                    else if (type.equals("0410")) {
                        //+ taha 22-02-2021 delete reversal if response is found
                        Utility.DEBUG_LOG(TAG, "before deleteReversal after getting reversal's response");
                        generateReversal.deleteReversal();
                    }

                    transactionContract.hideProgress();
                    transactionContract.toastShow(message);
                    Utility.DEBUG_LOG(TAG, "toast: " + message);
                }
//                //+ taha 22-02-2021 delete reversal after 2nd gen ac - this logic is modified as reversal is re-inserted in case of AAC
                Utility.DEBUG_LOG(TAG, "before deleteReversal after getting response");
                generateReversal.deleteReversal();
            }
            Message msg = new Message();
            Bundle data = new Bundle();
            data.putString("value", "receive finished");
            msg.setData(data);
            onlineResponse.sendMessage(msg);
            transactionContract.hideProgress();
            //insertTransaction(appDatabase,transactionDetail);
        }
    };

    public PaymentAmountPresenter(IEMV iemv) {
        this.iemv = iemv;
    }

    public PaymentAmountPresenter(TransactionContract transactionContract, PaymentContract paymentContract, Handler handler,
                                  boolean isSucc, AssetManager assetManager, IEMV iemv, IPinpad ipinpad, IBeeper iBeeper,
                                  IPrinter printer, String transactionAmount, String from, Context context, ISerialPort serialPort, IDeviceInfo iDeviceInfo, String saleippMonths) {
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountPresenter +");
        this.transactionContract = transactionContract;
        this.paymentContract = paymentContract;
        this.iemv = iemv;
        this.ipinpad = ipinpad;
        this.iBeeper = iBeeper;
        this.printer = printer;
        this.assetManager = assetManager;
        receiptPrinter = new ReceiptPrinter(handler);
        errorMap = new ErrorMap();
        this.from = from;
        this.transactionAmount = transactionAmount;
        this.vs = new Validation();
        this.context = new WeakReference<>(context) ;
        this.serialPort = serialPort;
        this.postRequest = new PostRequest();
        this.iDeviceInfo = iDeviceInfo;
        this.saleippMonths = saleippMonths;
        UpdatedYear = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
        Utility.DEBUG_LOG("updateYear", UpdatedYear);
        Repository repository = new Repository(this.context.get());
        terminalConfig = repository.getTerminalConfigurationFromUi();
        ((DashboardContainer)context).setTransactionDetail(new TransactionDetail());

        // reset the aid and rid
        if (firstBootUp) {
            EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, context);
            try {
                emvSetAidRid.setAID(3);
                emvSetAidRid.setRID(3);
                emvSetAidRid.setAID(1);
                emvSetAidRid.setRID(1);
                firstBootUp = false;

            } catch (Exception e) {
                Utility.DEBUG_LOG(TAG, "it works" + e);
            }
        }


        //sending post request


        //sendGetRequest();

       /* emptytable(appDatabase);
        SharedPref.clear();*/
        //doSetKeys();
        // emptytable(appDatabase);

      /*  boolean bRet;
        try {
            bRet = ipinpad.isKeyExist(2, 1);
            transactionContract.toastShow("loadMainKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }*/
        doSetKeys();
        loadConfiguration();
        ISO8583u.SimulatorType st;
        switch (headerType) {
            case ("AuthAll"):
                st = ISO8583u.SimulatorType.AuthAll;
                break;
            case "SimulatorHost":
                st = ISO8583u.SimulatorType.SimulatorHost;
                break;
            case "ActualBank":
                st = ISO8583u.SimulatorType.ActualBank;
                break;
            default:
                st = ISO8583u.SimulatorType.AuthAll;
                break;
        }
        initialize8583data(st);
        //getSerialPort();

        // check assets fonts and copy to file system for Service
        InitializeFontFiles();
        if (!isSucc) {
            Utility.DEBUG_LOG("TAG", "deviceService connect fail!");
        } else {
            Utility.DEBUG_LOG("TAG", "deviceService connect success");
            initializeEMV();
            initializePinInputListener();
            initializeLast4DigitsListener();
        }


//        try {
//            serialPort.open();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
    }


    public PaymentAmountPresenter(TransactionContract transactionContract, PaymentContract paymentContract, Handler handler,
                                  boolean isSucc, AssetManager assetManager, IEMV iemv, IPinpad ipinpad, IBeeper iBeeper,
                                  IPrinter printer, String transactionAmount, String from, Context context, ISerialPort serialPort, IDeviceInfo iDeviceInfo, String saleippMonths, String authNumber) {
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountPresenter +");
        this.transactionContract = transactionContract;
        this.paymentContract = paymentContract;
        this.iemv = iemv;
        this.ipinpad = ipinpad;
        this.iBeeper = iBeeper;
        this.printer = printer;
        this.assetManager = assetManager;
        receiptPrinter = new ReceiptPrinter(handler);
        errorMap = new ErrorMap();
        this.from = from;
        this.transactionAmount = transactionAmount;
        this.vs = new Validation();
        this.context = new WeakReference<>(context) ;
        this.serialPort = serialPort;
        this.postRequest = new PostRequest();
        this.iDeviceInfo = iDeviceInfo;
        this.saleippMonths = saleippMonths;
        this.authNumber = authNumber;
        UpdatedYear = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
        Utility.DEBUG_LOG("updateYear", UpdatedYear);
        Repository repository = new Repository(this.context.get());
        terminalConfig = repository.getTerminalConfigurationFromUi();
        ((DashboardContainer)context).setTransactionDetail(new TransactionDetail());

        // reset the aid and rid
        if (firstBootUp) {
            EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, context);
            try {
                emvSetAidRid.setAID(3);
                emvSetAidRid.setRID(3);
                emvSetAidRid.setAID(1);
                emvSetAidRid.setRID(1);
                firstBootUp = false;
            } catch (Exception e) {
                Utility.DEBUG_LOG(TAG, "it works" + e);
            }
        }


        //sending post request


        //sendGetRequest();

       /* emptytable(appDatabase);
        SharedPref.clear();*/
        //doSetKeys();
        // emptytable(appDatabase);

      /*  boolean bRet;
        try {
            bRet = ipinpad.isKeyExist(2, 1);
            transactionContract.toastShow("loadMainKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }*/
        doSetKeys();
        loadConfiguration();
        ISO8583u.SimulatorType st;
        switch (headerType) {
            case ("AuthAll"):
                st = ISO8583u.SimulatorType.AuthAll;
                break;
            case "SimulatorHost":
                st = ISO8583u.SimulatorType.SimulatorHost;
                break;
            case "ActualBank":
                st = ISO8583u.SimulatorType.ActualBank;
                break;
            default:
                st = ISO8583u.SimulatorType.AuthAll;
                break;
        }
        initialize8583data(st);
        //getSerialPort();

        // check assets fonts and copy to file system for Service
        InitializeFontFiles();
        if (!isSucc) {
            Utility.DEBUG_LOG("TAG", "deviceService connect fail!");
        } else {
            Utility.DEBUG_LOG("TAG", "deviceService connect success");
            initializeEMV();
            initializePinInputListener();
            initializeLast4DigitsListener();
        }


//        try {
//            serialPort.open();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
    }


//    public ISerialPort getSerialPort() {
//        try {
//            serialPort.init(115200,0,8);
//            Utility.DEBUG_LOG("Port connected", String.valueOf(serialPort.init(115200,0,8)));
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//        return serialPort;
//    }

    @SuppressLint("CheckResult")
    private static void insertTransaction(final AppDatabase db, TransactionDetail transactionDetail) {

        Completable.fromAction(() -> db.transactionDetailDao().insertTransaction(transactionDetail))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                        }// completed with success
                        , throwable -> {
                        }// there was an error
                );
    }


    private boolean IsReversalNeeded(TransactionDetail txn) {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "+ IsReversalNeeded() +");

        Utility.DEBUG_LOG(TAG, "MTI:" + txn.getMessageType());
        if (txn.getMessageType().equals("0200"))
            rv = true;
        Utility.DEBUG_LOG(TAG, "rv:" + rv);
        return rv;
    }

    public void updateAidFromCard() throws RemoteException {

        Utility.DEBUG_LOG(TAG, "+ updateAidFromCard +");
        int[] tagList = {0x9F06};

//        EmvTagValuePair emvTagValuePairCVM = null,emvTagValuePairTVR=null, emvTagValuePairAID=null ,emvTagValuePairAIP=null, emvTagValuePairCryptogram=null,emvTagValuePairCID=null,emvTagValuePairTermCap=null,emvTagValuePairTermAdd=null,emvTagValuePairIAC_Default=null,emvTagValuePairIAC_Denial=null;
        byte[] tlv = new byte[0];

        List<EmvTagValuePair> list = new ArrayList<EmvTagValuePair>();
        int count = 0;
        for (int tag : tagList) {
            Utility.DEBUG_LOG("EMV_TAGS", String.valueOf(tag));
            Utility.DEBUG_LOG("EMV_TAGS", Integer.toHexString(tag));
            Utility.DEBUG_LOG("EMV_TAGS", String.valueOf(iemv.getCardData(Integer.toHexString(tag).toUpperCase())));

            tlv = iemv.getCardData(Integer.toHexString(tag).toUpperCase());

//                    TagConstants.resetTagConstants();
            if (null != tlv && tlv.length > 0) {
                Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(tlv));
                if (tag == 0x9F06) {
                    Utility.DEBUG_LOG("Aid-Value 3", Utility.byte2HexStr(tlv));
                    ((DashboardContainer) context.get()).getTransactionDetail().setAidCode(Utility.byte2HexStr(tlv));
                } else {
                    Utility.DEBUG_LOG(TAG, "getCardData:" + Integer.toHexString(tag) + ", fails");
                }
            }
        }
    }

    public void updateEmvTagsForPrinting() throws RemoteException {
        Utility.DEBUG_LOG(TAG, "+ updateEmvTagsForPrinting +");
        int[] tagList = {
                0x9F02, 0x9F03, 0x82, 0x9F36, 0x9F26, 0x9F27,
                0x9F34, 0x5F20,
                0x9F1E, 0x9F10, 0x9F09, 0x9F33, 0x9F1A, 0x9F35, 0x95, 0x5F2A, 0x9F41,
                0x9A, 0x9C, 0x9F37, 0x84, 0x5F34, 0x9F06, 0x9F40, 0x9B, 0x9F0D, 0x9F0E, 0x9F0F, 0x5A, 0x9F6E, 0x9F66,
                0x9F12, 0x50, 0x5F25, 0x9F6C

        };

//        EmvTagValuePair emvTagValuePairCVM = null,emvTagValuePairTVR=null, emvTagValuePairAID=null ,emvTagValuePairAIP=null, emvTagValuePairCryptogram=null,emvTagValuePairCID=null,emvTagValuePairTermCap=null,emvTagValuePairTermAdd=null,emvTagValuePairIAC_Default=null,emvTagValuePairIAC_Denial=null;
        byte[] tlv = new byte[0];

        List<EmvTagValuePair> list = new ArrayList<EmvTagValuePair>();
        int count = 0;
        for (int tag : tagList) {
            Utility.DEBUG_LOG("EMV_TAGS", String.valueOf(tag));
            Utility.DEBUG_LOG("EMV_TAGS", Integer.toHexString(tag));
            Utility.DEBUG_LOG("EMV_TAGS", String.valueOf(iemv.getCardData(Integer.toHexString(tag).toUpperCase())));

            tlv = iemv.getCardData(Integer.toHexString(tag).toUpperCase());

//                    TagConstants.resetTagConstants();
            if (null != tlv && tlv.length > 0) {


                Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(tlv));
                if (tag == 0x9F34) {
                    Utility.DEBUG_LOG("CVM_Result", Utility.byte2HexStr(tlv));
                    TagConstants.CVM_Result = Utility.byte2HexStr(tlv);
                }
                if (tag == 0x9F33) {
                    Utility.DEBUG_LOG("TERM_CAP", Utility.byte2HexStr(tlv));
                    TagConstants.TERM_CAP = Utility.byte2HexStr(tlv);
                }
//                            else if(tag==149){
                else if (tag == 0x95) {
                    Utility.DEBUG_LOG("TVR-Value", String.valueOf(new EmvTagValuePair("TVR", Utility.byte2HexStr(tlv))));
                    ((DashboardContainer) context.get()).getTransactionDetail().setTvrCode(Utility.byte2HexStr(tlv));
                    Utility.DEBUG_LOG("TVR_Cons", Utility.byte2HexStr(tlv));
                    TagConstants.TVR_Cons = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F06) {
                    Utility.DEBUG_LOG("Aid-Value 2", Utility.byte2HexStr(tlv));
                    ((DashboardContainer) context.get()).getTransactionDetail().setAidCode(Utility.byte2HexStr(tlv));
                    ((DashboardContainer) context.get()).getTransactionDetail().setAidCode(Utility.byte2HexStr(tlv));
                } else if (tag == 0x9F40) {
                    Utility.DEBUG_LOG("TERM_ADD-Value", Utility.byte2HexStr(tlv));
                    TagConstants.TERM_ADD = Utility.byte2HexStr(tlv);
                } else if (tag == 0x82) {
                    Utility.DEBUG_LOG("AIP-Value", Utility.byte2HexStr(tlv));
                    TagConstants.AIP = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F26) {
                    Utility.DEBUG_LOG("Crypto-Value", Utility.byte2HexStr(tlv));
                    TagConstants.Cryptogram = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F27) {
                    Utility.DEBUG_LOG("CID-Value", Utility.byte2HexStr(tlv));
                    TagConstants.CID = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F0D) {
                    Utility.DEBUG_LOG("CID-IAC_Default", Utility.byte2HexStr(tlv));
                    TagConstants.IAC_Default = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F0E) {
                    Utility.DEBUG_LOG("CID-IAC_Denial", Utility.byte2HexStr(tlv));
                    TagConstants.IAC_Denial = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F0F) {
                    Utility.DEBUG_LOG("CID-IAC_Online", Utility.byte2HexStr(tlv));
                    TagConstants.IAC_Online = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9B) {
                    Utility.DEBUG_LOG("Constant TSI", Utility.byte2HexStr(tlv));
                    TagConstants.TSI = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F66) {
                    Utility.DEBUG_LOG("Constant TTQ", Utility.byte2HexStr(tlv));
                    TagConstants.TTQ = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F6C) {
                    Utility.DEBUG_LOG(TAG, "Constant CTQ:" + Utility.byte2HexStr(tlv));
                    TagConstants.CTQ = Utility.byte2HexStr(tlv);
                } else if (tag == 0x9F12) {
                    Utility.DEBUG_LOG("Card Name1", Utility.byte2HexStr(tlv));
                    String hex = Utility.byte2HexStr(tlv);
                    //data8583.put(ISO8583u.F_Track_1_Data_45,Utility.byte2HexStr(tlv));
                    if (hex.length() % 2 != 0) {
                        System.err.println("Invlid hex string.");
                        return;
                    }
                    StringBuilder builder = new StringBuilder();

                    for (int i = 0; i < hex.length(); i = i + 2) {
                        // Step-1 Split the hex string into two character group
                        String s = hex.substring(i, i + 2);
                        // Step-2 Convert the each character group into integer using valueOf method
                        int n = Integer.valueOf(s, 16);
                        // Step-3 Cast the integer value to char
                        builder.append((char) n);
                    }
                    Utility.DEBUG_LOG("tag Card Name1", builder.toString());


                    ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme(builder.toString());
                } else if (tag == 0x50) {
                    Utility.DEBUG_LOG("Card Name2", Utility.byte2HexStr(tlv));
                    String hex = Utility.byte2HexStr(tlv);
                    //data8583.put(ISO8583u.F_Track_1_Data_45,Utility.byte2HexStr(tlv));
                    if (hex.length() % 2 != 0) {
                        System.err.println("Invlid hex string.");
                        return;
                    }
                    StringBuilder builder = new StringBuilder();

                    for (int i = 0; i < hex.length(); i = i + 2) {
                        // Step-1 Split the hex string into two character group
                        String s = hex.substring(i, i + 2);
                        // Step-2 Convert the each character group into integer using valueOf method
                        int n = Integer.valueOf(s, 16);
                        // Step-3 Cast the integer value to char
                        builder.append((char) n);
                    }
                    Utility.DEBUG_LOG("tag Card Name2", builder.toString());

                    ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme(builder.toString());
                } else if (tag == 0x91) {
                    Utility.DEBUG_LOG("tag 0x91:", Utility.byte2HexStr(tlv));
                } else if (tag == 0x5F25) {
                    Utility.DEBUG_LOG(TAG, "tag 0x5f25: " + Utility.byte2HexStr(tlv));
                } else {
                    Utility.DEBUG_LOG(TAG, "getCardData:" + Integer.toHexString(tag) + ", fails");
                }
            }
        }
    }

    public boolean isVepsTransaction() throws RemoteException {
        boolean rv = false;
        String transactionAmount2 = DataFormatterUtil.formattedAmount(transactionAmount);
        String cardType = "";
        double transactionAmountD = 0.0f;
        double vepsLimit = 0.0f;

        Utility.DEBUG_LOG(TAG, "+ isVepsTransaction +");
        updateAidFromCard();


        Utility.DEBUG_LOG(TAG, "transactionAmount:" + transactionAmount);
        Utility.DEBUG_LOG(TAG, "transactionAmount2:" + transactionAmount2);
        Utility.DEBUG_LOG(TAG, "Transaction AID:" + ((DashboardContainer) context.get()).getTransactionDetail().getAidCode());
        Utility.DEBUG_LOG(TAG, "track2:" + ((DashboardContainer) context.get()).getTransactionDetail().getTrack2());
        Utility.DEBUG_LOG(TAG, "savedpan:" + savedPan);
        cardRanges = savedPan.substring(0, 6);
        cardType = DataFormatterUtil.setCardTypeScheme(Integer.parseInt(cardRanges));
        Utility.DEBUG_LOG(TAG, "cardRanges:" + cardRanges);
        Utility.DEBUG_LOG(TAG, "cardType:" + cardType);

        transactionAmountD = Double.parseDouble(transactionAmount2);
        Utility.DEBUG_LOG(TAG, "transactionAmountD:" + transactionAmountD);

        if (cardType.equals("VISA")) {
            vepsLimit = Double.parseDouble(VEPS_LIMIT);
        } else if (cardType.equals("MASTERCARD")) {
            vepsLimit = Double.parseDouble(VEPS_LIMIT_MC);
        } else if (cardType.equals("UPI")) {
            vepsLimit = Double.parseDouble(VEPS_LIMIT_UPI);
        } else if (cardType.equals("PAYPAK")) {
            vepsLimit = Double.parseDouble(VEPS_LIMIT_PAYPAK);
        } else if (cardType.equals("JCB")) {
            vepsLimit = Double.parseDouble(VEPS_LIMIT_JCB);
        } else if (cardType.equals("AMEX")) {
            vepsLimit = Double.parseDouble(VEPS_LIMIT_AMEX);
        }


        if (txnTypePresenter.equals("REDEEM") || txnTypePresenter.equals("AUTH")) {
//            vepsLimit = 0.00;
            return false;
        }

        Utility.DEBUG_LOG(TAG, "VEPS limit loaded: " + vepsLimit + "; card scheme: " + cardType);

        if (transactionAmountD <= vepsLimit) {
            Utility.DEBUG_LOG(TAG, "Transaction under VEPS limit, do not prompt for PIN");
            rv = true;
        }

        Utility.DEBUG_LOG(TAG, "isVepsTransaction:return rv:" + rv);
        return rv;
//        return true;//for testing
    }

    public boolean isUpiCtlsPinSuppressed() throws RemoteException {
        boolean rv = false;
        String transactionAmount2 = DataFormatterUtil.formattedAmount(transactionAmount);
        double cvmLimitD = 0.0f;
        double transactionAmountD = 0.0f;
        Utility.DEBUG_LOG(TAG, "+ isUpiCtlsPinSuppressed +");
                    /*
                    1. A000000333010101 – UnionPay Debit
                    2. A000000333010102 – UnionPay Credit
                    3. A000000333010103 – UnionPay Quasi Credit
                     */
        String debit = "A000000333010101";
        String credit = "A000000333010102";
        String quasiCredit = "A000000333010103";
        int isRmbCurrency = SharedPref.read("isRmbCurrency");
        Utility.DEBUG_LOG(TAG, "isRmbCurrency:" + isRmbCurrency);

        String cvmLimit = terminalConfig.getCvmLimitUpi();

        updateAidFromCard();
        Utility.DEBUG_LOG(TAG, "transactionAmount:" + transactionAmount);
        Utility.DEBUG_LOG(TAG, "transactionAmount2:" + transactionAmount2);
        Utility.DEBUG_LOG(TAG, "Transaction AID:" + ((DashboardContainer) context.get()).getTransactionDetail().getAidCode());
        Utility.DEBUG_LOG(TAG, "cvmLimit:" + cvmLimit);
        cvmLimitD = Double.parseDouble(cvmLimit);
        transactionAmountD = Double.parseDouble(transactionAmount2);
        Utility.DEBUG_LOG(TAG, "cvmLimitD:" + cvmLimitD);
        Utility.DEBUG_LOG(TAG, "transactionAmountD:" + transactionAmountD);
        if (((DashboardContainer) context.get()).getTransactionDetail().getAidCode() == null) {
            Utility.DEBUG_LOG(TAG, "getAidCode() == null");
            return rv;
        } else if (isRmbCurrency == 0)//PKR currency
        {
            Utility.DEBUG_LOG(TAG, "PKR currency...");
            if (
                    (((DashboardContainer) context.get()).getTransactionDetail().getAidCode().equals(credit) ||
                            ((DashboardContainer) context.get()).getTransactionDetail().getAidCode().equals(quasiCredit))
                            && ((DashboardContainer) context.get()).getTransactionDetail().getCardType().equals("Contactless")
            ) {
                Utility.DEBUG_LOG(TAG, "UPI credit or quasiCredit AID found [CTLS]");
                if (transactionAmountD < cvmLimitD) {
                    Utility.DEBUG_LOG(TAG, "Amount less than CVM, still suppress PIN prompt");
                    rv = true;
                }
            }

        } else if (isRmbCurrency == 1)//RMB currency
        {
            Utility.DEBUG_LOG(TAG, "RMB currency...");
            if (
                    (((DashboardContainer) context.get()).getTransactionDetail().getAidCode().equals(credit) ||
                            ((DashboardContainer) context.get()).getTransactionDetail().getAidCode().equals(quasiCredit))
                            && ((DashboardContainer) context.get()).getTransactionDetail().getCardType().equals("Contactless")
            ) {
                Utility.DEBUG_LOG(TAG, "UPI credit or quasiCredit AID found [CTLS]");
                if (transactionAmountD < cvmLimitD) {
                    Utility.DEBUG_LOG(TAG, "Amount less than CVM, still suppress PIN prompt");
                    rv = true;
                }
            }

            if (((DashboardContainer) context.get()).getTransactionDetail().getAidCode().equals(debit)
                    && ((DashboardContainer) context.get()).getTransactionDetail().getCardType().equals("Contactless")
                    && transactionAmountD == 1.00f
            ) {
                Utility.DEBUG_LOG(TAG, "UPI Debit card CTLS case with amount 1.00");
                rv = true;
            }
        }

        Utility.DEBUG_LOG(TAG, "isUpiCtlsPinSuppressed:return rv:" + rv);
        return rv;
//        return true;//for testing
    }

    /**
     * \brief initialize the pin pad listener
     * <p>
     * \code{.java}
     * \endcode
     */
    private void initializePinInputListener() {
        pinInputListener = new PinInputListener.Stub() {
            @Override
            public void onInput(int len, int key) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onInput, len:" + len + ", key:" + key);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onConfirm");
//                //+ taha 02-03-2021 patch for UPI debit card
//                if ( isUpiCtlsPinSuppressed() )
//                {
//                    Utility.DEBUG_LOG(TAG,"Suppress PIN prompt");
//                    iemv.importPin(1, null);
//                    isPinInput=false;
////                    CARD_AUTH_METHOD = "SIGNATURE";
//                }
//                else
//                {
//                    Utility.DEBUG_LOG(TAG,"proceed normally win PIN input");
//                    iemv.importPin(1, data);
//                    savedPinBlock = data;
//                }
                iemv.importPin(1, data);
                savedPinBlock = data;
            }

            @Override
            public void onCancel() throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onCancel");
                DashboardContainer.backStack();
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onError, code:" + errorCode);
                DashboardContainer.backStack();
            }
        };
    }

    private void initializeLast4DigitsListener() {
        last4DigitsListener = new PinInputListener.Stub() {
            @Override
            public void onInput(int len, int key) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "Last4digits onInput, len:" + len + ", key:" + key);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "Last4digits onConfirm");
                iemv.importPin(1, data);
                last4Digits = data;
                Utility.DEBUG_LOG(TAG, "last4Digits:" + last4Digits);
            }

            @Override
            public void onCancel() throws RemoteException {
                Utility.DEBUG_LOG(TAG, "Last4digits onCancel");
                DashboardContainer.backStack();
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "Last4digits onError, code:" + errorCode);
                DashboardContainer.backStack();
            }
        };
    }

    private void loadConfiguration() {
        // TransPrinter.initialize(printer); // SHIVAM UPDATE HERE FOR TEXT PRINTER
        //CARD_AUTH_METHOD = "SIGNATURE";
        this.rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();

        Constants.BATCH_NO = String.format(Locale.getDefault(), "%06d", SharedPref.read("batch_no"));
//
        int inv = SharedPref.read("invoice_no");
        inv = inv + 1;
        SharedPref.write("invoice_no", inv);
        Constants.INVOICE_NO = String.format(Locale.getDefault(), "%06d", SharedPref.read("invoice_no"));
        Utility.DEBUG_LOG("Generate Invoice Number", INVOICE_NO);
        txnTypePresenter = String.valueOf(SharedPref.read("button_fragment", txnTypePresenter));
        Utility.DEBUG_LOG("Presenter type", txnTypePresenter);

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            this.headerType = obj.getString("Header Type");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = assetManager.open("config.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void initialize8583data(ISO8583u.SimulatorType st) {
        ISO8583u.simType = st;
    }

    // check assets fonts and copy to file system for Service -- start
    private void InitializeFontFiles() {
        PrinterFonts.initialize(assetManager);
    }

    /**
     * \brief show the pinpad
     * <p>
     * \code{.java}
     * \endcode
     */
    private void doPinPad(boolean isOnlinePin, int retryTimes) {
        Bundle param = new Bundle();
        Bundle globleparam = new Bundle();
        String panBlock = savedPan;

        byte[] pinLimit = {4, 5, 6, 8, 12}; //todo add 0 to bypass pin, if needed
        param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "ENTER YOUR CARD PIN");
        param.putString("promptsFont", "/system/fonts/DroidSans-Bold.ttf");
        param.putByteArray("displayKeyValue", new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, 20);
        param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, isOnlinePin);
        param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, panBlock);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_desType_int, ConstIPinpad.startPinInput.param.Value_desType_3DES);
        if (!isOnlinePin) {
            param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "OFFLINE PIN, retry times:" + retryTimes);
            ((DashboardContainer) context.get()).getTransactionDetail().setCVM("OFFLINE_PIN"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
        }
        //globleparam.putString( ConstIPinpad.startPinInput.globleParam.KEY_Display_One_String, "[1]");
        try {

            ipinpad.startPinInput(workKeyId, param, globleparam, pinInputListener);
            ((DashboardContainer) context.get()).getTransactionDetail().setCVM("ONLINE_PIN"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void doPinPadSynchronous(boolean isOnlinePin, int retryTimes) {
        Utility.DEBUG_LOG(TAG, "Before doPinPad");
        doPinPad(isOnlinePin, retryTimes);
        Utility.DEBUG_LOG(TAG, "After doPinPad");
        while (true) {
            if (savedPinBlock == null) {
                Utility.DEBUG_LOG(TAG, "in while");
                try {
                    sleep(1 * 1000);
                } catch (Exception e) {
                    Utility.DEBUG_LOG(TAG, "exception:" + e.getMessage().toString());
                }
            } else {
                Utility.DEBUG_LOG(TAG, "break pin input");
                break;
            }
        }
    }

    byte[] getReversalPacket(ISO8583u iso8583u, GenerateReversal generateReversal) {


        Utility.DEBUG_LOG(TAG, "+ getReversalPacket +");
        byte[] packet = null;
        //                reversalBeingProcessed = true;
        SparseArray<String> reversal = generateReversal.generateReversalPacket();
        if (reversal.get(ISO8583u.F_55) != null) {
            SparseArray<String> tagOfF55Reversal = generateReversal.createTagOf55();
            if (tagOfF55Reversal != null) {
                for (int i = 0; i < tagOfF55Reversal.size(); i++) {
                    int tag = tagOfF55Reversal.keyAt(i);
                    String value = tagOfF55Reversal.valueAt(i);
                    if (value.length() > 0) {
                        byte[] tmp = iso8583u.appendF55(tag, value);
                        if (tmp == null) {
                            Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                        } else {
                            Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                        }
                    }
                }
            }
        }
        packet = iso8583u.makePacket(reversal, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
        return packet;
    }

    private void setOfflineTransactionFields() throws RemoteException {
        Utility.DEBUG_LOG(TAG, "+ setOfflineTransactionFields +");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("HHmmss", Locale.getDefault());
        String transactionTime = sdf.format(new Date());
        sdf = new SimpleDateFormat("MMdd", Locale.getDefault());
        String transactionDate = sdf.format(new Date());
        Utility.DEBUG_LOG(TAG, "Date:" + transactionDate);
        Utility.DEBUG_LOG(TAG, "Time:" + transactionTime);

        Utility.DEBUG_LOG(TAG, "Step 1");
        ((DashboardContainer) context.get()).getTransactionDetail().setIsAdvice(true);
        ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(transactionDate);
        ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(transactionTime);
        ((DashboardContainer) context.get()).getTransactionDetail().setAmount(this.transactionAmount);
        ((DashboardContainer) context.get()).getTransactionDetail().setTransactionAmount(this.transactionAmount);
        ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
        Utility.DEBUG_LOG(TAG, "Step 2");
        byte[] packet;
        ISO8583u iso8583u;
        iso8583u = new ISO8583u();
        if (tagOfF55 != null) {
            for (int i = 0; i < tagOfF55.size(); i++) {
                int tag = tagOfF55.keyAt(i);
                String value = tagOfF55.valueAt(i);
                Utility.DEBUG_LOG(TAG, "tag:" + tag + ", value:" + value);
                if (value.length() > 0) {
                    byte[] tmp = iso8583u.appendF55(tag, value);
                    if (tmp == null) {
                        Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                    } else {
                        Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                        if (Integer.toHexString(tag).equals("84")) {
                            aid = Utility.byte2HexStr(tmp).substring(4);
                        }
                    }
                }
            }
            tagOfF55 = null;
        }

        Utility.DEBUG_LOG(TAG, "Step 3");

        String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
        String currentDate = new SimpleDateFormat("MMdd", Locale.getDefault()).format(new Date());
        data8583.put(ISO8583u.F_STAN_11, INVOICE_NO);
        Utility.DEBUG_LOG(TAG, "invoice 4" + INVOICE_NO);
        Utility.DEBUG_LOG(TAG, "before iso8583u.makePacket data8583");
        packet = iso8583u.makePacket(data8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
        ((DashboardContainer) context.get()).getTransactionDetail().setMId(MERCHANT_ID);
        ((DashboardContainer) context.get()).getTransactionDetail().setTId(TERMINAL_ID);
        ((DashboardContainer) context.get()).getTransactionDetail().setStan(data8583.get(ISO8583u.F_STAN_11));
        ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);
        ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(currentDate);
        ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(currentTime);

        Utility.DEBUG_LOG(TAG, "Step 4");
        if (((DashboardContainer) context.get()).getTransactionDetail().getCardNo() == null) {
            ((DashboardContainer) context.get()).getTransactionDetail().setCardNo(savedPan);
        }
        ((DashboardContainer) context.get()).getTransactionDetail().setTxnType(txnTypePresenter);
        cardRanges = savedPan.substring(0, 6);
        cardType = DataFormatterUtil.setCardTypeScheme(Integer.parseInt(cardRanges));
        ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme2(cardType);
        if (((DashboardContainer) context.get()).getTransactionDetail().getCardScheme() == null) {
            ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme(cardType);

        }
        Utility.DEBUG_LOG(TAG, "Step 5");
        ((DashboardContainer) context.get()).getTransactionDetail().setCancelTxnType("None");
        Utility.DEBUG_LOG(TAG, "invoice 1" + INVOICE_NO);
        ((DashboardContainer) context.get()).getTransactionDetail().setInvoiceNo(INVOICE_NO);
        ((DashboardContainer) context.get()).getTransactionDetail().setBatchNo(BATCH_NO);
        ((DashboardContainer) context.get()).getTransactionDetail().setTrack2(track2);
        ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);
        Utility.DEBUG_LOG(TAG, "Step 6");
        Utility.DEBUG_LOG(TAG, "MTI:" + txnTypePresenter);
        switch (txnTypePresenter) {
            case "SALE":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(SALE_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(SALE_PROCESSING_CODE);
                data8583.put(ISO8583u.F_ProcessingCode_03, SALE_PROCESSING_CODE);
                break;

            case "SALEIPP":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(SALE_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(SALEIPP_PROCESSING_CODE);
                data8583.put(ISO8583u.F_ProcessingCode_03, SALEIPP_PROCESSING_CODE);
                Utility.DEBUG_LOG("SALEIPP", String.valueOf(data8583));
                break;

            case "PRE AUTH":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(PREAUTH_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(PRE_AUTH_PROCESSING_CODE);
                break;
            case "AUTH":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(PREAUTH_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode("000000");
                break;
            case "REDEEM":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(ORBIT_REDEEM_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(ORBIT_REDEEM_PROCESSING_CODE);
                redeemCheck = true;
                break;
            case "ORBIT INQUIRY":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(ORBIT_INQUIRY_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(ORBIT_INQUIRY_PROCESSING_CODE);
                break;
            case "CASH OUT":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(CASH_OUT_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(CASH_OUT_PROCESSING_CODE);
                break;

            case "CASH ADV":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(CASH_ADV_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(CASH_ADV_PROCESSING_CODE);
                break;
            case "REFUND":
                ((DashboardContainer) context.get()).getTransactionDetail().setMessageType(CASH_ADV_MESSAGE_TYPE);
                ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode(REFUND_MESSAGE_TYPE);
                break;
        }
        Utility.DEBUG_LOG(TAG, "Step 7");

        ((DashboardContainer) context.get()).getTransactionDetail().setAcquiringInstitutionIdentificationCode(ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        ((DashboardContainer) context.get()).getTransactionDetail().setForwardingInstitutionIdentificationCode(FORWARDING_INSTITUTION_IDENTIFICATION_CODE);

//        set9F27("40");
        if (!(txnTypePresenter.equals("ORBIT INQUIRY")) && !(txnTypePresenter.equals("AUTH"))) {
            // for (int i = 0; i < 300; i++){
            insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
            // }
            Utility.DEBUG_LOG(TAG, "insert ((DashboardContainer) context.get()).getTransactionDetail() obj to DB 1");
        }

        set8A("Y3");
//        Bundle onlineResult = new Bundle();
//        onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, "Y3");
//        iemv.inputOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
//            @Override
//            public void onProccessResult(int result, Bundle data) throws RemoteException {
//                Utility.DEBUG_LOG(TAG, "+ onProccessResult +");
//                Utility.DEBUG_LOG(TAG, "onProccessResult callback:");
//                String str = "RESULT:" + result +
//                        "\nTC_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_TC_DATA_String, "not defined") +
//                        "\nSCRIPT_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_SCRIPT_DATA_String, "not defined") +
//                        "\nREVERSAL_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_REVERSAL_DATA_String, "not defined");
//                transactionContract.toastShow(str);
//            }
//        });
    }

    private boolean isOfflineTransaction() {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "+ isOfflineTransaction +");
        String saleOfflineLimitStr = terminalConfig.getSaleOfflineLimit();
        String transactionAmountStr;
        if (this.transactionAmount.indexOf(".") > 0) // decimal found
        {
            Utility.DEBUG_LOG(TAG, "Decimal found, assign as is..");
            transactionAmountStr = this.transactionAmount;
        } else {
            Utility.DEBUG_LOG(TAG, "BCD encoded amount, format it...");
            transactionAmountStr = DataFormatterUtil.formattedAmount(this.transactionAmount);
        }
        this.transactionAmount = transactionAmountStr;
        double transactionAmountD = 0, saleOfflineLimitD = 0;
        saleOfflineLimitD = Double.parseDouble(saleOfflineLimitStr);
        transactionAmountD = Double.parseDouble(transactionAmountStr);
        Utility.DEBUG_LOG(TAG, "saleOfflineLimitD:" + saleOfflineLimitD);
        Utility.DEBUG_LOG(TAG, "transactionAmountD:" + transactionAmountD);
        if (transactionAmountD <= saleOfflineLimitD) {
            Utility.DEBUG_LOG(TAG, "Amount < offline limit, approve offline");
            try {
                setOfflineTransactionFields();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            rv = true;
        }
        return rv;
    }

    /**
     * \Brief make purchase fields
     */

    /*public void doPurchase(String amount, String tipAmount) {
        //

        SparseArray<String> data8583_u_purchase = new SparseArray<>();

                SalePackageGenerator salePackageGenerator= new SalePackageGenerator();
                data8583_u_purchase= salePackageGenerator.getSalePacket();
        String cleanamount = amount.replaceAll("[*,]", "");
        float a = Double.parseDouble(cleanamount);
        if(a>0) {
            cleanAmount = Double.parseDouble(cleanamount);
            float b = Double.parseDouble(tipAmount);
            float c = a + b;
            cleanamount = String.format(Locale.getDefault(), "%012d", (int) (Double.parseDouble(cleanamount) * 100));
            tipAmount = String.format(Locale.getDefault(), "%012d", (int) (Double.parseDouble(tipAmount) * 100));
            transAmount = String.format(Locale.getDefault(), "%012d", (int) (c * 100));
            //transAmount= amount+tipAmount;
            this.tipAmount = tipAmount;
            this.transactionAmount = cleanamount;
            //data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
            data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, transAmount);
            data8583 = data8583_u_purchase;
            doTransaction();
        }
        else {
            transactionContract.transactionFailed("Error","amount should be greater than 0");
            DashboardContainer.switchFragmentWithBackStack(new HomeFragment(), "home");
        }


    }*/ /// Shivam's code update on 15 Jan 2021 for amount Decimal point calculation
    public void doPurchase(String amount, String tipAmount, FallbackOrignatorType... isFallback) {
        //
        Utility.DEBUG_LOG(TAG, "+ doPurchase +");
        SparseArray<String> data8583_u_purchase = new SparseArray<>();

        SalePackageGenerator salePackageGenerator = new SalePackageGenerator();
        data8583_u_purchase = salePackageGenerator.getSalePacket();
        Utility.DEBUG_LOG("SL Amount", amount);//taha: this ok

        String cleanamount = amount.replaceAll("[*,]", "");
        String cleanamount1 = tipAmount.replaceAll("[*,]", "");

        double a = Double.parseDouble(cleanamount);
        Utility.DEBUG_LOG("SL Amount", String.valueOf(a));//taha: this not ok
        if (a > 0) {
            cleanAmount = Double.parseDouble(cleanamount);
            Double b = Double.parseDouble(cleanamount1);
            Utility.DEBUG_LOG(TAG, "B" + b);
            Double c = (a + b);

            if (TIP_ENABLED.equals("Y")) {
                cleanamount=String.format(Locale.getDefault(), "%.2f", (Double) (Double.parseDouble(cleanamount))); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                ((DashboardContainer) context.get()).getTransactionDetail().setTransactionAmount(cleanamount);
                ((DashboardContainer) context.get()).getTransactionDetail().setTipAmount(cleanamount1);
                if (b > 0) {
                    int adjustCounter = ((DashboardContainer) context.get()).getTransactionDetail().getTipNumAdjustCounter();
                    //adjustCounter++;
                    Utility.DEBUG_LOG(TAG, "adjustCounter:" + adjustCounter);
                    adjustCounter++;
                    ((DashboardContainer) context.get()).getTransactionDetail().setTipNumAdjustCounter(adjustCounter);
                    Utility.DEBUG_LOG(TAG, "adjustCounter increment:" + adjustCounter);
                }
            }
            Utility.DEBUG_LOG("SL Amount C", String.valueOf(c));
            cleanamount = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(cleanamount) * 100));
            tipAmount = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(cleanamount1) * 100));
            transAmount = String.format(Locale.getDefault(), "%012.0f", (Double) (c * 100));
            //transAmount= amount+tipAmount;
            Utility.DEBUG_LOG("SL transAmount", transAmount);
            this.tipAmount = tipAmount;
            this.transactionAmount = cleanamount;
            //data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
//            if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
//
//                if ((Constants.TIP_ENABLED.equalsIgnoreCase("Y")))
//                    data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, tipAmount);
//                data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, transAmount);
//            }
            Utility.DEBUG_LOG(TAG, "TIPAMOUNT" + tipAmount);

            if ((Constants.TIP_ENABLED.equalsIgnoreCase("Y")) && txnTypePresenter.equals("SALE"))
                data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, tipAmount);


            data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, transAmount);

            Utility.DEBUG_LOG(TAG, "Transamount" + transAmount);

            if (txnTypePresenter.equals("SALEIPP")) {
                double tAmount = Double.parseDouble(DataFormatterUtil.formattedAmount(transAmount));
                double tenure = Double.parseDouble(saleippMonths);
                Utility.DEBUG_LOG("SALEIPP amount", String.valueOf(tAmount));
                Utility.DEBUG_LOG("SALEIPP tenure", String.valueOf(tenure));
                installment = tAmount / tenure;
                Utility.DEBUG_LOG("SALEIPP installment", String.valueOf(installment));
                ((DashboardContainer) context.get()).getTransactionDetail().setSaleippInstallments(String.valueOf(installment));
                ((DashboardContainer) context.get()).getTransactionDetail().setSaleippMonth(String.valueOf(saleippMonths));
            }

            data8583 = data8583_u_purchase;
            if (isFallback.length > 0) {
//                if ( fallbackType == FT_Dip )
//                    inputLabelAmount.setText("Swipe");
//                else if ( fallbackType == FT_Swipe )
//                    inputLabelAmount.setText("Tap / Insert / Swipe");
//                else if ( fallbackType == FT_Ctls )
//                    inputLabelAmount.setText("Insert / Swipe");

                if (isFallback[0] == FallbackOrignatorType.FT_Dip) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to swipe");
                    doTransaction(ReaderType.RT_Mag);
                } else if (isFallback[0] == FallbackOrignatorType.FT_Swipe) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to Tap / Insert / Swipe");
                    doTransaction(ReaderType.RT_All);
                } else if (isFallback[0] == FallbackOrignatorType.FT_Ctls) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to Insert / Swipe");
                    doTransaction(ReaderType.RT_Chip_Mag);
                } else if (isFallback[0] == FallbackOrignatorType.FT_SeeYourPhone) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to Tap / Insert / Swipe");
                    doTransaction(ReaderType.RT_All);
                }

            } else {
                Utility.DEBUG_LOG(TAG, "before doTransaction");
                doTransaction(ReaderType.RT_All);
            }
        } else {
            transactionContract.transactionFailed("Error", "amount should be greater than 0");
            DashboardContainer.backStack();
        }


    }

    public void doPurchase(String amount,String saleippMonths, String tipAmount, FallbackOrignatorType... isFallback) {
        //
        this.saleippMonths  = saleippMonths;
        Utility.DEBUG_LOG(TAG, "+ doPurchase +");
        SparseArray<String> data8583_u_purchase = new SparseArray<>();

        SalePackageGenerator salePackageGenerator = new SalePackageGenerator();
        data8583_u_purchase = salePackageGenerator.getSalePacket();
        Utility.DEBUG_LOG("SL Amount", amount);//taha: this ok

        String cleanamount = amount.replaceAll("[*,]", "");
        String cleanamount1 = tipAmount.replaceAll("[*,]", "");

        double a = Double.parseDouble(cleanamount);
        Utility.DEBUG_LOG("SL Amount", String.valueOf(a));//taha: this not ok
        if (a > 0) {
            cleanAmount = Double.parseDouble(cleanamount);
            Double b = Double.parseDouble(cleanamount1);
            Utility.DEBUG_LOG(TAG, "B" + b);
            Double c = (a + b);

            if (TIP_ENABLED.equals("Y")) {
                cleanamount=String.format(Locale.getDefault(), "%.2f", (Double) (Double.parseDouble(cleanamount))); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                ((DashboardContainer) context.get()).getTransactionDetail().setTransactionAmount(cleanamount);
                ((DashboardContainer) context.get()).getTransactionDetail().setTipAmount(cleanamount1);
                if (b > 0) {
                    int adjustCounter = ((DashboardContainer) context.get()).getTransactionDetail().getTipNumAdjustCounter();
                    //adjustCounter++;
                    Utility.DEBUG_LOG(TAG, "adjustCounter:" + adjustCounter);
                    adjustCounter++;
                    ((DashboardContainer) context.get()).getTransactionDetail().setTipNumAdjustCounter(adjustCounter);
                    Utility.DEBUG_LOG(TAG, "adjustCounter increment:" + adjustCounter);
                }
            }
            Utility.DEBUG_LOG("SL Amount C", String.valueOf(c));
            cleanamount = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(cleanamount) * 100));
            tipAmount = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(cleanamount1) * 100));
            transAmount = String.format(Locale.getDefault(), "%012.0f", (Double) (c * 100));
            //transAmount= amount+tipAmount;
            Utility.DEBUG_LOG("SL transAmount", transAmount);
            this.tipAmount = tipAmount;
            this.transactionAmount = cleanamount;
            //data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
//            if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
//
//                if ((Constants.TIP_ENABLED.equalsIgnoreCase("Y")))
//                    data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, tipAmount);
//                data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, transAmount);
//            }
            Utility.DEBUG_LOG(TAG, "TIPAMOUNT" + tipAmount);

            if ((Constants.TIP_ENABLED.equalsIgnoreCase("Y")) && txnTypePresenter.equals("SALE"))
                data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, tipAmount);


            data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, transAmount);

            Utility.DEBUG_LOG(TAG, "Transamount" + transAmount);

            if (txnTypePresenter.equals("SALEIPP")) {
                double tAmount = Double.parseDouble(DataFormatterUtil.formattedAmount(transAmount));
                double tenure = Double.parseDouble(saleippMonths);
                Utility.DEBUG_LOG("SALEIPP amount", String.valueOf(tAmount));
                Utility.DEBUG_LOG("SALEIPP tenure", String.valueOf(tenure));
                installment = tAmount / tenure;
                Utility.DEBUG_LOG("SALEIPP installment", String.valueOf(installment));
                ((DashboardContainer) context.get()).getTransactionDetail().setSaleippInstallments(String.valueOf(installment));
                ((DashboardContainer) context.get()).getTransactionDetail().setSaleippMonth(String.valueOf(saleippMonths));
            }

            data8583 = data8583_u_purchase;
            if (isFallback.length > 0) {
//                if ( fallbackType == FT_Dip )
//                    inputLabelAmount.setText("Swipe");
//                else if ( fallbackType == FT_Swipe )
//                    inputLabelAmount.setText("Tap / Insert / Swipe");
//                else if ( fallbackType == FT_Ctls )
//                    inputLabelAmount.setText("Insert / Swipe");

                if (isFallback[0] == FallbackOrignatorType.FT_Dip) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to swipe");
                    doTransaction(ReaderType.RT_Mag);
                } else if (isFallback[0] == FallbackOrignatorType.FT_Swipe) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to Tap / Insert / Swipe");
                    doTransaction(ReaderType.RT_All);
                } else if (isFallback[0] == FallbackOrignatorType.FT_Ctls) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to Insert / Swipe");
                    doTransaction(ReaderType.RT_Chip_Mag);
                } else if (isFallback[0] == FallbackOrignatorType.FT_SeeYourPhone) {
                    Utility.DEBUG_LOG(TAG, "before doTransaction fallback to Tap / Insert / Swipe");
                    doTransaction(ReaderType.RT_All);
                }

            } else {
                Utility.DEBUG_LOG(TAG, "before doTransaction");
                doTransaction(ReaderType.RT_All);
            }
        } else {
            transactionContract.transactionFailed("Error", "amount should be greater than 0");
            DashboardContainer.backStack();
        }


    }

    public void doTransaction(ReaderType readerType) {


        // set some fields
        data8583.put(ISO8583u.F_TID_41, TERMINAL_ID);//42017890  UPI //11111112 UBL
        data8583.put(ISO8583u.F_MerchantID_42, MERCHANT_ID);//200000005000510 UPI // 1100000001     //// UBL

        //+ taha 17-03-2021 reset to some value

        //  Constants.CARD_AUTH_METHOD = "SIGNATURE"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
        ((DashboardContainer) context.get()).getTransactionDetail().setCVM("SIGNATURE");// SHIVAM UPDATE HERE FOR TEXT PRINTER
        // do search card and online request
        Utility.DEBUG_LOG(TAG, "before doSearchCard");
        doSearchCard(readerType);
    }

    public void doSearchCard(ReaderType readerType) {

        Utility.DEBUG_LOG(TAG, "doSearchCard(readerType)");
        transactionContract.toastShow("show");
        if(Constants.isEcrReq && Constants.ecrType.equals("M"))
            ECR.ECRMsgWrite("Card Prompt");
        Bundle cardOption = new Bundle();
        ((DashboardContainer) context.get()).getTransactionDetail().setIsFallback(false);
//        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_unsupported);
        switch (readerType) {
            case RT_Chip:
                Utility.DEBUG_LOG(TAG, "enable dip only");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportSmartCard", true);
                break;
            case RT_Mag:
                Utility.DEBUG_LOG(TAG, "enable mag only");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportMagCard", true);
                ((DashboardContainer) context.get()).getTransactionDetail().setIsFallback(true);

                break;
            case RT_CTLS:
                Utility.DEBUG_LOG(TAG, "enable ctls only");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportCTLSCard", true);
                break;
            case RT_Chip_Mag:
                Utility.DEBUG_LOG(TAG, "enable dip and mag readers");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportMagCard", true);
                cardOption.putBoolean("supportSmartCard", true);
                break;
            case RT_All:
                Utility.DEBUG_LOG(TAG, "enable all readers");
                if(context.get()!=null) {
                    cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                    cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                    cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                    cardOption.putBoolean("supportMagCard", true);
                    cardOption.putBoolean("supportCTLSCard", true);
                    cardOption.putBoolean("supportSmartCard", true);
                }
                break;
            default:
                Utility.DEBUG_LOG(TAG, "default: enable all readers");
                Utility.DEBUG_LOG(TAG,"onBackward1:"+onBackward);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportMagCard", true);
                cardOption.putBoolean("supportCTLSCard", true);
                cardOption.putBoolean("supportSmartCard", true);
                break;
        }
        ;
//        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean("supportMagCard", true);
//        //+ masood 01-09-2020 commented below line
//        cardOption.putBoolean("supportCTLSCard", true);
////        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean("supportSmartCard", true);
        String transactionAmount = this.transactionAmount;


        try {

            iemv.checkCard(cardOption, 30, new CheckCardListener.Stub() {
                        @Override
                        public void onCardSwiped(Bundle track) throws RemoteException {
                            Utility.DEBUG_LOG(TAG, "onCardSwiped ...");

                            iBeeper.startBeep(200);

                            String pan = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String);
                            String track1 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String);
                            track2 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String);
                            String track3 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String);
                            String serviceCode = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String);

                            Utility.DEBUG_LOG("cardNam", track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String));
                            //+ taha 23-02-2021 regex logic is incorrect, remaking it
                            //cardholdername = track1.replaceAll(".+\\d\\^", "");
//                            cardholdername = cardholdername.replaceAll("\\^.+", "").trim();
                            Utility.DEBUG_LOG(TAG, "track1:" + track1);
                            cardholdername = track1.replaceAll("(.*\\^)(.*)(\\^.*)", "$2");
                            Utility.DEBUG_LOG(TAG, cardholdername);
                            String pan1 = pan.substring(0, 6);
                            String Last4digits = track2;
                            String cardnumber = Last4digits.replaceAll("\\=.*", "");
                            int startNumber = cardnumber.length() - 4;
                            int endNumber = cardnumber.length();
                            String fourDigits = cardnumber.substring(startNumber, endNumber);
                            Utility.DEBUG_LOG("regex", String.valueOf(fourDigits));
                            Utility.DEBUG_LOG("context.get().get()", String.valueOf(context.get()));

                            boolean bl = vs.validateCardRange(pan);
                            if (!bl) {

                                transactionContract.hideProgress();
                                transactionContract.transactionFailed("Error", "Bin Not Supported");
                                DashboardContainer.backStack();
                            } else {
                                savedPan = pan;
                                String cardExpiryMag = track2.replaceAll(".+\\=", "");
                                Utility.DEBUG_LOG("cardExpiryMag", cardExpiryMag);
                                int cardexp = Integer.parseInt(cardExpiryMag.substring(0, 4));
                                String cardExpiryYear = cardExpiryMag.substring(0, 2);
                                String cardExpiryMonth = cardExpiryMag.substring(2, 4);
                                cardExpiry = cardExpiryMonth + "/" + cardExpiryYear;
                                String expiryCheck = new SimpleDateFormat("yyMM", Locale.getDefault()).format(new Date());
                                int currentDateExpiry = Integer.parseInt(expiryCheck);


                                if (null != track2) {
                                    Utility.DEBUG_LOG(TAG, "track2: " + track2);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setTrack2(track2);
                                    Utility.DEBUG_LOG(TAG, "before put F_Track_2_Data_35 swipe");

                                    data8583.put(ISO8583u.F_Track_2_Data_35, track2); //refill this
                                }
                                Utility.DEBUG_LOG(TAG, "getIsFallback" + ((DashboardContainer) context.get()).getTransactionDetail().getIsFallback());
                                if (((DashboardContainer) context.get()).getTransactionDetail().getIsFallback()) {
                                    Utility.DEBUG_LOG(TAG, "In case of fallback ignore track2 service code first digit");
                                } else if (isChipCardFromTrack2()) {
                                    Utility.DEBUG_LOG(TAG, "Chip card detected1, fallback to dip/ctls");
//                                    HomeFragment newFragment = new HomeFragment();
//                                    transactionContract.hideProgress();
                                    transactionContract.transactionFailed("ERROR", "Chip card detected");
//                                    DashboardContainer.switchFragmentWithBackStack(newFragment, "Failed");
                                    if(txnTypePresenter.equals("SALEIPP")) {
                                        DashboardContainer.switchFragmentWithBackStack(new SaleIPPFragment(FallbackOrignatorType.FT_Dip, transactionAmount,saleippMonths), "saleipp fragment");
                                    }else {
                                         DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(FallbackOrignatorType.FT_Swipe, transactionAmount, tipAmount), "payment fragment");
                                    }
                                    return;
                                }
                                if (currentDateExpiry > cardexp) {

                                    transactionContract.hideProgress();
                                    transactionContract.transactionFailed("error", "Card Expired");
                                    DashboardContainer.backStack();
                                } else {
                                    Utility.DEBUG_LOG(TAG, "cardExpiry 1: " + cardExpiry);
                                    Utility.DEBUG_LOG(TAG, "cardExpiryYear+cardExpiryMonth: " + cardExpiryYear + cardExpiryMonth);
                                    if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
                                        Utility.DEBUG_LOG(TAG, "trying to set expiry...");
//                                        data8583.put(ISO8583u.F_DateOfExpired_14, cardExpiry);
                                        data8583.put(ISO8583u.F_DateOfExpired_14, cardExpiryYear + cardExpiryMonth);
                                    }
                                    data8583.put(ISO8583u.F_ServiceCode_61, FIELD_61);
                                    if (HOST_INDEX_FROM_DB != 0) {
                                        data8583.put(ISO8583u.F_AccountNumber_02, savedPan);
                                    }
                                    if (txnTypePresenter.equals("AUTH")) {
                                        data8583.put(ISO8583u.F_AccountNumber_02, savedPan);
                                    }
                                    Utility.DEBUG_LOG(TAG, "onCardSwiped ...1");
                                    byte[] bytes = Utility.hexStr2Byte(track2);
                                    Utility.DEBUG_LOG(TAG, "Track2:" + track2 + " (" + Utility.byte2HexStr(bytes) + ")");
                                    Boolean bIsKeyExist = null;
                                    try {
                                        bIsKeyExist = ipinpad.isKeyExist(DUKPTKEY, 1);
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    if (!bIsKeyExist) {
                                        Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
                                    }
                                    byte[] enctypted = new byte[0];
                                    try {
                                        enctypted = ipinpad.dukptEncryptData(1, 1, 1, bytes, new byte[]{0, 0, 0, 0, 0, 0, 0, 0,});
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    if (null == enctypted) {
                                        Utility.DEBUG_LOG(TAG, "NO DUKPT Encrypted got");
                                    } else {
                                        Utility.DEBUG_LOG(TAG, "DUKPT:" + Utility.byte2HexStr(enctypted));
                                    }
                                    try {
                                        bIsKeyExist = ipinpad.isKeyExist(DUKPTKEY, 1);
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    if (!bIsKeyExist) {
                                        Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
                                    }

                                    Utility.DEBUG_LOG(TAG, "onCardSwiped ...3");
                                    ((DashboardContainer) context.get()).getTransactionDetail().setCardType("Magstripe");
                                    if (getLast4DigitsSync() == Last4DigitsCodes.L4D_OK) {
                                        Utility.DEBUG_LOG(TAG, "Last 4 digits ok");
                                    } else {
                                        Utility.DEBUG_LOG(TAG, "before new HomeFragment");

                                        DashboardContainer.backStack();
                                        return;
                                    }
                                    //+ taha 02-04-2021 below is for PIN pad type input - experimental
//                                    doLast4DigitsSynchronous(true,3);
//

                                    if (isPinRequiredUsingServiceCode() == true) {


                                        if (isVepsTransaction()) {
                                            Utility.DEBUG_LOG(TAG, "Suppress PIN prompt for VEPS transaction");
                                        } else {
                                            Utility.DEBUG_LOG(TAG, "before doPinPadSynchronous");
                                            doPinPadSynchronous(true, 1);
                                            Utility.DEBUG_LOG(TAG, "after doPinPadSynchronous");
                                        }

                                    }
                                    if (savedPinBlock != null) {
                                        Utility.DEBUG_LOG(TAG, "pin data present1:" + Utility.byte2HexStr(savedPinBlock));
                                        data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
                                    } else {
                                        Utility.DEBUG_LOG(TAG, "pin data not present1");
                                    }
                                    if (((DashboardContainer) context.get()).getTransactionDetail().getIsFallback()) {
                                        ((DashboardContainer) context.get()).getTransactionDetail().setCardType("Fallback"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                        if (savedPinBlock == null) {
                                            Utility.DEBUG_LOG(TAG, "fallback detected (no pin); setting 802");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("802");
                                            data8583.put(ISO8583u.F_POSEntryMode_22, "802");
                                        } else {
                                            Utility.DEBUG_LOG(TAG, "fallback detected (with pin); setting 801");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("801");
                                            data8583.put(ISO8583u.F_POSEntryMode_22, "801");
                                        }
                                    } else {
                                        ((DashboardContainer) context.get()).getTransactionDetail().setCardType("Magstripe"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                        if (savedPinBlock == null) {
                                            Utility.DEBUG_LOG(TAG, "no pin entered 022");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("022");
                                            data8583.put(ISO8583u.F_POSEntryMode_22, "022");
                                        } else {
                                            Utility.DEBUG_LOG(TAG, "pin entered 021");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("021");
                                            data8583.put(ISO8583u.F_POSEntryMode_22, "021");
                                        }
                                    }


                                    Utility.DEBUG_LOG(TAG, "before onlineRequest.run");
                                    if(!txnTypePresenter.equals("COMPLETION")) {
                                        onlineRequest.run();
                                        if (isoResponse == null) {
                                            //transactionContract.transactionFailed("Response is Null","Transaction failed");
                                        } else {
                                            // Utility.DEBUG_LOG("iso-respponse", String.valueOf(isoResponse));
                                            String responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                                            String messageType = isoResponse.getUnpack(ISO8583u.F_MessageType_00);
                                            String processingCode = isoResponse.getUnpack(ISO8583u.F_ProcessingCode_03);
                                            if (responseCode.equals("00"))//success
                                            {
                                                if (messageType.equals("0410")) {
                                                    //+ taha 19-01-2021 below line commented as reversal handled above
                                                    //deleteTransaction();
                                                    //generateReversal.deleteTransaction();
                                                    // generateReversal.printReversal(receiptPrinter,printer,assetManager,merchant,cardName,cardExpiry);
                                                    onlineRequest.run();

                                                    responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                                                    messageType = isoResponse.getUnpack(ISO8583u.F_MessageType_00);
                                                    if (responseCode.equals("00") && messageType.equals("0210"))//success
                                                    {
                                                   /* if(messageType.equals("0410")) {
                                                        generateReversal.deleteTransaction();
                                                        // generateReversal.printReversal(receiptPrinter,printer,assetManager,merchant,cardName,cardExpiry);
                                                        onlineRequest.run();
                                                    }

                                                    else  {*/
                                                        Utility.DEBUG_LOG(TAG, "SL: In Here...1");
                                                        //totalAmountStr = isoResponse.getUnpack(ISO8583u.F_AmountOfTransactions_04);
                                                        if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
                                                            ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                                                            Utility.DEBUG_LOG("response code in if 2", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                                                        }
                                                        Utility.DEBUG_LOG("response code 1", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                                                        totalAmountStr = ((DashboardContainer) context.get()).getTransactionDetail().getAmount();
                                                        dateFromResponse = isoResponse.getUnpack(ISO8583u.F_TxDate_13);
                                                        timeFromResponse = isoResponse.getUnpack(ISO8583u.F_TxTime_12);
                                                        Utility.DEBUG_LOG(TAG, "timeFromResponse:" + timeFromResponse);
                                                        Utility.DEBUG_LOG(TAG, "dateFromResponse:" + dateFromResponse);
                                                        Utility.DEBUG_LOG(TAG, "system time to update:" + UpdatedYear + dateFromResponse + ";" + timeFromResponse);
                                                        iDeviceInfo.updateSystemTime(UpdatedYear + dateFromResponse, timeFromResponse);
                                                        rnn = isoResponse.getUnpack(ISO8583u.F_RRN_37);
                                                        transAmount = DataFormatterUtil.formattedAmount(totalAmountStr);
                                                        transDate = DataFormatterUtil.formattedDate(dateFromResponse);
                                                        transTime = DataFormatterUtil.formattedTime(timeFromResponse);

                                                        ((DashboardContainer) context.get()).getTransactionDetail().setAmount(transAmount);
                                                        if (TIP_ENABLED.equals("Y")) {
                                                            ((DashboardContainer) context.get()).getTransactionDetail().setTransactionAmount(transactionAmount);
                                                            ((DashboardContainer) context.get()).getTransactionDetail().setTipAmount(tipAmount);
                                                        }
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
                                                        if (HOST_INDEX_FROM_DB == 0) {
                                                            ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(dateFromResponse);
                                                            ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(timeFromResponse);
                                                            ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);
                                                            ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);
                                                            //((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);
                                                        }
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);

                                                        if (!(txnTypePresenter.equals("ORBIT INQUIRY")) && !(txnTypePresenter.equals("AUTH"))) {
                                                            // for (int i = 0; i < 300; i++) {
                                                            insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
                                                            Utility.DEBUG_LOG(TAG, "insert ((DashboardContainer) context.get()).getTransactionDetail() obj to DB 2");
                                                            //  }

                                                            Utility.DEBUG_LOG(TAG, "printSaleReceipt 2");
                                                            //receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter,"",Bal,false);
                                                            if (!(txnTypePresenter.equals("PRE AUTH")) && !(txnTypePresenter.equals("AUTH"))) {
                                                                postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
                                                            }
                                                        }
                                                        Utility.DEBUG_LOG(TAG, "before transPrinter.print() 2");
                                                        //  transPrinter = new PrintRecpSale(context.get());
                                                        Utility.DEBUG_LOG(TAG, "checking redeem " + redeemCheck);
                                                        //  transPrinter.initializeData(((DashboardContainer) context.get()).getTransactionDetail(), Bal, redeemCheck, "MERCHANT COPY");
                                                        Utility.DEBUG_LOG(TAG, "before print() 2");
                                                        //transPrinter.print();
                                                        Utility.DEBUG_LOG(TAG, "after print() 2");

                                                        //masood commented this
                                                        receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "MERCHANT COPY", txnTypePresenter, "", Bal, false); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                                        //postRequest.makeJsonObjReq(context, transactionDetail);
                                                        //masood commented this
                                                        transactionContract.transactionSuccessCustomer("CUSTOMER COPY", ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, true); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                                        paymentContract.showPaymentSuccessDialog(((DashboardContainer) context.get()).getTransactionDetail());

//                                                        try {
//                                                            //iBeeper.startBeep(200);
//                                                        } catch (RemoteException e) {
//                                                            e.printStackTrace();
//                                                        }

                                                    } else {
                                                        transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
                                                        if (isEcrEnable.equals("Y"))
                                                            ECR.ecrWrite(((DashboardContainer) context.get()).getTransactionDetail().getTxnType(), DataFormatterUtil.formattedDateECR(isoResponse.getUnpack(ISO8583u.F_TxDate_13)), DataFormatterUtil.formattedTimeWithSec(isoResponse.getUnpack(ISO8583u.F_TxTime_12)), DataFormatterUtil.maskCardNo(((DashboardContainer) context.get()).getTransactionDetail().getCardNo()), ((DashboardContainer) context.get()).getTransactionDetail().getCardType(), ((DashboardContainer) context.get()).getTransactionDetail().getCardHolderName(), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getAmount()), "N/A", "N/A", responseCode, errorMap.errorCodes.get(responseCode), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getTipAmount()),((DashboardContainer) context.get()).getTransactionDetail().getAidCode(),((DashboardContainer) context.get()).getTransactionDetail().getTvrCode(),((DashboardContainer) context.get()).getTransactionDetail().getCardScheme());


                                                    }
                                                } else {
                                                    Utility.DEBUG_LOG("SL: ", "Without Reversal Mag Approve");
                                                    if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                                                        Utility.DEBUG_LOG("response code in if 2", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                                                    }
                                                    totalAmountStr = ((DashboardContainer) context.get()).getTransactionDetail().getAmount();
                                                    dateFromResponse = isoResponse.getUnpack(ISO8583u.F_TxDate_13);
                                                    timeFromResponse = isoResponse.getUnpack(ISO8583u.F_TxTime_12);
                                                    Utility.DEBUG_LOG(TAG, "timeFromResponse:" + timeFromResponse);
                                                    Utility.DEBUG_LOG(TAG, "dateFromResponse:" + dateFromResponse);
                                                    Utility.DEBUG_LOG(TAG, "system time to update:" + UpdatedYear + dateFromResponse + ";" + timeFromResponse);
                                                    iDeviceInfo.updateSystemTime(UpdatedYear + dateFromResponse, timeFromResponse);
                                                    rnn = isoResponse.getUnpack(ISO8583u.F_RRN_37);
                                                    if (txnTypePresenter.equals("REDEEM")) {
                                                        field_63 = isoResponse.getUnpack(ISO8583u.F_SettlementData_63);
                                                        Utility.DEBUG_LOG("field63", field_63);
                                                        field_63 = isoResponse.getUnpack(ISO8583u.F_SettlementData_63);
                                                        // orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, 18)); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                                        orbit_amount = DataFormatterUtil.formattedAmount(field_63.substring(18, 30)); // SHIVAM UPDATE HERE FOR TEXT PRINTER
//                                                    boolean orbit_available = Pattern.matches("[^0-9]", field_63.substring(6, 18));
//                                                    if(orbit_available){
//                                                        orbit_bal =  DataFormatterUtil.formattedAmount(field_63.substring(6, 18).replaceAll("[^0-9*].",""));
//                                                        Utility.DEBUG_LOG("special char", orbit_bal);
//                                                    }
//                                                    else{
//                                                        orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, 18));
//                                                        Utility.DEBUG_LOG("non special char", orbit_bal);
//                                                    }

                                                        int lenght = 18;
                                                        for (int i = 7; i <= 18; ++i) {
                                                            char temp = field_63.charAt(i);
                                                            if (!(temp >= 48 && temp <= 57)) {
                                                                lenght = i;
                                                            }
                                                        }
                                                        if (lenght > 18)
                                                            lenght = 18;
                                                        orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, lenght));

                                                        orbit_key = field_63.substring(30, 39);

//                                                        ((DashboardContainer) context.get()).getTransactionDetail().setRedeemAmount(orbit_amount);
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setAmount(orbit_amount);
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setRedeemBalance(orbit_bal);
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setRedeemKey(orbit_key);
                                                        // Shivam added pos entry mode here for redeem only


                                                        Utility.DEBUG_LOG("field63_amount", orbit_amount);
                                                        Utility.DEBUG_LOG("field63_bal", ((DashboardContainer) context.get()).getTransactionDetail().getRedeemBalance());
                                                        Utility.DEBUG_LOG("field63_bal", orbit_bal);
                                                        Utility.DEBUG_LOG("field63_key", orbit_key);
                                                        Utility.DEBUG_LOG("field63", field_63);
                                                    }
                                                    if (txnTypePresenter.equals("ORBIT INQUIRY")) {
                                                        Bal = DataFormatterUtil.formattedAmount(isoResponse.getUnpack(ISO8583u.F_BalancAmount_54));
                                                        Utility.DEBUG_LOG("balInquiry", Bal);
                                                    }
                                                    transAmount = DataFormatterUtil.formattedAmount(totalAmountStr);
                                                    transDate = DataFormatterUtil.formattedDate(dateFromResponse);
                                                    transTime = DataFormatterUtil.formattedTime(timeFromResponse);
                                                    ((DashboardContainer) context.get()).getTransactionDetail().setAmount(transAmount);
                                                    ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
                                                    if (HOST_INDEX_FROM_DB == 0) {
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(dateFromResponse);
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(timeFromResponse);
                                                        ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);
                                                    }
                                                    ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);
                                                    paymentContract.showPaymentSuccessDialog(((DashboardContainer) context.get()).getTransactionDetail());
                                                    if (!(txnTypePresenter.equals("ORBIT INQUIRY")) && !(txnTypePresenter.equals("AUTH"))) {
                                                        // for (int i = 0; i < 300; i++) {
                                                        insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
                                                        Utility.DEBUG_LOG(TAG, "insert ((DashboardContainer) context.get()).getTransactionDetail() obj to DB 3");
                                                        // }
                                                        Utility.DEBUG_LOG(TAG, "printSaleReceipt 3");
                                                        //receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter,"",Bal,false);
                                                        //+ taha 22-02-2021 this is called for magnetic card swipe
                                                        //+ taha 22-02-2021 move this after receipt printing
                                                        Utility.DEBUG_LOG(TAG, "before deleteReversal after printing");
                                                        generateReversal.deleteReversal();
                                                        if (!(txnTypePresenter.equals("PRE AUTH")) && !(txnTypePresenter.equals("AUTH"))) {
                                                            postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
                                                        }

                                                    }
                                                    Utility.DEBUG_LOG(TAG, "before transPrinter.print() 3");
                                                    //transPrinter = new PrintRecpSale(context.get());
                                                    // Bundle bundle = new Bundle();
                                                    Utility.DEBUG_LOG(TAG, "checking redeem " + redeemCheck);
                                                    // transPrinter.initializeData(((DashboardContainer) context.get()).getTransactionDetail(), Bal, redeemCheck, "MERCHANT COPY");
                                                    Utility.DEBUG_LOG(TAG, "before print() 3");
                                                    // transPrinter.print();
                                                    Utility.DEBUG_LOG(TAG, "after print() 3");
                                                    if ((txnTypePresenter.equals("ORBIT INQUIRY")))
                                                        receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "CUSTOMER COPY", txnTypePresenter, "", Bal, false);
                                                    else
                                                        receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "MERCHANT COPY", txnTypePresenter, "", Bal, false);
                                                    //postRequest.makeJsonObjReq(context, transactionDetail);
                                                    //masood commentewd this
                                                    transactionContract.transactionSuccessCustomer("CUSTOMER COPY", ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, true);
                                                    //try {
//                                                        iBeeper.startBeep(200);
//                                                    } catch (RemoteException e) {
//                                                        e.printStackTrace();
//                                                    }

                                                }
                                            } else {
                                                transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
                                                if (isEcrEnable.equals("Y"))
                                                    ECR.ecrWrite(((DashboardContainer) context.get()).getTransactionDetail().getTxnType(), DataFormatterUtil.formattedDateECR(isoResponse.getUnpack(ISO8583u.F_TxDate_13)), DataFormatterUtil.formattedTimeWithSec(isoResponse.getUnpack(ISO8583u.F_TxTime_12)), DataFormatterUtil.maskCardNo(((DashboardContainer) context.get()).getTransactionDetail().getCardNo()), ((DashboardContainer) context.get()).getTransactionDetail().getCardType(), ((DashboardContainer) context.get()).getTransactionDetail().getCardHolderName(), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getAmount()), "N/A", "N/A", responseCode, errorMap.errorCodes.get(responseCode), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getTipAmount()),((DashboardContainer) context.get()).getTransactionDetail().getAidCode(),((DashboardContainer) context.get()).getTransactionDetail().getTvrCode(),((DashboardContainer) context.get()).getTransactionDetail().getCardScheme());


                                            }
                                        }
                                    }
                                    else{
                                            cardType = DataFormatterUtil.setCardTypeScheme(Integer.parseInt(cardRanges));
                                            String currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
                                            String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
                                            String date = currentDate.substring(0,4);
                                            Log.d(TAG,"TIME:"+currentTime);
                                            Log.d(TAG,"DATE:"+date);


                                            ((DashboardContainer) context.get()).getTransactionDetail().setTId(TERMINAL_ID);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setMId(MERCHANT_ID);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setStan(INVOICE_NO);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setInvoiceNo(INVOICE_NO);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(date);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(currentTime);
                                            if(TIP_ENABLED.equals("Y")){
                                                ((DashboardContainer) context.get()).getTransactionDetail().setAmount(DataFormatterUtil.formattedAmount(transAmount));
                                            }else{
                                                ((DashboardContainer) context.get()).getTransactionDetail().setAmount(DataFormatterUtil.formattedAmount(transactionAmount));
                                            }
                                            ((DashboardContainer) context.get()).getTransactionDetail().setCardNo(savedPan);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme(cardType);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme2(cardType);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setBatchNo(BATCH_NO);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setMessageType("");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode("000000");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setAcquiringInstitutionIdentificationCode(ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setForwardingInstitutionIdentificationCode(FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setTxnType(txnTypePresenter);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setCancelTxnType("None");
                                            ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(authNumber);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setIsAdvice(true);
                                            ((DashboardContainer) context.get()).getTransactionDetail().setCompletion(true);
                                            Log.d(TAG, "before onlineRequest.run");
                                            Bal="-";
                                            insertTransaction(AppDatabase.getAppDatabase(context.get()),((DashboardContainer) context.get()).getTransactionDetail());

                                            Log.d(TAG, "before print() mag");
                                            receiptPrinter.printSaleReceipt(printer, assetManager,  ((DashboardContainer) context.get()).getTransactionDetail(), aid, "MERCHANT COPY", txnTypePresenter, "", Bal, false);


                                            paymentContract.showPaymentSuccessDialog( ((DashboardContainer) context.get()).getTransactionDetail());
                                            transactionContract.transactionSuccessCustomer("Customer Copy", ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, false);
                                        }

                                }
                            }
                            isoResponse = null;
                        }

                        @Override
                        public void onCardPowerUp() throws RemoteException {
                            Utility.DEBUG_LOG(TAG, "+ onCardPowerUp() +");
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
                            Utility.DEBUG_LOG(TAG, "before dEMV-1");
                            ((DashboardContainer) context.get()).getTransactionDetail().setCardType("Chip");
                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card, TransType.T_PURCHASE, transAmount);
                            Utility.DEBUG_LOG(TAG, "after dEMV-1");
                        }

                        @Override
                        public void onCardActivate() throws RemoteException {
                            Utility.DEBUG_LOG(TAG, "+ onCardActivate() +");
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
                            CTLS = true;
                            Utility.DEBUG_LOG(TAG, "before dEMV-2");
                            ((DashboardContainer) context.get()).getTransactionDetail().setCardType("Contactless");
                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_contactless, TransType.T_PURCHASE, transAmount);
                            Utility.DEBUG_LOG(TAG, "after dEMV-2");
                        }

                        @Override
                        public void onTimeout() throws RemoteException {
                            Utility.DEBUG_LOG(TAG, "+ onTimeout() +");
                            transactionContract.transactionFailed("Time Out", "Card listener Closed, Please Re-Attempt the Transaction");
                            Utility.DEBUG_LOG(TAG, "before new HomeFragment");
                            DashboardContainer.backStack();
                        }

                        @Override
                        public void onError(int error, String message) throws RemoteException {
//                            transactionContract.toastShow("error:" + error + ", msg:" + message);
                            Utility.DEBUG_LOG(TAG, "+ onError() +");
                            Utility.DEBUG_LOG(TAG, "Error code 1: " + error + " Error message: " + message);
                            switch (error) {
                                case 1:
                                    transactionContract.transactionFailed("ERROR", "Magnetic Stripe Error");
                                    Utility.DEBUG_LOG(TAG, "before new HomeFragment");
                                    DashboardContainer.backStack();
                                    break;

                                case 2:
                                    transactionContract.transactionFailed("ERROR", "Smart Card Insert Error");
                                    Utility.DEBUG_LOG(TAG, "before new HomeFragment");
                                    DashboardContainer.backStack();
                                    break;

                                case 3:
                                    transactionContract.transactionFailed("ERROR", "Fallback");
                                    Utility.DEBUG_LOG(TAG, "ERROR: fallback from pament amount presenter1");
                                    try {
                                        if ((!Objects.equals(txnTypePresenter, "SALEIPP"))) {
                                            Utility.DEBUG_LOG(TAG, "transactionAmount:" + transactionAmount);
                                            Utility.DEBUG_LOG(TAG, "transAmount:" + transAmount);
                                            Utility.DEBUG_LOG(TAG, "tipAmount:" + tipAmount);
                                            Utility.DEBUG_LOG(TAG, "going to present fallback screen for Dip [orignator]");
                                            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(FallbackOrignatorType.FT_Dip, transactionAmount, tipAmount), "payment fragment");
                                        }
                                        else{
                                            DashboardContainer.switchFragmentWithBackStack(new SaleIPPFragment(FallbackOrignatorType.FT_Dip, transactionAmount,saleippMonths), "saleipp fragment");

                                        }
//                                       DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(true), "payment fragment");
//                                        DashboardContainer.backStack();
                                    } catch (Exception e) {
                                        Utility.DEBUG_LOG(TAG, "Exception:" + e.getMessage().toString());
                                    }
                                    break;

                                case 4:
                                    transactionContract.transactionFailed("ERROR", "Contactless Card Read Error");
                                    Utility.DEBUG_LOG(TAG, "before new HomeFragment");
                                    DashboardContainer.backStack();
                                    break;

                                case 5:
                                    transactionContract.transactionFailed("ERROR", "Contactless Card Active Error");
                                    DashboardContainer.backStack();
                                    break;
                                case 6:
                                    transactionContract.transactionFailed("ERROR", "Found Multi-Cards");
                                    DashboardContainer.backStack();
                                    break;

                                case 99:
                                    transactionContract.transactionFailed("ERROR", "Service Crash");
                                    DashboardContainer.backStack();
                                    break;

                                case 100:
                                    transactionContract.transactionFailed("ERROR", "Request Case Exception");
                                    DashboardContainer.backStack();
                                    break;

                            }
                        }
                    }
            );
        }catch (RemoteException e) {
            e.printStackTrace();
        }

    }


//    public void doSearchCard() {
//        ReaderType readerType = ReaderType.RT_All;
//        Utility.DEBUG_LOG(TAG, "doSearchCard()");
//        //transactionContract.toastShow("show");
//        Bundle cardOption = new Bundle();
//        ((DashboardContainer) context.get()).getTransactionDetail().setIsFallback(false);
////        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_unsupported);
//        switch (readerType) {
//            case RT_Chip:
//                Utility.DEBUG_LOG(TAG, "enable dip only");
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean("supportSmartCard", true);
//                break;
//            case RT_Mag:
//                Utility.DEBUG_LOG(TAG, "enable mag only");
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean("supportMagCard", true);
//                ((DashboardContainer) context.get()).getTransactionDetail().setIsFallback(false);
//                break;
//            case RT_CTLS:
//                Utility.DEBUG_LOG(TAG, "enable ctls only");
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean("supportCTLSCard", true);
//                break;
//            case RT_Chip_Mag:
//                Utility.DEBUG_LOG(TAG, "enable dip and mag readers");
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean("supportMagCard", true);
//                cardOption.putBoolean("supportSmartCard", true);
//                break;
//            case RT_All:
//                Utility.DEBUG_LOG(TAG, "enable all readers");
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean("supportMagCard", true);
//                cardOption.putBoolean("supportCTLSCard", true);
//                cardOption.putBoolean("supportSmartCard", true);
//                break;
//            default:
//                Utility.DEBUG_LOG(TAG, "default: enable all readers");
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//                cardOption.putBoolean("supportMagCard", true);
//                cardOption.putBoolean("supportCTLSCard", true);
//                cardOption.putBoolean("supportSmartCard", true);
//                break;
//        }
//        ;
////        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
////        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
////        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
////        cardOption.putBoolean("supportMagCard", true);
////        //+ masood 01-09-2020 commented below line
////        cardOption.putBoolean("supportCTLSCard", true);
//////        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
////        cardOption.putBoolean("supportSmartCard", true);
//        String transactionAmount = this.transactionAmount;
//
//
//
//        try {
//
//            iemv.checkCard(cardOption, 30, new CheckCardListener.Stub() {
//                        @Override
//                        public void onCardSwiped(Bundle track) throws RemoteException {
//                            Utility.DEBUG_LOG(TAG, "onCardSwiped2 ...");
//
//                            iBeeper.startBeep(200);
//
//                            String pan = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String);
//                            String track1 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String);
//                            track2 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String);
//                            String track3 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String);
//                            String serviceCode = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String);
//
//                            Utility.DEBUG_LOG("cardNam", track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String));
//                            //+ taha 23-02-2021 regex logic is incorrect, remaking it
//                            //cardholdername = track1.replaceAll(".+\\d\\^", "");
////                            cardholdername = cardholdername.replaceAll("\\^.+", "").trim();
//                            Utility.DEBUG_LOG(TAG, "track1:" + track1);
//                            cardholdername = track1.replaceAll("(.*\\^)(.*)(\\^.*)", "$2");
//                            Utility.DEBUG_LOG(TAG, cardholdername);
//                            String pan1 = pan.substring(0, 6);
//                            String Last4digits = track2;
//                            String cardnumber = Last4digits.replaceAll("\\=.*", "");
//                            int startNumber = cardnumber.length() - 4;
//                            int endNumber = cardnumber.length();
//                            String fourDigits = cardnumber.substring(startNumber, endNumber);
//                            Utility.DEBUG_LOG("regex", String.valueOf(fourDigits));
//                            Utility.DEBUG_LOG("context.get()", String.valueOf(context.get()));
//
//                            boolean bl = vs.validateCardRange(pan);
//                            if (!bl) {
//
//                                transactionContract.hideProgress();
//                                transactionContract.transactionFailed("Error", "Bin Not Supported");
//                                DashboardContainer.backStack();
//                            } else {
//                                savedPan = pan;
//                                String cardExpiryMag = track2.replaceAll(".+\\=", "");
//                                Utility.DEBUG_LOG("cardExpiryMag", cardExpiryMag);
//                                int cardexp = Integer.parseInt(cardExpiryMag.substring(0, 4));
//                                String cardExpiryYear = cardExpiryMag.substring(0, 2);
//                                String cardExpiryMonth = cardExpiryMag.substring(2, 4);
//                                cardExpiry = cardExpiryMonth + "/" + cardExpiryYear;
//                                String expiryCheck = new SimpleDateFormat("yyMM", Locale.getDefault()).format(new Date());
//                                int currentDateExpiry = Integer.parseInt(expiryCheck);
//                                if (null != track2) {
//                                    Utility.DEBUG_LOG(TAG, "track2: " + track2);
//                                    ((DashboardContainer) context.get()).getTransactionDetail().setTrack2(track2);
//                                    Utility.DEBUG_LOG("TAG", "before put F_Track_2_Data_35 reversal");
//                                    data8583.put(ISO8583u.F_Track_2_Data_35, track2); //refill this
//                                }
//                                Utility.DEBUG_LOG(TAG, "getIsFallback" + ((DashboardContainer) context.get()).getTransactionDetail().getIsFallback());
//                                if (((DashboardContainer) context.get()).getTransactionDetail().getIsFallback()) {
//                                    Utility.DEBUG_LOG(TAG, "In case of fallback ignore track2 service code first digit");
//                                } else if (isChipCardFromTrack2()) {
//                                    Utility.DEBUG_LOG(TAG, "Chip card detected2");
//
//                                    transactionContract.hideProgress();
//                                    transactionContract.transactionFailed("ERROR", "Chip card detected");
//                                    DashboardContainer.backStack();
//                                    return;
//                                }
//                                if (currentDateExpiry > cardexp) {
//
//                                    transactionContract.hideProgress();
//                                    transactionContract.transactionFailed("error", "Card Expired");
//                                    DashboardContainer.backStack();
//                                }
//                                else{ //Removed for certification
//                                    Utility.DEBUG_LOG(TAG, "cardExpiry 2:" + cardExpiry);
//                                    if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
//                                        data8583.put(ISO8583u.F_DateOfExpired_14, cardExpiry);
//                                    }
//                                    data8583.put(ISO8583u.F_ServiceCode_61, FIELD_61);
//                                    if (HOST_INDEX_FROM_DB != 0) {
//                                        data8583.put(ISO8583u.F_AccountNumber_02, savedPan);
//                                    }
//                                    Utility.DEBUG_LOG(TAG, "onCardSwiped ...1");
//                                    byte[] bytes = Utility.hexStr2Byte(track2);
//                                    Utility.DEBUG_LOG(TAG, "Track2:" + track2 + " (" + Utility.byte2HexStr(bytes) + ")");
//                                    Boolean bIsKeyExist = null;
//                                    try {
//                                        bIsKeyExist = ipinpad.isKeyExist(DUKPTKEY, 1);
//                                    } catch (RemoteException e) {
//                                        e.printStackTrace();
//                                    }
//                                    if (!bIsKeyExist) {
//                                        Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
//                                    }
//                                    byte[] enctypted = new byte[0];
//                                    try {
//                                        enctypted = ipinpad.dukptEncryptData(1, 1, 1, bytes, new byte[]{0, 0, 0, 0, 0, 0, 0, 0,});
//                                    } catch (RemoteException e) {
//                                        e.printStackTrace();
//                                    }
//                                    if (null == enctypted) {
//                                        Utility.DEBUG_LOG(TAG, "NO DUKPT Encrypted got");
//                                    } else {
//                                        Utility.DEBUG_LOG(TAG, "DUKPT:" + Utility.byte2HexStr(enctypted));
//                                    }
//                                    try {
//                                        bIsKeyExist = ipinpad.isKeyExist(DUKPTKEY, 1);
//                                    } catch (RemoteException e) {
//                                        e.printStackTrace();
//                                    }
//                                    if (!bIsKeyExist) {
//                                        Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
//                                    }
//
//                                    Utility.DEBUG_LOG(TAG, "onCardSwiped ...3");
//                                    ((DashboardContainer) context.get()).getTransactionDetail().setCardType("Magstripe");
//                                    if (isPinRequiredUsingServiceCode() == true) {
//                                        Utility.DEBUG_LOG(TAG, "before doPinPadSynchronous");
//                                        doPinPadSynchronous(true, 1);
//                                        Utility.DEBUG_LOG(TAG, "after doPinPadSynchronous");
//                                    }
//                                    if (savedPinBlock != null) {
//                                        Utility.DEBUG_LOG(TAG, "pin data present2:" + Utility.byte2HexStr(savedPinBlock));
//                                        data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
//                                    } else {
//                                        Utility.DEBUG_LOG(TAG, "pin data not present2");
//                                    }
//                                    if (((DashboardContainer) context.get()).getTransactionDetail().getIsFallback()) {
//                                        if (savedPinBlock == null) {
//                                            Utility.DEBUG_LOG(TAG, "fallback detected (no pin); setting 802");
//                                            data8583.put(ISO8583u.F_POSEntryMode_22, "802");
//                                        } else {
//                                            Utility.DEBUG_LOG(TAG, "fallback detected (with pin); setting 801");
//                                            data8583.put(ISO8583u.F_POSEntryMode_22, "801");
//                                        }
//                                    } else {
//                                        if (savedPinBlock == null) {
//                                            Utility.DEBUG_LOG(TAG, "no pin entered 022");
//                                            data8583.put(ISO8583u.F_POSEntryMode_22, "022");
//                                        } else {
//                                            Utility.DEBUG_LOG(TAG, "pin entered 021");
//                                            data8583.put(ISO8583u.F_POSEntryMode_22, "021");
//                                        }
//                                    }
//
//                                    Utility.DEBUG_LOG(TAG, "before onlineRequest.run");
//
//                                    // masood completion
//                                    onlineRequest.run();
//                                    if (isoResponse == null) {
//                                        //transactionContract.transactionFailed("Response is Null","Transaction failed");
//                                    } else {
//                                        // Utility.DEBUG_LOG("iso-respponse", String.valueOf(isoResponse));
//                                        String responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
//                                        String messageType = isoResponse.getUnpack(ISO8583u.F_MessageType_00);
//                                        String processingCode = isoResponse.getUnpack(ISO8583u.F_ProcessingCode_03);
//                                        if (responseCode.equals("00"))//success
//                                        {
//                                            if (messageType.equals("0410")) {
//                                                //+ taha 19-01-2021 below line commented as reversal handled above
//                                                //deleteTransaction();
//                                                //generateReversal.deleteTransaction();
//                                                // generateReversal.printReversal(receiptPrinter,printer,assetManager,merchant,cardName,cardExpiry);
//                                                onlineRequest.run();
//
//                                                responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
//                                                messageType = isoResponse.getUnpack(ISO8583u.F_MessageType_00);
//                                                if (responseCode.equals("00") && messageType.equals("0210"))//success
//                                                {
//                                                   /* if(messageType.equals("0410")) {
//                                                        generateReversal.deleteTransaction();
//                                                        // generateReversal.printReversal(receiptPrinter,printer,assetManager,merchant,cardName,cardExpiry);
//                                                        onlineRequest.run();
//                                                    }
//
//                                                    else  {*/
//                                                    Utility.DEBUG_LOG("SL: In Here", "..1");
//                                                    //totalAmountStr = isoResponse.getUnpack(ISO8583u.F_AmountOfTransactions_04);
//                                                    if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
//                                                        ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
//                                                        Utility.DEBUG_LOG("response code in if 2", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
//                                                    }
//                                                    Utility.DEBUG_LOG("response code 1", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
//                                                    totalAmountStr = ((DashboardContainer) context.get()).getTransactionDetail().getAmount();
//                                                    dateFromResponse = isoResponse.getUnpack(ISO8583u.F_TxDate_13);
//
//                                                    timeFromResponse = isoResponse.getUnpack(ISO8583u.F_TxTime_12);
//                                                    Utility.DEBUG_LOG("time res 1", timeFromResponse);
//                                                    Utility.DEBUG_LOG("date res 1", dateFromResponse);
//                                                    iDeviceInfo.updateSystemTime(UpdatedYear + dateFromResponse, timeFromResponse);
//                                                    rnn = isoResponse.getUnpack(ISO8583u.F_RRN_37);
//                                                    transAmount = DataFormatterUtil.formattedAmount(totalAmountStr);
//                                                    transDate = DataFormatterUtil.formattedDate(dateFromResponse);
//                                                    transTime = DataFormatterUtil.formattedTime(timeFromResponse);
//
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setAmount(transAmount);
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
//                                                    if (HOST_INDEX_FROM_DB == 0) {
//                                                        ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(dateFromResponse);
//                                                        ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(timeFromResponse);
//                                                        ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);
//                                                        ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);
//                                                        ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);
//                                                    }
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);
//
//                                                    if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
//                                                        // for (int i = 0; i < 300; i++) {
//                                                        insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
//                                                        Utility.DEBUG_LOG(TAG, "insert ((DashboardContainer) context.get()).getTransactionDetail() obj to DB 4");
//                                                        //  }
//
//                                                        // receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter,"",Bal,false);
//                                                        if (!(txnTypePresenter.equals("PRE AUTH")) && !(txnTypePresenter.equals("AUTH"))) {
//                                                            postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
//                                                        }
//                                                    }
////                                                    transPrinter = new PrintRecpSale(context.get());
////                                                    //Bundle bundle = new Bundle();
////                                                    transPrinter.initializeData(((DashboardContainer) context.get()).getTransactionDetail(),Bal,false,"MERCHANT COPY");
////                                                    transPrinter.print();
//                                                    // receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter,"",Bal,false);
//                                                    // postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
//                                                    transactionContract.transactionSuccessCustomer("Customer Copy", ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, true);
//                                                    paymentContract.showPaymentSuccessDialog(((DashboardContainer) context.get()).getTransactionDetail());
//
//
////                                                        try {
////                                                            //iBeeper.startBeep(200);
////                                                        } catch (RemoteException e) {
////                                                            e.printStackTrace();
////                                                        }
//
//                                                } else {
//                                                    transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
//                                                    if (isEcrEnable.equals("Y"))
//                                                        ECR.ecrWrite(((DashboardContainer) context.get()).getTransactionDetail().getTxnType(), DataFormatterUtil.formattedDateECR(isoResponse.getUnpack(ISO8583u.F_TxDate_13)), DataFormatterUtil.formattedTimeWithSec(isoResponse.getUnpack(ISO8583u.F_TxTime_12)), DataFormatterUtil.maskCardNo(((DashboardContainer) context.get()).getTransactionDetail().getCardNo()), ((DashboardContainer) context.get()).getTransactionDetail().getCardType(), ((DashboardContainer) context.get()).getTransactionDetail().getCardHolderName(), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getAmount()), "N/A", "N/A", responseCode, errorMap.errorCodes.get(responseCode), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getTipAmount()));
//
//
//                                                }
//                                            } else {
//                                                Utility.DEBUG_LOG("SL: ", "Without Reversal Mag Approve");
//                                                if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
//                                                    Utility.DEBUG_LOG("response code in if 2", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
//                                                }
//                                                totalAmountStr = ((DashboardContainer) context.get()).getTransactionDetail().getAmount();
//                                                dateFromResponse = isoResponse.getUnpack(ISO8583u.F_TxDate_13);
//                                                timeFromResponse = isoResponse.getUnpack(ISO8583u.F_TxTime_12);
//                                                Utility.DEBUG_LOG("time res 1", timeFromResponse);
//                                                Utility.DEBUG_LOG("date res 1", dateFromResponse);
//                                                iDeviceInfo.updateSystemTime(UpdatedYear + dateFromResponse, timeFromResponse);
//                                                rnn = isoResponse.getUnpack(ISO8583u.F_RRN_37);
//                                                if (txnTypePresenter.equals("REDEEM")) {
//                                                    field_63 = isoResponse.getUnpack(ISO8583u.F_SettlementData_63);
//                                                    //Utility.DEBUG_LOG("field63",field_63);
//                                                    field_63 = isoResponse.getUnpack(ISO8583u.F_SettlementData_63);
//                                                    orbit_amount = DataFormatterUtil.formattedAmount(field_63.substring(18, 30)); // SHIVAM UPDATE HERE FOR TEXT PRINTER
//                                                   // orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, 18)); // SHIVAM UPDATE HERE FOR TEXT PRINTER
//
//
////                                                    boolean orbit_available = Pattern.matches("[^0-9]", field_63.substring(6, 18));
////                                                    if(orbit_available){
////                                                        orbit_bal =  DataFormatterUtil.formattedAmount(field_63.substring(6, 18).replaceAll("[^0-9*].",""));
////                                                        Utility.DEBUG_LOG("special char", orbit_bal);
////                                                    }
////                                                    else{
////                                                        orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, 18));
////                                                        Utility.DEBUG_LOG("non special char", orbit_bal);
////                                                    }
//
//                                                    int lenght=18;
//                                                    for(int i=7;i<=18;++i)
//                                                    {
//                                                        char temp= field_63.charAt(i);
//                                                        if(!(temp >= 48 && temp <=57))
//                                                        {
//                                                            lenght=i;
//
//                                                        }
//                                                    }
//                                                    if(lenght > 18 )
//                                                        lenght=18;
//                                                    orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, lenght));
//
//                                                    orbit_key = field_63.substring(30, 39);
////                                                        ((DashboardContainer) context.get()).getTransactionDetail().setRedeemAmount(orbit_amount);
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setAmount(orbit_amount);
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setRedeemBalance(orbit_bal);
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setRedeemKey(orbit_key);
//                                                    Utility.DEBUG_LOG("field63_amount", orbit_amount);
//                                                    Utility.DEBUG_LOG("field63_bal", orbit_bal);
//                                                    Utility.DEBUG_LOG("field63_key", orbit_key);
//                                                    Utility.DEBUG_LOG("field63", field_63);
//                                                }
//                                                if (txnTypePresenter.equals("ORBIT INQUIRY")) {
//                                                    Bal = DataFormatterUtil.formattedAmount(isoResponse.getUnpack(ISO8583u.F_BalancAmount_54));
//                                                    Utility.DEBUG_LOG("balInquiry", Bal);
//                                                }
//                                                transAmount = DataFormatterUtil.formattedAmount(totalAmountStr);
//                                                transDate = DataFormatterUtil.formattedDate(dateFromResponse);
//                                                transTime = DataFormatterUtil.formattedTime(timeFromResponse);
//                                                ((DashboardContainer) context.get()).getTransactionDetail().setAmount(transAmount);
//                                                ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
//                                                if (HOST_INDEX_FROM_DB == 0) {
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(dateFromResponse);
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(timeFromResponse);
//                                                    ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);
//                                                }
//                                                ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);
//                                                paymentContract.showPaymentSuccessDialog(((DashboardContainer) context.get()).getTransactionDetail());
//                                                if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
////                                                for (int i = 0; i < 300; i++) {
//                                                    insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
//                                                    Utility.DEBUG_LOG(TAG, "insert ((DashboardContainer) context.get()).getTransactionDetail() obj to DB 5");
//                                                    // }
//
//                                                    //  receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter,"",Bal,false);
//                                                    if (!(txnTypePresenter.equals("PRE AUTH")) && !(txnTypePresenter.equals("AUTH"))) {
//                                                        postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
//                                                    }
//
//                                                }
////                                                transPrinter = new PrintRecpSale(context.get());
////                                                //Bundle bundle = new Bundle();
////                                                transPrinter.initializeData(((DashboardContainer) context.get()).getTransactionDetail(),Bal,false,"MERCHANT COPY");
////                                                transPrinter.print();
//                                                // receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter,"",Bal,false);
//                                                //postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
//                                                transactionContract.transactionSuccessCustomer("CUSTOMER COPY",  ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, true); // SHIVAM UPDATE HERE FOR TEXT PRINTER
////                                             try {
////                                                        iBeeper.startBeep(200);
////                                                    } catch (RemoteException e) {
////                                                        e.printStackTrace();
////                                                    }
//
//                                            }
//                                        } else {
//                                            transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
//                                            if (isEcrEnable.equals("Y"))
//                                                ECR.ecrWrite(((DashboardContainer) context.get()).getTransactionDetail().getTxnType(), DataFormatterUtil.formattedDateECR(isoResponse.getUnpack(ISO8583u.F_TxDate_13)), DataFormatterUtil.formattedTimeWithSec(isoResponse.getUnpack(ISO8583u.F_TxTime_12)), DataFormatterUtil.maskCardNo(((DashboardContainer) context.get()).getTransactionDetail().getCardNo()), ((DashboardContainer) context.get()).getTransactionDetail().getCardType(), ((DashboardContainer) context.get()).getTransactionDetail().getCardHolderName(), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getAmount()), "N/A", "N/A", responseCode, errorMap.errorCodes.get(responseCode), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getTipAmount()));
//
//
//                                        }
//                                    }
//                                }
//                            }
//                            isoResponse = null;
//                        }
//
//                        @Override
//                        public void onCardPowerUp() throws RemoteException {
//                            Utility.DEBUG_LOG(TAG, "+ onCardPowerUp() +");
//                            iemv.stopCheckCard();
//                            iemv.abortEMV();
//                            iBeeper.startBeep(200);
//                            Utility.DEBUG_LOG(TAG, "before dEMV-1");
//                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card, TransType.T_PURCHASE, transAmount);
//                        }
//
//                        @Override
//                        public void onCardActivate() throws RemoteException {
//                            Utility.DEBUG_LOG(TAG, "+ onCardActivate() +");
//                            iemv.stopCheckCard();
//                            iemv.abortEMV();
//                            iBeeper.startBeep(200);
//                            CTLS = true;
//                            Utility.DEBUG_LOG(TAG, "before dEMV-2");
//                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_contactless, TransType.T_PURCHASE, transAmount);
//                        }
//
//                        @Override
//                        public void onTimeout() throws RemoteException {
//                            Utility.DEBUG_LOG(TAG, "+ onTimeout() +");
//                            transactionContract.transactionFailed("Time Out", "Card listener Closed, Please Re-Attempt the Transaction");
//                            Utility.DEBUG_LOG(TAG, "before new HomeFragment");
//                            DashboardContainer.backStack();
//                        }
//
//                        @Override
//                        public void onError(int error, String message) throws RemoteException {
////                            transactionContract.toastShow("error:" + error + ", msg:" + message);
//                            Utility.DEBUG_LOG(TAG, "+ onError() +");
//                            Utility.DEBUG_LOG(TAG, "Error code 2: " + error + " Error message: " + message);
//                            switch (error) {
//                                case 1:
//                                    transactionContract.transactionFailed("ERROR", "Magnetic Stripe Error");
//                                    DashboardContainer.backStack();
//                                    break;
//
//                                case 2:
//                                    transactionContract.transactionFailed("ERROR", "Smart Card Insert Error");
//                                    DashboardContainer.backStack();
//                                    break;
//
//                                case 3:
//                                    transactionContract.transactionFailed("ERROR", "Fallback");
//                                    Utility.DEBUG_LOG(TAG, "ERROR: fallback from payment amount presenter2");
//                                    try {
//                                        if ((!Objects.equals(txnTypePresenter, "SALEIPP"))) {
//                                            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(FallbackOrignatorType.FT_Dip, transactionAmount, tipAmount), "payment fragment");
//                                        }
////                                       DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(true), "payment fragment");
////                                        DashboardContainer.backStack();
//                                    } catch (Exception e) {
//                                        Utility.DEBUG_LOG(TAG, "Exception:" + e.getMessage().toString());
//                                    }
//                                    break;
//
//                                case 4:
//                                    transactionContract.transactionFailed("ERROR", "Contactless Card Read Error");
//                                    DashboardContainer.backStack();
//                                    break;
//
//                                case 5:
//                                    transactionContract.transactionFailed("ERROR", "Contactless Card Active Error");
//                                    DashboardContainer.backStack();
//                                    break;
//                                case 6:
//                                    transactionContract.transactionFailed("ERROR", "Found Multi-Cards");
//                                    DashboardContainer.backStack();
//                                    break;
//
//                                case 99:
//                                    transactionContract.transactionFailed("ERROR", "Service Crash");
//                                    DashboardContainer.backStack();
//                                    break;
//
//                                case 100:
//                                    transactionContract.transactionFailed("ERROR", "Request Case Exception");
//                                    DashboardContainer.backStack();
//                                    break;
//
//                            }
//                        }
//                    }
//            );
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//
//
//    }

    /**
     * \brief sample of EMV
     * <p>
     * \code{.java}
     * \endcode
     *
     * @seeF
     */
    void doEMV(int type, TransType transType, String amount) {
        //

        Utility.DEBUG_LOG(TAG, "start EMV demo");
        int isRmbCurrency = SharedPref.read("isRmbCurrency");
        Utility.DEBUG_LOG(TAG, "isRmbCurrency:" + isRmbCurrency);
        transactionContract.showProgress("Processing card");

        Bundle emvIntent = new Bundle();
        emvIntent.putInt(ConstIPBOC.startEMV.intent.KEY_cardType_int, type);
        if (transType == TransType.T_PURCHASE) {
            emvIntent.putLong(ConstIPBOC.startEMV.intent.KEY_authAmount_long, Long.valueOf(amount));
        }
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_merchantName_String, MERCHANT_NAME);//


        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_merchantId_String, MERCHANT_ID);  // 010001020270123
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_terminalId_String, TERMINAL_ID);   // 00000001
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
//        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_unsupported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportSM_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isQPBOCForceOnline_boolean, ConstIPBOC.startEMV.intent.VALUE_unforced);
        if (type == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless) {   // todo, check here
            emvIntent.putByte(ConstIPBOC.startEMV.intent.KEY_transProcessCode_byte, (byte) 0x00);
        }
        emvIntent.putBoolean("isSupportPBOCFirst", false);
        if (isRmbCurrency == 1) {
            Utility.DEBUG_LOG(TAG, "Set RMB currency 0156");
            emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_transCurrCode_String, "0156");
        } else {
            Utility.DEBUG_LOG(TAG, "Set PKR currency 0586");
            emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_transCurrCode_String, "0586");
        }
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_otherAmount_String, "0");

        try {
            HashMap<Integer, Integer> tlvMap = new HashMap<Integer, Integer>();
            tlvMap.put(4, 1);//how to find the kernel version?
            iemv.reRegistKernelVersion(tlvMap);

            Map<String, Integer> map = new HashMap<>();
            map.put("A0000007361010", 13455445);
            map.put("A0000007362010", 13455445);
            map.put("A0000007363010", 13455445);
            //map.put("A0000007362010", 0);

            iemv.registerKernelAID(map);

            iemv.startEMV(ConstIPBOC.startEMV.processType.full_process, emvIntent, emvHandler);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void initializeEMV() {

        /**
         * \brief initialize the call back listener of EMV
         *
         *  \code{.java}
         * \endcode
         * @version
         * @see
         *
         */
        emvHandler = new EMVHandler.Stub() {
            @Override
            public void onRequestAmount() throws RemoteException {
            }

            @Override
            public void onSelectApplication(List<Bundle> appList) throws RemoteException {
                List<String> aidNamesList = new ArrayList<>();
                List<String> aidLabelList = new ArrayList<>();
                Utility.DEBUG_LOG(TAG, "+ onSelectApplication +");
                for (Bundle aidBundle : appList) {
                    String aidName = aidBundle.getString("aidName");
                    aid = aidBundle.getString("aid");
                    String aidLabel = aidBundle.getString("aidLabel");
                    Utility.DEBUG_LOG(TAG, "AID Name=" + aidName + " | AID Label=" + aidLabel + " | AID=" + aid);
                    aidNamesList.add(aidName);
                    aidLabelList.add(aidLabel);
                }
                paymentContract.showMultipleAid(aidLabelList);
            }

            /**
             * \brief confirm the card info
             * show the card info and import the confirm result
             * \code{.java}
             * \endcode
             *
             */
            @Override
            public void onConfirmCardInfo(Bundle info) throws RemoteException {

                Utility.DEBUG_LOG(TAG, "+ onConfirmCardInfo +");
                Utility.DEBUG_LOG(TAG, "onConfirmCardInfo...");
                transactionContract.hideProgress();
                savedPan = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_PAN_String);
                String result = "onConfirmCardInfo callback, " +
                        "\nPAN:" + savedPan +
                        "\nTRACK2:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String) +
                        "\nCARD_SN:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_CARD_SN_String) +
                        "\nSERVICE_CODE:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_SERVICE_CODE_String) +
                        "\nEXPIRED_DATE:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_EXPIRED_DATE_String);
                String pan_validate = savedPan.substring(0, 6);

                boolean bl = vs.validateCardRange(savedPan);
                if (!bl) {
                    Utility.DEBUG_LOG(TAG, "before new HomeFragment");

                    transactionContract.hideProgress();
                    transactionContract.transactionFailed("Error", "Bin Not Supported");
                    DashboardContainer.backStack();
                } else {
                    track2 = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String);
                    Utility.DEBUG_LOG(TAG, "KEY_TRACK2_String:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String));
                    Utility.DEBUG_LOG(TAG, "track2 orig:" + track2);

                    track2 = track2.substring(0, track2.length()); // + taha 23-02-2021 this was failing odd track2 length
//                    track2 = track2.substring(0, track2.length() - 1);
                    Utility.DEBUG_LOG(TAG, "track2 after substring:" + track2);
                    String cardExpiryChip = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_EXPIRED_DATE_String);
                    int cardexp = Integer.parseInt(cardExpiryChip);
                    String expiryCheck = new SimpleDateFormat("yyMM", Locale.getDefault()).format(new Date());
                    int currentDateExpiry = Integer.parseInt(expiryCheck);
                    if (null != track2) {
                        Utility.DEBUG_LOG(TAG, "track2: " + track2);
                        ((DashboardContainer) context.get()).getTransactionDetail().setTrack2(track2);
                    }
                    if(currentDateExpiry > cardexp){
                        transactionContract.hideProgress();
                        transactionContract.transactionFailed("error","Card Expired");
                        DashboardContainer.backStack();
                    }

                    else{
                        String yearOfExpiry = cardExpiryChip.substring(0, 2);
                        String monthOfExpiry = cardExpiryChip.substring(2, 4);
                        Utility.DEBUG_LOG("", monthOfExpiry + yearOfExpiry);
                        String dateOfExpiryReverse = monthOfExpiry + yearOfExpiry;
                        Utility.DEBUG_LOG(TAG, "before put F_Track_2_Data_35 onConfirmCardInfo");
                        Utility.DEBUG_LOG(TAG, "track2:" + track2);
                        data8583.put(ISO8583u.F_Track_2_Data_35, track2);
                        Utility.DEBUG_LOG(TAG, "cardExpiry 3:" + cardExpiry);
                        if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
                            data8583.put(ISO8583u.F_DateOfExpired_14, cardExpiryChip);
                        }
                        if (HOST_INDEX_FROM_DB != 0) {
                            data8583.put(ISO8583u.F_AccountNumber_02, savedPan);
                        }
                        if(txnTypePresenter.equals("AUTH")) {
                            data8583.put(ISO8583u.F_AccountNumber_02, savedPan);
                        }
                        cardExpiry = monthOfExpiry + "/" + yearOfExpiry;
                        transactionContract.toastShow("onConfirmCardInfo:" + result);
                        iemv.importCardConfirmResult(ConstIPBOC.importCardConfirmResult.pass.allowed);
                        Utility.DEBUG_LOG(TAG, "after importCardConfirmResult()");

//                    cardRanges = savedPan.substring(0, 6);
//                    cardType = DataFormatterUtil.setCardTypeScheme(Integer.parseInt(cardRanges));
//                    Utility.DEBUG_LOG(TAG, "cardRanges 1:" + cardRanges);
//                    Utility.DEBUG_LOG(TAG, "cardType 1:" + cardType);
//                    Utility.DEBUG_LOG(TAG,"((DashboardContainer) context.get()).getTransactionDetail().getCardScheme()"+cardType );
//                   // if(Float.parseFloat(transactionAmount) > Float.parseFloat(CVM_LIMIT_PAYPAK)){
                    }
                }
            }

            boolean isUpiDebitCardAndCtls() {
                boolean rv = false;
                Utility.DEBUG_LOG(TAG, "+ isUpiDebitCard +");
                String upiDebitAid = "A000000333010101";
                Utility.DEBUG_LOG(TAG, "Transaction AID:" + ((DashboardContainer) context.get()).getTransactionDetail().getAidCode());
                if (((DashboardContainer) context.get()).getTransactionDetail().getAidCode().equals(upiDebitAid)
                        && ((DashboardContainer) context.get()).getTransactionDetail().getCardType().equals("Contactless")) {
                    Utility.DEBUG_LOG(TAG, "UPI Debit AID found [CTLS]");
                    rv = true;
                }
                return rv;
            }

            boolean isUpiAndCtls() throws RemoteException {
                boolean rv = false;
                Utility.DEBUG_LOG(TAG, "+ isUpiAndCtls +");
                updateAidFromCard();

                String aid = ((DashboardContainer) context.get()).getTransactionDetail().getAidCode();
                String cardType = ((DashboardContainer) context.get()).getTransactionDetail().getCardType();
                Utility.DEBUG_LOG(TAG, "aid:" + aid);
                Utility.DEBUG_LOG(TAG, "cardType:" + cardType);
                if (
                        (aid.equals("A000000333010101") || aid.equals("A000000333010102") || aid.equals("A000000333010103"))
                                && cardType.equals("ctls")
                ) {
                    rv = true;
                    Utility.DEBUG_LOG(TAG, "UPI CTLS aid found, return: " + rv);
                }

                return rv;
            }

            /**
             * \brief show the pin pad
             *
             *  \code{.java}
             * \endcode
             *
             */
            //masood completion
            @Override
            public void onRequestInputPIN(boolean isOnlinePin, int retryTimes) throws RemoteException {
                transactionContract.toastShow("onRequestInputPIN isOnlinePin:" + isOnlinePin);
                Utility.DEBUG_LOG(TAG, "+ onRequestInputPIN +");
                cardRanges = savedPan.substring(0, 6);
                cardType = DataFormatterUtil.setCardTypeScheme(Integer.parseInt(cardRanges));
                Utility.DEBUG_LOG(TAG, "cardRanges 1:" + cardRanges);
                Utility.DEBUG_LOG(TAG, "cardType 1:" + cardType);
                Utility.DEBUG_LOG(TAG, "((DashboardContainer) context.get()).getTransactionDetail().getCardScheme()" + cardType);
                //Double.parseDouble(DataFormatterUtil.formattedAmount(transAmount));
                Utility.DEBUG_LOG(TAG, "transaction Amount " + Double.parseDouble(DataFormatterUtil.formattedAmount(transAmount)));


                if (isVepsTransaction()) {
                    Utility.DEBUG_LOG(TAG, "Suppress PIN prompt for VEPS transaction");
                    iemv.importPin(1, null);
                    isPinInput = true;
                    Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"NO_CVM\"");
                    //CARD_AUTH_METHOD = "NO_CVM"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
                    ((DashboardContainer) context.get()).getTransactionDetail().setCVM("NO_CVM"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                }
                //PayPak CVM work
//                else if(cardType == "PAYPAK" && (Double.parseDouble(DataFormatterUtil.formattedAmount(transAmount)) < Double.parseDouble(CVM_LIMIT_PAYPAK))){
//                        Utility.DEBUG_LOG(TAG, "((DashboardContainer) context.get()).getTransactionDetail().getCardScheme()" + cardType);
//                        Utility.DEBUG_LOG(TAG, "Float.parseFloat(transactionAmount)" + Float.parseFloat(transactionAmount));
//                        Utility.DEBUG_LOG(TAG, "Float.parseFloat(CVM_LIMIT_PAYPAK)" + Double.parseDouble(CVM_LIMIT_PAYPAK));
//                        iemv.importPin(1, null);
//                }

//                //+ taha 02-03-2021 patch for UPI debit card
                else if (isUpiCtlsPinSuppressed()) {
                    Utility.DEBUG_LOG(TAG, "Suppress PIN prompt");
                    iemv.importPin(1, null);
                    isPinInput = true;
                    Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"NO_CVM\"");
                    //  CARD_AUTH_METHOD = "NO_CVM"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
//                    CARD_AUTH_METHOD = "SIGNATURE"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
                    ((DashboardContainer) context.get()).getTransactionDetail().setCVM("NO_CVM"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                } else {
                    Utility.DEBUG_LOG(TAG, "normal flow...");
                    // show the pin pad, import the pin block
                    isPinInput = true;
                    doPinPad(isOnlinePin, retryTimes);
                    if (isOnlinePin) {
                        Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"ONLINE_PIN\"");
                        if(Constants.isEcrReq && Constants.ecrType.equals("M"))
                            ECR.ECRMsgWrite("PIN Prompt");
                        // CARD_AUTH_METHOD = "ONLINE_PIN";// SHIVAM UPDATE HERE FOR TEXT PRINTER
                        ((DashboardContainer) context.get()).getTransactionDetail().setCVM("ONLINE_PIN");// SHIVAM UPDATE HERE FOR TEXT PRINTER
                    } else if (!isOnlinePin) {
                        Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"OFFLINE_PIN\"");
                        //CARD_AUTH_METHOD = "OFFLINE_PIN";// SHIVAM UPDATE HERE FOR TEXT PRINTER
                        ((DashboardContainer) context.get()).getTransactionDetail().setCVM("OFFLINE_PIN");// SHIVAM UPDATE HERE FOR TEXT PRINTER
                    }
                }
//                Utility.DEBUG_LOG(TAG,"normal flow...");
//                // show the pin pad, import the pin block
//                isPinInput=true;
//                doPinPad(isOnlinePin, retryTimes);
//                if (isOnlinePin) {
//                    CARD_AUTH_METHOD = "ONLINE_PIN";
//                } else if (!isOnlinePin) {
//                    CARD_AUTH_METHOD = "OFFLINE_PIN";
//                }


            }

            @Override
            public void onConfirmCertInfo(String certType, String certInfo) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "+ onConfirmCertInfo +");
                transactionContract.toastShow("onConfirmCertInfo, type:" + certType + ",info:" + certInfo);
                iemv.importCertConfirmResult(ConstIPBOC.importCertConfirmResult.option.CONFIRM);
            }

            private void updateTagConstants(int tag, byte[] tlv) {

            }

            public boolean isPaypakBin() {
                Utility.DEBUG_LOG(TAG, "+ isPaypakBin +");
                boolean rv = false;
                String pan = "";
                long paypakBinStart = 220000;
                long paypakBinEnd = 229999;
                long binL = 0;
                if (track2 != null) {
                    pan = track2.substring(0, 6);
                    Utility.DEBUG_LOG(TAG, "pan:" + pan);
                    binL = Integer.parseInt(pan);
                    Utility.DEBUG_LOG(TAG, "binL:" + binL);
                    if (binL >= paypakBinStart && binL <= paypakBinEnd) {
                        rv = true;
                    }
                }
                Utility.DEBUG_LOG(TAG, "return rv:" + rv);
                return rv;
            }


            // masood completion
            @SuppressLint("LongLogTag")
            @Override
            public void onRequestOnlineProcess(Bundle aaResult) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "+ onRequestOnlineProcess +");
                Utility.DEBUG_LOG(TAG, "onRequestOnlineProcess...");
                transactionContract.hideProgress();
                int result = aaResult.getInt(ConstPBOCHandler.onRequestOnlineProcess.aaResult.KEY_RESULT_int);
                boolean signature = aaResult.getBoolean(ConstPBOCHandler.onRequestOnlineProcess.aaResult.KEY_SIGNATURE_boolean);

                //shivam add CVM Method on 19Feb2021
                int CVM_result = aaResult.getInt(ConstPBOCHandler.onRequestOnlineProcess.aaResult.RESULT_CTLS_CVMR);

                /*
                 *     |---0 NO_CVM<br>
                 *     |---1 CVM_PIN<br>
                 *     |---2 CVM_SIGN<br>
                 *     |---3 CVM_CDCVM<br>
                 */
                Utility.DEBUG_LOG(TAG, "CVM_result:" + CVM_result);
//                if ( isUpiAndCtls(((DashboardContainer) context.get()).getTransactionDetail()) )
//                {
//                    Utility.DEBUG_LOG(TAG,"UPI and CTLS transaction...");
////                    if ( CVM_result == TagConstants.CVM_SIGN ) {
////                        Utility.DEBUG_LOG(TAG,"Signature detected, change it to no cvm");
////                        CARD_AUTH_METHOD = "NO_CVM";
////                        CVM_result = TagConstants.NO_CVM;
////                    }
//                    if (CVM_result == TagConstants.CVM_SIGN ) {
//                        Utility.DEBUG_LOG(TAG, "SIGNATURE REQUIRED");
//                        Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"SIGNATURE\"");
//                        CARD_AUTH_METHOD = "SIGNATURE";
//                    } else if (CVM_result == TagConstants.NO_CVM) {
//                        Utility.DEBUG_LOG(TAG, "NO CVM REQUIRED");
//                        Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"NO_CVM\"");
//                        CARD_AUTH_METHOD = "NO_CVM";
//                    } else if (CVM_result == TagConstants.CVM_PIN) {
//                        Utility.DEBUG_LOG(TAG,"savedPinBlock:"+savedPinBlock);
//                        if ( savedPinBlock != null )
//                        {
//                            Utility.DEBUG_LOG(TAG, "PIN REQUIRED");
//                            Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"ONLINE_PIN\"");
//                            CARD_AUTH_METHOD = "ONLINE_PIN";
//                        }
//                        else
//                        {
//                            Utility.DEBUG_LOG(TAG, "NO CVM REQUIRED [pin input not found]");
//                            Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"NO_CVM\"");
//                            CARD_AUTH_METHOD = "NO_CVM";
//                        }
//                    } else if (CVM_result == TagConstants.CVM_CDCVM) {
//                        Utility.DEBUG_LOG(TAG, "CDCVM .");
//                        Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"CDCVM\"");
//                        CARD_AUTH_METHOD = "CDCVM";
//                    }
//                }
//                else
//                {
//                    if(!(isPinInput)) { //if pin is not prompt
//                        if (CVM_result == 2 || signature == true) {
//                            Utility.DEBUG_LOG(TAG, "SIGNATURE REQUIRED");
//                            Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"SIGNATURE\"");
//                            CARD_AUTH_METHOD = "SIGNATURE";
//                        } else if (CVM_result == 0 || signature == true) {
//                            Utility.DEBUG_LOG(TAG, "NO CVM REQUIRED");
//                            Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"NO_CVM\"");
//                            CARD_AUTH_METHOD = "NO_CVM";
//                        } else if (CVM_result == 1) {
//                            Utility.DEBUG_LOG(TAG, "PIN REQUIRED");
//                            Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"ONLINE_PIN\"");
//                            CARD_AUTH_METHOD = "ONLINE_PIN";
//                        } else if (CVM_result == 3) {
//                            Utility.DEBUG_LOG(TAG, "CDCVM .");
//                            Utility.DEBUG_LOG(TAG,"CARD_AUTH_METHOD=\"CDCVM\"");
//                            CARD_AUTH_METHOD = "CDCVM";
//                        }
//                    }
//                }
                if (!(isPinInput)) { //if pin is not prompt
                    if (CVM_result == TagConstants.CVM_SIGN || signature == true) {
                        Utility.DEBUG_LOG(TAG, "SIGNATURE REQUIRED");
                        Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"SIGNATURE\"");
                        //  CARD_AUTH_METHOD = "SIGNATURE"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
                        ((DashboardContainer) context.get()).getTransactionDetail().setCVM("SIGNATURE"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                    } else if (CVM_result == TagConstants.NO_CVM || signature == true) {
                        Utility.DEBUG_LOG(TAG, "NO CVM REQUIRED");
                        Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"NO_CVM\"");
                        // CARD_AUTH_METHOD = "NO_CVM"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
                        ((DashboardContainer) context.get()).getTransactionDetail().setCVM("NO_CVM"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                    } else if (CVM_result == TagConstants.CVM_PIN) {
                        Utility.DEBUG_LOG(TAG, "PIN REQUIRED");
                        Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"ONLINE_PIN\"");
                        //CARD_AUTH_METHOD = "ONLINE_PIN"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
                        ((DashboardContainer) context.get()).getTransactionDetail().setCVM("ONLINE_PIN"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                    } else if (CVM_result == TagConstants.CVM_CDCVM) {
                        Utility.DEBUG_LOG(TAG, "CDCVM .");
                        Utility.DEBUG_LOG(TAG, "CARD_AUTH_METHOD=\"CDCVM\"");
                        //CARD_AUTH_METHOD = "CDCVM"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
                        ((DashboardContainer) context.get()).getTransactionDetail().setCVM("CDCVM"); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                    }
                }
                isPinInput = false;
                ////shivam add CVM Method on 19Feb2021
                transactionContract.toastShow("onRequestOnlineProcess result=" + CVM_result + " signal=" + signature);
                switch (result) {
                    case ConstPBOCHandler.onRequestOnlineProcess.aaResult.VALUE_RESULT_AARESULT_ARQC:
                    case ConstPBOCHandler.onRequestOnlineProcess.aaResult.VALUE_RESULT_QPBOC_ARQC:
                        transactionContract.toastShow(aaResult.getString(ConstPBOCHandler.onRequestOnlineProcess.aaResult.KEY_ARQC_DATA_String));
                        Utility.DEBUG_LOG(TAG, "Taha: logging ARQC data string");
                        Utility.DEBUG_LOG(TAG, aaResult.getString(ConstPBOCHandler.onRequestOnlineProcess.aaResult.KEY_ARQC_DATA_String));
                        break;
                    case ConstPBOCHandler.onRequestOnlineProcess.aaResult.VALUE_RESULT_PAYPASS_EMV_ARQC:
                        break;
                }

                byte[] tlv = new byte[0];
                tagOfF55 = new SparseArray<>();

// CVM -> CardHolder Verification Method
// TVR-> Terminal Verification Results
// TSI-> Transaction Status Information
//                EmvTagValuePair emvTagValuePairCVM = null,emvTagValuePairTVR=null,emvTagValuePairAID=null ,emvTagValuePairAIP=null, emvTagValuePairCryptogram=null,emvTagValuePairCID=null,emvTagValuePairTermCap=null,emvTagValuePairTermAdd=null,emvTagValuePairIAC_Default=null,emvTagValuePairIAC_Denial=null;

                int[] tagList = {
                        0x9F02, 0x9F03, 0x82, 0x9F36, 0x9F26, 0x9F27,
                        0x9F34, 0x5F20,
                        0x9F1E, 0x9F10, 0x9F09, 0x9F33, 0x9F1A, 0x9F35, 0x95, 0x5F2A, 0x9F41,
                        0x9A, 0x9F37, 0x84, 0x5F34, 0x9F6E, 0x9C,
                        0x9F1D, 0x9F24, 0x9F06
                };
                if (isPaypakBin()) {
                    Utility.DEBUG_LOG(TAG, "Paypak bin found");
                    tagList = Utility.addElement(tagList, 0x4F);
                    Utility.DEBUG_LOG(TAG, "after addElement");
                } else {
                    Utility.DEBUG_LOG(TAG, "Paypak bin not found");
                }
                Utility.DEBUG_LOG(TAG, "Before getting tagList");
                List<EmvTagValuePair> list = new ArrayList<EmvTagValuePair>();
                int count = 0;
                for (int tag : tagList) {
                    Utility.DEBUG_LOG(TAG, "Tag:" + String.valueOf(tag));
                    Utility.DEBUG_LOG(TAG, "Value:" + String.valueOf(iemv.getCardData(Integer.toHexString(tag).toUpperCase())));

                    tlv = iemv.getCardData(Integer.toHexString(tag).toUpperCase());

//                    TagConstants.resetTagConstants();
                    if (null != tlv && tlv.length > 0) {


                        Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(tlv));
                        //if(tag==40769){
                        if (tag == 0x9F41) {
                            tagOfF55.put(tag, Utility.byte2HexStr(tlv).substring(2));  // build up the field 55 todo check this
                        }
                        if (tag == 0x5F20) {

                            Utility.DEBUG_LOG("masoodtag", Utility.byte2HexStr(tlv));
                            String hex = Utility.byte2HexStr(tlv);
                            //data8583.put(ISO8583u.F_Track_1_Data_45,Utility.byte2HexStr(tlv));
                            if (hex.length() % 2 != 0) {
                                System.err.println("Invlid hex string.");
                                return;
                            }

                            StringBuilder builder = new StringBuilder();

                            for (int i = 0; i < hex.length(); i = i + 2) {
                                // Step-1 Split the hex string into two character group
                                String s = hex.substring(i, i + 2);
                                // Step-2 Convert the each character group into integer using valueOf method
                                int n = Integer.valueOf(s, 16);
                                // Step-3 Cast the integer value to char
                                builder.append((char) n);
                            }
                            Utility.DEBUG_LOG("tagname", builder.toString());
                            ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(builder.toString());
                        }
//                       else if(tag==24372){
                        else if (tag == 0x5F34) {
                            data8583.put(ISO8583u.F_ApplicationPANSequenceNumber_23, "0" + Utility.byte2HexStr(tlv));//todo check this
                            ((DashboardContainer) context.get()).getTransactionDetail().setPanSequenceNumber(Utility.byte2HexStr(tlv));
                            Utility.DEBUG_LOG(TAG, "Pan sequence number:" + ((DashboardContainer) context.get()).getTransactionDetail().getPanSequenceNumber());

                            if (HOST_INDEX_FROM_DB == 0) {
                                tagOfF55.put(tag, Utility.byte2HexStr(tlv));  // build up the field 55
                            }
                        } else {
                            tagOfF55.put(tag, Utility.byte2HexStr(tlv));  // build up the field 55
                            Utility.DEBUG_LOG(TAG, "tag:" + Integer.toString(tag, 16));
                            Utility.DEBUG_LOG(TAG, "tlv (converted):" + Utility.byte2HexStr(tlv));
                            if (tag == 0x9f33) {
                                for (byte b : tlv) {
                                    String var1;
//                                    var1 = Integer.toHexString(b & 255);
                                    var1 = Integer.toString(b & 255, 16);

                                    Utility.DEBUG_LOG(TAG, var1);
                                }
//                                Utility.DEBUG_LOG(TAG,"9f33:"
//                                        + Integer.toString(tlv[0], 16)
//                                        + Integer.toString(tlv[1], 16)
//                                        + Integer.toString(tlv[2], 16));
                            }
                            if (tag == 0x9F34) {
                                Utility.DEBUG_LOG("CVM_Result", Utility.byte2HexStr(tlv));
                                TagConstants.CVM_Result = Utility.byte2HexStr(tlv);
                            }
                            if (tag == 0x9F33) {
                                Utility.DEBUG_LOG("TERM_CAP", Utility.byte2HexStr(tlv));
                                TagConstants.TERM_CAP = Utility.byte2HexStr(tlv);
                            }
//                            else if(tag==149){
                            else if (tag == 0x95) {
                                Utility.DEBUG_LOG("TVR-Value", String.valueOf(new EmvTagValuePair("TVR", Utility.byte2HexStr(tlv))));
                                ((DashboardContainer) context.get()).getTransactionDetail().setTvrCode(Utility.byte2HexStr(tlv));
                                Utility.DEBUG_LOG("TVR_Cons", Utility.byte2HexStr(tlv));
                                TagConstants.TVR_Cons = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F06) {
                                Utility.DEBUG_LOG("Aid-Value 1", Utility.byte2HexStr(tlv));
                                ((DashboardContainer) context.get()).getTransactionDetail().setAidCode(Utility.byte2HexStr(tlv));
                                //((DashboardContainer) context.get()).getTransactionDetail().setAidCode();
                            } else if (tag == 0x9F40) {
                                Utility.DEBUG_LOG("TERM_ADD-Value", Utility.byte2HexStr(tlv));
                                TagConstants.TERM_ADD = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x82) {
                                Utility.DEBUG_LOG("AIP-Value", Utility.byte2HexStr(tlv));
                                TagConstants.AIP = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F26) {
                                Utility.DEBUG_LOG("Crypto-Value", Utility.byte2HexStr(tlv));
                                TagConstants.Cryptogram = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F27) {
                                Utility.DEBUG_LOG("CID-Value", Utility.byte2HexStr(tlv));
                                TagConstants.CID = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F0D) {
                                Utility.DEBUG_LOG("CID-IAC_Default", Utility.byte2HexStr(tlv));
                                TagConstants.IAC_Default = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F0E) {
                                Utility.DEBUG_LOG("CID-IAC_Denial", Utility.byte2HexStr(tlv));
                                TagConstants.IAC_Denial = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F0F) {
                                Utility.DEBUG_LOG("CID-IAC_Online", Utility.byte2HexStr(tlv));
                                TagConstants.IAC_Online = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9B) {
                                Utility.DEBUG_LOG("Constant TSI", Utility.byte2HexStr(tlv));
                                TagConstants.TSI = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F66) {
                                Utility.DEBUG_LOG("Constant TTQ", Utility.byte2HexStr(tlv));
                                TagConstants.TTQ = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x9F6C) {
                                Utility.DEBUG_LOG("CTQ", "Constant CTQ:" + Utility.byte2HexStr(tlv));
                                TagConstants.CTQ = Utility.byte2HexStr(tlv);
                            } else if (tag == 0x5F25) {
                                Utility.DEBUG_LOG(TAG, "0x5f25: " + Utility.byte2HexStr(tlv));
                            } else if (tag == 0x5F2A) {
                                Utility.DEBUG_LOG(TAG, "0x5F2A: " + Utility.byte2HexStr(tlv));
                            }
























                        }
                    } else {
                        Utility.DEBUG_LOG(TAG, "getCardData:" + Integer.toHexString(tag) + ", fails");
                    }
                }

                tlv = iemv.getCardData(Integer.toHexString(0x50).toUpperCase());
                    Utility.DEBUG_LOG("Card Name1", Utility.byte2HexStr(tlv));
                    String hex = Utility.byte2HexStr(tlv);
                    //data8583.put(ISO8583u.F_Track_1_Data_45,Utility.byte2HexStr(tlv));
                    if (hex.length() % 2 != 0) {
                        System.err.println("Invlid hex string.");
                        return;
                    }
                    StringBuilder builder = new StringBuilder();

                    for (int i = 0; i < hex.length(); i = i + 2) {
                        // Step-1 Split the hex string into two character group
                        String s = hex.substring(i, i + 2);
                        // Step-2 Convert the each character group into integer using valueOf method
                        int n = Integer.valueOf(s, 16);
                        // Step-3 Cast the integer value to char
                        builder.append((char) n);
                    }
                    Utility.DEBUG_LOG("tag Card Name1", builder.toString());
                    cardType=builder.toString();
//                    Utility.DEBUG_LOG("")
                    ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme(cardType);


                // set the pin block
                /*if (savedPinBlock!=null){

                    if(HOST_INDEX_FROM_DB!=0) {
                        data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
                        data8583.put(ISO8583u.F_SecurityRelateControlInformation_53, SECURITY_RELATED_CONTROL_INFORMATION);
                        data8583.put(ISO8583u.F_POSEntryMode_22, "071");
                    }// 02 mag, 05 smart, 07 ctls; 1 pin
                }*/// Taha's code updated on 15-Jan-2021 for PINBlock'

                if (savedPinBlock != null) {
                    Utility.DEBUG_LOG(TAG, "pin data present3:" + Utility.byte2HexStr(savedPinBlock));
                    data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
//                    data8583.put(ISO8583u.F_SecurityRelateControlInformation_53, SECURITY_RELATED_CONTROL_INFORMATION);
                    if (((DashboardContainer) context.get()).getTransactionDetail().getCardType() == "Magstripe") {
                        Utility.DEBUG_LOG(TAG, "setting 021");
                        ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("021");
                        data8583.put(ISO8583u.F_POSEntryMode_22, "021");
                    } else if (((DashboardContainer) context.get()).getTransactionDetail().getCardType() == "Contactless") {
                        Utility.DEBUG_LOG(TAG, "setting 071");
                        ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("071");
                        data8583.put(ISO8583u.F_POSEntryMode_22, "071");
                    } else if (((DashboardContainer) context.get()).getTransactionDetail().getCardType() == "Chip") {
                        Utility.DEBUG_LOG(TAG, "setting 051");
                        ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("051");
                        data8583.put(ISO8583u.F_POSEntryMode_22, "051");
                    }
//                    data8583.put(ISO8583u.F_POSEntryMode_22, "051");
//                    if(HOST_INDEX_FROM_DB!=0) {
//                        data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
//                        data8583.put(ISO8583u.F_SecurityRelateControlInformation_53, SECURITY_RELATED_CONTROL_INFORMATION);
//                        data8583.put(ISO8583u.F_POSEntryMode_22, "071");
//                    }// 02 mag, 05 smart, 07 ctls; 1 pin
                } else {
                    Utility.DEBUG_LOG(TAG, "pin data not present3");
                    if (((DashboardContainer) context.get()).getTransactionDetail().getCardType() == "Magstripe") {
                        Utility.DEBUG_LOG(TAG, "setting 022");
                        ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("022");
                        data8583.put(ISO8583u.F_POSEntryMode_22, "022");
                    } else if (((DashboardContainer) context.get()).getTransactionDetail().getCardType() == "Contactless") {
                        Utility.DEBUG_LOG(TAG, "setting 072");
                        ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("072");
                        data8583.put(ISO8583u.F_POSEntryMode_22, "072");
                    } else if (((DashboardContainer) context.get()).getTransactionDetail().getCardType() == "Chip") {
                        Utility.DEBUG_LOG(TAG, "setting 052");
                        ((DashboardContainer) context.get()).getTransactionDetail().setPosEntryMode("052");
                        data8583.put(ISO8583u.F_POSEntryMode_22, "052");
                    }
                }


                ((DashboardContainer) context.get()).getTransactionDetail().setCardNo(savedPan);
                ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);

                Utility.DEBUG_LOG("expiry 2", ((DashboardContainer) context.get()).getTransactionDetail().getCardexpiry());
                ((DashboardContainer) context.get()).getTransactionDetail().setTag55(tagOfF55);
                Utility.DEBUG_LOG(TAG, "start online request");
                // masood completion
                if(!txnTypePresenter.equals("COMPLETION")) {
                    onlineRequest.run();
                    Utility.DEBUG_LOG(TAG, "online request finished");
                    Utility.DEBUG_LOG(TAG, "tagOfF55:" + tagOfF55);
                    if (tagOfF55 != null) {
                        Utility.DEBUG_LOG(TAG, "tagOfF55:" + tagOfF55.toString());
                    }
//                Utility.DEBUG_LOG(TAG, "test: before tagOfF55.clear");
//                try
//                {
//                    tagOfF55.clear();
//                }
//                catch( Exception e)
//                {
//                    Utility.DEBUG_LOG(TAG,"Exception:"+e.getMessage().toString());
//                }
                    ////new response handler.....
                    if (isoResponse == null) {
                        if (((DashboardContainer) context.get()).getTransactionDetail().getIsAdvice()) {
                            Utility.DEBUG_LOG(TAG, "Is advice true, do not show error message");
                        } else {
                            //  all cases handled
                            transactionContract.transactionFailed("Response is Null", "Transaction failed");
                        }
                    } else {
                        // stan = isoResponse.getUnpack(ISO8583u.F_STAN_11);
                        String responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                        String messageType = isoResponse.getUnpack(ISO8583u.F_MessageType_00);
                        String processingCode = isoResponse.getUnpack(ISO8583u.F_ProcessingCode_03);
                        Utility.DEBUG_LOG(TAG, "messageType:" + messageType);
                        if (responseCode.equals("00"))//success
                        {
                            if (messageType.equals("0410")) {
                                //+ taha 19-01-2021 below line commented as reversal handled above
                                //deleteTransaction();
                                //generateReversal.deleteTransaction();
                                // generateReversal.printReversal(receiptPrinter,printer,assetManager,merchant,cardName,cardExpiry);
                                Utility.DEBUG_LOG(TAG, "reversal processed 0410");
                                Utility.DEBUG_LOG(TAG, "before onlineRequest.run() post 0410");
                                onlineRequest.run();
                                Utility.DEBUG_LOG(TAG, "after onlineRequest.run() post 0410");

                                responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                                messageType = isoResponse.getUnpack(ISO8583u.F_MessageType_00);
                                if (responseCode.equals("00") && messageType.equals("0210"))//success
                                {
                                    Utility.DEBUG_LOG(TAG, "0210 detected");

                                    if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
                                        ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                                        Utility.DEBUG_LOG("response code in if 2", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                                    }
                                    Utility.DEBUG_LOG("response code 2", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                                    //totalAmountStr = isoResponse.getUnpack(ISO8583u.F_AmountOfTransactions_04);
                                    totalAmountStr = ((DashboardContainer) context.get()).getTransactionDetail().getAmount();
                                    dateFromResponse = isoResponse.getUnpack(ISO8583u.F_TxDate_13);
                                    timeFromResponse = isoResponse.getUnpack(ISO8583u.F_TxTime_12);
                                    Utility.DEBUG_LOG("time res 1", timeFromResponse);
                                    Utility.DEBUG_LOG("date res 1", dateFromResponse);
                                    iDeviceInfo.updateSystemTime(UpdatedYear + dateFromResponse, timeFromResponse);
                                    rnn = isoResponse.getUnpack(ISO8583u.F_RRN_37);
                                    transAmount = DataFormatterUtil.formattedAmount(totalAmountStr);
                                    transactionAmount = DataFormatterUtil.formattedAmount(transactionAmount);
                                    tipAmount = DataFormatterUtil.formattedAmount(tipAmount);
                                    transDate = DataFormatterUtil.formattedDate(dateFromResponse);
                                    transTime = DataFormatterUtil.formattedTime(timeFromResponse);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setCardNo(savedPan);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);
                                    Utility.DEBUG_LOG("expiry 3", ((DashboardContainer) context.get()).getTransactionDetail().getCardexpiry());
                                    ((DashboardContainer) context.get()).getTransactionDetail().setTrack2(track2.substring(0, track2.length() - 1));//todo save the track2 as is
                                    ((DashboardContainer) context.get()).getTransactionDetail().setAmount(transAmount);
                                    if (TIP_ENABLED.equalsIgnoreCase("Y")) {
                                        ((DashboardContainer) context.get()).getTransactionDetail().setTransactionAmount(transactionAmount);
                                        ((DashboardContainer) context.get()).getTransactionDetail().setTipAmount(tipAmount);
                                    }
                                    ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
                                    if (HOST_INDEX_FROM_DB == 0) {
                                        ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(dateFromResponse);
                                        ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(timeFromResponse);
                                        ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);

                                    }

                                    paymentContract.showPaymentSuccessDialog(((DashboardContainer) context.get()).getTransactionDetail());


                                    // receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid,"Merchant Copy",txnTypePresenter,"");
                                    if (!(txnTypePresenter.equals("ORBIT INQUIRY")) && !(txnTypePresenter.equals("AUTH"))) {
                                        // for (int i = 0; i < 300; i++) {
                                        insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
                                        Utility.DEBUG_LOG(TAG, "insert ((DashboardContainer) context.get()).getTransactionDetail() obj to BD 6");
                                        // }
                                        Utility.DEBUG_LOG(TAG, "before printSaleReceipt 4 [after reversal processing - commented for testing]");
//                                    receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter, "", Bal,false);
                                        if (!(txnTypePresenter.equals("PRE AUTH")) && !(txnTypePresenter.equals("AUTH"))) {
                                            postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
                                        }
                                    }
//                                transPrinter = new PrintRecpSale(context.get());
//                                // Bundle bundle = new Bundle();
//                                transPrinter.initializeData(((DashboardContainer) context.get()).getTransactionDetail(),Bal,false,"MERCAHNT COPY");
//                                transPrinter.print();
                                    //masood commented this
                                    // receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "Merchant Copy", txnTypePresenter, "", Bal,false);
                                    //postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
                                    transactionContract.transactionSuccessCustomer("CUSTOMER COPY", ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, true); // SHIVAM UPDATE HERE FOR TEXT PRINTER

                                    //iBeeper.startBeep(200);
                                } else {
                                    transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
                                    Utility.DEBUG_LOG(TAG, "Trans Decline" + errorMap.errorCodes.get(responseCode));
                                    if (isEcrEnable.equals("Y"))
                                        ECR.ecrWrite(((DashboardContainer) context.get()).getTransactionDetail().getTxnType(), DataFormatterUtil.formattedDateECR(isoResponse.getUnpack(ISO8583u.F_TxDate_13)), DataFormatterUtil.formattedTimeWithSec(isoResponse.getUnpack(ISO8583u.F_TxTime_12)), DataFormatterUtil.maskCardNo(((DashboardContainer) context.get()).getTransactionDetail().getCardNo()), ((DashboardContainer) context.get()).getTransactionDetail().getCardType(), ((DashboardContainer) context.get()).getTransactionDetail().getCardHolderName(), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getAmount()), "N/A", "N/A", responseCode, errorMap.errorCodes.get(responseCode), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getTipAmount()),((DashboardContainer) context.get()).getTransactionDetail().getAidCode(),((DashboardContainer) context.get()).getTransactionDetail().getTvrCode(),((DashboardContainer) context.get()).getTransactionDetail().getCardScheme());


                                }
                            } else {

                                Utility.DEBUG_LOG(TAG, "processing other than reversal");
                                if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
                                    ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                                    Utility.DEBUG_LOG("response code in if 3", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                                }
                                Utility.DEBUG_LOG("response code 3", ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                                //masood change this
                                //totalAmountStr = isoResponse.getUnpack(ISO8583u.F_AmountOfTransactions_04);
                                totalAmountStr = ((DashboardContainer) context.get()).getTransactionDetail().getAmount();
                                dateFromResponse = isoResponse.getUnpack(ISO8583u.F_TxDate_13);
                                timeFromResponse = isoResponse.getUnpack(ISO8583u.F_TxTime_12);
                                Utility.DEBUG_LOG("time res 1", timeFromResponse);
                                Utility.DEBUG_LOG("date res 1", dateFromResponse);
                                iDeviceInfo.updateSystemTime(UpdatedYear + dateFromResponse, timeFromResponse);
                                rnn = isoResponse.getUnpack(ISO8583u.F_RRN_37);
                                if (txnTypePresenter.equals("REDEEM")) {
                                    field_63 = isoResponse.getUnpack(ISO8583u.F_SettlementData_63);
                                    orbit_amount = DataFormatterUtil.formattedAmount(field_63.substring(18, 30));  // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                    //  orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, 18));  // SHIVAM UPDATE HERE FOR TEXT PRINTER

                                    //  boolean orbit_available = Pattern.matches("[^0-9]", field_63.substring(6, 18));
                                    int lenght = 18;
                                    for (int i = 7; i <= 18; ++i) {
                                        char temp = field_63.charAt(i);
                                        if (!(temp >= 48 && temp <= 57)) {
                                            lenght = i;
                                        }
                                    }
                                    if (lenght > 18)
                                        lenght = 18;
                                    orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, lenght));
//                                if(field_63.substring(6, 18).contains("[^0-9*]")){
//                                    orbit_bal =  DataFormatterUtil.formattedAmount(field_63.substring(6, 18).replaceAll("[^0-9*].",""));
//                                    Utility.DEBUG_LOG("special char", orbit_bal);
//                                }
//                                else{
//                                    orbit_bal = DataFormatterUtil.formattedAmount(field_63.substring(6, 18));
//                                    Utility.DEBUG_LOG("non special char", orbit_bal);
//                                }
                                    orbit_key = field_63.substring(30, 39);
//                                ((DashboardContainer) context.get()).getTransactionDetail().setRedeemAmount(orbit_amount);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setAmount(orbit_amount);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setRedeemBalance(orbit_bal);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setRedeemKey(orbit_key);
                                    Utility.DEBUG_LOG("field63_amount", orbit_amount);
                                    Utility.DEBUG_LOG("field63_bal", orbit_bal);
                                    Utility.DEBUG_LOG("field63_key", orbit_key);
                                    Utility.DEBUG_LOG("field63", field_63);
                                }
                                if (txnTypePresenter.equals("ORBIT INQUIRY")) {
                                    Bal = DataFormatterUtil.formattedAmount(isoResponse.getUnpack(ISO8583u.F_BalancAmount_54));
                                    Utility.DEBUG_LOG("balInquiry", Bal);
                                }
                                transAmount = DataFormatterUtil.formattedAmount(totalAmountStr);
                                transactionAmount = DataFormatterUtil.formattedAmount(transactionAmount);
                                tipAmount = DataFormatterUtil.formattedAmount(tipAmount);
                                transDate = DataFormatterUtil.formattedDate(dateFromResponse);
                                transTime = DataFormatterUtil.formattedTime(timeFromResponse);
                                ((DashboardContainer) context.get()).getTransactionDetail().setCardNo(savedPan);
                                ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);
                                Utility.DEBUG_LOG("expiry 4", ((DashboardContainer) context.get()).getTransactionDetail().getCardexpiry());
                                ((DashboardContainer) context.get()).getTransactionDetail().setTrack2(track2.substring(0, track2.length() - 1));
                                ((DashboardContainer) context.get()).getTransactionDetail().setAmount(transAmount);
                                if (TIP_ENABLED.equalsIgnoreCase("Y")) {
                                    ((DashboardContainer) context.get()).getTransactionDetail().setTransactionAmount(transactionAmount);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setTipAmount(tipAmount);
                                }
                                ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
                                if (HOST_INDEX_FROM_DB == 0) {
                                    ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(dateFromResponse);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(timeFromResponse);
                                    ((DashboardContainer) context.get()).getTransactionDetail().setRrn(rnn);
                                }
                                paymentContract.showPaymentSuccessDialog(((DashboardContainer) context.get()).getTransactionDetail());
                                if (!(txnTypePresenter.equals("ORBIT INQUIRY")) && !(txnTypePresenter.equals("AUTH"))) {
                                    // for (int i = 0; i < 300; i++) {
                                    insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
                                    Utility.DEBUG_LOG(TAG, "insert ((DashboardContainer) context.get()).getTransactionDetail() obj to BD 7");
                                    // }
                                    Utility.DEBUG_LOG(TAG, "before printSaleReceipt 5 [commented]");
//                                receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid,"Merchant Copy",txnTypePresenter,"",Bal,false);
                                    if (!(txnTypePresenter.equals("PRE AUTH")) && !(txnTypePresenter.equals("AUTH"))) {
                                        postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
                                    }
                                }
//                            transPrinter = new PrintRecpSale(context.get());
//                            // Bundle bundle = new Bundle();
//                            transPrinter.initializeData(((DashboardContainer) context.get()).getTransactionDetail(),Bal,false,"MERCHANT COPY");
//                            transPrinter.print();
                                //masood commented this
                                //receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid,"Merchant Copy",txnTypePresenter,"",Bal,false);
                                //postRequest.makeJsonObjReq(context.get(), ((DashboardContainer) context.get()).getTransactionDetail());
                                // iBeeper.startBeep(200);
                                transactionContract.transactionSuccessCustomer("CUSTOMER COPY", ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, true); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                //receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid,"Merchant Copy",txnTypePresenter,"");
                                // emv logs
                            }
                        } else {
                            transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
                            Utility.DEBUG_LOG(TAG, isoResponse.getUnpack(ISO8583u.F_TxDate_13));
                            if (isEcrEnable.equals("Y"))
                                ECR.ecrWrite(((DashboardContainer) context.get()).getTransactionDetail().getTxnType(), DataFormatterUtil.formattedDateECR(isoResponse.getUnpack(ISO8583u.F_TxDate_13)), DataFormatterUtil.formattedTimeWithSec(isoResponse.getUnpack(ISO8583u.F_TxTime_12)), DataFormatterUtil.maskCardNo(((DashboardContainer) context.get()).getTransactionDetail().getCardNo()), ((DashboardContainer) context.get()).getTransactionDetail().getCardType(), ((DashboardContainer) context.get()).getTransactionDetail().getCardHolderName(), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getAmount()), "N/A", "N/A", responseCode, errorMap.errorCodes.get(responseCode), DataFormatterUtil.formattedAmount(((DashboardContainer) context.get()).getTransactionDetail().getTipAmount()),((DashboardContainer) context.get()).getTransactionDetail().getAidCode(),((DashboardContainer) context.get()).getTransactionDetail().getTvrCode(),((DashboardContainer) context.get()).getTransactionDetail().getCardScheme());

                        }
                    }

                    Utility.DEBUG_LOG(TAG, "tagOfF55 final:" + tagOfF55);
                    if (tagOfF55 != null) {
                        Utility.DEBUG_LOG(TAG, "tagOfF55 final:" + tagOfF55.toString());
                    }

                    // import the online result
                    Bundle onlineResult = new Bundle();
                    onlineResult.putBoolean(ConstIPBOC.inputOnlineResult.onlineResult.KEY_isOnline_boolean, true);
                    if (isoResponse.unpackValidField[ISO8583u.F_ResponseCode_39]) {
                        Utility.DEBUG_LOG(TAG, "setting F39: " + isoResponse.getUnpack(ISO8583u.F_ResponseCode_39));
                        onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, isoResponse.getUnpack(ISO8583u.F_ResponseCode_39));
//                    onlineResult.putString("respCode", isoResponse.getUnpack(ISO8583u.F_ResponseCode_39));
                        //set8A(isoResponse.getUnpack(ISO8583u.F_ResponseCode_39));
                    } else {
                        onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, "00");
                    }

                    if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
                        //
                        Utility.DEBUG_LOG(TAG, "setting F38: " + isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                        onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                        Utility.DEBUG_LOG(TAG, "response code in if 4-a:" + ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
//                    onlineResult.putString("authCode", isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                    } else {
                        onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, "123456");
                        Utility.DEBUG_LOG(TAG, "response code in if 4-b:" + ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());
                    }
                    Utility.DEBUG_LOG(TAG, "after condition " + ((DashboardContainer) context.get()).getTransactionDetail().getAuthorizationIdentificationResponseCode());

                    Utility.DEBUG_LOG(TAG, "isoResponse:" + isoResponse);
                    Utility.DEBUG_LOG(TAG, "isoResponse.unpackValidField[55]:" + isoResponse.unpackValidField[55]);
                    // onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, "910A1A1B1C1D1E1F2A2B30307211860F04DA9F790A0000000100001A1B1C1D");
                    if (isoResponse.unpackValidField[55]) {
                        Utility.DEBUG_LOG(TAG, "isoResponse.getUnpack(55):" + isoResponse.getUnpack(55));
                        Utility.DEBUG_LOG(TAG, "setting: " + isoResponse.getUnpack(55));
                        onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, isoResponse.getUnpack(55));
                        Utility.DEBUG_LOG(TAG, "KEY_field55_String [found in response]:" + onlineResult.getString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String));
//
//                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, "910818ED6A3300860000");
////                    onlineResult.putString("field55", "910818ED6A3300860000");
//                    Utility.DEBUG_LOG(TAG,"KEY_field55_String [hard coding for test]:"+onlineResult.getString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String));
                    }
//                else
//                {
//                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, "5F3401019F3303E0F9C8950500000000009F1A0201569A039707039F3704F965E43082027C009F3602041C9F260805142531F709C8669C01009F02060000000000125F2A0201569F101307010103A02000010A01000000000063213EC29F2701809F1E0831323334353637389F0306000000000000910ABCF266A64FF136630010");
//                    Utility.DEBUG_LOG(TAG,"KEY_field55_String [not found in response]:"+onlineResult.getString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String));
//                }


                    iemv.inputOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
                        @Override
                        public void onProccessResult(int result, Bundle data) throws RemoteException {
                            Utility.DEBUG_LOG(TAG, "+ onProccessResult +");
                            Utility.DEBUG_LOG(TAG, "onProccessResult callback:");
                            Utility.DEBUG_LOG(TAG, "onProccessResult result:" + result);
                            String str = "RESULT:" + result +
                                    "\nTC_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_TC_DATA_String, "not defined") +
                                    "\nSCRIPT_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_SCRIPT_DATA_String, "not defined") +
                                    "\nREVERSAL_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_REVERSAL_DATA_String, "not defined");
                            transactionContract.toastShow(str);

//                        String resultTLV = data.getString("TC_TLV");
                            String resultTLV = data.getString("TC_TLV");
                            Utility.DEBUG_LOG(TAG, "CardData :" + "onResult TLV: " + resultTLV);
                            Utility.DEBUG_LOG(TAG, "CardData :" + "onResult 98: " + Arrays.toString(iemv.getCardData("98")));

                            String[] tlv_tag_list = {"98"};

                            Utility.DEBUG_LOG(TAG, "CardData :" + "onResult tlvlist: " + iemv.getAppTLVList(tlv_tag_list));
                            Utility.DEBUG_LOG(TAG, "CardData :" + "onResult emvdata: " + iemv.getEMVData("98"));

                            switch (result) {
                                case ConstOnlineResultHandler.onProccessResult.result.TC:
                                    transactionContract.toastShow("TC");
                                    Utility.DEBUG_LOG(TAG, "before printSaleReceipt 5a");
                                    Utility.DEBUG_LOG(TAG, "before printSaleReceipt 5a");
                                    updateEmvTagsForPrinting();

                                    //+ taha 03-03-2021 below is depreceted

                                    ((DashboardContainer) context.get()).getTransactionDetail().setAidCode(aid);

                                    if (txnTypePresenter.equals("ORBIT INQUIRY"))
                                        receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "CUSTOMER COPY", txnTypePresenter, "", Bal, false);
                                    else
                                        receiptPrinter.printSaleReceipt(printer, assetManager, ((DashboardContainer) context.get()).getTransactionDetail(), aid, "MERCHANT COPY", txnTypePresenter, "", Bal, false);
                                    Utility.DEBUG_LOG(TAG, "before transPrinter.print() 4");
                                    //transPrinter = new PrintRecpSale(context);
                                    Utility.DEBUG_LOG(TAG, "checking redeem " + redeemCheck);
                                    // masood commented this due to printing issue 2021-06-24
//                                new Thread(new Runnable() {
//                                    public void run() {
                                    Utility.DEBUG_LOG(TAG, "in print() 4");

//                                        if (transactionDetail.getTxnType().equals("ORBIT INQUIRY")) {
//                                            Utility.DEBUG_LOG(TAG, "Print customer copy for inquiry");
//                                            transPrinter.initializeData(transactionDetail, Bal, redeemCheck, "CUSTOMER COPY");
//                                            transPrinter.setTransactionContract(transactionContract);
//                                            Utility.DEBUG_LOG(TAG, "before print() 4");
//                                            transPrinter.print();
//                                        } else {
//                                            Utility.DEBUG_LOG(TAG, "Print merchant copy for others...");
//                                            transPrinter.initializeData(transactionDetail, Bal, redeemCheck, "MERCHANT COPY");
//                                            transPrinter.setTransactionContract(transactionContract);
//                                            Utility.DEBUG_LOG(TAG, "before print() 4");
//                                            transPrinter.print();
//                                            //System.gc();
//                                        }

                                    Utility.DEBUG_LOG(TAG, "after print() 4 thread");
//                                    }
//                                }).start();
// SHIVAM UPDATE HERE FOR TEXT PRINTER
                                    Utility.DEBUG_LOG(TAG, "after print() 4");
                                    System.gc();

                                    //+ taha 22-02-2021 move this after receipt printing
                                    Utility.DEBUG_LOG(TAG, "before deleteReversal after printing");
                                    generateReversal.deleteReversal();
                                    Utility.DEBUG_LOG(TAG, "before printEmvLogsWrapper2");

                                    printEmvLogsWrapper(3000);

                                    break;
                                case ConstOnlineResultHandler.onProccessResult.result.Online_AAC:
                                    String responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                                    if (!responseCode.equals("00")) // Host responded transaction with response code 35 i.e. “Contact Acquirer”  & response code (01) “Please call Bank” but X990 generates its reversal
                                        Utility.DEBUG_LOG(TAG, "No Revesal needed");
                                    else {
                                        transactionContract.toastShow("Online_AAC");
                                        updateEmvTagsForPrinting();
                                        Utility.DEBUG_LOG(TAG, "before printEmvLogsWrapper3");
                                        printEmvLogsWrapper(3000);

                                        //+ taha 24-02-2021 re-insert reversabefore deleteReversal after getting responsel in case of AAC

                                        Reversal r = new Reversal();
                                        r.copy(((DashboardContainer) context.get()).getTransactionDetail());
                                        Utility.DEBUG_LOG(TAG, "before insertReversal2");
                                        generateReversal.insertReversal(r);
                                    }

//                            case ConstOnlineResultHandler.onProccessResult.result.ERROR:
//                                DialogUtil.errorDialog(context.get(),"Error!","Online Error");
//                                break;
                                default:
                                    transactionContract.toastShow("error, code:" + result);
                                    break;
                            }
                        }
                    });
                }
                else{
                    String currentDate = new SimpleDateFormat("ddMMyyyy", Locale.getDefault()).format(new Date());
                    String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
                    String date = currentDate.substring(0,4);
                    Log.d(TAG,"TIME:"+currentTime);
                    Log.d(TAG,"DATE:"+date);


                    ((DashboardContainer) context.get()).getTransactionDetail().setTId(TERMINAL_ID);
                    ((DashboardContainer) context.get()).getTransactionDetail().setMId(MERCHANT_ID);
                    ((DashboardContainer) context.get()).getTransactionDetail().setStan(INVOICE_NO);
                    ((DashboardContainer) context.get()).getTransactionDetail().setInvoiceNo(INVOICE_NO);
                    ((DashboardContainer) context.get()).getTransactionDetail().setTxnDate(date);
                    ((DashboardContainer) context.get()).getTransactionDetail().setTxnTime(currentTime);
                    if(TIP_ENABLED.equals("Y")){
                        ((DashboardContainer) context.get()).getTransactionDetail().setAmount(DataFormatterUtil.formattedAmount(transAmount));
                    }else{
                        ((DashboardContainer) context.get()).getTransactionDetail().setAmount(DataFormatterUtil.formattedAmount(transactionAmount));
                    }
                    ((DashboardContainer) context.get()).getTransactionDetail().setCardNo(savedPan);
                    //((DashboardContainer) context.get()).getTransactionDetail().setCardScheme(cardType);
                    cardRanges = savedPan.substring(0, 6);
                    cardType = DataFormatterUtil.setCardTypeScheme(Integer.parseInt(cardRanges));
                    ((DashboardContainer) context.get()).getTransactionDetail().setCardScheme2(cardType);
                    ((DashboardContainer) context.get()).getTransactionDetail().setBatchNo(BATCH_NO);
                    ((DashboardContainer) context.get()).getTransactionDetail().setStatus("approved");
                    ((DashboardContainer) context.get()).getTransactionDetail().setProcessingCode("000000");
                    ((DashboardContainer) context.get()).getTransactionDetail().setMessageType("");
                    ((DashboardContainer) context.get()).getTransactionDetail().setAcquiringInstitutionIdentificationCode(ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
                    ((DashboardContainer) context.get()).getTransactionDetail().setForwardingInstitutionIdentificationCode(FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
                    ((DashboardContainer) context.get()).getTransactionDetail().setCardexpiry(cardExpiry);
                    ((DashboardContainer) context.get()).getTransactionDetail().setTxnType(txnTypePresenter);
                    ((DashboardContainer) context.get()).getTransactionDetail().setCancelTxnType("None");
                    ((DashboardContainer) context.get()).getTransactionDetail().setAuthorizationIdentificationResponseCode(authNumber);
                    // ((DashboardContainer) context.get()).getTransactionDetail().setCardHolderName(cardholdername);
                    ((DashboardContainer) context.get()).getTransactionDetail().setIsAdvice(true);
                    ((DashboardContainer) context.get()).getTransactionDetail().setCompletion(true);
                    Bal="-";
                    insertTransaction(AppDatabase.getAppDatabase(context.get()), ((DashboardContainer) context.get()).getTransactionDetail());
                    receiptPrinter.printSaleReceipt(printer, assetManager,  ((DashboardContainer) context.get()).getTransactionDetail(), aid, "MERCHANT COPY", txnTypePresenter, "", Bal, false);

                    paymentContract.showPaymentSuccessDialog( ((DashboardContainer) context.get()).getTransactionDetail());
                    transactionContract.transactionSuccessCustomer("Customer Copy", ((DashboardContainer) context.get()).getTransactionDetail(), "CUSTOMER COPY", txnTypePresenter, Bal, false);

                }
            }

            @Override
            public void onTransactionResult(int result, Bundle data) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "+ onTransactionResult +");
                Utility.DEBUG_LOG(TAG, "onTransactionResult");
                String msg = data.getString("ERROR");
                Utility.DEBUG_LOG(TAG, "onTransactionResult result = " + result + ",msg = " + msg);
                transactionContract.hideProgress();
                Utility.DEBUG_LOG(TAG, "internet:" + internet);
                if (internet) {
                    // transactionContract.transactionFailed(String.valueOf(result),msg);

                    switch (result) {

                        case ConstPBOCHandler.onTransactionResult.result.AARESULT_TC:
                            Utility.DEBUG_LOG(TAG, "AARESULT_TC / below floor limit?");
                            Utility.DEBUG_LOG(TAG, "transactionAmount:" + transactionAmount);
                            Utility.DEBUG_LOG(TAG, "transAmount:" + transAmount);
                            Utility.DEBUG_LOG(TAG, "before isOfflineTransaction 2");
                            if (isOfflineTransaction()) {
                                Utility.DEBUG_LOG(TAG, "Offline approval...");
                                Utility.DEBUG_LOG(TAG, "before transPrinter.print() 5");
                                // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                //  transPrinter = new PrintRecpSale(context);
                                Utility.DEBUG_LOG(TAG, "checking redeem " + redeemCheck);
                                // transPrinter.initializeData(transactionDetail, Bal, redeemCheck, "MERCHANT COPY");
                                // transPrinter.setTransactionContract(transactionContract);
                                Utility.DEBUG_LOG(TAG, "before print() 5");
                                //transPrinter.print();
// SHIVAM UPDATE HERE FOR TEXT PRINTER
                                Utility.DEBUG_LOG(TAG, "after print() ");
                                DashboardContainer.backStack();
                                return;
                            }
//                            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(FallbackOrignatorType.FT_OfflineApproval, transactionAmount), "payment fragment");

                            break;
                        case ConstPBOCHandler.onTransactionResult.result.TRY_AGAIN:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "Unable to Read Card, Please Try again!");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_NO_APP:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "EMV no Application");
                            //+ taha 24-02-2020 fallback instead
//                            DashboardContainer.backStack();
                            Utility.DEBUG_LOG(TAG, "transactionAmount:" + transactionAmount);
                            Utility.DEBUG_LOG(TAG, "transAmount:" + transAmount);
                            if(txnTypePresenter.equals("SALEIPP")) {
                                DashboardContainer.switchFragmentWithBackStack(new SaleIPPFragment(FallbackOrignatorType.FT_Dip, transactionAmount,saleippMonths), "saleipp fragment");
                            }else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(FallbackOrignatorType.FT_Dip, transactionAmount, tipAmount), "payment fragment");
                            }
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_BALANCE_EXCEED:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "EMV BALANCE EXCEED");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_CARD_BIN_CHECK_FAIL:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "Unable to Read Card, Please Re-Attempt the Transaction");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.GAC_ERROR:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "GAC Error, Please Re-Attempt the Transaction");
                            Utility.DEBUG_LOG(TAG,"GAC_ERROR "+ConstPBOCHandler.onTransactionResult.result.GAC_ERROR);
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.EMV_MULTI_CARD_ERROR:
                            // multi-cards found
                            transactionContract.transactionFailed("Failed", "multi-cards found, Please Re-Attempt the Transaction");
                            transactionContract.toastShow(data.getString(ConstPBOCHandler.onTransactionResult.data.KEY_ERROR_String));
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.QPBOC_ABORT:
                            // Transaction abort
                            transactionContract.transactionFailed("Failed", "Transaction abort, Please Re-Attempt the Transaction");
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.AARESULT_AAC:
                            Utility.DEBUG_LOG(TAG, "AARESULT_AAC 1");
//                            if ( Double.parseDouble(((DashboardContainer) context.get()).getTransactionDetail().getTransactionAmount()) < Double.parseDouble(terminalConfig.getSaleOfflineLimit()) )
                            if (((DashboardContainer) context.get()).getTransactionDetail().getIsAdvice()) {
                                Utility.DEBUG_LOG(TAG, "isAdvice || maybe txn amount < terminal offline limit ");
                                set8A("Y3");
//                                Bundle onlineResult = new Bundle();
//                                onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, "Y3");
//                                iemv.inputOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
//                                    @Override
//                                    public void onProccessResult(int result, Bundle data) throws RemoteException {
//                                        Utility.DEBUG_LOG(TAG, "+ onProccessResult +");
//                                        Utility.DEBUG_LOG(TAG, "onProccessResult callback:");
//                                        String str = "RESULT:" + result +
//                                                "\nTC_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_TC_DATA_String, "not defined") +
//                                                "\nSCRIPT_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_SCRIPT_DATA_String, "not defined") +
//                                                "\nREVERSAL_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_REVERSAL_DATA_String, "not defined");
//                                        transactionContract.toastShow(str);
//                                    }
//                                });
                            } else {
                                Utility.DEBUG_LOG(TAG, "normal transaction");
                                // refuse on action analysis
                                transactionContract.transactionFailed("Failed", "Transaction Declined");

//                            transactionContract.transactionFailed("Failed", "refuse on action analysis, Please Re-Attempt the Transaction");
                            }
                            updateEmvTagsForPrinting();
                            Utility.DEBUG_LOG(TAG, "before printEmvLogsWrapper 4");
                            printEmvLogsWrapper(3000);
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.NEED_CONTACT:
                            // need contact
                            Utility.DEBUG_LOG(TAG, "TIPAMOUNT proceed" + tipAmount);
                            transactionContract.transactionFailed("Failed", "Proceed without CTLS");
                            Utility.DEBUG_LOG(TAG, "Proceed without CTLS");
                            if(txnTypePresenter.equals("SALEIPP")) {
                                DashboardContainer.switchFragmentWithBackStack(new SaleIPPFragment(FallbackOrignatorType.FT_Dip, transactionAmount,saleippMonths), "saleipp fragment");
                            }else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(FallbackOrignatorType.FT_Ctls, transactionAmount, tipAmount), "payment fragment");
                            }
                            // DashboardContainer.backStack();
//                            doSearchCard(ReaderType.RT_Chip_Mag);
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_CARD_BLOCK:
                            // need contact
                            transactionContract.transactionFailed("Failed", "Card Blocked");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_FALLBACK:
                            // need contact
                            transactionContract.transactionFailed("Failed", "Insert Mag");
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.QPBOC_AAC:
                            // need contact
                            transactionContract.transactionFailed("Failed", "Decline by card");
                            TagConstants.resetTagConstants();
                            Utility.DEBUG_LOG(TAG, "before printEmvLogsWrapper4");

                            printEmvLogsWrapper(3000);
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.EMV_SEE_PHONE:
                            Utility.DEBUG_LOG("SL", "See Your Phone2");
                            transactionContract.transactionFailed("failed", "See Your Phone");
                            try {
                                sleep(2 * 1000);
                            } catch (Exception e) {
                                Utility.DEBUG_LOG(TAG, "exception:" + e.getMessage().toString());
                            }
                            if(txnTypePresenter.equals("SALEIPP")) {
                                DashboardContainer.switchFragmentWithBackStack(new SaleIPPFragment(FallbackOrignatorType.FT_Dip, transactionAmount,saleippMonths), "saleipp fragment");
                            }else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(FallbackOrignatorType.FT_SeeYourPhone, transactionAmount, tipAmount), "payment fragment");
                            }
//                            doSearchCard(ReaderType.RT_All);
//
//                           DashboardContainer.backStack();
                            break;
//                        case ConstPBOCHandler.onTransactionResult.result.EMV_APP_BLOCKED:
//                            transactionContract.transactionFailed("Failed", msg);
//                            DashboardContainer.backStack();
//                            break;
                        default:
                            Utility.DEBUG_LOG(TAG, "default failure case");
                            transactionContract.transactionFailed("Failed", msg);
                            DashboardContainer.backStack();
                            break;
                    }
                } else {
                    Utility.DEBUG_LOG(TAG, "no connection established");
                    set8A("Z3");
//                    Bundle onlineResult = new Bundle();
//                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, "Z3");
//                    iemv.inputOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
//                        @Override
//                        public void onProccessResult(int result, Bundle data) throws RemoteException {
//                            Utility.DEBUG_LOG(TAG, "+ onProccessResult +");
//                            Utility.DEBUG_LOG(TAG, "onProccessResult callback:");
//                            String str = "RESULT:" + result +
//                                    "\nTC_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_TC_DATA_String, "not defined") +
//                                    "\nSCRIPT_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_SCRIPT_DATA_String, "not defined") +
//                                    "\nREVERSAL_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_REVERSAL_DATA_String, "not defined");
//                            transactionContract.toastShow(str);
//                        }
//                    });


                    switch (result) {
                        case ConstPBOCHandler.onTransactionResult.result.AARESULT_AAC:
                            Utility.DEBUG_LOG(TAG, "AARESULT_AAC 2");
                            // refuse on action analysis
                            transactionContract.transactionFailed("Failed", "Transaction Declined");

//                            transactionContract.transactionFailed("Failed", "refuse on action analysis, Please Re-Attempt the Transaction");
                            updateEmvTagsForPrinting();
                            Utility.DEBUG_LOG(TAG, "before printEmvLogsWrapper 4");
                            printEmvLogsWrapper(3000);
//                            Utility.DEBUG_LOG(TAG,"Before setting Y3 2");
//                            set8A("5933");

                            DashboardContainer.backStack();
                            break;
                        default:
                            Utility.DEBUG_LOG(TAG, "default failure case");
                            transactionContract.transactionFailed("Failed", msg);
                            DashboardContainer.backStack();
                            break;
                    }
                }

            }
        };
    }



    /**
     * \brief set main key and work key
     * <p>
     * \code{.java}
     * \end code
     *
     * @see IPinpad
     */
    void doSetKeys() {
        boolean bRet;
        try {
            bRet = ipinpad.loadMainKey(mainKeyId, Utility.hexStr2Byte(mainKey_MasterKey), null);
            transactionContract.toastShow("loadMainKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        try {
            bRet = ipinpad.loadWorkKey(PinpadKeyType.PINKEY, mainKeyId, workKeyId, Utility.hexStr2Byte(pinKey_WorkKey), null);
            transactionContract.toastShow("loadWorkKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void set8A(String val) throws RemoteException {
//        List<String> tlvList = new ArrayList<>();
//        Utility.DEBUG_LOG(TAG,"+ set8A +");
//        tlvList.add("8A02"+val);
//        Utility.DEBUG_LOG(TAG,"tlvList.get(0):"+tlvList.get(0).toString());
//        try {
//            iemv.setEMVData(tlvList);
//            Utility.DEBUG_LOG(TAG,tlvList.get(0));
//        } catch (RemoteException e) {
//            e.printStackTrace();
//            Utility.DEBUG_LOG(TAG,"Exception: " + e.getMessage().toString());
//        }
        Bundle onlineResult = new Bundle();
        Utility.DEBUG_LOG(TAG, "Setting 8A: " + val);
        onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, val);
        iemv.inputOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
            @Override
            public void onProccessResult(int result, Bundle data) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "+ onProccessResult +");
                Utility.DEBUG_LOG(TAG, "onProccessResult callback:");
                Utility.DEBUG_LOG(TAG, "I am here callback:");

                String str = "RESULT:" + result +
                        "\nTC_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_TC_DATA_String, "not defined") +
                        "\nSCRIPT_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_SCRIPT_DATA_String, "not defined") +
                        "\nREVERSAL_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_REVERSAL_DATA_String, "not defined");
                transactionContract.toastShow(str);

                switch (result){
                    case ConstOnlineResultHandler.onProccessResult.result.ERROR:
                        transactionContract.transactionFailed("Error!","Connection Failed");
                        break;
                    default:
                        transactionContract.toastShow("error, code:" + result);
                        break;
                }
            }
        });
    }

    private void set9F27(String val) {
        Utility.DEBUG_LOG(TAG, "+ set9F27 +");
        List<String> tlvList = new ArrayList<>();
        tlvList.add("9F2701" + val);
        Utility.DEBUG_LOG(TAG, "tlvList.get(0):" + tlvList.get(0).toString());
        try {
            iemv.setEMVData(tlvList);
            Utility.DEBUG_LOG(TAG, tlvList.get(0));
        } catch (RemoteException e) {
            e.printStackTrace();
            Utility.DEBUG_LOG(TAG, "Exception: " + e.getMessage().toString());
        }
    }

    public void selectedAidIndex(int i) {
        try {
            iemv.importAppSelection(i);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public boolean isPinRequiredUsingServiceCode() {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "((DashboardContainer) context.get()).getTransactionDetail():" + ((DashboardContainer) context.get()).getTransactionDetail());
        if (((DashboardContainer) context.get()).getTransactionDetail() == null) {
            rv = false;
        }
        if (((DashboardContainer) context.get()).getTransactionDetail().getTrack2() == null) {
            Utility.DEBUG_LOG(TAG, "trk2:" + ((DashboardContainer) context.get()).getTransactionDetail().getTrack2());
            rv = false;
        } else {
            int separatorIndex = 0;
            char serviceCode3rdDigit = 0;
            String trk2 = ((DashboardContainer) context.get()).getTransactionDetail().getTrack2();
            if ((separatorIndex = trk2.indexOf('=')) > 0) {
                serviceCode3rdDigit = trk2.charAt(separatorIndex + 7);
            } else if ((separatorIndex = trk2.indexOf('D')) > 0) {
                serviceCode3rdDigit = trk2.charAt(separatorIndex + 7);
            }
            Utility.DEBUG_LOG(TAG, "Track2:" + trk2);
            Utility.DEBUG_LOG(TAG, "service code 3rd digit:" + serviceCode3rdDigit);
            if (
                //+ taha 17-03-2021 commenting to pass amex certification test case [UL tool]
                    serviceCode3rdDigit == '0' ||
                            serviceCode3rdDigit == '1' ||
                            serviceCode3rdDigit == '2' ||
                            serviceCode3rdDigit == '3' ||
                            serviceCode3rdDigit == '4' ||
                            serviceCode3rdDigit == '5' ||
                            serviceCode3rdDigit == '6' ||
                            serviceCode3rdDigit == '7'
            ) {
                rv = true;
            }
        }
        Utility.DEBUG_LOG(TAG, "return rv:" + rv);
        return rv;
    }

    public boolean isChipCardFromTrack2() {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "((DashboardContainer) context.get()).getTransactionDetail():" + ((DashboardContainer) context.get()).getTransactionDetail());
        if (((DashboardContainer) context.get()).getTransactionDetail() == null) {
            rv = false;
        }
        if (((DashboardContainer) context.get()).getTransactionDetail().getTrack2() == null) {
            Utility.DEBUG_LOG(TAG, "trk2:" + ((DashboardContainer) context.get()).getTransactionDetail().getTrack2());
            rv = false;
        } else {
            int separatorIndex = 0;
            char serviceCode1stDigit = 0;
            String trk2 = ((DashboardContainer) context.get()).getTransactionDetail().getTrack2();
            if ((separatorIndex = trk2.indexOf('=')) > 0) {
                serviceCode1stDigit = trk2.charAt(separatorIndex + 5);
            } else if ((separatorIndex = trk2.indexOf('D')) > 0) {
                serviceCode1stDigit = trk2.charAt(separatorIndex + 7);
            }
            Utility.DEBUG_LOG(TAG, "Track2:" + trk2);
            Utility.DEBUG_LOG(TAG, "service code 1st digit:" + serviceCode1stDigit);
            if (
                    serviceCode1stDigit == '2' ||
                            serviceCode1stDigit == '6'
            ) {
                rv = true;
            }
        }
        Utility.DEBUG_LOG(TAG, "return rv:" + rv);
        return rv;
    }

    public Last4DigitsCodes getLast4DigitsWrapper() {
        Last4DigitsCodes rv = Last4DigitsCodes.L4D_NA;
        while (true) {
            rv = getLast4DigitsSync();
        }
    }

    public Last4DigitsCodes getLast4DigitsSync() {
        Last4DigitsCodes rv = Last4DigitsCodes.L4D_NA;
        Utility.DEBUG_LOG(TAG, "+ getLast4DigitsSync +");
        Utility.DEBUG_LOG(TAG, "before transactionContract.last4Digits");
        String track2 = ((DashboardContainer) context.get()).getTransactionDetail().getTrack2();
        Utility.DEBUG_LOG(TAG, "track2:" + track2);
        if (track2 == null)
            return rv;
        int separator = track2.indexOf("=");
        if (separator <= 0)
            return rv;
        ((DashboardContainer) context.get()).getTransactionDetail().setLast4Digits("");
        //setLast4Digits
        transactionContract.last4Digits2(((DashboardContainer) context.get()).getTransactionDetail(), "Enter Last 4 Digits...");
        while (true) {
            Utility.DEBUG_LOG(TAG, "((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits():" + ((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits());
            Utility.DEBUG_LOG(TAG, "((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits().length():" + ((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits().length());
            if (((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits().length() == 0) {
                Utility.DEBUG_LOG(TAG, "in while");
                try {
                    sleep(1 * 1000);
                } catch (Exception e) {
                    Utility.DEBUG_LOG(TAG, "exception:" + e.getMessage().toString());
                }
            } else {
                Utility.DEBUG_LOG(TAG, "break last4digits input");
                break;
            }
        }
        Utility.DEBUG_LOG(TAG, "after transactionContract.last4Digits");

        Utility.DEBUG_LOG(TAG, "Last 4 digits:" + ((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits());
        if (((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits().length() == 4) {
            if (separator != 0) {
                String pan = track2.substring(0, separator);
                Utility.DEBUG_LOG(TAG, "PAN:" + pan);
                String last4FromPan = pan.substring(pan.length() - 4, pan.length());
                Utility.DEBUG_LOG(TAG, "last4FromPan:" + last4FromPan);
//                if ( last4FromPan == ((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits() )
                if (last4FromPan.equals(((DashboardContainer) context.get()).getTransactionDetail().getLast4Digits())) {
                    Utility.DEBUG_LOG(TAG, "return true");
//                    return true;
                    rv = Last4DigitsCodes.L4D_OK;
                } else {
                    Utility.DEBUG_LOG(TAG, "return false");
//                    return false;
                    rv = Last4DigitsCodes.L4D_Invalid;
                }
            }
        } else {
            rv = Last4DigitsCodes.L4D_Invalid;
        }
        return rv;
    }

    public Last4DigitsCodes getPanManualEntrySync() {
        Last4DigitsCodes rv = Last4DigitsCodes.L4D_NA;
        Utility.DEBUG_LOG(TAG, "+ getPanManualEntrySync +");
        Utility.DEBUG_LOG(TAG, "before transactionContract.panManualEntry");
        transactionContract.panManualEntry(((DashboardContainer) context.get()).getTransactionDetail(), "Enter PAN...");
        while (true) {
            if (((DashboardContainer) context.get()).getTransactionDetail().getPan().length() == 0) {
                Utility.DEBUG_LOG(TAG, "in while");
                try {
                    sleep(1 * 1000);
                } catch (Exception e) {
                    Utility.DEBUG_LOG(TAG, "exception:" + e.getMessage().toString());
                }
            } else {
                Utility.DEBUG_LOG(TAG, "break pan input");
                break;
            }
        }
        Utility.DEBUG_LOG(TAG, "after transactionContract.panManualEntry");

        Utility.DEBUG_LOG(TAG, "pan:" + ((DashboardContainer) context.get()).getTransactionDetail().getPan());
        if (((DashboardContainer) context.get()).getTransactionDetail().getPan().length() >= 12) {
            Utility.DEBUG_LOG(TAG, "return true");
            rv = Last4DigitsCodes.L4D_OK;
        } else {
            rv = Last4DigitsCodes.L4D_Invalid;
        }
        return rv;
    }

    ;

    //+ below for PIN pad type input, not used current
    private void doLast4Digits(boolean isOnlinePin, int retryTimes) {
        Bundle param = new Bundle();
        Bundle globleparam = new Bundle();
        String panBlock = savedPan;

        byte[] pinLimit = {4}; //todo add 0 to bypass pin, if needed
        param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "ENTER LAST 4 DIGITS");
        param.putString("promptsFont", "/system/fonts/DroidSans-Bold.ttf");
        param.putByteArray("displayKeyValue", new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
        param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, 20);
        param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, isOnlinePin);
        param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, panBlock);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_desType_int, ConstIPinpad.startPinInput.param.Value_desType_3DES);
        if (!isOnlinePin) {
            param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "OFFLINE PIN, retry times:" + retryTimes);
        }
        //globleparam.putString( ConstIPinpad.startPinInput.globleParam.KEY_Display_One_String, "[1]");
        try {

            ipinpad.startPinInput(workKeyId, param, globleparam, last4DigitsListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void doLast4DigitsSynchronous(boolean isOnlinePin, int retryTimes) {
        Utility.DEBUG_LOG(TAG, "Before doLast4Digits");
        doLast4Digits(isOnlinePin, retryTimes);
        Utility.DEBUG_LOG(TAG, "After doLast4Digits");
        while (true) {
            if (last4Digits == null) {
                Utility.DEBUG_LOG(TAG, "in while");
                try {
                    sleep(1 * 1000);
                } catch (Exception e) {
                    Utility.DEBUG_LOG(TAG, "exception:" + e.getMessage().toString());
                }
            } else {
                Utility.DEBUG_LOG(TAG, "break last4Digits input");
                break;
            }
        }
    }

    public void printEmvLogsWrapper(int printAfterMilliSecs) {
        Utility.DEBUG_LOG(TAG, "+ printEmvLogsWrapper +");
        if (emvLogsPrinted) {
            Utility.DEBUG_LOG(TAG, "logs already printed, return");
            return;
        }
        int printEmvLogs = SharedPref.read("printEmvLogs");
        Utility.DEBUG_LOG(TAG, "printEmvLogs:" + printEmvLogs);
        if (printEmvLogs == 0) {
            Utility.DEBUG_LOG(TAG, "EMV log printing disabled, return");
            return;
        }
        emvLogsPrinted = true;
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                receiptPrinter.printEmvLogs(printer, assetManager, false);
            }
        }, printAfterMilliSecs);
    }

    public void setTag8A(String value8A) {
        Utility.DEBUG_LOG(TAG, "+ setTag8A +");
        // set aid to kernel
        List<String> tags = new ArrayList<String>();
        tags.add("8A025933");
        try {
            iemv.setEMVData(tags);
        } catch (RemoteException e) {
            e.printStackTrace();
            Utility.DEBUG_LOG(TAG, "Exception: " + e.getMessage().toString());
        }
    }

    public void printIsoLogsWrapper(String data, int printAfterMilliSecs) {
        Utility.DEBUG_LOG(TAG, "+ printIsoRequestWrapper +");
        int printIsoLogs = SharedPref.read("printIsoLogs");
        Utility.DEBUG_LOG(TAG, "printIsoLogs:" + printIsoLogs);
        if (printIsoLogs == 0) {
            Utility.DEBUG_LOG(TAG, "printIsoLogs disabled, return");
            return;
        }
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                receiptPrinter.printIsoLogs(data, printer, assetManager, true);
            }
        }, printAfterMilliSecs);

        try {
            sleep(3 * 1000);
        } catch (Exception e) {
            Utility.DEBUG_LOG(TAG, "exception:" + e.getMessage().toString());
        }

    }

    public enum FallbackOrignatorType {FT_NA, FT_Dip, FT_Ctls, FT_Swipe, FT_SeeYourPhone}

    /**
     * \Brief the transaction type
     * <p>
     * Prefix
     * T_ means transaction
     * M_ means management
     */
    enum TransType {
        T_BANLANCE, T_PURCHASE,
        M_SIGNIN
    }

    public enum Last4DigitsCodes {L4D_NA, L4D_Cancel, L4D_Invalid, L4D_OK}

}
