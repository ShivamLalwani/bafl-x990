package cn.access.group.android_all_banks_pos.Utilities;
/*
 *  author: Derrick
 *  Time: 2019/12/9 16:18
 */

import android.app.Activity;
import cn.access.group.android_all_banks_pos.Utilities.Utility;


import java.util.ArrayList;
import java.util.List;

import cn.access.group.android_all_banks_pos.MainActivity;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;

public class ActivityCollector {

    public static List<Activity> activityList = new ArrayList<>();

    public static void addActivity (Activity activity){
        activityList.add(activity);
    }

    public static void removeActivity (Activity activity){
        activityList.remove(activity);
    }

    public static void finishAll(){
        for (Activity activity : activityList){
            if (!activity.isFinishing()){
                activity.finish();
            }
        }
    }

    public static void finishAllTransActivity(){
        for (Activity activity : activityList){
            if (!activity.isFinishing() && !(activity instanceof MainActivity)){
                activity.finish();
            }
        }
    }



}
