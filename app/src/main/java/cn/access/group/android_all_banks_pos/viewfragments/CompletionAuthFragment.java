package cn.access.group.android_all_banks_pos.viewfragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DecimalDigitsInputFilter;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.FourDigitCardFormatWatcher;
import cn.access.group.android_all_banks_pos.Utilities.NumberTextWatcherForThousand;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Validation;
import cn.access.group.android_all_banks_pos.adapter.aidListAdapter;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.dao.TransactionDetailDao;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author muhammad.humayun
 * on 8/20/2019.
 * this is a view call which is reponsible for starting the hardware service and invoking the presenter of this view
 */
public class CompletionAuthFragment extends Fragment{

    @BindView(R.id.completionHeading)
    TextView authHeading;

    @BindView(R.id.textLayoutCompletion)
    LinearLayout textLayoutAuth;

    @BindView(R.id.editTextAuthId)
    TextView editTextAuthId;




    String transactionAmount;
    Unbinder unbinder;
    IBeeper iBeeper;
    public static IDeviceService idevice;
    boolean isSucc;
    private static final String TAG = "CompAuthFrag";
    Intent intent = new Intent();
    Dialog alertDialog;
    String buttonClicked;
    public String current = "";
    public boolean bool;
    private WeakReference<Context> mContext;
    public boolean isFallback;
    String authNumber = "";

    public CompletionAuthFragment() {

    }

    @SuppressLint("ValidFragment")
    public CompletionAuthFragment(boolean isFallback, String transactionAmount) {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         Utility.DEBUG_LOG(TAG,"+ PaymentAmountFragment:onCreateView() +");

        View rootView = inflater.inflate(R.layout.fragment_completion_auth, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        editTextAuthId.requestFocus();

        InputMethodManager imm = (InputMethodManager) mContext.get().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        editTextAuthId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i== EditorInfo.IME_ACTION_DONE){
                    authNumber = editTextAuthId.getText().toString();
                     Utility.DEBUG_LOG(TAG,"+ PaymentAmountFragment:onCreateView() +"+authNumber);
                    DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(authNumber),"payment fragment");
                    //Toast.makeText(getApplicationContext(),"Done pressed",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        alertDialog=new Dialog(context);
        mContext = new WeakReference<>(context);
        Utility.DEBUG_LOG(TAG,"PaymentAmountFragment:onAttach()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if ( getActivity() != null)
            getActivity().unbindService(conn);
    }


    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private final ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            new CountDownTimer(01, 01) {
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);
                    try {
                         Utility.DEBUG_LOG("onServiceConnected","onServiceConnected--Control here..");
                         Utility.DEBUG_LOG("terminal serial",Constants.TERMINAL_SERIAL );
                        iBeeper = idevice.getBeeper();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }

                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    /**
     * this method animates the view, and executes the purchase function of the presenter which takes amount as parameter
     */

    // mmasood: countdown timer in sales screen


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        buttonClicked = "";
        // conn = null;
        unbinder.unbind();

    }



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
             Utility.DEBUG_LOG(TAG,"+ PaymentFragment:Handler:handleMessage()+ ");
             Utility.DEBUG_LOG(TAG,"before getData");

            String str = msg.getData().toString();
             Utility.DEBUG_LOG(TAG,"Message:" + str);
            super.handleMessage(msg);
//            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };



    public void setTransactionAmount(String transactionAmount)
    {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionAmount()
    {
        return transactionAmount;
    }

    public boolean getIsFallback()
    {
        return isFallback;
    }




}

