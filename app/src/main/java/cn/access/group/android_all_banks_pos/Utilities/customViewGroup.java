package cn.access.group.android_all_banks_pos.Utilities;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;

public class customViewGroup extends ViewGroup {
    public customViewGroup(Context context) {
        super(context);
    }
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Utility.DEBUG_LOG("customViewGroup", "**********Intercepted");
        return true;
    }
}
