package cn.access.group.android_all_banks_pos.repository;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.Issuer;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.*;


/**
 * Created by Syed Masood Azeem 12-21-2020.
 * how to use this class ...
 */
public class Repository  {
    //private AppDatabase appDatabase;
    private TransactionDetail transaction;
    private CardRanges cardRanges;
    private String TAG="repository";
    private WeakReference<Context> context;


    public Repository(Context context) {
        this.context = new WeakReference<>(context);
    }


    @SuppressLint("CheckResult")
    public TransactionDetail findTransactionByStatus(String status){
        AppDatabase.getAppDatabase(context.get()).transactionDetailDao().findByStatus(status)
                .subscribeOn(Schedulers.newThread())
                .subscribe(transactionDetail -> transaction=transactionDetail);
        return  transaction;
    }

    @SuppressLint("CheckResult")
    public void deleteTransaction(){
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).transactionDetailDao().delete(transaction))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
//    public LiveData<TransactionDetail> findByAuthId(String authId) {
//        return appDatabase.transactionDetailDao().findByAuthId(authId);
//    }
///////////////////////////////////Issuer config//////////////////////////////////////////////////////
    @SuppressLint("CheckResult")
    public void setIssuerConfig(){
          /*  TerminalConfig terminalConfig1= new TerminalConfig(terminalConfig.getTcid(),
                    terminalConfig.getMerchantID(),terminalConfig.getTerminalID(),
                    terminalConfig.getMerchantName(),terminalConfig.getHostIp(),
                    terminalConfig.getHostPort(),terminalConfig.getNiiNum());*/

        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).issuerDao().insertIssuerConfig(Issuer.issuerList()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"iSSUER added");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
    @SuppressLint("CheckResult")
    public void updateIssuerConfig( Issuer issuer) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).issuerDao().updateIssuerConfig(issuer))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"isssuer updated");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
    public void getIssuerConfig() {
        try {
            List<Issuer> issuerList = getAllIssuerConfig();
            Utility.DEBUG_LOG(TAG,"config found");
            Issuer issuerConfig = issuerList.get(0);
            HOST_INDEX_FROM_DB= issuerConfig.getIssuerid(); //0 is ubl
            //write
            SharedPref.write("ISS_NAME",issuerConfig.getIssName() );
            SharedPref.write("ISS_NUM", issuerConfig.getIssNum());
            SharedPref.write("ISS_ACQUIRER", issuerConfig.getAcquirer());
            SharedPref.write("ISS_BANK_FUND", issuerConfig.getBankFunded());
            SharedPref.write("ISS_MOD10", issuerConfig.getMod10());
            SharedPref.write("ISS_CHECK_EXPIRY", issuerConfig.getCheckExpiry());
            SharedPref.write("ISS_MANUAL_ENTRY", issuerConfig.getManualEntry());
            SharedPref.write("ISS_PRINT_EXPIRY", issuerConfig.getPrintExpiry());
            SharedPref.write("ISS_LAST4DIGIT_MAG", issuerConfig.getLast4DMag());
            SharedPref.write("ISS_LAST4DIGIT_SMART", issuerConfig.getLast4DSmart());
            SharedPref.write("ISS_PIN_BYPASS", issuerConfig.getPINBypass());
            SharedPref.write("ISS_CHSERVE_CODE", issuerConfig.getChServCode());
            SharedPref.write("ISS_CHECKPIN", issuerConfig.getCheckPin());
            SharedPref.write("ISS_CHSIGN", issuerConfig.getChSign());
            SharedPref.write("ISS_ISSUERACTIVATED", issuerConfig.getIssuerActivated());
            SharedPref.write("ISS_REFUND_ENABLE", issuerConfig.getRefundEnable());
            SharedPref.write("ISS_VOID_ENABLE", issuerConfig.getVoidEnable());
            SharedPref.write("ISS_PREAUTH_ENABLE", issuerConfig.getPreAuthEnable());
            SharedPref.write("ISS_COMPLETION_ENABLE", issuerConfig.getCompletionEnable());
            SharedPref.write("ISS_ADJUST_ENABLE", issuerConfig.getAdjustEnable());
            SharedPref.write("ISS_CASHADV_ENABLE", issuerConfig.getAdvCashEnable());
            SharedPref.write("ISS_ORBITREDEEM_ENABLE", issuerConfig.getOrbitRedeemEnable());
            SharedPref.write("ISS_FORCEDlBP_ENABLE", issuerConfig.getForcedLBPEnable());


            //read
            ISS_NAME = SharedPref.read("ISS_NAME","");
            ISS_NUM = SharedPref.read("ISS_NUM","");
            ISS_ACQUIRER = SharedPref.read("ISS_ACQUIRER","");
            ISS_BANK_FUND= SharedPref.read("ISS_BANK_FUND","");
            ISS_MOD10= SharedPref.read("ISS_MOD10","");
            ISS_CHECK_EXPIRY= SharedPref.read("ISS_CHECK_EXPIRY","");
            ISS_MANUAL_ENTRY= SharedPref.read("ISS_MANUAL_ENTRY","");
            ISS_PRINT_EXPIRY= SharedPref.read("ISS_PRINT_EXPIRY","");
            ISS_LAST4DIGIT_MAG= SharedPref.read("ISS_LAST4DIGIT_MAG","");
            ISS_LAST4DIGIT_SMART= SharedPref.read("ISS_LAST4DIGIT_SMART","");
            ISS_PIN_BYPASS= SharedPref.read("ISS_PIN_BYPASS","");
            ISS_CHSERVE_CODE= SharedPref.read("ISS_CHSERVE_CODE","");
            ISS_CHECKPIN= SharedPref.read("ISS_CHECKPIN","");
            ISS_CHSIGN= SharedPref.read("ISS_CHSIGN","");
            ISS_ISSUERACTIVATED= SharedPref.read("ISS_ISSUERACTIVATED","");
            ISS_REFUND_ENABLE= SharedPref.read("ISS_REFUND_ENABLE","");
            ISS_VOID_ENABLE= SharedPref.read("ISS_VOID_ENABLE","");
            ISS_PREAUTH_ENABLE= SharedPref.read("ISS_PREAUTH_ENABLE","");
            ISS_COMPLETION_ENABLE= SharedPref.read("ISS_COMPLETION_ENABLE","");
            ISS_ADJUST_ENABLE= SharedPref.read("ISS_ADJUST_ENABLE","");
            ISS_CASHADV_ENABLE= SharedPref.read("ISS_CASHADV_ENABLE","");
            ISS_ORBITREDEEM_ENABLE= SharedPref.read("ISS_ORBITREDEEM_ENABLE","");
            ISS_FORCEDlBP_ENABLE= SharedPref.read("ISS_FORCEDlBP_ENABLE","");


        }
        catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("CheckResult")
    public void emptyTableIssuerConfig(){
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).issuerDao().nukeTable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG," issuer table cleared");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }


    public List<Issuer> getAllIssuerConfig() throws ExecutionException, InterruptedException {
        return new GetIssuerConfigAsyncTask().execute().get();
    }
    @SuppressLint("StaticFieldLeak")
    private class GetIssuerConfigAsyncTask extends AsyncTask<Void, Void,List<Issuer>>
    {

        @Override
        protected List<Issuer> doInBackground(Void... voids) {
            return AppDatabase.getAppDatabase(context.get()).issuerDao().issuerGetConfig();
        }
    }
    ///////////////////////////////////Issuer config//////////////////////////////////////////////////////

///////////////////////////////////card config//////////////////////////////////////////////////////
    @SuppressLint("CheckResult")
    public void setCardConfig(){
      /*  TerminalConfig terminalConfig1= new TerminalConfig(terminalConfig.getTcid(),
                terminalConfig.getMerchantID(),terminalConfig.getTerminalID(),
                terminalConfig.getMerchantName(),terminalConfig.getHostIp(),
                terminalConfig.getHostPort(),terminalConfig.getNiiNum());*/

        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).cardRangesDao().insertConfig(CardRanges.cardRangesList()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"card ranges added");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
    @SuppressLint("CheckResult")
    public void updateCardRanges( CardRanges cardRanges) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).cardRangesDao().updateCardRanges(cardRanges))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"config updated");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
    public void getCardRangesConfig() {
        try {
            List<CardRanges> cardRangesList = getAllCardRanges();
            Utility.DEBUG_LOG(TAG,"config found card ranges");
            CardRanges cardRanges = cardRangesList.get(0);
            HOST_INDEX_FROM_DB= cardRanges.getCrid(); //0 is ubl
            //write
            SharedPref.write("CARD_TYPE_NAME",cardRanges.getCardTypeName() );
            SharedPref.write("LOWER_RANGE", cardRanges.getLowerRange());
            SharedPref.write("HIGHER_RANGE", cardRanges.getHigherRange());
            SharedPref.write("CARD_LENGTH", cardRanges.getCardLenght());
            SharedPref.write("CARD_ISSUER", cardRanges.getCardIssuer());
            //read
            CARD_TYPE_NAME = SharedPref.read("CARD_TYPE_NAME","");
            LOWER_RANGE = SharedPref.read("LOWER_RANGE");
            HIGHER_RANGE = SharedPref.read("HIGHER_RANGE");
            CARD_LENGTH = SharedPref.read("CARD_LENGTH","");
            CARD_ISSUER = SharedPref.read("CARD_ISSUER","");

        }
        catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }


    @SuppressLint("CheckResult")
    public void emptyTableCardRanges(){
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).cardRangesDao().nukeTable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG," table cleared");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }

    @SuppressLint("CheckResult")
    public void updateSettleCounterRepository(int SETTLECOUNTER, int tcid) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).terminalConfigDao().updateSettleCounter(SETTLECOUNTER,tcid))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"Settle Counter Updated in terminal config DB");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
    @SuppressLint("CheckResult")
    public void updateTipNumAdjustRepository(int TIPADJUST, int tcid) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).terminalConfigDao().updateTipNumAdjust(TIPADJUST,tcid))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"Tip Num Updated in terminal config DB");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }


    public List<CardRanges> getAllCardRanges() throws ExecutionException, InterruptedException {
        return new GetCardRangesAsyncTask().execute().get();
    }
    @SuppressLint("StaticFieldLeak")
    private class GetCardRangesAsyncTask extends AsyncTask<Void, Void,List<CardRanges>>
    {

        @Override
        protected List<CardRanges> doInBackground(Void... voids) {
            return AppDatabase.getAppDatabase(context.get()).cardRangesDao().cardRangesConfig();
        }
    }
    ///////////////////////////////////card config//////////////////////////////////////////////////////


    ///////////////////////////////terminal Config//////////////////////////////////////////////////
    ///only to be used once!
    @SuppressLint("CheckResult")
    public void setTerminalConfig(){
      /*  TerminalConfig terminalConfig1= new TerminalConfig(terminalConfig.getTcid(),
                terminalConfig.getMerchantID(),terminalConfig.getTerminalID(),
                terminalConfig.getMerchantName(),terminalConfig.getHostIp(),
                terminalConfig.getHostPort(),terminalConfig.getNiiNum());*/

        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).terminalConfigDao().insertConfig(TerminalConfig.insertInitialConfig()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                        Utility.DEBUG_LOG(TAG,"config added");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }

    @SuppressLint("CheckResult")
    public void updateTerminalConfiguration( TerminalConfig terminalConfig) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).terminalConfigDao().updateConfiguration(terminalConfig))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                    Utility.DEBUG_LOG(TAG,"config updated using load json");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
    @SuppressLint("CheckResult")
    public void updateBatchNumber(String batchNo, int tcid) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).terminalConfigDao().updateBatch(batchNo,tcid))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"Batch updated in terminal config DB");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }

    @SuppressLint("CheckResult")
    public void terminalConfigurationByID(int tcid) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).terminalConfigDao().terminalConfigurationByID(tcid))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG,"config updated");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }


    public TerminalConfig getTerminalConfigurationFromUi()
    {
        Utility.DEBUG_LOG(TAG,"+ getTerminalConfigurationFromUi +");
        Utility.DEBUG_LOG(TAG,"CVM_LIMIT_AMEX:"+CVM_LIMIT_AMEX);
        TerminalConfig terminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID,
                MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),

                CVM_LIMIT_VISA,
                CVM_LIMIT_MC,
                CVM_LIMIT_UPI,
                CVM_LIMIT_PAYPAK,
                CVM_LIMIT_JCB,
                CVM_LIMIT_AMEX,

                SALE_OFFLINE_LIMIT, SALE_MIN_AMOUNT_LIMIT, SALE_MAX_AMOUNT_LIMIT,

                VEPS_LIMIT,
                VEPS_LIMIT_MC,
                VEPS_LIMIT_UPI,
                VEPS_LIMIT_PAYPAK,
                VEPS_LIMIT_JCB,
                VEPS_LIMIT_AMEX,

                PortalUrl,
                PortalUserName,
                PortalPassword,
                PortalTimeout,
                PortalTxnUpload,
                SETTLECOUNTER,
                SETTLERANGE,
                TIPNUMADJUST,
                isEcrEnable,
                ecrType

        );

        Utility.DEBUG_LOG("REPOSITORY","TIP_ENABLED:"+TIP_ENABLED);
//        TerminalConfig terminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID,
//                MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),
//                "300.00", "0.00", "0.00", "999999.99", "1500.00");
        return terminalConfig;
    }

    public void getTerminalConfiguration(){
        try {
            Utility.DEBUG_LOG(TAG,"+ getTerminalConfiguration +");
           List<TerminalConfig> terminalConfigList = getAllTransactions();
            Utility.DEBUG_LOG(TAG,"config found");
           TerminalConfig terminalConfig = terminalConfigList.get(0);
           HOST_INDEX_FROM_DB= terminalConfig.getTcid(); //0 is ubl
           //write
            SharedPref.write("hostIP", terminalConfig.getHostIp());
            SharedPref.write("hostPort",Integer.parseInt(terminalConfig.getHostPort()));
            SharedPref.write("TERMINAL_ID", terminalConfig.getTerminalID());
            SharedPref.write("MERCHANT_ID", terminalConfig.getMerchantID());
            SharedPref.write("NII", terminalConfig.getNiiNum());
            SharedPref.write("merchantName", terminalConfig.getMerchantName());
            SharedPref.write("TIP_ENABLED",terminalConfig.getTipEnabled());
            SharedPref.write("COM_ETHERNET", terminalConfig.getComEthernet());
            SharedPref.write("COM_GPRS", terminalConfig.getComGPRS());
            SharedPref.write("ENABLE_LOGS", terminalConfig.getEnableLogs());
            SharedPref.write("MARKET_SEG", terminalConfig.getMarketSeg());
            SharedPref.write("TIP_THRESHOLD", terminalConfig.getTipThershold());
            SharedPref.write("FALLBACK_ENABLE", terminalConfig.getFallbackEnable());
            SharedPref.write("MSGHEADER_LENHEX", terminalConfig.getMsgHeaderLenHex());
            SharedPref.write("SSL_ENABLE", terminalConfig.getSslEnable());
            SharedPref.write("TOPUP_ENABLE", terminalConfig.getTopUpEnable());
            SharedPref.write("MANUAL_ENTRY", terminalConfig.getManualEntry());
            SharedPref.write("AUTO_SETTLEMENT", terminalConfig.getAutoSettlement());
            SharedPref.write("AUTO_SETTLETIME", terminalConfig.getAutoSettleTime());
            SharedPref.write("AMEX_ID", terminalConfig.getAmexID());
            SharedPref.write("PIN_BYPASS", terminalConfig.getPinByPass());
            SharedPref.write("MOD10", terminalConfig.getMod10());
            SharedPref.write("CHECK_EXPIRY", terminalConfig.getCheckExpiry());
            SharedPref.write("MANUAL_ENTRY_ISSUER", terminalConfig.getManualEntryIssuer());
            SharedPref.write("LAST_4DIGIT_MAG", terminalConfig.getLast4digitMag());
            SharedPref.write("LAST_4DIGIT_ICC", terminalConfig.getLast4digitIcc());
            SharedPref.write("ACQUIRER", terminalConfig.getAcquirer());
            SharedPref.write("LOW_BIN", terminalConfig.getLowBIN());
            SharedPref.write("HIGH_BIN", terminalConfig.getHighBIN());
            SharedPref.write("ISSUER", terminalConfig.getIssuer());
            SharedPref.write("CURRENCY", terminalConfig.getCurrency());
            SharedPref.write("USER_PASSWORD_CHANGE", terminalConfig.getuserPassword());
            SharedPref.write("MANAGER_PASSWORD_CHANGE", terminalConfig.getmanagerPassword());
            SharedPref.write("SYSTEM_PASSWORD_CHANGE", terminalConfig.getSystemuserPassword());
            SharedPref.write("SUPER_USER_PASSWORD_CHANGE", terminalConfig.getSuperuserPassword());
            SharedPref.write("HEADER_LINE_1", terminalConfig.getHeaderLine1());
            SharedPref.write("HEADER_LINE_2", terminalConfig.getHeaderLine2());
            SharedPref.write("HEADER_LINE_3", terminalConfig.getHeaderLine3());
            SharedPref.write("HEADER_LINE_4", terminalConfig.getHeaderLine4());
            SharedPref.write("FOOTER_LINE_1", terminalConfig.getFooterLine1());
            SharedPref.write("FOOTER_LINE_2", terminalConfig.getFooterLine2());
            SharedPref.write("PRI_MEDIUM", terminalConfig.getPriMedium());
            SharedPref.write("SEC_MEDIUM", terminalConfig.getSecMdeium());
            SharedPref.write("batch_no", Integer.parseInt(terminalConfig.getBatchNo()));
            SharedPref.write("TMS_IP", terminalConfig.getTmsIp());
            SharedPref.write("TMS_PORT", terminalConfig.getTmsPort());
            SharedPref.write("SEC_IP", terminalConfig.getSecIp());
            SharedPref.write("SEC_PORT", Integer.parseInt(terminalConfig.getSecPort()));

            SharedPref.write("CVM_LIMIT_VISA", terminalConfig.getCvmLimitVisa());
            SharedPref.write("CVM_LIMIT_MC", terminalConfig.getCvmLimitMc());
            SharedPref.write("CVM_LIMIT_UPI", terminalConfig.getCvmLimitUpi());
            SharedPref.write("CVM_LIMIT_PAYPAK", terminalConfig.getCvmLimitPaypak());
            SharedPref.write("CVM_LIMIT_JCB", terminalConfig.getCvmLimitJcb());
            SharedPref.write("CVM_LIMIT_AMEX", terminalConfig.getCvmLimitAmex());
            Utility.DEBUG_LOG(TAG,"writing CVM_LIMIT_AMEX:"+terminalConfig.getCvmLimitAmex());

            SharedPref.write("SALE_OFFLINE_LIMIT", terminalConfig.getSaleOfflineLimit());
            SharedPref.write("SALE_MIN_AMOUNT_LIMIT", terminalConfig.getSaleMinAmtLimit());
            SharedPref.write("SALE_MAX_AMOUNT_LIMIT", terminalConfig.getSaleMaxAmtLimit());

            SharedPref.write("VEPS_LIMIT", terminalConfig.getVepsLimit());
            SharedPref.write("VEPS_LIMIT_MC", terminalConfig.getVepsLimitMc());
            SharedPref.write("VEPS_LIMIT_UPI", terminalConfig.getVepsLimitUpi());
            SharedPref.write("VEPS_LIMIT_PAYPAK", terminalConfig.getVepsLimitPaypak());
            SharedPref.write("VEPS_LIMIT_JCB", terminalConfig.getVepsLimitJcb());
            SharedPref.write("VEPS_LIMIT_AMEX", terminalConfig.getVepsLimitAmex());


            SharedPref.write("PortalUrl", terminalConfig.getPortalUrl());
            SharedPref.write("PortalUserName", terminalConfig.getPortalUserName());
            SharedPref.write("PortalPassword", terminalConfig.getPortalPassword());
            SharedPref.write("PortalTimeout", terminalConfig.getPortalTimeout());
            SharedPref.write("PortalTxnUpload", terminalConfig.getPortalTxnUpload());
            SharedPref.write("settleCounter", terminalConfig.getSettleCounter());
            SharedPref.write("settleRange", terminalConfig.getSettleRange());
            SharedPref.write("tipNumAdjust", terminalConfig.getTipNumAdjust());
            SharedPref.write("ecrEnabled", terminalConfig.getEcrEnabled());
            SharedPref.write("ecrType", terminalConfig.getEcrType());





            //assign
            TERMINAL_ID= SharedPref.read("TERMINAL_ID","");
            MERCHANT_ID= SharedPref.read("MERCHANT_ID","");
            MERCHANT_NAME= SharedPref.read("merchantName","");
            hostIP= SharedPref.read("hostIP","");
            hostPort= SharedPref.read("hostPort");
            NII= SharedPref.read("NII","");
            TIP_ENABLED = SharedPref.read("TIP_ENABLED","");
            Utility.DEBUG_LOG(TAG,"TIP_ENABLED repository"+TIP_ENABLED);
            COM_ETHERNET = SharedPref.read("COM_ETHERNET", "");
            COM_GPRS = SharedPref.read("COM_GPRS", "");
            ENABLE_LOGS = SharedPref.read("ENABLE_LOGS", "");
            MARKET_SEG = SharedPref.read("MARKET_SEG", "");
            TIP_THRESHOLD = SharedPref.read("TIP_THRESHOLD", "");
            FALLBACK_ENABLE = SharedPref.read("FALLBACK_ENABLE", "");
            MSGHEADER_LENHEX = SharedPref.read("MSGHEADER_LENHEX", "");
            SSL_ENABLE = SharedPref.read("SSL_ENABLE", "");
            TOPUP_ENABLE = SharedPref.read("TOPUP_ENABLE", "");
            MANUAL_ENTRY = SharedPref.read("MANUAL_ENTRY", "");
            AUTO_SETTLEMENT = SharedPref.read("AUTO_SETTLEMENT", "");
            AUTO_SETTLETIME = SharedPref.read("AUTO_SETTLETIME", "");
            AMEX_ID = SharedPref.read("AMEX_ID", "");
            PIN_BYPASS = SharedPref.read("PIN_BYPASS", "");
            MOD10 = SharedPref.read("MOD10", "");
            CHECK_EXPIRY = SharedPref.read("CHECK_EXPIRY", "");
            MANUAL_ENTRY_ISSUER = SharedPref.read("MANUAL_ENTRY_ISSUER", "");
            LAST_4DIGIT_MAG = SharedPref.read("LAST_4DIGIT_MAG", "");
            LAST_4DIGIT_ICC= SharedPref.read("LAST_4DIGIT_ICC", "");
            ACQUIRER = SharedPref.read("ACQUIRER", "");
            LOW_BIN = SharedPref.read("LOW_BIN", "");
            HIGH_BIN = SharedPref.read("HIGH_BIN", "");
            ISSUER = SharedPref.read("ISSUER", "");
            CURRENCY = SharedPref.read("CURRENCY", "");
            USER_PASSWORD_CHANGE = SharedPref.read("USER_PASSWORD_CHANGE", "");
            MANAGER_PASSWORD_CHANGE = SharedPref.read("MANAGER_PASSWORD_CHANGE", "");
            SYSTEM_PASSWORD_CHANGE = SharedPref.read("SYSTEM_PASSWORD_CHANGE", "");
            SUPER_USER_PASSWORD_CHANGE = SharedPref.read("SUPER_USER_PASSWORD_CHANGE", "");
            HEADER_LINE_1 = SharedPref.read("HEADER_LINE_1", "");
            HEADER_LINE_2 = SharedPref.read("HEADER_LINE_2", "");
            HEADER_LINE_3 = SharedPref.read("HEADER_LINE_3", "");
            HEADER_LINE_4 = SharedPref.read("HEADER_LINE_4", "");
            FOOTER_LINE_1 = SharedPref.read("FOOTER_LINE_1", "");
            FOOTER_LINE_2 = SharedPref.read("FOOTER_LINE_2", "");
            PRI_MEDIUM = SharedPref.read("PRI_MEDIUM", "");
            SEC_MEDIUM = SharedPref.read("SEC_MEDIUM", "");
            String.format(Locale.getDefault(),"%06d", (int)21);
           // BATCH_NO = SharedPref.read("batch_no", "");
            BATCH_NO = String.format(Locale.getDefault(), "%06d", SharedPref.read("batch_no"));
            TMS_IP = SharedPref.read("TMS_IP", "");
            TMS_PORT = SharedPref.read("TMS_PORT", "");
            SEC_IP = SharedPref.read("SEC_IP", "");
            SEC_PORT = SharedPref.read("SEC_PORT");

//            CVM_LIMIT = SharedPref.read("CVM_LIMIT", "");
            CVM_LIMIT_VISA = SharedPref.read("CVM_LIMIT_VISA", "");
            CVM_LIMIT_MC = SharedPref.read("CVM_LIMIT_MC", "");
            CVM_LIMIT_UPI = SharedPref.read("CVM_LIMIT_UPI", "");
            CVM_LIMIT_PAYPAK = SharedPref.read("CVM_LIMIT_PAYPAK", "");
            CVM_LIMIT_JCB = SharedPref.read("CVM_LIMIT_JCB", "");
            CVM_LIMIT_AMEX = SharedPref.read("CVM_LIMIT_AMEX", "");
            Utility.DEBUG_LOG(TAG,"CVM_LIMIT_AMEX:"+CVM_LIMIT_AMEX);

            SALE_OFFLINE_LIMIT = SharedPref.read("SALE_OFFLINE_LIMIT", "");
            SALE_MIN_AMOUNT_LIMIT = SharedPref.read("SALE_MIN_AMOUNT_LIMIT", "");
            SALE_MAX_AMOUNT_LIMIT = SharedPref.read("SALE_MAX_AMOUNT_LIMIT", "");

            VEPS_LIMIT = SharedPref.read("VEPS_LIMIT", "");
            VEPS_LIMIT_MC = SharedPref.read("VEPS_LIMIT_MC", "");
            VEPS_LIMIT_UPI = SharedPref.read("VEPS_LIMIT_UPI", "");
            VEPS_LIMIT_PAYPAK = SharedPref.read("VEPS_LIMIT_PAYPAK", "");
            VEPS_LIMIT_JCB = SharedPref.read("VEPS_LIMIT_JCB", "");
            VEPS_LIMIT_AMEX = SharedPref.read("VEPS_LIMIT_AMEX", "");

            PortalUrl =  SharedPref.read("PortalUrl","");
            PortalUserName =  SharedPref.read("PortalUserName","");
            PortalPassword =  SharedPref.read("PortalPassword","");
            PortalTimeout =  SharedPref.read("PortalTimeout","");
            PortalTxnUpload =  SharedPref.read("PortalTxnUpload","");
            SETTLECOUNTER = SharedPref.read("settleCounter");
            SETTLERANGE = SharedPref.read("settleRange");
            TIPNUMADJUST = SharedPref.read("tipNumAdjust");
            isEcrEnable =  SharedPref.read("ecrEnabled","");
            ecrType = SharedPref.read("ecrType","");

            Utility.DEBUG_LOG(TAG,"before SharedPref.read(\"maintenanceThreshold\"");
//            String maintenanceThresholdStr =null;
//            try
//            {
//                maintenanceThresholdStr = SharedPref.read("maintenanceThreshold","125");
//            }
//            catch (Exception e)
//            {
//                Utility.DEBUG_LOG(TAG,"Unable to read maintenanceThreshold: "+e.getMessage().toString());
//            }
//            Utility.DEBUG_LOG(TAG,"maintenanceThresholdStr:"+maintenanceThresholdStr);
//            if ( maintenanceThresholdStr == null )
//            {
//                int maintenanceThreshold = 125;
//                Utility.DEBUG_LOG(TAG,"maintenanceThreshold 1:"+maintenanceThreshold);
//                MAINTENANCE_THRESHOLD = Integer.toString(maintenanceThreshold);
//            }
//            else
//            {
//                int maintenanceThreshold = Integer.parseInt(maintenanceThresholdStr);
//                Utility.DEBUG_LOG(TAG,"maintenanceThreshold 2:"+maintenanceThreshold);
//                MAINTENANCE_THRESHOLD = Integer.toString(maintenanceThreshold);
//            }

            int maintenanceThreshold = 0;
            maintenanceThreshold = SharedPref.read("maintenanceThreshold",0);
            Utility.DEBUG_LOG(TAG,"maintenanceThreshold:"+maintenanceThreshold);
            MAINTENANCE_THRESHOLD = Integer.toString(maintenanceThreshold);
            SharedPref.write("maintenanceThreshold",maintenanceThreshold);

            Utility.DEBUG_LOG(TAG,"- getTerminalConfiguration -");

        }
        catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("CheckResult")
    public void deleteBatch() {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).transactionDetailDao().nukeTable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {

                        }// completed with success
                        , throwable -> {

                        }// there was an error
                );
    }

//    @SuppressLint("CheckResult")
//    public final TransactionDetail lastReceiptPrint(){
//      TransactionDetail Lasttransaction;
//
//        AppDatabase.getAppDatabase(context.get()).transactionDetailDao().lastReceipt()
//                .subscribeOn(Schedulers.newThread())
//                .subscribe(transactionDetail ->{ Lasttransaction=transactionDetail
//                });
//        return Lasttransaction;
//    }

    @SuppressLint("CheckResult")
    public void updateTransactionTip(String tipAmount, String invoice, String txnType, String amount,boolean isAdvice, int tipNumAdjustCounter, String ProcCode) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).transactionDetailDao().updateTip(tipAmount,invoice,txnType,amount,isAdvice,tipNumAdjustCounter,ProcCode))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ }// completed with success
                        ,throwable ->{}// there was an error
                );
    }

/*
      method to delete all terminal configuration.
 */
    @SuppressLint("CheckResult")
    public void emptyTableTerminalConfig(){
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).terminalConfigDao().nukeTable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                            Utility.DEBUG_LOG(TAG," table cleared");
                        }// completed with success
                        ,throwable ->{}// there was an error
                );
    }


    @SuppressLint("CheckResult")
    public TransactionDetail findByInvoiceNumber(String s){
        try {
            AppDatabase.getAppDatabase(context.get()).transactionDetailDao().findByInvoiceNoAdjust(s)
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(transactionDetail -> transaction = transactionDetail);
        }
        catch (Exception e){
        }
        return  transaction;
    }
    @SuppressLint("CheckResult")
    public TransactionDetail findByAuthNumber(String s){
        try {
            AppDatabase.getAppDatabase(context.get()).transactionDetailDao().findByAuthNo(s)
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(transactionDetail -> transaction = transactionDetail);
        }
        catch (Exception e){
        }
        return  transaction;
    }
    @SuppressLint("CheckResult")
    public TransactionDetail findByInvoiceNoAdjust( String s){
        try {
            AppDatabase.getAppDatabase(context.get()).transactionDetailDao().findByInvoiceNoAdjust(s)
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(transactionDetail -> transaction = transactionDetail);
        }
        catch (Exception e){
        }
        return  transaction;
    }

    @SuppressLint("CheckResult")
    public void updateTransaction(String transactionAmount, String invoice, String txnType, String amount,boolean isAdvice) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).transactionDetailDao().updateAmount(transactionAmount,invoice,txnType,amount,isAdvice))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ }// completed with success
                        ,throwable ->{}// there was an error
                );
    }





    public List<TerminalConfig> getAllTransactions() throws ExecutionException, InterruptedException {
        return new GetTransactionsAsyncTask().execute().get();
    }
    @SuppressLint("StaticFieldLeak")
    public class GetTransactionsAsyncTask extends AsyncTask<Void, Void,List<TerminalConfig>>
    {

        @Override
        protected List<TerminalConfig> doInBackground(Void... voids) {
            return AppDatabase.getAppDatabase(context.get()).terminalConfigDao().terminalConfiguration();
        }
    }

    //////////////////////////////////terminal config///////////////////////////////////////////////



                                    ///profileInit////


}
