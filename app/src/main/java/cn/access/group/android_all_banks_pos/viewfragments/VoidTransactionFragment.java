package cn.access.group.android_all_banks_pos.viewfragments;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.adapter.MyVoidTransactionRecyclerViewAdapter;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.contracts.VoidContract;
import cn.access.group.android_all_banks_pos.presenter.VoidTransactionPresenter;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class VoidTransactionFragment extends Fragment  implements TransactionContract , VoidContract {

    Unbinder unbinder;
    IBeeper iBeeper;
    IPrinter printer;
    public static IDeviceService idevice;
    TransactionDetail transactionDetail;
    TransactionDetail transactionDetail2 = new TransactionDetail();
    String password;
    VoidTransactionPresenter voidTransactionPresenter;
    private static final String TAG = "EMVDemo";
    Intent intent = new Intent();
    boolean isSucc;
    AssetManager assetManager;
    @BindView(R.id.list)
    RecyclerView recyclerView;
    View view;
    ReceiptPrinter receiptPrinter;
    boolean isButtonClicked = false;
    private WeakReference<Context>  mContext;
    //TransPrinter transPrinter; // SHIVAM UPDATE HERE FOR TEXT PRINTER
    public VoidTransactionFragment() {
        receiptPrinter = new ReceiptPrinter(handler);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("CheckResult")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_voidtransaction_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        assetManager = getResources().getAssets();
        // Set the adapter
        if (view instanceof RecyclerView) {
            //Context context = view.mContext;
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext.get()));
            AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().getAllTransactionsLive()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::setRecycleViewData);
        }
        //initCountDownTimer(view);
        return view;
    }

    // masood count down timer on payment success
//    public void initCountDownTimer(View v) {
//        new CountDownTimer(30000, 1000) {
//            public void onTick(long millisUntilFinished) {
////                if(isButtonClicked) {
////                    cancel();
////                }
//            }
//            public void onFinish() {
//                DashboardContainer.backStack();
//                //DialogUtil.hideKeyboard(getActivity());
//                Utility.DEBUG_LOG("timer", "working");
//            }
//
//        }.start();
//    }
    private void setRecycleViewData(List<TransactionDetail> transactionDetails){
        if(recyclerView!=null) {
            MyVoidTransactionRecyclerViewAdapter adapter = new MyVoidTransactionRecyclerViewAdapter(transactionDetails, mListener);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Pass your adapter interface in the constructor
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);

        mContext = new WeakReference<>(context);
    }

    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private final ServiceConnection conn = new ServiceConnection() {
        @SuppressLint("CheckResult")
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            idevice = IDeviceService.Stub.asInterface(service);
            try {
                iBeeper = idevice.getBeeper();
                printer = idevice.getPrinter();
               // Constants.IPRINTER = printer;
                TransPrinter.initialize(printer);
                TransactionDetail defaultDetail= new TransactionDetail();
                defaultDetail.setEcrRefNo("-1");
                if(Constants.ecrType.equals("M") && Constants.isEcrReq) {
                    AppDatabase.getAppDatabase(getContext()).transactionDetailDao().findByEcrRefNo(Constants.ecrRefNo)
                            .subscribeOn(Schedulers.newThread())
                            .defaultIfEmpty(defaultDetail)
                            .subscribe(transactionDetail -> {
                                if (!transactionDetail.getEcrRefNo().equals("-1")) {
//                                if (transactionDetail.getEcrRefNo().equals(Constants.ecrRefNo)) {
                                    voidTransactionPresenter = new VoidTransactionPresenter(VoidTransactionFragment.this,
                                            VoidTransactionFragment.this, handler, isSucc, assetManager, printer,
                                            transactionDetail, mContext.get());
                                    voidTransactionPresenter.doVoid();
                                   // Constants.isEcrReq = false;
                                } else {
                                    transactionFailed("VOID", "No Transaction Found");
                                    DashboardContainer.switchFragment(new HomeFragment(), "home");

                                }
                            });
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            //Toast.makeText(mContext, "bind service success", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        unbinder.unbind();
        if (getActivity() != null) {
            getActivity().unbindService(conn);
        }
        hideProgress();
    }

    // log & display handler
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG,"before getData");
            Utility.DEBUG_LOG(TAG, msg.getData().getString("msg"));
//            Toast.makeText(mContext, msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();

        }
    };
    private MyVoidTransactionRecyclerViewAdapter.OnListFragmentInteractionListener mListener = item -> {
        transactionDetail = item;
        DialogUtil.showKey(getActivity());
        DialogUtil.inputDialog(mContext.get(),"Input Password","Enter User Password", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                EditText text= sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                password =   text.getText().toString();
                if(password.contentEquals(Constants.MANAGER_PASSWORD_CHANGE)){
                    //show confirmation
                    voidTransactionPresenter = new VoidTransactionPresenter(VoidTransactionFragment.this,VoidTransactionFragment.this,handler,isSucc,assetManager,printer,transactionDetail,mContext.get());
                    isButtonClicked = true;
                    sweetAlertDialog.dismissWithAnimation();
                    DialogUtil.confirmDialog(mContext.get(), "Void it", sweetAlertDialog1 -> {
                        voidTransactionPresenter.doVoid();
                        sweetAlertDialog1.dismissWithAnimation();
                    });
                }else{
                    DialogUtil.errorDialog(mContext.get(),"Error!","Invalid Password");
                    sweetAlertDialog.dismissWithAnimation();
                }
            }
        },InputType.TYPE_CLASS_NUMBER, PasswordTransformationMethod.getInstance());

    };
    @Override
    public void askConfirmation() {
        DialogUtil.confirmDialog(mContext.get(), "Are you Sure ??", sweetAlertDialog -> {
            voidTransactionPresenter.doVoid();
            sweetAlertDialog.dismissWithAnimation();
        });
    }

    @Override
    public void toastShow(String str) {
        // getActivity().runOnUiThread(() -> Toast.makeText(mContext.get(), str, Toast.LENGTH_SHORT).show());

    }
    @Override
    public void showProgress(String title) {
        if(getActivity()!=null) {
            getActivity().runOnUiThread(() -> MainApplication.showProgressDialog(mContext.get(), title));
        }
        else{
            Utility.DEBUG_LOG("showProgress","error");
        }

    }
    @Override
    public void hideProgress() {
        if(getActivity()!=null) {
            getActivity().runOnUiThread(MainApplication::hideProgressDialog);
        }else{
                Utility.DEBUG_LOG("showProgress","error");
            }
    }
    @Override
    public void transactionFailed(String title,String error) {
        if(getActivity()!=null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.errorDialog(mContext.get(), title, error);
                }
            });
        }
        else{
            Utility.DEBUG_LOG("transactionFailed","error");
        }

    }
    @Override
    public void transactionSuccess(String content) {
        if(getActivity()!=null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.successDialog(mContext.get(), content);
                }
            });
        }
        else{
            Utility.DEBUG_LOG("transactionSuccess","error");
        }
    }

    @Override
    public void transactionSuccessCustomer(String content, TransactionDetail transactionDetail, String copy, String txnType, String balance, boolean redeemCheck) {
        if(getActivity()!=null) {
            Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                    }
                    catch (Exception e)
                    {
                        Utility.DEBUG_LOG(TAG,"Exception: "+e.getMessage().toString());
                    }
                    Utility.DEBUG_LOG(TAG,"before showing dialog of customer copy");
                    // SHIVAM UPDATE HERE FOR TEXT PRINTER
                    DialogUtil.confirmDialogCustomer(mContext.get(), content, "Print Customer Copy", "", sweetAlertDialog1 -> {
//                        transPrinter = new PrintRecpSale(mContext.get());
//                        transPrinter.initializeData(transactionDetail,balance,false, copy);
//                        transPrinter.print();

                        receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, transactionDetail.getAidCode(), "CUSTOMER COPY", txnType, "", balance,redeemCheck);
                        sweetAlertDialog1.dismissWithAnimation();
                        // SHIVAM UPDATE HERE FOR TEXT PRINTER


                        DashboardContainer.backStack();
                        sweetAlertDialog1.dismissWithAnimation();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            Constants.BANK_COPY = true;
//                            if(Constants.BANK_COPY) {
//                                DialogUtil.confirmDialogCustomer(mContext.get(), "BANK COPY","Print Bank Copy","", sweetAlertDialog1 -> {
//                                    receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "BANK COPY", txnType);
//                                    sweetAlertDialog1.dismissWithAnimation();
//                                    DashboardContainer.backStack();
//                                });
//                            }
//                            Constants.BANK_COPY=false;
//                        }
//                    }, 1000);
                    });


                }
            });
        } else{
            Utility.DEBUG_LOG("transactionSuccessCus","error");
        }
    }


    @Override
    public void rocEmpty(String content, TransactionDetail transactionDetail, String copy, String txnType) {

    }

    @Override
    public boolean last4Digits(String text) {
        return false;
    }

    @Override
    public boolean last4Digits2(TransactionDetail transactionDetail, String text) {
        return false;
    }

    @Override
    public boolean panManualEntry(TransactionDetail transactionDetail, String text){
        return false;
    }

    @SuppressLint("CheckResult")
    public TransactionDetail findByECRRefNumber( ){
        try {
            AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().findByEcrRefNo(Constants.ecrRefNo)
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(transactionDetail -> transactionDetail2 = transactionDetail);
            //Constants.ecrRefNo="0";

        }
        catch (Exception e){
        }
        return  transactionDetail2;
    }

}
