package cn.access.group.android_all_banks_pos.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cn.access.group.android_all_banks_pos.repository.model.ProfileInit;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
@Dao
public interface ProfileInitDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConfig(ProfileInit profileInit);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertManyConfig(List<ProfileInit> profileInits);

    @Query("SELECT * FROM profileinit ")
    List<ProfileInit> terminalConfiguration();

    @Query("SELECT * FROM profileinit WHERE tcid LIKE  :tcid ")
    ProfileInit terminalConfigurationByID(int tcid);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateConfiguration(ProfileInit profileInit);

    @Query("DELETE FROM profileinit")
    void nukeTable();
}
