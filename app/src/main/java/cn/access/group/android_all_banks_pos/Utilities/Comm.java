package cn.access.group.android_all_banks_pos.Utilities;

import android.content.Context;
import android.util.Log;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.Socket;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.SSL_ENABLE;

/**
 * Created by Humayun on 2019/9/23.
 */

public class Comm {

    private static final String TAG = "EMVDemo-Comm";
    String ip;
    int port;
    private WeakReference<Context> context;
    SSLComm sslComm;
    int ConnectionCount = 0;
    Boolean rv = false;
    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private int status;


    public Comm() {
        status = 0;
        ip = "";
        port = 0;
        outputStream = null;
        inputStream = null;
    }

    public Comm(String ip, int port, Context context) {
        status = 0;
        this.ip = ip;
        this.port = port;
        outputStream = null;
        inputStream = null;
        this.context = new WeakReference<>(context);
        sslComm = new SSLComm(context, ip, port);
        ConnectionCount = 0;
    }


    public boolean connect(String ip, int port) throws ConnectException {

        this.ip = ip;
        this.port = port;

        return connect();

    }

    public boolean connect() {

        ConnectionCount++;
        //Utility.DEBUG_LOG(TAG, "SSL:"+ String.valueOf(IS_SSL));
        if (SSL_ENABLE.equals("Y")) {
            Utility.DEBUG_LOG(TAG, "SSL connect");

//            SSLClient sslClient = new SSLClient(sslComm, SSLClient.ACTION.connect);
//            if (null != sslClient) {
//                sslClient.start();
//            }
            if (ConnectionCount < 2) {
                rv = sslComm.connect();
            }
            status = 1;
            Utility.DEBUG_LOG("Comm", String.valueOf(rv));
            return rv;
        } else {

            if (status > 0) {
                if ((this.ip == ip) && this.port == port) {
                    return true;
                } else {
                    disconnect();
                }
            }
            try {
                socket = new Socket(ip, port);
                if (null == socket) {
                    return false;
                }
                this.ip = ip;
                this.port = port;
                status = 1;
                return true;
            } catch (IOException e) {
                Utility.DEBUG_LOG(TAG, "Exception: " + e.getMessage().toString());
                e.printStackTrace();
            }
            return false;
        }
    }


    public int send(byte[] data) {
        if (SSL_ENABLE.equals("Y")) {
            return sslComm.send(data);
        } else {
            if (status <= 0) {
                return 0;
            }

            Utility.DEBUG_LOG(TAG, "SEND:");
            Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(data));

            try {
                outputStream = socket.getOutputStream();
                if (null == outputStream) {
                    return 0;
                }

                outputStream.write(data);
                outputStream.flush();
                return data.length;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }


    public byte[] receive(int wantLength, int timeoutSecond) throws IOException {
        if (SSL_ENABLE.equals("Y")) {
            return sslComm.receive(wantLength, timeoutSecond);
        } else {

            if (status <= 0) {
                return null;
            }

            Utility.DEBUG_LOG(TAG, "revieve:");
            socket.setSoTimeout(timeoutSecond * 1000);
            inputStream = socket.getInputStream();
//
//
            Utility.DEBUG_LOG(TAG, "revieve:");
//
            if (null == inputStream) {
                return null;
            }
            byte[] tmp = new byte[wantLength];
            int recvLen = inputStream.read(tmp);
            if (recvLen > 0) {
                byte[] ret = new byte[recvLen];
                System.arraycopy(tmp, 0, ret, 0, recvLen);
                Utility.DEBUG_LOG(TAG, "SSL ret:" + String.valueOf(ret));
                return ret;
            } else if (recvLen == 0) {
                return null;
            }

            return null;
        }
    }

    public void disconnect() {
        //sslComm.close();
        status = 0;
        try {
            if (null != inputStream) {
                inputStream.close();
                inputStream = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (null != outputStream) {
                outputStream.close();
                outputStream = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (null != socket) {
                socket.close();
                socket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        status = 0;
    }
}
