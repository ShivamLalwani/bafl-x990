
package cn.access.group.android_all_banks_pos.viewfragments;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DecimalDigitsInputFilter;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.FourDigitCardFormatWatcher;
import cn.access.group.android_all_banks_pos.Utilities.NumberTextWatcherForThousand;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Validation;
import cn.access.group.android_all_banks_pos.adapter.aidListAdapter;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.CompletionPresenter;
import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.dao.TransactionDetailDao;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_NA;

/**
 * @author muhammad.humayun
 * on 8/20/2019.
 * this is a view call which is reponsible for starting the hardware service and invoking the presenter of this view
 */
@SuppressLint("ValidFragment")
public class CompletionAdviceFragment extends Fragment implements TransactionContract,PaymentContract {

    @BindView(R.id.headingAdjustTip)
    TextView headingAdjustTip;

    @BindView(R.id.textLayoutAdjustTip)
    LinearLayout textLayoutAdjustTip;

    @BindView(R.id.textScreenAdjustTip)
    TextView textScreenAdjustTip;

    @BindView(R.id.amountFieldAdjustTip)
    TextView amountFieldAdjustTip;

    @BindView(R.id.keyboard_layout_Adjust_tip)
    LinearLayout keyboard_layout_Adjust_tip;

    @BindView(R.id.tipAdjustList)
    TableLayout tipAdjustList;

    @BindView(R.id.tipAdjustCardNo)
    TextView tipAdjustCardNo;

    @BindView(R.id.tipAdjustInvoiceNo)
    TextView tipAdjustInvoiceNo;

    @BindView(R.id.tipAdjustTxnType)
    TextView tipAdjustTxnType;

    @BindView(R.id.tipAdjustAmount)
    TextView tipAdjustAmount;

    @BindView(R.id.tipAdjustTipAmount)
    TextView tipAdjustTipAmount;


    @BindView(R.id.tipAdjustAuthID)
    TextView tipAdjustAuthID;

    @BindView(R.id.tipAdjustButton)
    Button tipAdjustButton;

    @BindView(R.id.tipAdjustCancelButton)
    Button tipAdjustCancelButton;

    @BindView(R.id.cardBoxLayout)
    ConstraintLayout cardBoxLayout;


    @Override
    public void toastShow(String str) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (str.contentEquals("show")) {
                    } else {
                        Message msg = new Message();
                        msg.getData().putString("msg", str);
                        handler.sendMessage(msg);
                        // Toast.makeText(getContext(), str, Toast.LENGTH_SHORT).show();
                        Utility.DEBUG_LOG(TAG, str);
                    }
                }
            });
        }
        else{
            Utility.DEBUG_LOG("showProgress","Error Dialog null");
        }
    }

    @Override
    public void showProgress(String title) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.showProgressDialog(mContext.get(), title);
                }
            });
        }
        else{
            Utility.DEBUG_LOG("showProgress","Error Dialog null");
        }

    }

    @Override
    public void hideProgress() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.hideProgressDialog();
                }
            });
        }else{
            Utility.DEBUG_LOG("hideProgress","Error Dialog null");
        }
    }

    @Override
    public void showPaymentSuccessDialog(TransactionDetail transactionDetail) {
        DialogPaymentSuccessFragment newFragment = new DialogPaymentSuccessFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("transactionDetail", transactionDetail);
        newFragment.setArguments(bundle);
        DashboardContainer.switchFragmentWithBackStack(newFragment, "success fragment");
    }

    @Override
    public void showMultipleAid(List<String> aidList) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final aidListAdapter myadapter = new aidListAdapter(mContext.get(), R.layout.row_item, aidList);
                    aidListView.setAdapter(myadapter);
                    aidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            //PASS POSITION TO PRESENTER
                            position = position + 1;
                            completionPresenter.selectedAidIndex(position);
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
            });
        }
        else{
            Utility.DEBUG_LOG("showMultipleAid","Error Dialog null");
        }
    }

    @Override
    public void transactionFailed(String title, String error) {
        if(getActivity() != null ) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.errorDialog(mContext.get(), title, error);
                }
            });
        }
        else{
            Utility.DEBUG_LOG("transactionFailed","Error Dialog null");
        }
    }

    @Override
    public void rocEmpty(String content, TransactionDetail transactionDetail, String copy, String txnType){
//        getActivity().runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                DialogUtil.confirmDialogCustomer(getContext(), content, sweetAlertDialog1 -> {
//                    receiptPrinter.printSaleReceipt(printer, assetManager,transactionDetail , "aid",copy,txnType);
//                    sweetAlertDialog1.dismissWithAnimation();
//                    SharedPref.countedTransactionWrite(getContext(),null);
//                    SharedPref.writeList(getContext(),null);
//                });
//            }
//        });
    }

    @Override
    public boolean last4Digits(String text1) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.inputDialog(mContext.get(), "Enter Last 4 Digits", "Enter Last 4 Digits", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                            //show confirmation
                            if (text1.equals(text.getText().toString())) {
                                bool = true;
                                sweetAlertDialog.dismissWithAnimation();
                            } else {
                                bool = false;
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        }
                    }, InputType.TYPE_CLASS_NUMBER, null);

                }
            });
            return bool;
        }
        else{
            Utility.DEBUG_LOG("Last$didgits", String.valueOf(bool));
            return bool;

        }
    }

    @Override
    public boolean last4Digits2(TransactionDetail transactionDetail, String text) {
        return false;
    }

    @Override
    public boolean panManualEntry(TransactionDetail transactionDetail, String text){
        return false;
    }

    @Override
    public void transactionSuccess(String content) {

    }

    @Override
    public void transactionSuccessCustomer(String content, TransactionDetail transactionDetail,String copy, String txnType, String balance, boolean redeemCheck) {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (!(txnType.equals("ORBIT INQUIRY"))) {
                    try {
                        Thread.sleep(3000);
                    }
                    catch (Exception e)
                    {
                        Utility.DEBUG_LOG(TAG,"Exception: "+e.getMessage().toString());
                    }
                    Utility.DEBUG_LOG(TAG,"before showing dialog of customer copy");
                    DialogUtil.confirmDialogCustomer(mContext.get(), content, "Print Customer Copy", "", sweetAlertDialog1 -> { // SHIVAM UPDATE HERE FOR TEXT PRINTER
                        receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "CUSTOMER COPY", txnType, "", balance,redeemCheck); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                        sweetAlertDialog1.dismissWithAnimation();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                        SharedPref.countedTransactionWrite(getContext(),null);
//                        SharedPref.writeList(getContext(),null);
//                        Constants.BANK_COPY = true;
//                            if(Constants.BANK_COPY) {
//                                DialogUtil.confirmDialogCustomer(getContext(), content, "Print Bank Copy", "", sweetAlertDialog1 -> {
//                                    receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "Bank Copy", txnType);
//                                    sweetAlertDialog1.dismissWithAnimation();
//                                });
//                            }
//                            Constants.BANK_COPY=false;
//                        }
//                    }, 1000);
                    });


                }
                else{
                    receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "CUSTOMER COPY", txnType, "", balance,redeemCheck); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                }
            }
        });
    }

    enum ReaderType { RT_Chip, RT_Mag, RT_CTLS, RT_Chip_Mag, RT_All };




    String transactionAmount;
    TransactionDetail td;

    CompletionPresenter completionPresenter;
    Unbinder unbinder;
    IEMV iemv;
    IPinpad ipinpad;
    IBeeper iBeeper;
    IPrinter printer;
    private ReceiptPrinter receiptPrinter;
    public static IDeviceService idevice;

    IRFCardReader irfCardReader;
    IDeviceInfo iDeviceInfo;

    boolean isSucc;
    AssetManager assetManager;
    ISerialPort serialPort;
    private static final String TAG = "CompAdviceFrag";
    Animation animSlideRight;
    Intent intent = new Intent();
    Dialog alertDialog;
    ListView aidListView;
    String buttonClicked;
    public String current = "";
    public boolean bool;
    MyKeyListener myKeyListener;
    private WeakReference<Context>  mContext;
    PaymentAmountPresenter.FallbackOrignatorType fallbackOrignatorType = FT_NA;
    String amount = "";
    String authId = "";

    @SuppressLint("ValidFragment")
    public CompletionAdviceFragment(TransactionDetail td , String authId) {
        this.authId = authId;
        this.td = td;
        receiptPrinter = new ReceiptPrinter(handler);
    }


    @SuppressLint("ValidFragment")
    public CompletionAdviceFragment(PaymentAmountPresenter.FallbackOrignatorType fallbackType,TransactionDetail td , String authId) {
        Utility.DEBUG_LOG(TAG,"+ CompletionAdviceFragment(FallbackType) +");

        this.authId = authId;
        this.td = td;

        // Required empty public constructor
        receiptPrinter = new ReceiptPrinter(handler);
        this.fallbackOrignatorType = fallbackType;

        Utility.DEBUG_LOG(TAG,"fallbackType: "+ fallbackType );

    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.DEBUG_LOG(TAG,"+ FragmentTipAdjust:onCreateView() +");
        //Repository repo = new Repository(AppDatabase.getAppDatabase(Context));
        View rootView = inflater.inflate(R.layout.fragment_completion_advise, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        cardBoxLayout =  (ConstraintLayout) rootView.findViewById(R.id.cardBoxLayout);
        tipAdjustCardNo.setText(DataFormatterUtil.maskCardNo(td.getCardNo()));
        tipAdjustInvoiceNo.setText(td.getInvoiceNo());
        tipAdjustTxnType.setText(td.getTxnType());
        tipAdjustAuthID.setText(td.getAuthorizationIdentificationResponseCode());
        tipAdjustAmount.setText((new DecimalFormat("0.00").format(Double.parseDouble(td.getAmount()))));
        if(Constants.TIP_ENABLED.equals("Y")){
            tipAdjustTipAmount.setVisibility(View.VISIBLE);
            tipAdjustTipAmount.setText(td.getTipAmount());
        }



        //keyboard_layout_Adjust_tip.setVisibility(View.VISIBLE);
        myKeyListener = new MyKeyListener(amountFieldAdjustTip);
        rootView.findViewById(R.id.key1).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key2).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key3).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key4).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key5).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key6).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key7).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key8).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key9).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key0).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.keyclr).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key_delete).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key_confirm).setOnClickListener(myKeyListener);
        return rootView;
    }


    @OnClick({R.id.tipAdjustButton,R.id.tipAdjustCancelButton})
    @Nullable

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tipAdjustButton:
               // DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "COMPLETION FRAGMENT");
                tipAdjustList.setVisibility(View.GONE);
                tipAdjustCancelButton.setVisibility(View.GONE);
                tipAdjustButton.setVisibility(View.GONE);
                keyboard_layout_Adjust_tip.setVisibility(View.VISIBLE);
                textLayoutAdjustTip.setVisibility(View.VISIBLE);
                headingAdjustTip.setText("Enter Amount");
                break;

            case R.id.tipAdjustCancelButton:
                DashboardContainer.backStack();;
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        assetManager = getResources().getAssets();
        alertDialog=new Dialog(context);
        mContext = new WeakReference<>(context);
        Utility.DEBUG_LOG(TAG,"PaymentAmountFragment:onAttach()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null)
            getActivity().unbindService(conn);
    }


    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            new CountDownTimer(01, 01) {
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);
                    try {
                        Utility.DEBUG_LOG("onServiceConnected","onServiceConnected--Control here..");
                        Utility.DEBUG_LOG("terminal serial", Constants.TERMINAL_SERIAL );
                        ipinpad = idevice.getPinpad(1);
                        iBeeper = idevice.getBeeper();
                        printer = idevice.getPrinter();
                        irfCardReader = idevice.getRFCardReader();
                        iDeviceInfo = idevice.getDeviceInfo();

                        iemv = idevice.getEMV();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }

                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    /**
     * this method animates the view, and executes the purchase function of the presenter which takes amount as parameter
     */

    // mmasood: countdown timer in sales screen


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        buttonClicked = "";
        // conn = null;
        unbinder.unbind();

    }

    private void animationsFlow() {
        Animation animSlideLeft = AnimationUtils.loadAnimation(mContext.get(), R.anim.slide_out_left);
        keyboard_layout_Adjust_tip.startAnimation(animSlideLeft);
        animSlideRight = AnimationUtils.loadAnimation(mContext.get(), R.anim.slide_in_faster);
        animSlideLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                headingAdjustTip.setText("TAP / INSERT / SWIPE");
                textScreenAdjustTip.setVisibility(View.GONE);
                amountFieldAdjustTip.setVisibility(View.GONE);
                keyboard_layout_Adjust_tip.setVisibility(View.GONE);
                cardBoxLayout.startAnimation(animSlideRight);
                cardBoxLayout.setVisibility(View.VISIBLE);

//                    if ( isFallback )
//                    {
//                        Utility.DEBUG_LOG(TAG,"before doPurchase 4 fallback");
//                        completionPresenter.doPurchase(amount,"0", true);
//                    }
//                    else
//                    {
//                        Utility.DEBUG_LOG("CompletionFragment",amount);
//                        Utility.DEBUG_LOG(TAG,"before doPurchase 3");
//                        completionPresenter.doPurchase(amount,"0");
//                    }
                Utility.DEBUG_LOG(TAG,"amount [completion]:"+amount);
                if ( fallbackOrignatorType != FT_NA )
                {
                    Utility.DEBUG_LOG(TAG,"before doPurchase 4 fallback from [completion]:" + fallbackOrignatorType);
                    completionPresenter.doPurchase(amount,"0", fallbackOrignatorType);
                }
                else
                {
                    Utility.DEBUG_LOG(TAG,"before completionPresenter.doPurchase completionAdvice");
                    completionPresenter.doPurchase(amount,"0");
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Utility.DEBUG_LOG(TAG,"+ PaymentFragment:Handler:handleMessage()+ ");
            Utility.DEBUG_LOG(TAG,"before getData");

            String str = msg.getData().toString();
            Utility.DEBUG_LOG(TAG,"Message:" + str);
            super.handleMessage(msg);
//            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };

    class MyKeyListener implements View.OnClickListener{
        StringBuilder stringBuilder = new StringBuilder("");
        TextView selectedAmount;

        public MyKeyListener(TextView selectedAmount) {
            this.selectedAmount = selectedAmount;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.key0:
                    if (stringBuilder.length() >= 0 & stringBuilder.length() < 6)
                    {
                        stringBuilder.append("0");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key1:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("1");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key2:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("2");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key3:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("3");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key4:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("4");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key5:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("5");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key6:
                    if (stringBuilder.length() < 6){
                        stringBuilder.append("6");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key7:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("7");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key8:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("8");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key9:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("9");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.keyclr:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.delete(0,stringBuilder.length());
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_delete:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_confirm:


                    amount = amountFieldAdjustTip.getText().toString();
                    if(Constants.TIP_ENABLED.equalsIgnoreCase("Y")){
                        //double tipamount = Double.parseDouble(tipAmount);
//                        double txnAmount = Double.parseDouble(td.getTransactionAmount());
//                        Utility.DEBUG_LOG("COMPLETION", String.valueOf(tipamount));
//                        Utility.DEBUG_LOG("COMPLETION", String.valueOf(txnAmount));
//                        double totalAmount =tipamount + txnAmount;
//                        Utility.DEBUG_LOG("COMPLETION", String.valueOf(totalAmount));
//                        td.setAmount(String.valueOf(totalAmount));
                    }
                    else{
                       td.setAmount(amount);
                    }

                    td.setCancelTxnType(td.getTxnType());
                    td.setTxnType("COMPLETION");
                    td.setIsAdvice(true);
                    //updateTransaction(appDatabase,authId, td.getTxnType(), td.getAmount(),td.getIsAdvice() );
                    completionPresenter = new CompletionPresenter(CompletionAdviceFragment.this, CompletionAdviceFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer,transactionAmount,"sale",mContext.get(),serialPort,td);
                    animationsFlow();
                    //keyboard_layout_Adjust_tip.setVisibility(View.VISIBLE);



//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            receiptPrinter.printSaleReceipt(printer,assetManager,td,td.getAidCode(),"MERCAHNT COPY",td.getTxnType(),"","",false);
//                            Utility.DEBUG_LOG("transaction","found");
//                            DialogUtil.confirmDialogCustomer(mContext.get(), "Customer Copy", "Print Customer Copy", "", sweetAlertDialog1 -> {
//                                receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "Customer Copy", td.getTxnType(),"","",false);
//                                sweetAlertDialog1.dismissWithAnimation();
//                                DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
//                            });
//                            Utility.DEBUG_LOG("Handler", "Running Handler");
//                        }
//                    }, 1000);
                    break;
            }
            if (stringBuilder.length() > 0) {
                double num = Double.parseDouble(stringBuilder.toString());
                selectedAmount.setText(big2(num / 100));
            } else {
                selectedAmount.setText("0.00");
            }
        }
        private String big2(double d) {
            DecimalFormat format = new DecimalFormat("0.00");
            return format.format(d);
        }
    }

    public void setTransactionAmount(String transactionAmount)
    {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionAmount()
    {
        return transactionAmount;
    }

//    public boolean getIsFallback()
//    {
//        return isFallback;
//    }

//    public List<TransactionDetail> getAllTransactions() throws ExecutionException, InterruptedException {
//        return new GetTransactionsAsyncTaskAdjust().execute().get();
//    }
//    @SuppressLint("StaticFieldLeak")
//    private class GetTransactionsAsyncTaskAdjust extends AsyncTask<Void, Void,List<TransactionDetail>>
//    {
//        @Override
//        protected List<TransactionDetail> doInBackground(Void... voids) {
//            return appDatabase.transactionDetailDao().getAllTransactions();
//        }
//    }

//    @SuppressLint("CheckResult")
//    private static void updateTransaction(final AppDatabase db, String authId, String txnType, String amount,String isAdvice) {
//        Completable.fromAction(() -> db.transactionDetailDao().updateCompletion(authId,txnType,amount,isAdvice))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(()->{ }// completed with success
//                        ,throwable ->{}// there was an error
//                );
//    }



}



