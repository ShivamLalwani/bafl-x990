package cn.access.group.android_all_banks_pos.repository.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AMEX_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLEMENT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLETIME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CHECK_EXPIRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_ETHERNET;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_GPRS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CURRENCY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ENABLE_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FALLBACK_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_3;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_4;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HIGH_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_ICC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_MAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LOW_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANAGER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY_ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_NAME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MOD10;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MSGHEADER_LENHEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.NII;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PIN_BYPASS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PRI_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SSL_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SUPER_USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SYSTEM_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_THRESHOLD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TOPUP_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostIP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostPort;

/**
 * on 10/29/2019.
 */
@Entity(tableName = "CardRanges")
public class CardRanges implements Serializable {
    @PrimaryKey()
    private int crid;
    @ColumnInfo(name = "CardTypeName")
    private String CardTypeName;
    @ColumnInfo(name = "LowerRange")
    private int LowerRange;
    @ColumnInfo(name = "HigherRange")
    private int HigherRange;
    @ColumnInfo(name = "CardLenght")
    private String CardLenght;
    @ColumnInfo(name = "CardIssuer")
    private String CardIssuer;

    public CardRanges(){

    }
    public CardRanges(int crid, String cardTypeName, int lowerRange, int higherRange, String cardLenght, String cardIssuer) {
        this.crid = crid;
        this.CardTypeName = cardTypeName;
        this.LowerRange = lowerRange;
        this.HigherRange = higherRange;
        this.CardLenght = cardLenght;
        this.CardIssuer = cardIssuer;
    }


    public int getCrid() {
        return crid;
    }

    public void setCrid(int crid) {
        this.crid = crid;
    }

    public String getCardTypeName() {
        return CardTypeName;
    }

    public void setCardTypeName(String cardTypeName) {
        CardTypeName = cardTypeName;
    }

    public int getLowerRange() {
        return LowerRange;
    }

    public void setLowerRange(int lowerRange) {
        LowerRange = lowerRange;
    }

    public int getHigherRange() {
        return HigherRange;
    }

    public void setHigherRange(int higherRange) {
        HigherRange = higherRange;
    }

    public String getCardLenght() {
        return CardLenght;
    }

    public void setCardLenght(String cardLenght) {
        CardLenght = cardLenght;
    }

    public String getCardIssuer() {
        return CardIssuer;
    }

    public void setCardIssuer(String cardIssuer) {
        CardIssuer = cardIssuer;
    }


    // 0th entry in db is ubl configuration initial Single configuration
    public static CardRanges insertInitialConfig() {
        return new CardRanges(0, "VISA 1", 400000, 499999, "6", "1");
    }

    // 1st entry in db is UPI configuration initial Single configuration
   /* public static TerminalConfig insertUPIConfig(){
        return new TerminalConfig(1,"200000005000510","42017890",
                "UPI TEST HOST", "202.61.40.171","7006","444","N");// initial UPI config
    }*/

    public static List<CardRanges> cardRangesList(){
        List<CardRanges> cardConfig = new ArrayList<>();
        cardConfig.add(0,insertInitialConfig());
        cardConfig.add(1, new CardRanges(1, "MASTER 1", 222100, 272099,"6","2"));
        cardConfig.add(2, new CardRanges(2, "MASTER 2", 510000, 559999,"6","2"));
        cardConfig.add(3, new CardRanges(3, "JCB 1", 350000, 359999,"6","3"));
        cardConfig.add(4, new CardRanges(4, "JCB 2", 830000, 849999,"6","3"));
        cardConfig.add(5, new CardRanges(5, "AMEX 1", 340000, 349999,"6","4"));
        cardConfig.add(6, new CardRanges(6, "AMEX 2", 370000, 379999,"6","4"));
        cardConfig.add(7, new CardRanges(7, "UPI 1", 620000, 629999,"6","5"));
        cardConfig.add(8, new CardRanges(8, "UPI 2", 680000, 699999,"6","5"));
        cardConfig.add(9, new CardRanges(9, "UPI 3", 810000, 819999,"6","5"));
        cardConfig.add(10, new CardRanges(10, "PAYPAK 1", 220000, 229999,"6","6"));
        //terminalConfigs.add(1,insertUPIConfig());
        return cardConfig;
    }
}
