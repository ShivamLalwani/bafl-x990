package cn.access.group.android_all_banks_pos.viewfragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.FourDigitCardFormatWatcher;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.adapter.aidListAdapter;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_VISA;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MAX_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MIN_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_OFFLINE_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_Ctls;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_Dip;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_NA;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_SeeYourPhone;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_Swipe;

/**
 * @author muhammad.humayun
 * on 8/20/2019.
 * this is a view call which is reponsible for starting the hardware service and invoking the presenter of this view
 */
public class PaymentAmountFragment extends Fragment implements TransactionContract, PaymentContract {
    @BindView(R.id.amountField)
    TextView amountField;
    @BindView(R.id.tipAmountField)
    EditText tipAmountField;
    @BindView(R.id.textView12)
    TextView inputLabelAmount;
    @BindView(R.id.tipLayout)
    View tipLayout;
    @BindView(R.id.authIdField)
    EditText authIdField;
    @BindView(R.id.authIdButton)
    Button authIdButton;
    @BindView(R.id.cardBoxLayout)
    ConstraintLayout cardBoxLayout;
    @BindView(R.id.amoutButton)
    Button amountButton;
    @BindView(R.id.textLayout)
    LinearLayout textLayout;
    @BindView(R.id.authIdLayout)
    LinearLayout authIdLayout;
    @BindView(R.id.textScreen)
    TextView textScreen;
    @BindView(R.id.nextButton)
    Button nextButton;
    @BindView(R.id.keyboard_layout)
    LinearLayout keyboard_layout;
    @BindView(R.id.manualEntryLayout)
    LinearLayout manualEntryLayout;
    @BindView(R.id.manualEntryCardNumber)
    EditText manualEntryCardNumber;
    @BindView(R.id.manualEntryText)
    TextView manualEntryText;
    @BindView(R.id.imageView9)
    ImageView imageView9;
    @BindView(R.id.imageView11)
    ImageView imageView11;

    String transactionAmount;
    String tipAmount = "0.00";
    PaymentAmountPresenter paymentAmountPresenter;
    Unbinder unbinder ;
    IEMV iemv;
    IPinpad ipinpad;
    IBeeper iBeeper;
    IPrinter printer;
    private ReceiptPrinter receiptPrinter;
    public static IDeviceService idevice;
    String authNumber = "";
    IRFCardReader irfCardReader;
    boolean isSucc;
    AssetManager assetManager;
    ISerialPort serialPort;
    private static final String TAG = "PaymentAmountFrag";
    Animation animSlideRight;
    Intent intent = new Intent();
    Dialog alertDialog;
    ListView aidListView;
    String buttonClicked;
    public String current = "";
    int count = 0;
    public boolean bool;
    MyKeyListener myKeyListener;
    WeakReference<Context> mContext;
    public boolean isFallback;
    ViewGroup viewGroup;
    int confirmCount = 0;

    PaymentAmountPresenter.FallbackOrignatorType fallbackOrignatorType = FT_NA;
    IDeviceInfo iDeviceInfo;
    String saleippMonths = "";

    public void logTempValues() {
        Utility.DEBUG_LOG(TAG, "+ logTempValues +");
//        Utility.DEBUG_LOG(TAG,"CVM_LIMIT:"+CVM_LIMIT);
        Utility.DEBUG_LOG(TAG, "CVM_LIMIT_VISA:" + CVM_LIMIT_VISA);
        Utility.DEBUG_LOG(TAG, "CVM_LIMIT_MC:" + CVM_LIMIT_MC);
        Utility.DEBUG_LOG(TAG, "CVM_LIMIT_UPI:" + CVM_LIMIT_UPI);
        Utility.DEBUG_LOG(TAG, "CVM_LIMIT_PAYPAK:" + CVM_LIMIT_PAYPAK);
        Utility.DEBUG_LOG(TAG, "CVM_LIMIT_JCB:" + CVM_LIMIT_JCB);
        Utility.DEBUG_LOG(TAG, "CVM_LIMIT_AMEX:" + CVM_LIMIT_AMEX);

        Utility.DEBUG_LOG(TAG, "SALE_OFFLINE_LIMIT:" + SALE_OFFLINE_LIMIT);
        Utility.DEBUG_LOG(TAG, "SALE_MIN_AMOUNT_LIMIT:" + SALE_MIN_AMOUNT_LIMIT);
        Utility.DEBUG_LOG(TAG, "SALE_MAX_AMOUNT_LIMIT:" + SALE_MAX_AMOUNT_LIMIT);

        Utility.DEBUG_LOG(TAG, "VEPS_LIMIT:" + VEPS_LIMIT);
        Utility.DEBUG_LOG(TAG, "VEPS_LIMIT_MC:" + VEPS_LIMIT_MC);
        Utility.DEBUG_LOG(TAG, "VEPS_LIMIT_UPI:" + VEPS_LIMIT_UPI);
        Utility.DEBUG_LOG(TAG, "VEPS_LIMIT_PAYPAK:" + VEPS_LIMIT_PAYPAK);
        Utility.DEBUG_LOG(TAG, "VEPS_LIMIT_JCB:" + VEPS_LIMIT_JCB);
        Utility.DEBUG_LOG(TAG, "VEPS_LIMIT_AMEX:" + VEPS_LIMIT_AMEX);
    }

    public PaymentAmountFragment() {
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountFragment() +");
        logTempValues();
        // Required empty public constructor
        receiptPrinter = new ReceiptPrinter(handler);
        Utility.DEBUG_LOG(TAG, "imageView9:" + imageView9);
        Utility.DEBUG_LOG(TAG, "imageView10:" + imageView11);
    }

    @SuppressLint("ValidFragment")
    public PaymentAmountFragment(String authNumber) {
       this.authNumber = authNumber;
    }

    @SuppressLint("ValidFragment")
    public PaymentAmountFragment(PaymentAmountPresenter.FallbackOrignatorType fallbackType, String transactionAmount, String tipAmount) {
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountFragment(FallbackType) +");
        logTempValues();
        // Required empty public constructor
        receiptPrinter = new ReceiptPrinter(handler);
        this.fallbackOrignatorType = fallbackType;
        this.transactionAmount = transactionAmount;
        if(TIP_ENABLED.equals("Y")) {
            this.tipAmount = DataFormatterUtil.formattedAmount(tipAmount);
            Utility.DEBUG_LOG(TAG,"tipamount"+ DataFormatterUtil.formattedAmount(tipAmount));
        }
        //+ taha 23-02-2021 logic to handle zero padded amount or decimal type amount
        if (this.transactionAmount.indexOf(".") > 0)//actual amount, do nothing
        {

        } else//make it decimal type
        {
            double d = Double.parseDouble(this.transactionAmount);
            Utility.DEBUG_LOG(TAG, "d org=" + d);
            d = d / 100.0f;
            Utility.DEBUG_LOG(TAG, "d new=" + d);
            String s = String.valueOf(d);
            Utility.DEBUG_LOG(TAG, "s=" + s);
            this.transactionAmount = s;
            Utility.DEBUG_LOG(TAG, "new this.transactionAmount=" + this.transactionAmount);
        }
        Utility.DEBUG_LOG(TAG, "fallbackType: " + fallbackType + "; transactionAmount: " + transactionAmount);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountFragment:onCreateView() +");

        View rootView = inflater.inflate(R.layout.fragment_payment_amount, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        imageView9 = (ImageView) rootView.findViewById(R.id.imageView9);
        imageView11 = (ImageView) rootView.findViewById(R.id.imageView11);
        viewGroup = rootView.findViewById(android.R.id.content);
        receiptPrinter = new ReceiptPrinter(handler);
        Utility.DEBUG_LOG(TAG, "imageView9:" + imageView9);
        Utility.DEBUG_LOG(TAG, "imageView10:" + imageView11);

        if (this.isFallback) {
            imageView9.setVisibility(View.INVISIBLE);
            imageView11.setVisibility(View.INVISIBLE);
        }
        buttonClicked = String.valueOf(SharedPref.read("button_fragment", buttonClicked));
        amountButton = (Button) rootView.findViewById(R.id.amoutButton);
        cardBoxLayout = (ConstraintLayout) rootView.findViewById(R.id.cardBoxLayout);

//        amountField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
//                //Find the currently focused view, so we can grab the correct window token from it.
//                View view = getActivity().getCurrentFocus();
//                //If no view currently has focus, create a new one, just so we can grab a window token from it
//                if (view == null) {
//                }
//                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                if(actionId == EditorInfo.IME_ACTION_DONE){
//                    amountButton.setVisibility(View.GONE);
//                    isButtonClicked = true;
//                    view = new View(getActivity());
//                    transactionAmount = amountField.getText().toString();
//                    tipAmount = tipAmountField.getText().toString();
//
//                    animationsFlow();
////                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")){
////                        paymentAmountPresenter.doPurchase(transactionAmount,tipAmount);
////
////                    }else {
////                        paymentAmountPresenter.doPurchase(transactionAmount,"0");
////                    }
//
//                }
//
//                return true;
//            }
//        });


        //+ taha 25-02-2021 changing fallback logic

//        if ( this.isFallback)
//        {
//            Utility.DEBUG_LOG(TAG,"Fallback detected, do not show amount screen...");
//            Utility.DEBUG_LOG(TAG,"buttonClicked:"+buttonClicked);
//            switch (buttonClicked){
//                case "SALE":
//                case "REFUND":
//                case "REDEEM":
//                case "CASH OUT":
//                case "CASH ADV":
//                case "PRE AUTH":
//                case "AUTH":
//                case "SALEIPP":
//                case "COMPLETION":
//                    authIdLayout.setVisibility(View.INVISIBLE);
//                    textLayout.setVisibility(View.VISIBLE);
//                    authIdButton.setVisibility(View.GONE);
//                    break;
//
////                case "COMPLETION":
////                    authIdLayout.setVisibility(View.VISIBLE);
////                    textLayout.setVisibility(View.INVISIBLE);
////                    authIdButton.setVisibility(View.VISIBLE);
////                    inputLabelAmount.setText("Enter Auth ID");
////                    break;
//
//                case "ORBIT INQUIRY":
//                    inputLabelAmount.setText("Press the Button");
//                    textLayout.setVisibility(View.GONE);
//                    //imm.toggleSoftInput(InputMethodManager.RESULT_HIDDEN, InputMethodManager.RESULT_HIDDEN);
//                    //cardBoxLayout.setVisibility(View.VISIBLE);
//                    amountButton.setVisibility(View.GONE);
//                    authIdButton.setVisibility(View.GONE);
//                    nextButton.setVisibility(View.GONE);
//                    keyboard_layout.setVisibility(View.GONE);
//
//
////                transactionAmount = "1.00";
//                    break;
//
//                case "MANUAL ENTRY":
//                    inputLabelAmount.setText("Enter Card");
//                    textLayout.setVisibility(View.GONE);
//                    manualEntryCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
//                    manualEntryLayout.setVisibility(View.VISIBLE);
//                    manualEntryCardNumber.setVisibility(View.VISIBLE);
//                    manualEntryText.setVisibility(View.VISIBLE);
//
//                    break;
//
//                default:
//                    throw new IllegalStateException("Unexpected value: " + buttonClicked);
//            };
//
//            Utility.DEBUG_LOG(TAG,"after switch");
//            alertDialog.setContentView(R.layout.aid_list_view);
//            Utility.DEBUG_LOG(TAG,"after setContentView");
//            aidListView=alertDialog.findViewById(R.id.pay_list_view);
//            Utility.DEBUG_LOG(TAG,"after findViewById");
//            assetManager = getResources().getAssets();
//            Utility.DEBUG_LOG(TAG,"after getResources");
//
//
//            Utility.DEBUG_LOG("button clicked:", buttonClicked);
//            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")){
//                nextButton.setVisibility(View.GONE);
//                amountButton.setVisibility(View.GONE);
//            }
//            else {
//                nextButton.setVisibility(View.GONE);
//                amountButton.setVisibility(View.GONE);
//            }
//            Utility.DEBUG_LOG(TAG,"after setVisibility");
//
//            inputLabelAmount.setText("Swipe");
//
//            double c = Double.parseDouble(transactionAmount.toString());
//            Utility.DEBUG_LOG(TAG,"Swipe amount 1: " + c);
//            //+ taha 23-02-2021 this is handled elsewhere
//            //            c = c / 100.0f;
//            Utility.DEBUG_LOG(TAG,"Swipe amount 2: " + c);
//            String tempAmount = String.format(Locale.getDefault(), "%.2f", (Double) (c));
//            Utility.DEBUG_LOG(TAG,"Swipe amount 3: "+ tempAmount);
//            amountField.setText(tempAmount.toString());
//
//
////            amountField.getText().toString();
//            keyboard_layout.setVisibility(View.GONE);
//
//            Utility.DEBUG_LOG(TAG,"before animationsFlow 4");
//            animationsFlow();
//        }
//        else
//        {
//            keyboard_layout.setVisibility(View.VISIBLE);
//            myKeyListener = new MyKeyListener(amountField);
//            rootView.findViewById(R.id.key1).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key2).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key3).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key4).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key5).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key6).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key7).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key8).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key9).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key0).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.keyclr).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key_delete).setOnClickListener(myKeyListener);
//            rootView.findViewById(R.id.key_confirm).setOnClickListener(myKeyListener);
//
//
//
//            switch (buttonClicked){
//                case "SALE":
//                case "REFUND":
//                case "REDEEM":
//                case "CASH OUT":
//                case "CASH ADV":
//                case "PRE AUTH":
//                case "AUTH":
//                case "SALEIPP":
//                case "COMPLETION":
//                    authIdLayout.setVisibility(View.INVISIBLE);
//                    textLayout.setVisibility(View.VISIBLE);
//                    authIdButton.setVisibility(View.GONE);
//                    break;
//
////                case "COMPLETION":
////                    authIdLayout.setVisibility(View.VISIBLE);
////                    textLayout.setVisibility(View.INVISIBLE);
////                    authIdButton.setVisibility(View.VISIBLE);
////                    inputLabelAmount.setText("Enter Auth ID");
////                    break;
//
//                case "ORBIT INQUIRY":
//                    inputLabelAmount.setText("Press the Button");
//                    textLayout.setVisibility(View.GONE);
//                    //imm.toggleSoftInput(InputMethodManager.RESULT_HIDDEN, InputMethodManager.RESULT_HIDDEN);
//                    //cardBoxLayout.setVisibility(View.VISIBLE);
//                    amountButton.setVisibility(View.GONE);
//                    authIdButton.setVisibility(View.GONE);
//                    nextButton.setVisibility(View.GONE);
//                    keyboard_layout.setVisibility(View.GONE);
//
//
////                transactionAmount = "1.00";
//                    break;
//
//                case "MANUAL ENTRY":
//                    inputLabelAmount.setText("Enter Card");
//                    textLayout.setVisibility(View.GONE);
//                    manualEntryCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
//                    manualEntryLayout.setVisibility(View.VISIBLE);
//                    manualEntryCardNumber.setVisibility(View.VISIBLE);
//                    manualEntryText.setVisibility(View.VISIBLE);
//
//                    break;
//
//                default:
//                    throw new IllegalStateException("Unexpected value: " + buttonClicked);
//            }
//
////        // Inflate the layout for this fragment
////        View rootView = inflater.inflate(R.layout.fragment_payment_amount, container, false);
////        unbinder = ButterKnife.bind(this, rootView);
//
//            //amountField.setText("0.00");
//            //this alert dialog shows the list of merchants to the user to select from
//            alertDialog.setContentView(R.layout.aid_list_view);
//            aidListView=alertDialog.findViewById(R.id.pay_list_view);
//            assetManager = getResources().getAssets();
//
//
////        buttonClicked = String.valueOf(SharedPref.read("button_fragment",buttonClicked));
//            Utility.DEBUG_LOG("button clicked", buttonClicked);
//            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")){
//                nextButton.setVisibility(View.GONE);
//                amountButton.setVisibility(View.GONE);
//            }
//            else {
//                nextButton.setVisibility(View.GONE);
//                amountButton.setVisibility(View.GONE);
//            }
//        }


        if (this.fallbackOrignatorType != FT_NA) {
            Utility.DEBUG_LOG(TAG, "Fallback detected, do not show amount screen...");
            Utility.DEBUG_LOG(TAG, "buttonClicked:" + buttonClicked);
            switch (buttonClicked) {
                case "SALE":
                case "REFUND":
                case "REDEEM":
                case "CASH OUT":
                case "CASH ADV":
                case "PRE AUTH":
                case "AUTH":
                case "SALEIPP":
                case "COMPLETION":
                    authIdLayout.setVisibility(View.INVISIBLE);
                    textLayout.setVisibility(View.VISIBLE);
                    authIdButton.setVisibility(View.GONE);
                    break;

//                case "COMPLETION":
//                    authIdLayout.setVisibility(View.VISIBLE);
//                    textLayout.setVisibility(View.INVISIBLE);
//                    authIdButton.setVisibility(View.VISIBLE);
//                    inputLabelAmount.setText("Enter Auth ID");
//                    break;

                case "ORBIT INQUIRY":
                    inputLabelAmount.setText("Press the Button");
                    textLayout.setVisibility(View.GONE);
                    //imm.toggleSoftInput(InputMethodManager.RESULT_HIDDEN, InputMethodManager.RESULT_HIDDEN);
                    //cardBoxLayout.setVisibility(View.VISIBLE);
                    amountButton.setVisibility(View.GONE);
                    authIdButton.setVisibility(View.GONE);
                    nextButton.setVisibility(View.GONE);
                    keyboard_layout.setVisibility(View.GONE);


//                transactionAmount = "1.00";
                    break;

                case "MANUAL ENTRY":
                    inputLabelAmount.setText("Enter Card");
                    textLayout.setVisibility(View.GONE);
                    manualEntryCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
                    manualEntryLayout.setVisibility(View.VISIBLE);
                    manualEntryCardNumber.setVisibility(View.VISIBLE);
                    manualEntryText.setVisibility(View.VISIBLE);

                    break;

                default:
                    throw new IllegalStateException("Unexpected value: " + buttonClicked);
            }
            ;

            Utility.DEBUG_LOG(TAG, "after switch");
            alertDialog.setContentView(R.layout.aid_list_view);
            Utility.DEBUG_LOG(TAG, "after setContentView");
            aidListView = alertDialog.findViewById(R.id.pay_list_view);
            Utility.DEBUG_LOG(TAG, "after findViewById");
            assetManager = getResources().getAssets();
            Utility.DEBUG_LOG(TAG, "after getResources");


            Utility.DEBUG_LOG("button clicked:", buttonClicked);
            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                nextButton.setVisibility(View.GONE);
                amountButton.setVisibility(View.GONE);
            } else {
                nextButton.setVisibility(View.GONE);
                amountButton.setVisibility(View.GONE);
            }
            Utility.DEBUG_LOG(TAG, "after setVisibility");
            if (fallbackOrignatorType == FT_Dip)
                inputLabelAmount.setText("Swipe");
            else if (fallbackOrignatorType == FT_Swipe)
                inputLabelAmount.setText("Tap / Insert / Swipe");
            else if (fallbackOrignatorType == FT_Ctls)
                inputLabelAmount.setText("Insert / Swipe");
            else if (fallbackOrignatorType == FT_SeeYourPhone)
                inputLabelAmount.setText("Tap / Insert / Swipe");

            Utility.DEBUG_LOG(TAG,"transactionAmount:"+transactionAmount);
            Utility.DEBUG_LOG(TAG,"tipAmount:"+tipAmount);
            double c = Double.parseDouble(transactionAmount.toString());
            double t = Double.parseDouble(tipAmount.toString());
            c += t;
            Utility.DEBUG_LOG(TAG, "Swipe amount 1: " + c);
            //+ taha 23-02-2021 this is handled elsewhere
            //            c = c / 100.0f;
            Utility.DEBUG_LOG(TAG, "Swipe amount 2: " + c);
            String tempAmount = String.format(Locale.getDefault(), "%.2f", (Double) (c));
            Utility.DEBUG_LOG(TAG, "Swipe amount 3: " + tempAmount);
            amountField.setText(tempAmount.toString());


//            amountField.getText().toString();
            keyboard_layout.setVisibility(View.GONE);

            Utility.DEBUG_LOG(TAG, "before animationsFlow 4");
            animationsFlow();
        } else {
            Utility.DEBUG_LOG(TAG,"non fallback else case...");
            keyboard_layout.setVisibility(View.VISIBLE);
            myKeyListener = new MyKeyListener(amountField);
            rootView.findViewById(R.id.key1).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key2).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key3).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key4).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key5).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key6).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key7).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key8).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key9).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key0).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.keyclr).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key_delete).setOnClickListener(myKeyListener);
            rootView.findViewById(R.id.key_confirm).setOnClickListener(myKeyListener);


            switch (buttonClicked) {
                case "SALE":
                case "REFUND":
                case "REDEEM":
                case "CASH OUT":
                case "CASH ADV":
                case "PRE AUTH":
                case "AUTH":
                case "SALEIPP":
                case "COMPLETION":
                    authIdLayout.setVisibility(View.INVISIBLE);
                    textLayout.setVisibility(View.VISIBLE);
                    authIdButton.setVisibility(View.GONE);
                    break;

//                case "COMPLETION":
//                    authIdLayout.setVisibility(View.VISIBLE);
//                    textLayout.setVisibility(View.INVISIBLE);
//                    authIdButton.setVisibility(View.VISIBLE);
//                    inputLabelAmount.setText("Enter Auth ID");
//                    break;

                case "ORBIT INQUIRY":
                    inputLabelAmount.setText("Press the Button");
                    textLayout.setVisibility(View.GONE);
                    //imm.toggleSoftInput(InputMethodManager.RESULT_HIDDEN, InputMethodManager.RESULT_HIDDEN);
                    //cardBoxLayout.setVisibility(View.VISIBLE);
                    amountButton.setVisibility(View.GONE);
                    authIdButton.setVisibility(View.GONE);
                    nextButton.setVisibility(View.GONE);
                    keyboard_layout.setVisibility(View.GONE);


//                transactionAmount = "1.00";
                    break;

                case "MANUAL ENTRY":
                    inputLabelAmount.setText("Enter Card");
                    textLayout.setVisibility(View.GONE);
                    manualEntryCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
                    manualEntryLayout.setVisibility(View.VISIBLE);
                    manualEntryCardNumber.setVisibility(View.VISIBLE);
                    manualEntryText.setVisibility(View.VISIBLE);

                    break;

                default:
                    if (buttonClicked.equals("")) {
                        Utility.DEBUG_LOG(TAG, "buttonClicked");
                    } else {
                        Utility.DEBUG_LOG(TAG, "buttonClicked" + buttonClicked);
                    }
                    break;
            }

//        // Inflate the layout for this fragment
//        View rootView = inflater.inflate(R.layout.fragment_payment_amount, container, false);
//        unbinder = ButterKnife.bind(this, rootView);

            //amountField.setText("0.00");
            //this alert dialog shows the list of merchants to the user to select from
            alertDialog.setContentView(R.layout.aid_list_view);
            aidListView = alertDialog.findViewById(R.id.pay_list_view);
            assetManager = getResources().getAssets();


//        buttonClicked = String.valueOf(SharedPref.read("button_fragment",buttonClicked));
            Utility.DEBUG_LOG("button clicked", buttonClicked);
            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                nextButton.setVisibility(View.GONE);
                amountButton.setVisibility(View.GONE);
            } else {
                nextButton.setVisibility(View.GONE);
                amountButton.setVisibility(View.GONE);
            }
        }

        //initCountDownTimer(rootView);
        return rootView;
    }
    // masood count down timer on payment success
//        public void initCountDownTimer(View v) {
//        new CountDownTimer(3000000, 1000) {
//            public void onTick(long millisUntilFinished) {
////                if(isButtonClicked){
////                    cancel();
////                    DashboardContainer.backStack();
////                    Utility.DEBUG_LOG("Timer","timer cancel");
////                }
//            }
//            public void onFinish() {
//                DashboardContainer.backStack();
//                //DialogUtil.hideKeyboard(getActivity());
//                Utility.DEBUG_LOG("timer", "working");
//                cancel();
//            }
//
//        }.start();
//    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = new WeakReference<>(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        alertDialog = new Dialog(context);
        Utility.DEBUG_LOG(TAG, "PaymentAmountFragment:onAttach()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null)
            getActivity().unbindService(conn);
        stopCheckCards();
    }

    public void stopCheckCards(){
        try {
            if(iemv!=null) {
                iemv.stopCheckCard();
                paymentAmountPresenter = null;
            }
            Utility.DEBUG_LOG(TAG,"STOPCHECKCARDS");
            //iemv.abortEMV();
//            iemv.resetEmvHandler(handler);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }


    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private ServiceConnection conn = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

//            amountField.addTextChangedListener(new NumberTextWatcherForThousand(amountField));
//            if(!(buttonClicked.equals("ORBIT INQUIRY"))) {
//                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_FORCED);
//            }
//            Utility.DEBUG_LOG("show Keyborad", "keyboard");
            // amountButton.setVisibility(View.GONE);

            new CountDownTimer(01, 01) {
                @SuppressLint("LongLogTag")
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);

                    try {
                        Utility.DEBUG_LOG("onServiceConnected", "onServiceConnected--Control here..");
                        //serialPort = idevice.getSerialPort("usb-rs232");
                        iemv = idevice.getEMV();
                        ipinpad = idevice.getPinpad(1);
                        iBeeper = idevice.getBeeper();
                        printer = idevice.getPrinter();
                        //TransPrinter.initialize(printer); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                        irfCardReader = idevice.getRFCardReader();
                        iDeviceInfo = idevice.getDeviceInfo();


                        Utility.DEBUG_LOG("Total Ram Payment", String.valueOf(iDeviceInfo.getRamTotal()));
                        Utility.DEBUG_LOG("Available Ram Payment", String.valueOf(iDeviceInfo.getRamAvailable()));
                        Utility.DEBUG_LOG("Battery Level Payment", String.valueOf(iDeviceInfo.getBatteryLevel()));
                        Utility.DEBUG_LOG("Battery Temperature Payment", String.valueOf(iDeviceInfo.getBatteryTemperature()));
                        Utility.DEBUG_LOG("Data Usage Level Payment", String.valueOf(iDeviceInfo.getMobileDataUsageTotal()));
                        // Utility.DEBUG_LOG("IdeviceSystemTime", String.valueOf(iDeviceInfo.updateSystemTime("20210205","130000")));
                        if (Constants.isEcrReq || buttonClicked.equals("ORBIT INQUIRY")) {
                            //Constants.isEcrReq = false;
//                            amountField.setEnabled(false);
//                            Utility.DEBUG_LOG("ECR Amount", Constants.ecrAmount);
//                            amountField.setText(DataFormatterUtil.formattedAmount(Constants.ecrAmount));
//                            amountField.setTextColor(getResources().getColor(R.color.green));
//                            transactionAmount = amountField.getText().toString();
//                            inputLabelAmount.setText("Tap / Insert");
//
//                            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y"))
//                                tipAmount = tipAmountField.getText().toString();
//                            if (!amountField.getText().toString().equals("")) {
//                                animationsFlow();
//                            }
                            Utility.DEBUG_LOG(TAG, "ECR Amount:" + Constants.ecrAmount);
                            amountField.setText(DataFormatterUtil.formattedAmount(Constants.ecrAmount));
                            // textScreen.setText(DataFormatterUtil.formattedAmount(Constants.ecrAmount));
                            amountField.setTextColor(getResources().getColor(R.color.green));
                            transactionAmount = amountField.getText().toString();

                            if (TIP_ENABLED.equals("Y") && MARKET_SEG.equals("T") && !(buttonClicked.equals("ORBIT INQUIRY"))) {
                                confirmCount = 1;
                                inputLabelAmount.setText("Enter Tip Amount");
                                keyboard_layout.setVisibility(View.VISIBLE);
                                amountField.setText("0.00");
                                tipAmount = amountField.getText().toString();
                                //  stringBuilder = new StringBuilder("");
                            } else {
                                inputLabelAmount.setText("Tap / Insert / Swipe");
                                keyboard_layout.setVisibility(View.GONE);
                                // textScreen.setVisibility(View.VISIBLE);
                            try {
                               if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }

                                Utility.DEBUG_LOG(TAG, "before animationsFlow 1");
                                animationsFlow();
                            }

                        }
                        } catch(RemoteException e){
                            e.printStackTrace();
                        }
                    if(buttonClicked.equals("COMPLETION")){
                        paymentAmountPresenter = new PaymentAmountPresenter(PaymentAmountFragment.this, PaymentAmountFragment.this, handler,
                                isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount, "sale", mContext.get(),
                                serialPort, iDeviceInfo, saleippMonths,authNumber);         }
                    else {
                        paymentAmountPresenter = new PaymentAmountPresenter(PaymentAmountFragment.this, PaymentAmountFragment.this, handler,
                                isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount, "sale", mContext.get(),
                                serialPort, iDeviceInfo, saleippMonths);
                    }
//                    paymentAmountPresenter = new PaymentAmountPresenter(PaymentAmountFragment.this, PaymentAmountFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, appDatabase,transactionAmount,"sale",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                        // Toast.makeText(getContext(), "bind service success", Toast.LENGTH_SHORT).show();
                    }


                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    /**
     * this method animates the view, and executes the purchase function of the presenter which takes amount as parameter
     */
    private void animationsFlow() {
        Animation animSlideLeft = AnimationUtils.loadAnimation(mContext.get(), R.anim.slide_out_left);
        amountButton.startAnimation(animSlideLeft);
        animSlideRight = AnimationUtils.loadAnimation(mContext.get(), R.anim.slide_in_faster);
        animSlideLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                amountButton.setVisibility(View.GONE);
                if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                    Utility.DEBUG_LOG(TAG, "TIP AMOUNT:" + tipAmount);
                    Utility.DEBUG_LOG(TAG, "Transaction AMOUNT:" + transactionAmount);
                    if(buttonClicked.equals("ORBIT INQUIRY")){
                        transactionAmount = "3002";
                        tipAmount = "0";
                    }
//                    paymentAmountPresenter.doPurchase(transactionAmount, tipAmount);
                    Utility.DEBUG_LOG(TAG, "transactionAmount:" + transactionAmount);
                    if (fallbackOrignatorType != FT_NA) {
                        Utility.DEBUG_LOG(TAG, "before doPurchase 4 fallback from [payment amount fragment]1:" + fallbackOrignatorType);
                        paymentAmountPresenter.doPurchase(transactionAmount, tipAmount, fallbackOrignatorType);
                    } else {
                        Utility.DEBUG_LOG(TAG, "before paymentAmountPresenter.doPurchase payment amount1");
                        paymentAmountPresenter.doPurchase(transactionAmount, tipAmount);
                    }
                }
                else {
                    if (buttonClicked.equals("ORBIT INQUIRY"))
                    {
                        transactionAmount = String.valueOf(1.00f + Float.parseFloat(VEPS_LIMIT));
                        Utility.DEBUG_LOG(TAG,"in fragment inquiry");
                    }
                    //+ taha 25-02-2021 change fallback logic
//                    if ( isFallback )
//                    {
//                        Utility.DEBUG_LOG(TAG,"transactionAmount:"+transactionAmount);
//                        Utility.DEBUG_LOG(TAG,"before doPurchase 4 fallback");
//                        paymentAmountPresenter.doPurchase(transactionAmount,"0", FT_Dip);
//                    }
//                    else
//                    {
//                        Utility.DEBUG_LOG(TAG,"transactionAmount:"+transactionAmount);
//                        Utility.DEBUG_LOG(TAG,"+ PaymentAmountFragment(isFallback)2 +");
//                        paymentAmountPresenter.doPurchase(transactionAmount,"0");
//
//                    }
                    Utility.DEBUG_LOG(TAG, "transactionAmount:" + transactionAmount);
                    if (fallbackOrignatorType != FT_NA) {
                        Utility.DEBUG_LOG(TAG, "before doPurchase 4 fallback from [payment amount fragment]:" + fallbackOrignatorType);
                        paymentAmountPresenter.doPurchase(transactionAmount, "0", fallbackOrignatorType);
                    } else {
                        Utility.DEBUG_LOG(TAG, "before paymentAmountPresenter.doPurchase payment amount");
                        paymentAmountPresenter.doPurchase(transactionAmount, "0");
                    }

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }
    // mmasood: countdown timer in sales screen


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        buttonClicked = "";
        // conn = null;
        unbinder.unbind();
        hideProgress();
       // unbinder = null;
        Utility.DEBUG_LOG(TAG,"onDestroyView");
//        if (conn != null)
//            getActivity().unbindService(conn);
    }

    @OnClick({R.id.amoutButton, R.id.nextButton, R.id.authIdButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.amoutButton:
                amountField.setEnabled(false);
                amountField.setTextColor(getResources().getColor(R.color.green));
                transactionAmount = amountField.getText().toString();
                inputLabelAmount.setText("Tap / Insert / Swipe");

                if (Constants.TIP_ENABLED.equalsIgnoreCase("Y"))
                    tipAmount = tipAmountField.getText().toString();
                if (!amountField.getText().toString().equals("")) {
                    Utility.DEBUG_LOG(TAG, "before animationsFlow 2");
                    animationsFlow();
                }
                break;

            case R.id.nextButton:
                inputLabelAmount.setVisibility(View.GONE);
                amountField.setVisibility(View.INVISIBLE);
                nextButton.setVisibility(View.GONE);
                tipLayout.setVisibility(View.GONE);
                amountButton.setVisibility(View.VISIBLE);

                break;

            case R.id.authIdButton:
                authIdLayout.setVisibility(View.GONE);
                textLayout.setVisibility(View.VISIBLE);
                authIdButton.setVisibility(View.GONE);
                inputLabelAmount.setText("Enter Amount");
//                if (!authIdField.getText().equals("")) {
//                    String c = String.valueOf(authIdField.getText());
//                    Utility.DEBUG_LOG("authid", String.valueOf(authIdField.getText()));
//                    findTransactionByAuthId(c);
//                    Utility.DEBUG_LOG("authid", "data basequery run");
//                }
                break;
        }

    }

    @Override
    public void toastShow(String str) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (str.contentEquals("show")) {
                        cardBoxLayout.setVisibility(View.VISIBLE);
                        cardBoxLayout.startAnimation(animSlideRight);
                    } else {
                        Message msg = new Message();
                        Utility.DEBUG_LOG(TAG, "before getData");

                        msg.getData().putString("msg", str);
                        handler.sendMessage(msg);
                        // Toast.makeText(getContext(), str, Toast.LENGTH_SHORT).show();
                        Utility.DEBUG_LOG(TAG, str);
                    }
                }
            });
        } else {
            Utility.DEBUG_LOG("showProgress", "Error Dialog null");
        }
    }

    @Override
    public void showProgress(String title) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    amountButton.setVisibility(View.GONE);
                    cardBoxLayout.setVisibility(View.GONE);
                    MainApplication.showProgressDialog(mContext.get(), title);
                }
            });
        } else {
            Utility.DEBUG_LOG("showProgress", "Error Dialog null");
        }

    }

    @Override
    public void hideProgress() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.hideProgressDialog();
                }
            });
        } else {
            Utility.DEBUG_LOG("hideProgress", "Error Dialog null");
        }
    }

    @Override
    public void showPaymentSuccessDialog(TransactionDetail transactionDetail) {
        DialogPaymentSuccessFragment newFragment = new DialogPaymentSuccessFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("transactionDetail", transactionDetail);
        newFragment.setArguments(bundle);
        DashboardContainer.switchFragmentWithBackStack(newFragment, "success fragment");
    }

    @Override
    public void showMultipleAid(List<String> aidList) {
        Utility.DEBUG_LOG(TAG, "+ showMultipleAid +");
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> {
                int paypakAidPosition = -1;

                final aidListAdapter myadapter = new aidListAdapter(mContext.get(), R.layout.row_item, aidList);
                Utility.DEBUG_LOG(TAG, "+ showMultipleAid + in getactivity()"+myadapter.getCount());
                Utility.DEBUG_LOG(TAG, "+ showMultipleAid + in aidList()"+aidList);


                aidListView.setOnItemClickListener((parent, view, position, id) -> {
                    //PASS POSITION TO PRESENTER
                    Utility.DEBUG_LOG(TAG, "Old position:" + position);
                    position = position + 1;
                    Utility.DEBUG_LOG(TAG, "New position:" + position);
                    paymentAmountPresenter.selectedAidIndex(position);
                    alertDialog.dismiss();
                });
                alertDialog.setCancelable(false);


                // shivam 08-04-2021 if application list size is 1 Select that by default
                if (aidList.size() == 1) {
                    paymentAmountPresenter.selectedAidIndex(1);
                    alertDialog.dismiss();
                }
                //+ taha 24-03-2021 default select paypak to pass their certification
                //+ shivam 08-04-2021 modified for paypak
                for (int i = 0; i < aidList.size(); i++) {
                    Utility.DEBUG_LOG(TAG, "i: " + i + ", aid:" + aidList.get(i));
                    Utility.DEBUG_LOG(TAG, "size of aid list"+String.valueOf(aidList.size()));

                    //Utility.DEBUG_LOG(TAG,);


                    if (aidList.get(i).toLowerCase().contains("paypak")) {
                        Utility.DEBUG_LOG(TAG, "Paypak found at index:" + i);
                        paypakAidPosition = i + 1;
                        Utility.DEBUG_LOG(TAG, "paypakAidPosition:" + paypakAidPosition);
                        paymentAmountPresenter.selectedAidIndex(paypakAidPosition);
                        alertDialog.dismiss();
                    }
                }
                aidListView.setAdapter(myadapter);
                alertDialog.show();
            });
        } else {
            Utility.DEBUG_LOG("showMultipleAid", "Error Dialog null");
        }
    }

    @Override
    public void transactionFailed(String title, String error) {
        Utility.DEBUG_LOG(TAG, "+ PAF:transactionFailed +");
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.errorDialog(mContext.get(), title, error);
                }
            });
        } else {
            Utility.DEBUG_LOG("transactionFailed", "Error Dialog null");
        }
    }

    @Override
    public void rocEmpty(String content, TransactionDetail transactionDetail, String copy, String txnType) {
//        getActivity().runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                DialogUtil.confirmDialogCustomer(getContext(), content, sweetAlertDialog1 -> {
//                    receiptPrinter.printSaleReceipt(printer, assetManager,transactionDetail , "aid",copy,txnType);
//                    sweetAlertDialog1.dismissWithAnimation();
//                    SharedPref.countedTransactionWrite(getContext(),null);
//                    SharedPref.writeList(getContext(),null);
//                });
//            }
//        });
    }

    @Override
    public boolean last4Digits(String text1) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.inputDialog(mContext.get(), "Enter Last 4 Digits", "Enter Last 4 Digits", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                            //show confirmation
                            if (text1.equals(text.getText().toString())) {
                                bool = true;
                                sweetAlertDialog.dismissWithAnimation();
                            } else {
                                bool = false;
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        }
                    }, InputType.TYPE_CLASS_NUMBER, null);

                }
            });
            return bool;
        } else {
            Utility.DEBUG_LOG("Last$didgits", String.valueOf(bool));
            return bool;

        }
    }

    @Override
    public boolean last4Digits2(TransactionDetail transactionDetail, String text) {
//        String rv = "";
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountFragment::last4Digits_taha +");
//        last4DigitsStr = "";
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.inputDialog(mContext.get(), "Enter Last 4 Digits", "Enter Last 4 Digits", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                            transactionDetail.setLast4Digits(text.getText().toString());
                            Utility.DEBUG_LOG(TAG, "last4DigitsStr:" + transactionDetail.getLast4Digits());
//                            rv = text.getText().toString();
                            //show confirmation
                            if (text.equals(text.getText().toString())) {
//                                bool = true;
//                                rv = true;
                                sweetAlertDialog.dismissWithAnimation();
                            } else {
//                                bool = false;
//                                rv = false;
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        }
                    }, InputType.TYPE_CLASS_NUMBER, null);

                }
            });
//            return bool;
        } else {
            Utility.DEBUG_LOG("Last$didgits", String.valueOf(bool));
//            return bool;

        }
        return rv;
    }

    @Override
    public boolean panManualEntry(TransactionDetail transactionDetail, String text) {
//        String rv = "";
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountFragment::panManualEntry +");
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.inputDialog(mContext.get(), "Enter PAN", "Enter PAN...", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                            transactionDetail.setPan(text.getText().toString());
                            Utility.DEBUG_LOG(TAG, "pan:" + transactionDetail.getPan());
                            if (text.equals(text.getText().toString())) {
                                sweetAlertDialog.dismissWithAnimation();
                            } else {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        }
                    }, InputType.TYPE_CLASS_NUMBER, null);

                }
            });
        } else {
        }
        return rv;
    }

    @Override
    public void transactionSuccess(String content) {

    }

    @Override
    public void transactionSuccessCustomer(String content, TransactionDetail transactionDetail, String copy, String txnType, String balance, boolean redeemCheck) {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (Exception e) {
                    Utility.DEBUG_LOG(TAG, "Exception: " + e.getMessage().toString());
                }
                if (!(txnType.equals("ORBIT INQUIRY"))) {
                    Utility.DEBUG_LOG(TAG, "show customer copy option");
                    Utility.DEBUG_LOG(TAG, "before showing dialog of customer copy");
// SHIVAM UPDATE HERE FOR TEXT PRINTER
                    DialogUtil.confirmDialogCustomer(mContext.get(), content, "Print Customer Copy", "", sweetAlertDialog1 -> {
//                        transPrinter = new PrintRecpSale(mContext);
//                        transPrinter.initializeData(transactionDetail, balance, redeemCheck, copy);
//                        transPrinter.print();
                        Log.d(TAG,"printer "+printer.toString());
                        Log.d(TAG,"assetManager "+assetManager.toString());
                        Log.d(TAG,"transactionDetail "+transactionDetail);
                        Log.d(TAG,"txnType "+txnType);
                        Log.d(TAG,"balance "+balance);
                        Log.d(TAG,"redeemCheck "+redeemCheck);
                        receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail,transactionDetail.getAidCode() , "CUSTOMER COPY", txnType, "", balance,redeemCheck);
// SHIVAM UPDATE HERE FOR TEXT PRINTER
                        sweetAlertDialog1.dismissWithAnimation();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                        SharedPref.countedTransactionWrite(getContext(),null);
//                        SharedPref.writeList(getContext(),null);
//                        Constants.BANK_COPY = true;
//                            if(Constants.BANK_COPY) {
//                                DialogUtil.confirmDialogCustomer(getContext(), content, "Print Bank Copy", "", sweetAlertDialog1 -> {
//                                    receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "Bank Copy", txnType);
//                                    sweetAlertDialog1.dismissWithAnimation();
//                                });
//                            }
//                            Constants.BANK_COPY=false;
//                        }
//                    }, 1000);
                    });
                } else {
                    Utility.DEBUG_LOG(TAG, "dont show customer copy option for orbit inquiry");
                }
            }
        });
    }

    // log & display handler
//    @SuppressLint("HandlerLeak")
//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            Utility.DEBUG_LOG(TAG,"+ PaymentFragment:handleMessage()+ ");
//            String string = msg.getData().getString("string");
//            super.handleMessage(msg);
//            Utility.DEBUG_LOG(TAG, msg.getData().getString("msg"));
////            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
//
//        }
//    };

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Utility.DEBUG_LOG(TAG, "+ PaymentFragment:Handler:handleMessage() +");
            Utility.DEBUG_LOG(TAG, "before getData");

            String str = msg.getData().toString();
            Utility.DEBUG_LOG(TAG, "Message:" + str);
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG, msg.getData().getString("msg"));
//            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };

    class MyKeyListener implements View.OnClickListener {
        StringBuilder stringBuilder = new StringBuilder("");

        TextView selectedAmount;

        public MyKeyListener(TextView selectedAmount) {
            this.selectedAmount = selectedAmount;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.key0:
                    if (stringBuilder.length() > 0 & stringBuilder.length() < 12) {
                        stringBuilder.append("0");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key1:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("1");
                        try {
                            if(iBeeper!=null) {
                               if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key2:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("2");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key3:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("3");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key4:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("4");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key5:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("5");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key6:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("6");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key7:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("7");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key8:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("8");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key9:
                    if (stringBuilder.length() < 12) {
                        stringBuilder.append("9");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.keyclr:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.delete(0, stringBuilder.length());
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_delete:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_confirm:
                    confirmCount++;
//                    if(isEcrEnable == "Y" && isEcrReq == true && TIP_ENABLED=="Y"){
//
//                    }


                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && (buttonClicked.equals("SALE") || buttonClicked.equals("COMPLETION"))) {
                        Utility.DEBUG_LOG(TAG,"confirmCount:"+confirmCount);
                        if (confirmCount == 1) {
                            transactionAmount = amountField.getText().toString();
                            amountField.setText("0.00");
                            tipAmount = amountField.getText().toString();
                            stringBuilder = new StringBuilder("");
                            inputLabelAmount.setText("Enter Tip Amount");
                        }
                        if (confirmCount == 2) {


                            tipAmount = amountField.getText().toString();
                            double thresholdAmount = 0.00;
                            double total = 0.00;
                            total = Double.valueOf(new DecimalFormat("0.00").format(Double.parseDouble(transactionAmount) + Double.parseDouble(tipAmount)));

                            Utility.DEBUG_LOG(TAG, "transactionamount Float:" + Double.parseDouble(transactionAmount));
                            Utility.DEBUG_LOG(TAG, "tipamount Float:" + Double.parseDouble(tipAmount));
                            Utility.DEBUG_LOG(TAG, "percentage Float:" + new DecimalFormat("0.00").format((2.5 * Double.parseDouble(transactionAmount)) / 100.00));
                            thresholdAmount = Double.valueOf(new DecimalFormat("0.00").format((Double.parseDouble(Constants.TIP_THRESHOLD) * Double.parseDouble(transactionAmount)) / 100.00));
                            Utility.DEBUG_LOG(TAG, "thresholdAmount Float:" + new DecimalFormat("0.00").format((Double.parseDouble(Constants.TIP_THRESHOLD) * Double.parseDouble(transactionAmount)) / 100.00));

                            //Utility.DEBUG_LOG(TAG,"percentage amount Float:" +new DecimalFormat("0.00").format(2.5 * Double.parseDouble(transactionAmount)));
                            if (Double.parseDouble(tipAmount) > thresholdAmount) {
                                Utility.DEBUG_LOG(TAG, "we are in if condition Float:" + Double.parseDouble(transactionAmount));
                               // Toast.makeText(mContext.get(),"Tip amount is too high",Toast.LENGTH_SHORT).show();
                                DialogUtil.errorDialog(mContext.get(), "Tip amount is too high", "Please re-enter!");
                                confirmCount = 1;
                            } else {


                                    final AlertDialog.Builder alert = new AlertDialog.Builder(mContext.get());

                                    LinearLayout lila1= new LinearLayout(mContext.get());
                                    lila1.setOrientation(LinearLayout.VERTICAL); //1 is for vertical orientation
                                    lila1.setPadding(10,10,10,10);
                                    final TextView input = new TextView(mContext.get());
                                    final TextView input1 = new TextView(mContext.get());
                                    final TextView input2 = new TextView(mContext.get());

                                    input.setPadding(0,0,0,10);
                                    input1.setPadding(0,0,0,10);
                                    input2.setPadding(0,0,0,10);
                                    input.setGravity(1);
                                    input1.setGravity(1);
                                    input2.setGravity(1);

                                    input.setText("Amount: "+transactionAmount);
                                    input1.setText("Tip Amount: "+tipAmount);
                                    input2.setText("Total AMount: "+String.valueOf(total));

                                    lila1.addView(input);
                                    lila1.addView(input1);
                                    lila1.addView(input2);
                                    alert.setView(lila1);

                                    alert.setIcon(R.drawable.coin);
                                    alert.setTitle("Total Amount");

                                double finalTotal = total;
                                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            // alert.setContentView(R.layout.activity_dashboard_container);
                                            if (inputLabelAmount != null){
                                                inputLabelAmount.setText("Tap / Insert / Swipe");
                                            }
                                            keyboard_layout.setVisibility(View.GONE);
                                            //stringBuilder = new StringBuilder(String.valueOf(Double.parseDouble(transactionAmount)+Double.parseDouble(tipAmount)));
                                            //stringBuilder = new StringBuilder("333");
                                            //amountField.setText(String.valueOf(Double.parseDouble(transactionAmount)+Double.parseDouble(tipAmount)));
                                            // textScreen.setVisibility(View.GONE);
                                            amountField.setTextColor(getResources().getColor(R.color.green));
                                            textScreen.setTextColor(getResources().getColor(R.color.green));
                                            amountField.setText(String.valueOf(new DecimalFormat("0.00").format(finalTotal)));
                                            Utility.DEBUG_LOG(TAG,"finalTotal:"+finalTotal);
                                            Utility.DEBUG_LOG(TAG,"Amount field value:"+amountField.getText());
                                            try {
                                               if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                                            } catch (RemoteException e) {
                                                e.printStackTrace();
                                            }
                                            animationsFlow();

                                          //  String value = input.getText().toString().trim();
                                           // Toast.makeText(mContext.get(), value, Toast.LENGTH_SHORT).show();
                                        }                     });
                                    alert.setNegativeButton("Cancel",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    dialog.cancel();
                                                    Utility.DEBUG_LOG(TAG,"before new HomeFragment");
                                                DashboardContainer.backStack();}     });

                                    alert.show();



                            }
                        }


                    } else if (buttonClicked.equals("SALEIPP")) {
                        amountField.setTextColor(getResources().getColor(R.color.green));
                        textScreen.setTextColor(getResources().getColor(R.color.green));
                        transactionAmount = amountField.getText().toString();
                        DashboardContainer.switchFragmentWithBackStack(new SaleIPPFragment(iemv, ipinpad, iBeeper, printer, AppDatabase.getAppDatabase(mContext.get()), transactionAmount, serialPort, iDeviceInfo, assetManager), "SALEIPP");
                    } else {
                        amountField.setTextColor(getResources().getColor(R.color.green));
                        textScreen.setTextColor(getResources().getColor(R.color.green));
                        transactionAmount = amountField.getText().toString();
                        inputLabelAmount.setText("Tap / Insert / Swipe");
                        keyboard_layout.setVisibility(View.GONE);
                        // textScreen.setVisibility(View.GONE);
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }

                        animationsFlow();
                    }


                    // paymentAmountPresenter.doPurchase(transactionAmount, String.valueOf('0'));
                    break;
            }
            if (stringBuilder.length() > 0) {
                double num = Double.parseDouble(stringBuilder.toString());
                selectedAmount.setText(big2(num / 100));
            } else {
                selectedAmount.setText("0.00");
            }
        }

        private String big2(double d) {
            DecimalFormat format = new DecimalFormat("0.00");
            return format.format(d);
        }
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public boolean getIsFallback() {
        return isFallback;
    }

    public PaymentAmountPresenter.FallbackOrignatorType getFallbackOrignatorType() {
        return fallbackOrignatorType;
    }

}
