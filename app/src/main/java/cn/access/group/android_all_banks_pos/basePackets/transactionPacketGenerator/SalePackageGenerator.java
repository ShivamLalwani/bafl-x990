package cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator;

import android.annotation.SuppressLint;
import android.util.Log;
import android.util.SparseArray;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UPIPacketSpec;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UblPacketSpec;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;

/**
 * on 11/4/2019.
 */


public class SalePackageGenerator {
    String TransTypePackageGenerator;

    private SparseArray<String> data8583_u_purchase = new SparseArray<>();

    public SalePackageGenerator() {
        // data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
        // host= SharedPref.read("merchantName","");

    }

    public SparseArray<String> getSalePacket() {
        SparseArray<String> data8583;
        switch (Constants.HOST_INDEX_FROM_DB) {
            /*case "alfalah":
                data8583= generatePacketAlfalah();
                break;
            case "hbl":
                data8583= generatePacketHbl();
                break;*/
            case 0:
                Utility.DEBUG_LOG("UblPacketSpec","before new UblPacketSpec");
                UblPacketSpec ublPacketSpec = new UblPacketSpec();
                TransTypePackageGenerator = String.valueOf(SharedPref.read("button_fragment", TransTypePackageGenerator));
                Utility.DEBUG_LOG("TransTypePackage case 0", TransTypePackageGenerator);

                switch (TransTypePackageGenerator) {
                    case "SALE":
                        data8583 = ublPacketSpec.generateSalePacket();
                        data8583_u_purchase = data8583;
                        break;

                    case "SALEIPP":
                        data8583 = ublPacketSpec.generateSaleIPPPacket();
                        data8583_u_purchase = data8583;
                        break;

                    case "PRE AUTH":
                        data8583 = ublPacketSpec.generatePreAuthPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "AUTH":
                        data8583 = ublPacketSpec.generateAuthPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "REDEEM":
                        data8583 = ublPacketSpec.generateOrbitRedeemPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "ORBIT INQUIRY":
                        data8583 = ublPacketSpec.generateOrbitInquiryPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "CASH OUT":
                        data8583 = ublPacketSpec.generateCashOutPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "COMPLETION":
                        data8583 = ublPacketSpec.generateCompletionPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "CASH ADV":
                        data8583 = ublPacketSpec.generateCashAdvPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "REFUND":
                        data8583 = ublPacketSpec.generateRefundPacket();
                        data8583_u_purchase = data8583;
                        break;
                }
                break;
            case 1:
                Utility.DEBUG_LOG("UPIPacketSpec","before new UPIPacketSpec");
                UPIPacketSpec upiPacketSpec = new UPIPacketSpec();
                TransTypePackageGenerator = String.valueOf(SharedPref.read("button_fragment", TransTypePackageGenerator));
                Utility.DEBUG_LOG("TransTypePackage case 1", TransTypePackageGenerator);
                switch (TransTypePackageGenerator) {
                    case "SALE":
                        data8583 = upiPacketSpec.generateSalePacket();
                        data8583_u_purchase = data8583;
                        break;

                    case "SALEIPP":
                        data8583 = upiPacketSpec.generateSaleIPPPacket();
                        data8583_u_purchase = data8583;
                        break;

                    case "PRE AUTH":
                        data8583 = upiPacketSpec.generatePreAuthPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "AUTH":
                        data8583 = upiPacketSpec.generateAuthPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "REDEEM":
                        data8583 = upiPacketSpec.generateOrbitRedeemPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "ORBIT INQUIRY":
                        data8583 = upiPacketSpec.generateOrbitInquiryPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "CASH OUT":
                        data8583 = upiPacketSpec.generateCashOutPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "COMPLETION":
                        data8583 = upiPacketSpec.generateCompletionPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "CASH ADV":
                        data8583 = upiPacketSpec.generateCashAdvPacket();
                        data8583_u_purchase = data8583;
                        break;
                    case "REFUND":
                        data8583 = upiPacketSpec.generateRefundPacket();
                        data8583_u_purchase = data8583;
                        break;
                }
                break;


        }

        Utility.DEBUG_LOG("TransTypePackage", TransTypePackageGenerator);
        switch (TransTypePackageGenerator) {
            case "SALE":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
                break;
            case "SALEIPP":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
                break;
            case "PRE AUTH":
            case "AUTH":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.PREAUTH_MESSAGE_TYPE);
                break;
            case "REDEEM":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.ORBIT_REDEEM_MESSAGE_TYPE);
                break;
            case "ORBIT INQUIRY":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.ORBIT_INQUIRY_MESSAGE_TYPE);
                break;
            case "CASH OUT":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.CASH_OUT_MESSAGE_TYPE);
                break;
            case "COMPLETION":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.COMPLETION_MESSAGE_TYPE);
                break;
            case "CASH ADV":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.CASH_ADV_MESSAGE_TYPE);
                break;
            case "REFUND":
                data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.CASH_ADV_MESSAGE_TYPE);
                break;
        }
        return data8583_u_purchase;

    }

}
