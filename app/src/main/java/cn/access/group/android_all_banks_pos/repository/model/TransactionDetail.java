package cn.access.group.android_all_banks_pos.repository.model;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.util.SparseArray;

import java.io.Serializable;

import cn.access.group.android_all_banks_pos.repository.Converters;
import io.reactivex.annotations.NonNull;

/**
 * on 8/29/2019.
 * this is an entity class which represents transaction detail table in room database
 */
@Entity(tableName = "TransactionDetail")
public class TransactionDetail implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int tdid;
    @ColumnInfo(name = "mId")
    private String mId;
    @ColumnInfo(name = "tId")
    private String tId;
    @ColumnInfo(name = "stan")
    private String stan;
    @ColumnInfo(name = "invoiceNo")
    private String invoiceNo;
    @ColumnInfo(name = "rrn")
    private  String rrn;
    @ColumnInfo(name = "txnDate")
    private String txnDate;
    @ColumnInfo(name = "txnTime")
    private String txnTime;
    @ColumnInfo(name = "amount")
    private  String amount;
    @ColumnInfo(name = "transactionAmount")
    private  String transactionAmount = "0.00";
    @ColumnInfo(name = "tipAmount")
    private  String tipAmount= "0.00";
    @ColumnInfo(name = "cardNo")
    private String cardNo;
    @ColumnInfo(name = "cardScheme")
    private String cardScheme;
    @ColumnInfo(name = "batchNo")
    private String batchNo;
    @ColumnInfo(name = "track2")
    private String track2;
    @ColumnInfo(name = "status")
    private String status;
    @ColumnInfo(name = "messageType")
    private String messageType;
    @ColumnInfo(name = "acquiringInstitutionIdentificationCode")
    private String acquiringInstitutionIdentificationCode;
    @ColumnInfo(name = "forwardingInstitutionIdentificationCode")
    private String forwardingInstitutionIdentificationCode;
    @ColumnInfo(name = "processingCode")
    private String processingCode;
    @ColumnInfo(name = "cardType")
    private String cardType;
    @ColumnInfo(name = "authorizationIdentificationResponseCode")
    private String authorizationIdentificationResponseCode;
    @ColumnInfo(name = "tag55")
    @TypeConverters(Converters.class)
    transient private SparseArray tag55;
    @ColumnInfo(name = "cardexpiry")
    private String cardexpiry;
    @ColumnInfo(name = "txnType")
    private String txnType;
    @ColumnInfo(name = "cancelTxnType")
    private String cancelTxnType;
    @ColumnInfo(name = "cardHolderName")
    private String cardHolderName;
    @ColumnInfo(name = "posEntryMode")
    private String posEntryMode;
    @ColumnInfo(name = "panSequenceNumber")
    private String panSequenceNumber;
    @ColumnInfo(name="redeemBalance")
    private String redeemBalance;
    @ColumnInfo(name="redeemKey")
    private String redeemKey;
    @ColumnInfo(name="tvrCode")
    private String tvrCode;
    @ColumnInfo(name="aidCode")
    private String aidCode;
    @ColumnInfo(name="isAdvice")
    private boolean isAdvice = false;
    @ColumnInfo(name="isFallback")
    private boolean isFallback;
  	@ColumnInfo(name="saleippMonth")
    private String saleippMonth = "00";
    @ColumnInfo(name="saleippInstallments")
    private String saleippInstallments = "00";
    @ColumnInfo(name="last4Digits")
    private String last4Digits = "";
    @ColumnInfo(name="pan")
    private String pan = "";
    @ColumnInfo(name="isVoid")
    private boolean isVoid;
    @ColumnInfo(name="tipNumAdjustCounter")
    private int tipNumAdjustCounter;
    @ColumnInfo(name="CVM") // SHIVAM UPDATE HERE FOR TEXT PRINTER
    private String CVM = "NOCVM"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
    @ColumnInfo(name="cardScheme2") // SHIVAM UPDATE HERE FOR TEXT PRINTER
    private String cardScheme2 = "UNKNOWN"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
    @ColumnInfo(name = "ecrRefNo")
    private String ecrRefNo;
    @ColumnInfo(name = "completion")
    private boolean completion = false;



    public boolean isCompletion() {
        return completion;
    }

    public void setCompletion(boolean completion) {
        this.completion = completion;
    }

    public void setIsVoid(boolean isVoid) { this.isVoid = isVoid; }

    public boolean getIsVoid() {
        return isVoid;
    }

    public void setIsFallback(boolean isFallback) { this.isFallback = isFallback; }

    public boolean getIsFallback() {
        return isFallback;
    }

    public void setPanSequenceNumber(String panSequenceNumber) { this.panSequenceNumber = panSequenceNumber; }

    public String getPanSequenceNumber() {
        return panSequenceNumber;
    }

    public void setPosEntryMode(String posEntryMode) {
        this.posEntryMode = posEntryMode;
    }

    public String getPosEntryMode() {
        return posEntryMode;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public int getTdid() {
        return tdid;
    }

    public void setTdid(int tdid) {
        this.tdid = tdid;
    }

    public String getMId() {
        return mId;
    }

    public void setMId(String mId) {
        this.mId = mId;
    }

    public String getTId() {
        return tId;
    }

    public void setTId(String tId) {
        this.tId = tId;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnTime() {
        return txnTime;
    }

    public void setTxnTime(String txnTime) {
        this.txnTime = txnTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTipAmount() {
        return tipAmount;
    }

    public void setTipAmount(String tipAmount) {
        this.tipAmount = tipAmount;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardScheme() {
        return cardScheme;
    }

    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getTrack2() {
        return track2;
    }

    public void setTrack2(String track2) {
        this.track2 = track2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getAcquiringInstitutionIdentificationCode() {
        return acquiringInstitutionIdentificationCode;
    }

    public void setAcquiringInstitutionIdentificationCode(String acquiringInstitutionIdentificationCode) {
        this.acquiringInstitutionIdentificationCode = acquiringInstitutionIdentificationCode;
    }

    public String getForwardingInstitutionIdentificationCode() {
        return forwardingInstitutionIdentificationCode;
    }

    public void setForwardingInstitutionIdentificationCode(String forwardingInstitutionIdentificationCode) {
        this.forwardingInstitutionIdentificationCode = forwardingInstitutionIdentificationCode;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getAuthorizationIdentificationResponseCode() {
        return authorizationIdentificationResponseCode;
    }

    public void setAuthorizationIdentificationResponseCode(String authorizationIdentificationResponseCode) {
        this.authorizationIdentificationResponseCode = authorizationIdentificationResponseCode;
    }

    public String getCardexpiry() {
        return cardexpiry;
    }

    public void setCardexpiry(String cardexpiry) {
        this.cardexpiry = cardexpiry;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getCancelTxnType() {
        return cancelTxnType;
    }

    public void setCancelTxnType(String cancelTxnType) {
        this.cancelTxnType = cancelTxnType;
    }


    public SparseArray getTag55() {
        return tag55;
    }

    public void setTag55(SparseArray tag55) {
        this.tag55 = tag55;
    }


    public String getRedeemBalance() {
        return redeemBalance;
    }

    public void setRedeemBalance(String redeemBalance) {
        this.redeemBalance = redeemBalance;
    }

    public String getRedeemKey() {
        return redeemKey;
    }

    public void setRedeemKey(String redeemKey) {
        this.redeemKey = redeemKey;
    }


    public String getTvrCode() {
        return tvrCode;
    }

    public void setTvrCode(String tvrCode) {
        this.tvrCode = tvrCode;
    }

    public String getAidCode() {
        return aidCode;
    }

    public void setAidCode(String aidCode) {
        this.aidCode = aidCode;
    }

    public boolean getIsAdvice() {
        return isAdvice;
    }

    public void setIsAdvice(boolean isAdvice) {
        this.isAdvice = isAdvice;
    }

    public String getSaleippMonth() {
        return saleippMonth;
    }

    public void setSaleippMonth(String saleippMonth) {
        this.saleippMonth = saleippMonth;
    }

    public String getSaleippInstallments() {
        return saleippInstallments;
    }

    public void setSaleippInstallments(String saleippInstallments) {
        this.saleippInstallments = saleippInstallments;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public int getTipNumAdjustCounter() {
        return tipNumAdjustCounter;
    }

    public void setTipNumAdjustCounter(int tipNumAdjustCounter) {
        this.tipNumAdjustCounter = tipNumAdjustCounter;
    }
    // SHIVAM UPDATE HERE FOR TEXT PRINTER

    public String getCVM() {
        return CVM;
    }

    public void setCVM(String CVM) {
        this.CVM = CVM;
    }


    public String getCardScheme2() {
        return cardScheme2;
    }

    public void setCardScheme2(String cardScheme2) {
        this.cardScheme2 = cardScheme2;
    }


    public String getEcrRefNo() {
        return ecrRefNo;
    }

    public void setEcrRefNo(String ecrRefNo) {
        this.ecrRefNo = ecrRefNo;
    }

// SHIVAM UPDATE HERE FOR TEXT PRINTER



}
