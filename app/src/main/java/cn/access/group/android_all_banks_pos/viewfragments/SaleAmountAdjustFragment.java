
package cn.access.group.android_all_banks_pos.viewfragments;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DecimalDigitsInputFilter;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.FourDigitCardFormatWatcher;
import cn.access.group.android_all_banks_pos.Utilities.NumberTextWatcherForThousand;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Validation;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSale;
import cn.access.group.android_all_banks_pos.adapter.aidListAdapter;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.dao.TransactionDetailDao;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author muhammad.humayun
 * on 8/20/2019.
 * this is a view call which is reponsible for starting the hardware service and invoking the presenter of this view
 */
@SuppressLint("ValidFragment")
public class SaleAmountAdjustFragment extends Fragment {

    @BindView(R.id.headingAdjustsale)
    TextView headingAdjustsale;

    @BindView(R.id.saleAdjustList)
    LinearLayout saleAdjustList;

    @BindView(R.id.saleAdjustCardNo)
    TextView saleAdjustCardNo;

    @BindView(R.id.saleAdjustTxnType)
    TextView saleAdjustTxnType;

    @BindView(R.id.saleAdjustInvoiceNo)
    TextView saleAdjustInvoiceNo;


    @BindView(R.id.saleAdjustAuthID)
    TextView saleAdjustAuthID;

    @BindView(R.id.saleAdjustAmount)
    TextView saleAdjustAmount;

    @BindView(R.id.saleAdjustTipAmount)
    TextView saleAdjustTipAmount;

    @BindView(R.id.saleAdjustButton)
    Button saleAdjustButton;

    @BindView(R.id.saleAdjustCancelButton)
    Button saleAdjustCancelButton;

    @BindView(R.id.textLayoutAdjustsale)
    LinearLayout textLayoutAdjustsale;

    @BindView(R.id.textScreenAdjustsale)
    TextView textScreenAdjustsale;

    @BindView(R.id.amountFieldAdjustsale)
    TextView amountFieldAdjustsale;

    @BindView(R.id.keyboard_layout_Adjust_sale)
    LinearLayout keyboard_layout_Adjust_sale;




    String transactionAmount;
    TransactionDetail td;
    Unbinder unbinder;
    IBeeper iBeeper;
    IPrinter printer;
    public static IDeviceService idevice;
    boolean isSucc;
    AssetManager assetManager;
    private static final String TAG = "EMVDemo";
    Intent intent = new Intent();
    Dialog alertDialog;
    String buttonClicked;
    public String current = "";
    public boolean bool;
    MyKeyListener myKeyListener;
    private WeakReference<Context> mContext;
    public boolean isFallback;
    String amount = "";
    String invoice = "";
    TransPrinter transPrinter;
    private ReceiptPrinter receiptPrinter;

    @SuppressLint("ValidFragment")
    public SaleAmountAdjustFragment(TransactionDetail td , String invoice) {
        this.invoice = invoice;
        this.td = td;
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.DEBUG_LOG(TAG,"+ FragmentTipAdjust:onCreateView() +");
        //Repository repo = new Repository(AppDatabase.getAppDatabase(Context));
        View rootView = inflater.inflate(R.layout.fragment_sale_amount_adjust, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        saleAdjustCardNo.setText(DataFormatterUtil.maskCardNo(td.getCardNo()));
        saleAdjustInvoiceNo.setText(td.getInvoiceNo());
        saleAdjustTxnType.setText(td.getTxnType());
        saleAdjustAuthID.setText(td.getAuthorizationIdentificationResponseCode());
        saleAdjustAmount.setText((new DecimalFormat("0.00").format(Double.parseDouble(td.getAmount()))));
        if(Constants.TIP_ENABLED.equalsIgnoreCase("Y")){
            saleAdjustTipAmount.setVisibility(View.VISIBLE);
            saleAdjustTipAmount.setText(td.getTipAmount());
        }
        receiptPrinter = new ReceiptPrinter(handler);

        //keyboard_layout_Adjust_tip.setVisibility(View.VISIBLE);
        myKeyListener = new MyKeyListener(amountFieldAdjustsale);
        rootView.findViewById(R.id.key1).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key2).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key3).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key4).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key5).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key6).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key7).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key8).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key9).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key0).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.keyclr).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key_delete).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key_confirm).setOnClickListener(myKeyListener);
        return rootView;
    }


    @OnClick({R.id.saleAdjustButton,R.id.saleAdjustCancelButton})
    @Nullable

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.saleAdjustButton:
                saleAdjustList.setVisibility(View.GONE);
                saleAdjustCancelButton.setVisibility(View.GONE);
                saleAdjustButton.setVisibility(View.GONE);
                keyboard_layout_Adjust_sale.setVisibility(View.VISIBLE);
                textLayoutAdjustsale.setVisibility(View.VISIBLE);
                headingAdjustsale.setText("Enter Amount");
                break;

            case R.id.saleAdjustCancelButton:
                DashboardContainer.backStack();
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        assetManager = getResources().getAssets();
        alertDialog=new Dialog(context);
        mContext = new WeakReference<>(context);

        Utility.DEBUG_LOG(TAG,"PaymentAmountFragment:onAttach()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null)
            getActivity().unbindService(conn);
    }


    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private final ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            new CountDownTimer(01, 01) {
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);
                    try {
                        Utility.DEBUG_LOG("onServiceConnected","onServiceConnected--Control here..");
                        Utility.DEBUG_LOG("terminal serial", Constants.TERMINAL_SERIAL );
                        iBeeper = idevice.getBeeper();
                        printer = idevice.getPrinter();
                        TransPrinter.initialize(printer);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }

                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    /**
     * this method animates the view, and executes the purchase function of the presenter which takes amount as parameter
     */

    // mmasood: countdown timer in sales screen


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        buttonClicked = "";
        // conn = null;
        unbinder.unbind();

    }



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Utility.DEBUG_LOG(TAG,"+ PaymentFragment:Handler:handleMessage()+ ");
            Utility.DEBUG_LOG(TAG,"before getData");

            String str = msg.getData().toString();
            Utility.DEBUG_LOG(TAG,"Message:" + str);
            super.handleMessage(msg);
//            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };

    class MyKeyListener implements View.OnClickListener{
        StringBuilder stringBuilder = new StringBuilder("");
        TextView selectedAmount;

        public MyKeyListener(TextView selectedAmount) {
            this.selectedAmount = selectedAmount;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.key0:
                    if (stringBuilder.length() >= 0 & stringBuilder.length() < 6)
                    {
                        stringBuilder.append("0");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key1:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("1");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key2:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("2");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key3:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("3");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key4:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("4");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key5:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("5");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key6:
                    if (stringBuilder.length() < 6){
                        stringBuilder.append("6");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key7:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("7");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key8:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("8");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key9:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("9");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.keyclr:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.delete(0,stringBuilder.length());
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_delete:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_confirm:
                    DecimalFormat df= new DecimalFormat("0.00");
                    amount = amountFieldAdjustsale.getText().toString();
                    double txnAmount = Double.parseDouble(amount);
                     if(td.getTransactionAmount()!=null && td.getTipAmount()!=null) {

                         double tipAmount = Double.parseDouble(td.getTipAmount());
                         double totalAmount = tipAmount + txnAmount;
                         td.setTransactionAmount(df.format(txnAmount));
                         td.setAmount(String.valueOf(df.format(totalAmount)));

                         Utility.DEBUG_LOG("saleadjustTipAmount", String.valueOf(tipAmount));
                         Utility.DEBUG_LOG("saleadjusttxnAmount", String.valueOf(txnAmount));
                         Utility.DEBUG_LOG("saleadjusttotalAmount", String.valueOf(totalAmount));

                         td.setCancelTxnType(td.getTxnType());
                         td.setTxnType("ADJUST");
                         td.setIsAdvice(true);
                         new Repository(mContext.get()).updateTransaction(amount, invoice, td.getTxnType(), td.getAmount(), td.getIsAdvice());
                         transPrinter = new PrintRecpSale(mContext.get());
                         transPrinter.initializeData(td,"",false,"MERCAHNT COPY");
                         transPrinter.print();
                         final Handler handler = new Handler();
                         handler.postDelayed(new Runnable() {
                             @Override
                             public void run() {
                               //  receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "MERCAHNT COPY", td.getTxnType(), "", "", false);
                                 Utility.DEBUG_LOG("transaction", "found");
                                 DialogUtil.confirmDialogCustomer(mContext.get(), "Customer Copy", "Print Customer Copy", "", sweetAlertDialog1 -> {
                                     receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "CUSTOMER COPY", td.getTxnType(),"","",false);

                                     //  receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "Customer Copy", td.getTxnType(), "", "", false);
                                     sweetAlertDialog1.dismissWithAnimation();
                                     DashboardContainer.backStack();
                                 });
                                 Utility.DEBUG_LOG("Handler", "Running Handler");
                             }
                         }, 1000);
                     }
                     else{
                         td.setAmount(df.format(txnAmount));
                         td.setCancelTxnType(td.getTxnType());
                         td.setTxnType("ADJUST");
                         td.setIsAdvice(true);
                         new Repository(mContext.get()).updateTransaction(amount, invoice, td.getTxnType(), td.getAmount(), td.getIsAdvice());
                         receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "CUSTOMER COPY", td.getTxnType(),"","",false);
                         ;
                         final Handler handler = new Handler();
                         handler.postDelayed(new Runnable() {
                             @Override
                             public void run() {
                                 //receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "MERCAHNT COPY", td.getTxnType(), "", "", false);
                                 Utility.DEBUG_LOG("transaction", "found");
                                 DialogUtil.confirmDialogCustomer(mContext.get(), "Customer Copy", "Print Customer Copy", "", sweetAlertDialog1 -> {
                                     //receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "Customer Copy", td.getTxnType(), "", "", false);

                                     receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "CUSTOMER COPY", td.getTxnType(),"","",false);


                                     sweetAlertDialog1.dismissWithAnimation();
                                     DashboardContainer.backStack();
                                 });
                                 Utility.DEBUG_LOG("Handler", "Running Handler");
                             }
                         }, 1000);
                     }
                    break;
            }
            if (stringBuilder.length() > 0) {
                double num = Double.parseDouble(stringBuilder.toString());
                selectedAmount.setText(big2(num / 100));
            } else {
                selectedAmount.setText("0.00");
            }
        }
        private String big2(double d) {
            DecimalFormat format = new DecimalFormat("0.00");
            return format.format(d);
        }
    }

    public void setTransactionAmount(String transactionAmount)
    {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionAmount()
    {
        return transactionAmount;
    }

    public boolean getIsFallback()
    {
        return isFallback;
    }



}


