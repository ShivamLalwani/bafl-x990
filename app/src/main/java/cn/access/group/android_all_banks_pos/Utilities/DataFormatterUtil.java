package cn.access.group.android_all_banks_pos.Utilities;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Range;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

/**
 * @author muhammad.humayun
 * on 8/22/2019.
 */
/*public class DataFormatterUtil {

    public static String formattedAmount(String amount){
        float totalAmountF = Integer.parseInt(amount) / (100.0f);
        amount =  String.format(Locale.getDefault(),"%.2f", totalAmountF);
        return amount;
    }*/ ///Shivam's code updated on 15Jan202021 for Report amount field

    public class DataFormatterUtil {

        public static String TAG = "DataFormatterUtil";
        public static String formattedAmount(String amount){
            Utility.DEBUG_LOG(TAG,"+ formattedAmount +");
            if ( amount == null )
            {
                Utility.DEBUG_LOG(TAG,"Null amount, return null");
                return null;
            }
            if ( amount.indexOf(".") > 0 ) // decimal found
            {
                Utility.DEBUG_LOG(TAG,"Decimal found, assign as is..");
            }
            else
            {
                Utility.DEBUG_LOG(TAG,"BCD encoded amount, format it...");
                Utility.DEBUG_LOG(TAG, "Amount old:" + amount);
                double totalAmountF = Double.parseDouble(amount) / (100.0f);
                amount =  String.format(Locale.getDefault(),"%.2f", totalAmountF);
                Utility.DEBUG_LOG(TAG, "Amount new:" + amount);
            }
            return amount;
        }

        public static String decimalToBcdAmount(String cleanamount)
        {
            Utility.DEBUG_LOG(TAG,"+ decimalToBcdAmount +");
            String cleanamount1 = cleanamount.replaceAll("[*,]", "");
            Double c = Double.parseDouble(cleanamount1);
            Utility.DEBUG_LOG(TAG,"c:" + String.valueOf(c));
            cleanamount1 = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(cleanamount1) * 100));
            Utility.DEBUG_LOG(TAG, "cleanAmount1:"+ cleanamount1);
            return cleanamount1;
        }


    public static String formattedTime(@NonNull String time){
        String hourStr = time.substring(0,2);
        String minuteStr = time.substring(2,4);
        time= hourStr+":"+minuteStr;
        return time;
    }

    public static String formattedTimeWithSec(@NonNull String time){
        String hourStr = time.substring(0,2);
        String minuteStr = time.substring(2,4);
        String secoundStr = time.substring(4,6);
        time= hourStr+":"+minuteStr+":"+secoundStr;
        return time;
    }
    public static String formattedDate(@NonNull String date){
        String monthStr = date.substring(0,2);
        String dayStr = date.substring(2,2+2);
        int iMonth = Integer.parseInt(monthStr);
        int iDay = Integer.parseInt(dayStr);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, iMonth-1);
        cal.set(Calendar.DAY_OF_MONTH, iDay );
        Date dt = cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy",Locale.getDefault());
        return df.format(dt);


    }
    public static String formattedDateECR(@NonNull String date){
//        String monthStr = date.substring(0,2);
//        String dayStr = date.substring(2,2+2);
//        int iMonth = Integer.parseInt(monthStr);
//        int iDay = Integer.parseInt(dayStr);
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.MONTH, iMonth-1);
//        cal.set(Calendar.DAY_OF_MONTH, iDay );
//        Date dt = cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("MMM/dd,/yyyy",Locale.getDefault());
        String monthStr = date.substring(0,2);
        String dayStr = date.substring(2,2+2);
        String date1 = dayStr + "/" + monthStr + "/" + "2021";
        Utility.DEBUG_LOG("SL + date" , date);
        return date1;

    }

    @NonNull
    public static String systemTime(){
        return String.format(Locale.getDefault(),"%02d%02d%02d",
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                Calendar.getInstance().get(Calendar.MINUTE),
                Calendar.getInstance().get(Calendar.SECOND));
    }

    @NonNull
    public static String systemDate(){
        return String.format(Locale.getDefault(),"%02d%02d", Calendar.getInstance().get(Calendar.MONTH)+1,
                Calendar.getInstance().get(Calendar.DATE));
    }


    @NonNull
    public static String systemDateAndTime(){
        return  systemDate()+systemTime();
    }


    public static String systemYear(){
       return String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2,4);
    }
    /**
     *
     * @param transactionDetail
     * @return field 90
     * THIS FIELDIS ONLY NEEDED FOR UPI VOID
     */
    @NonNull
    public static String generateField90(TransactionDetail transactionDetail){
        String aiic = appendZerosToEleven(transactionDetail.getAcquiringInstitutionIdentificationCode());
        String fiic = appendZerosToEleven(transactionDetail.getForwardingInstitutionIdentificationCode());

        return transactionDetail.getMessageType()+transactionDetail.getStan()+transactionDetail.getTxnDate()+
                transactionDetail.getTxnTime()+ aiic+fiic;
    }
    @NonNull
    public static String generateFieldReversal90(Reversal transactionDetail){
        String aiic = appendZerosToEleven(transactionDetail.getAcquiringInstitutionIdentificationCode());
        String fiic = appendZerosToEleven(transactionDetail.getForwardingInstitutionIdentificationCode());

        return transactionDetail.getMessageType()+transactionDetail.getStan()+transactionDetail.getTxnDate()+
                transactionDetail.getTxnTime()+ aiic+fiic;
    }

    /**
     *
     * @param data
     * @return appended field
     */
    private static String appendZerosToEleven(String data){
        int len;
        if(data.length()<11){
            len = data.length();
            int zeros = 11-len;
            StringBuilder z= new StringBuilder();
            for(int i = 1;i<= zeros;i++)
            {
                z.append("0");
            }
            data= z+data;
        }
        return data;
    }

    public static String insertCharacterForEveryNDistance(int distance, String original, char c){
        StringBuilder sb = new StringBuilder();
        char[] charArrayOfOriginal = original.toCharArray();
        for(int ch = 0 ; ch < charArrayOfOriginal.length ; ch++){
            if(ch % distance == 0)
                sb.append(c).append(charArrayOfOriginal[ch]);
            else
                sb.append(charArrayOfOriginal[ch]);
        }
        return sb.toString();
    }

    public static String hexToBinary(String hex) {
        int len = hex.length() * 4;
        String bin = new BigInteger(hex, 16).toString(2);

        //left pad the string result with 0s if converting to BigInteger removes them.
        if(bin.length() < len){
            int diff = len - bin.length();
            String pad = "";
            for(int i = 0; i < diff; ++i){
                pad = pad.concat("0");
            }
            bin = pad.concat(bin);
        }
        return bin;
    }



    public static String hexCharToBin(char c)
    {
        switch (Character.toUpperCase(c))
        {
            case '0': return "0000";
            case '1': return "0001";
            case '2': return "0010";
            case '3': return "0011";
            case '4': return "0100";
            case '5': return "0101";
            case '6': return "0110";
            case '7': return "0111";
            case '8': return "1000";
            case '9': return "1001";
            case 'A': return "1010";
            case 'B': return "1011";
            case 'C': return "1100";
            case 'D': return "1101";
            case 'E': return "1110";
            case 'F': return "1111";
            default: return  "0000";
        }
    }


    public static String setCardTypeScheme(int cardno){
        String cardType = "UNKNOWN";

        if (cardno > 400000 && cardno < 499999) {
            cardType="VISA";
        } else if (cardno > 222100 && cardno < 272099) {
            cardType="MASTERCARD"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
        }
        else if (cardno > 510000 && cardno < 559999) {
            cardType="MASTERCARD"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
        }
        else if (cardno > 350000 && cardno < 359999) {
            cardType="JCB";
        }
        else if (cardno > 830000 && cardno < 849999) {
            cardType="JCB";
        }
        else if (cardno > 340000 && cardno < 349999) {
            cardType="AMEX";
        }
        else if (cardno > 370000 && cardno < 379999) {
            cardType="AMEX";
        }
        else if (cardno > 620000 && cardno < 629999) {
            cardType="UPI"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
        }
        else if (cardno > 680000 && cardno < 699999) {
            cardType="UPI"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
        }
        else if (cardno > 810000 && cardno < 819999) {
            cardType="UPI"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
        }
        else if (cardno > 220000 && cardno < 229999) {
            cardType="PAYPAK";
        }
        return cardType;
    }


    public static String maskCardNo(String cardNo) {
        String lastFour = cardNo.substring(cardNo.length() - 4);
        String firstFour = cardNo.substring(0, 4);
        String next2 = cardNo.substring(4, 6);
        String stars1 = "****";
        String stars2 = "**";
        return firstFour + "" + next2 + stars2 + "" + stars1 + "" + lastFour;
    }


    public static String getNewStan(){
        String stan=String.format(Locale.getDefault(),"%06d", SharedPref.read("stan"));
        Utility.DEBUG_LOG("STAN","stan string:"+stan);
        int stan1 =  SharedPref.read("stan");
        Utility.DEBUG_LOG("STAN","stan integer:"+stan1);
        stan1=stan1+1;
        Utility.DEBUG_LOG("STAN","stan int after increment:"+stan1);
        SharedPref.write("stan",stan1);
        return stan;
//        return systemTime();
    }
}
