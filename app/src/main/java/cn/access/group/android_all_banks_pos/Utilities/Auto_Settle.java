package cn.access.group.android_all_banks_pos.Utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.presenter.SettlementPresenter;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import cn.access.group.android_all_banks_pos.viewfragments.SettlementFragment;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.autoSettleTask;


public class Auto_Settle {


    public static void performSettle() {

        if (autoSettleTask) {

//            autoSettleTask = false;
            HomeFragment.autoSettleTaskStop=false;
            SettlementFragment settlementFragment = (SettlementFragment)
                    DashboardContainer.fragmentManager.findFragmentByTag("settlement fragment");
            if(settlementFragment!=null){
                Utility.DEBUG_LOG("Auto_Settle","not null condition");
                MainApplication.hideProgressDialog();
                DashboardContainer.switchFragmentWithBackStack(settlementFragment, "settlement fragment");
            }else {
                Utility.DEBUG_LOG("Auto_Settle","null condition");
                settlementFragment = new SettlementFragment();
                MainApplication.hideProgressDialog();
                DashboardContainer.switchFragmentWithBackStack(settlementFragment, "settlement fragment");
            }
           // sr.settlementstart();
        }
    }


}
