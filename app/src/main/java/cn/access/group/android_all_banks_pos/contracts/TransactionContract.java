package cn.access.group.android_all_banks_pos.contracts;


import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

/**
 * @author muhammad.humayun
 * on 8/22/2019.
 */
public interface TransactionContract {
    void toastShow(String str);
    void showProgress(String title);
    void hideProgress();
    void transactionFailed(String title ,String error);


    void transactionSuccess(String content);
    void transactionSuccessCustomer(String content, TransactionDetail transactionDetail,String copy, String txnType, String balance, boolean redeemCheck);
    void rocEmpty(String content, TransactionDetail transactionDetail, String copy, String txnType);
    boolean last4Digits(String text);
    boolean last4Digits2(TransactionDetail transactionDetail, String text);
    boolean panManualEntry(TransactionDetail transactionDetail, String text);
}
