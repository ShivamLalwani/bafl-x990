package cn.access.group.android_all_banks_pos.Utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.RemoteException;

import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.viewfragments.PaymentAmountFragment;
import cn.access.group.android_all_banks_pos.viewfragments.SettlementFragment;
import cn.access.group.android_all_banks_pos.viewfragments.VoidTransactionFragment;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.CARD_AUTH_METHOD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ecrOfflineCounter;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ecrRefNo;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.isEcrReq;

/**
 * Created by Shivam Lalwani on 2021/01/21
 */
public class ECR {
    public static String TAG = "ECR";
    public static boolean isCancelTrans = false;
    static String ECRTxn = "null";
    static ISerialPort serialPortOriginal;
    public static final char STX = 02;
    public static final char ETX = 03;

    public static void initECR(ISerialPort serialPort) throws RemoteException {
        serialPort.init(115200, 0, 8);
        serialPort.open();
        serialPortOriginal = serialPort;
    }

    public static void ECRMsgWrite(String msg) {
        String txnResp = STX + msg + ETX;

        try {
            serialPortOriginal.write(txnResp.getBytes(), 3000);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public static void ECRRespParser() {
        if (ECRTxn.equals("SALE")) {
            String txnType;
            ECRTxn = "null";
            Constants.isEcrReq = true;
            ecrOfflineCounter=0;
            txnType = "SALE";
            SharedPref.write("button_fragment", txnType);
            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");

        }
        if (ECRTxn.equals("REFUND")) {
            String txnType;
            ECRTxn = "null";
            Constants.isEcrReq = true;
            ecrOfflineCounter=0;
            txnType = "REFUND";
            SharedPref.write("button_fragment", txnType);
            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");

        }
        if (ECRTxn.equals("VOID")) {
            String txnType;
            ECRTxn = "null";
            Constants.isEcrReq = true;
            ecrOfflineCounter=0;
            txnType = "VOID";
            SharedPref.write("button_fragment", txnType);
           // DashboardContainer.switchFragmentWithBackStack(new VoidTransactionFragment(), "payment fragment");
            DashboardContainer.switchFragmentWithBackStack(new VoidTransactionFragment(), "void fragment");

        }
        if (ECRTxn.equals("SETTLEMENT") ) {
            ECRTxn = "null";
            Utility.DEBUG_LOG(TAG, "before new SettlementFragment");
            Constants.isEcrReq = true;
            ecrOfflineCounter=0;
            SettlementFragment sr = new SettlementFragment();
            DashboardContainer.switchFragmentWithBackStack(sr, "settlement fragment");

        }

    }


    @SuppressLint("InvalidWakeLockTag")
    public static void ecrRead(ISerialPort serialPort, Context mcontext) throws RemoteException {
        byte[] result = new byte[100];
        String s;
        Context context = mcontext;

        ///setShowWhenLocked(true);

//        Utility.DEBUG_LOG("SL", "Loop");
        int length = 0;
        try {
            length = serialPort.read(result, 1024, 500);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (length > 0) {
            s = new String(result);
            int arr = (int) s.charAt(0);
            Utility.DEBUG_LOG("ECR Received data", s.substring(0, length) + " Length = " + length);

            if (Constants.ecrType.equals("M")) {

                if (s.charAt(0) == STX && s.charAt(length - 2) == ETX) {
                    Utility.DEBUG_LOG("SL ECR", "ECR MCDONALD");

                    if (s.substring(1, 13).equals("IsConnected?")) {
                        Utility.DEBUG_LOG("ECR", "**********  ECR Connection test **********");
                        String txnResp = STX + "OK" + ETX;
                        serialPortOriginal.write(txnResp.getBytes(), 3000);
                    } else if (s.substring(13, 18).equals("Sale&")) {
                        Utility.DEBUG_LOG("ECR", "**********  INIT ECR SALE **********");

                        Constants.ecrRefNo = s.substring(18, 26);
                        Constants.ecrAmount = s.substring(26, 38);
                        ECRTxn = "SALE";
                        Constants.isEcrReq = true;
                        String txnResp = STX + "OK" + ETX;
                        serialPortOriginal.write(txnResp.getBytes(), 3000);
                        isCancelTrans = true;
                    } else if (s.substring(13, 18).equals("Void&")) {
                        Utility.DEBUG_LOG("ECR", "**********  INIT ECR VOID **********");

                        Constants.ecrRefNo = s.substring(18, 26);
                        Constants.ecrAmount = s.substring(26, 38);
                        Utility.DEBUG_LOG("ECR", "ecrRefNo" + ecrRefNo);
                        ECRTxn = "VOID";
                        String txnResp = STX + "OK" + ETX;
                        serialPortOriginal.write(txnResp.getBytes(), 3000);
                        Constants.isEcrReq = true;
                        isCancelTrans = true;
                    } else if (s.substring(13, 23).equals("Settlement")) {
                        Utility.DEBUG_LOG("ECR", "**********  INIT ECR SETTLEMENT **********");

                        //Constants.ecrRefNo = s.substring(18, 26);
                        //Constants.ecrAmount = s.substring(26, 38);
                        //Utility.DEBUG_LOG("ECR", "ecrRefNo" + ecrRefNo);
                        ECRTxn = "SETTLEMENT";
                        String txnResp = STX + "OK" + ETX;
                        serialPortOriginal.write(txnResp.getBytes(), 3000);
                        Constants.isEcrReq = true;
                        isCancelTrans = false;
                    }
                    else if (s.substring(13, 20).equals("Refund&")) {
                        Utility.DEBUG_LOG("ECR", "**********  INIT ECR REFUND **********");

                        Constants.ecrRefNo = s.substring(20, 28);
                        Constants.ecrAmount = s.substring(28, 40);
                        Utility.DEBUG_LOG("ECR", "ecrRefNo" + ecrRefNo);
                        ECRTxn = "REFUND";
                        String txnResp = STX + "OK" + ETX;
                        serialPortOriginal.write(txnResp.getBytes(), 3000);
                        Constants.isEcrReq = true;
                        isCancelTrans = false;
                    }
                }

            } else if (Constants.ecrType.equals("U")) {
                if (s.substring(0, 4).equals("0200")) {
                    Utility.DEBUG_LOG("ECR", "SALE INITIATE");
                    Constants.ecrAmount = s.substring(4, 16);
                    ECRTxn = "SALE";
                    isCancelTrans = true;


                    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    if (!pm.isScreenOn()) {
                        Utility.DEBUG_LOG(TAG, "wake up");
                        PowerManager.WakeLock mWakelock;
                        mWakelock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                PowerManager.SCREEN_DIM_WAKE_LOCK, "target");
                        mWakelock.acquire();
                        mWakelock.release();
                    }

                }
                if (s.substring(0, 4).equals("0500")) {
                    Utility.DEBUG_LOG("ECR", "SETTLEMENT INITIATE");
                    ECRTxn = "SETTLEMENT";
                    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    if (!pm.isScreenOn()) {
                        Utility.DEBUG_LOG(TAG, "wake up");
                        PowerManager.WakeLock mWakelock;
                        mWakelock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP |
                                PowerManager.SCREEN_DIM_WAKE_LOCK, "target");
                        mWakelock.acquire();
                        mWakelock.release();
                    }
                    //ECRRespPrarser(ECRTxn);
                }

            }
        }
    }


    public static void ecrWrite(String txn, String date, String time, String cardNo, String cardEntry, String holderName, String totalAmount, String rrn, String authId, String RespCode, String Resp, String tipAmount, String aid, String tvr, String cardIss) throws RemoteException {
        Utility.DEBUG_LOG("ECR", "write data");
        isCancelTrans = false;
        String txnDetail = null;
        String cvm = null;
       // isEcrReq=false;


        if (txn.equals("VOID")) {
            totalAmount = "-" + totalAmount;
            tipAmount = "-" + tipAmount;
        }
        if (CARD_AUTH_METHOD.equals("ONLINE_PIN") || CARD_AUTH_METHOD.equals("OFFLINE_PIN")) {
            cvm = "PIN";
        } else if (CARD_AUTH_METHOD.equals("NO_CVM")) {
            cvm = "NOCVM";
        } else if (CARD_AUTH_METHOD.equals("SIGNATURE")) {
            cvm = "Signature";
        }
        if (txn.equals("VOID") || txn.equals("SALE")) {

            if (Constants.ecrType.equals("U")) {
                txnDetail = "TxnType = " + txn + "\r\n"
                        + "Date = " + date + "\r\n"
                        + "Time = " + time + "\r\n"
                        + "TID = " + Constants.TERMINAL_ID + "\r\n"
                        + "MID = " + Constants.MERCHANT_ID + "\r\n"
                        + "Batch No. = " + Constants.BATCH_NO + "\r\n"
                        + "Invoice No. = " + Constants.INVOICE_NO + "\r\n"
                        + "Card No. = " + cardNo + "\r\n"
                        + "Card Entry = " + cardEntry + "\r\n"
                        + "Card Holder Name = " + holderName + "\r\n"
                        + "Tip Amount = " + tipAmount + "\r\n"
                        + "Total Amount = " + totalAmount + "\r\n"
                        + "Response Code = " + RespCode + "\r\n"
                        + "Response = " + Resp + "\r\n"
                        + "RRN No. = " + rrn + "\r\n"
                        + "Auth Code = " + authId + "\r\n";
            } else if (Constants.ecrType.equals("M")) {
                txnDetail =
                        // "TxnResp=" + txn + "\r\n"
                            STX + "TxnResp=Date=" + date + "\r\n"
                                + "Time=" + time + "\r\n"
                                + "TID=" + Constants.TERMINAL_ID + "\r\n"
                                + "MID=" + Constants.MERCHANT_ID + "\r\n"
                                + "Txn=" + txn + "\r\n"
                                + "Batch=" + Constants.BATCH_NO + "\r\n"
                                + "Invoice=" + Constants.INVOICE_NO + "\r\n"
                                + "EcrRefNo=" + Constants.ecrRefNo + "\r\n"
                                + "Card=" + cardNo + "\r\n"
                                + "Entry=" + cardEntry + "\r\n"
                                + "Amount=" + totalAmount + "\r\n"
                                + "Currency=" + "PKR" + "\r\n"
                                + "RespCode=" + RespCode + "\r\n"
                                + "RespMsg=" + Resp + "\r\n"
                                + "RRN=" + rrn + "\r\n"
                                + "Auth=" + authId + "\r\n"
                                + "CVM=" + cvm + "\r\n"
                                + "AID=" + aid + "\r\n"
                                + "TVR=" + tvr + "\r\n"
                                //TC
                                + "Card Issuer=" + cardIss + "\r\n"
                                + "CardholderName=" + holderName + "\r\n" + ETX;

            }

            if(isEcrReq) {

                serialPortOriginal.write(txnDetail.getBytes(), 3000);
                isEcrReq=false;
                Utility.DEBUG_LOG(TAG,"RESPONSE ");
            }else if(!isEcrReq && txn.equals("VOID") && ecrOfflineCounter < 11)
            {
                serialPortOriginal.write(txnDetail.getBytes(), 3000);
                ecrOfflineCounter++;
            }
        }
    }

    public static void cancelMsg() throws RemoteException {
        if (isCancelTrans) {
            String txnDetail = "Cancelled\r\n";
            serialPortOriginal.write(txnDetail.getBytes(), 3000);
            isCancelTrans = false;
        }
    }

    public static void ecrWriteSettlement(String date, String time, String saleAmountStr, int saleCount, String voidAmountStr, int voidCount, String cashOutAmountStr, int cashOutCount, String orbitRedeemAmountStr, int orbitRedeemCount, String totalAmount, String totalCount) throws RemoteException {
        Utility.DEBUG_LOG("ECR", "write data for settlement");
        String txnDetail = null;
       //isEcrReq=false;
        SharedPref.write("isSettleRequiured","N");
        if (Constants.ecrType.equals("U")) {
            txnDetail =
                    "Date = " + date + "\r\n"
                            + "Time = " + time + "\r\n"
                            + "TID = " + Constants.TERMINAL_ID + "\r\n"
                            + "MID = " + Constants.MERCHANT_ID + "\r\n"
                            + "Batch No. = " + Constants.BATCH_NO + "\r\n"
                            + "Sale Amount = " + saleAmountStr + "\r\n"
                            + "Sale Count = " + saleCount + "\r\n"
                            + "Void Amount = " + voidAmountStr + "\r\n"
                            + "Void Count = " + voidCount + "\r\n"
                            + "Cashout Amount = " + cashOutAmountStr + "\r\n"
                            + "Cashout Count = " + cashOutCount + "\r\n"
                            + "Orbit Redeem Amount = " + orbitRedeemAmountStr + "\r\n"
                            + "Orbit Redeem Count = " + orbitRedeemCount + "\r\n"
                            + "Total Amount = " + totalAmount + "\r\n"
                            + "Response = " + "GBOK" + "\r\n";
        } else if (Constants.ecrType.equals("M")) {
            txnDetail =
                    STX + "SettlementResp=" + "Date = " + date + "\r\n"
                            + "Time = " + time + "\r\n"
                            + "TID = " + Constants.TERMINAL_ID + "\r\n"
                            + "MID = " + Constants.MERCHANT_ID + "\r\n"
                            + "Txn = Settlement\r\n"
                            + "Batch= " + Constants.BATCH_NO + "\r\n"
                            + "Currency = PKR\r\n"
                            + "Sale Amount = " + saleAmountStr + "\r\n"
                            + "Sale Count = " + saleCount + "\r\n"
                            + "Refund Amount = 0.00\r\n"
                            + "Refund Count = 0\r\n"
                            + "Void Amount = " + voidAmountStr + "\r\n"
                            + "Void Count = " + voidCount + "\r\n"
                            + "Total Amount = " + totalAmount + "\r\n"
                            + "Total Count = " + totalCount + "\r\n"
                            + "Response = " + "Approved" + "\r\n" + ETX;
        }

        if(isEcrReq) {
            serialPortOriginal.write(txnDetail.getBytes(), 3000);
            isEcrReq=false;
        }
    }

}