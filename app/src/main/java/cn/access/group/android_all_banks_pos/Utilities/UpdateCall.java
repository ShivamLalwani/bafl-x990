package cn.access.group.android_all_banks_pos.Utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.vfi.smartpos.system_service.aidl.IAppInstallObserver;
import com.vfi.smartpos.system_service.aidl.ISystemManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.access.group.android_all_banks_pos.BuildConfig;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.ProfileInit;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.viewfragments.UpdateFragment;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AMEX_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLEMENT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLETIME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CHECK_EXPIRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_ETHERNET;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_GPRS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CURRENCY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_VISA;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.EMV_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ENABLE_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FALLBACK_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_3;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_4;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HIGH_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_ICC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_MAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LOW_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANAGER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY_ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_NAME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MOD10;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MSGHEADER_LENHEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.NII;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PIN_BYPASS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PRI_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalPassword;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalTimeout;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalTxnUpload;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUserName;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MAX_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MIN_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_OFFLINE_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SSL_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SUPER_USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SYSTEM_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_SERIAL;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_THRESHOLD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TOPUP_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostIP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostPort;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateCall {

    public static String TAG = "UpdateCall";
    AppDatabase appDatabase;
    Context mContext;
    ISystemManager iSystemManager;
    TerminalConfig terminalConfig = new TerminalConfig();
    JSONObject jsonObject;
    String Checksum = "";
    String tmsUrl = "";
    DownloadNewVersion dn;
    ProgressDialog bar;
    int code = -2;
    ISystemManager systemManager = null;

    public UpdateCall(Context context,ISystemManager iSystemManager) throws JSONException {

        this.mContext = context;
        this.systemManager = iSystemManager;
        dn = new DownloadNewVersion(mContext);
    }

    ////////////////////////////////////////////TMS INITIALIZE////////////////////////////////////////////////
    public void  tmsInitialization(Context context) {

        // request for new version
        mContext = context;

        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = PortalUrl + "/BaflFileAuth?username=bafl.service&password=bafl.service!&vers="+ BuildConfig.VERSION_NAME;
        //showProgress("Connecting...");
        Utility.DEBUG_LOG(TAG, "TMS Connecting...");
        Utility.DEBUG_LOG(TAG, "url: " + url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener < JSONObject > () {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Utility.DEBUG_LOG("ResponseAPI 1", response.toString());
                        try {
                            Utility.DEBUG_LOG("ResponseAPI 1", response.getString("msg"));
                            Utility.DEBUG_LOG("ResponseAPI 1", "running download query");
                            String url = null;
                            //if server has newer version it will give the new url otherwsie exception occours

                            if (response.getString("msg").equals("NO ANY VERSION FOUND.")) {
                                Utility.DEBUG_LOG("response success","NO ANY VERSION FOUND.");
                                //2021 07 02 dialog not showing
                                DialogUtil.infoDialog(mContext,"No Updates Available!");

                                // show no version
                            }
                            else if (response.getString("msg").equals("Exception.")) {
                                Utility.DEBUG_LOG("response success","Exception.");
                                //2021 07 02 dialog not showing
                              //  DialogUtil.errorDialog(mContext,"Error!",response.getString("msg"));

                                // show exception
                            }
                            else {
                                profileDownload();
                                Checksum = response.getString("checksum");
                                tmsUrl = response.getString("url");
                                dn.execute();
                                Utility.DEBUG_LOG("response success",tmsUrl);
                            }

                        } catch (JSONException e) {

                            dn.cancel(true);

                            Toast.makeText(mContext, "ERROR:" + e, Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }

                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getCause();
                        DialogUtil.errorDialog(mContext,"Error", String.valueOf(error));
                        Utility.DEBUG_LOG("Error.ResponseAPI", error.toString());
                    }
                }
        );

        new UpdateFragment.NukeSSLCerts().nuke();
        queue.add(getRequest);
    }

    ////////////////////////////////////////////FILE CREATOR////////////////////////////////////////////////
    public void mCreateAndSaveFile(String params, String mJsonResponse) {
        try {
            Utility.DEBUG_LOG(TAG, "Creating file...");
            FileWriter file = new FileWriter("/sdcard/Download/" + params);
            file.write(mJsonResponse);
            Utility.DEBUG_LOG(TAG, "making json file");
            file.flush();
            file.close();
            Utility.DEBUG_LOG(TAG, "hide Creating file...");
            Utility.DEBUG_LOG(TAG, "profile successfully downloaded");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    ////////////////////////////////////////////CHECKSUM CREATOR////////////////////////////////////////////////
    public String fileToMD5(String filePath) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filePath);
            byte[] buffer = new byte[1024];
            MessageDigest digest = MessageDigest.getInstance("MD5");
            int numRead = 0;
            while (numRead != -1) {
                numRead = inputStream.read(buffer);
                if (numRead > 0)
                    digest.update(buffer, 0, numRead);
            }
            byte [] md5Bytes = digest.digest();
            return convertHashToString(md5Bytes);
        } catch (Exception e) {
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) { }
            }
        }
    }

    private String convertHashToString(byte[] md5Bytes) {
        String returnVal = "";
        for (int i = 0; i < md5Bytes.length; i++) {
            returnVal += Integer.toString(( md5Bytes[i] & 0xff ) + 0x100, 16).substring(1);
        }
        return returnVal;
    }

    ////////////////////////////////////////////Downloading new Version APK////////////////////////////////////////////////
    class DownloadNewVersion extends AsyncTask<String,Integer,Boolean> {

        Context context;

        DownloadNewVersion(Context context){
            this.context = context;
        }

        //show the progress dialog
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bar = new ProgressDialog(context);
            bar.setCancelable(false);

            bar.setMessage("Downloading...");
            bar.setIndeterminate(true);
            bar.setCanceledOnTouchOutside(false);
            bar.show();
        }

        // show progress update
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            bar.setIndeterminate(false);
            bar.setMax(100);
            bar.setProgress(progress[0]);
            String msg = "";
            if(progress[0]>99){
                msg="Finishing... ";
            }else {
                msg="Downloading... "+progress[0]+"%";
            }
            bar.setMessage(msg);
        }
        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            bar.dismiss();
            if(result){
                Toast.makeText(context,"Done!!", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context,"Error: Try Again", Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag = false;
            try {

                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                            }

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                            }
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });

                URL url = new URL(PortalUrl + "/BaflFileDown?username=bafl.service&password=bafl.service!&checksum="+Checksum+"&url="+tmsUrl);
                HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
                c.setUseCaches(false);
                c.setRequestMethod("GET");
                c.addRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");
                c.setDoOutput(false);
                c.connect();


                String PATH = "sdcard/Download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file,"baf.apk");
                if(outputFile.exists()){
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();
                int total_size = 4581692;//size of apk
                byte[] buffer = new byte[1024];
                int len1 = 0;
                int per = 0;
                int downloaded=0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    downloaded +=len1;
                    per = (int) (downloaded * 100 / total_size);
                    publishProgress(per);
                }
                fos.close();
                is.close();
                ProfileAck.downloadAcknowledgement(mContext);

                String s1 = fileToMD5("/sdcard/Download/baf.apk");
                String s2 = s1.toUpperCase();
                Utility.DEBUG_LOG(TAG,"checksum = "+Checksum);
                Utility.DEBUG_LOG(TAG,"checksum1 = "+s2);

                if(s2.equals(Checksum)){
                    installApk();
                    flag = true;
                }
                else {
                    dn.cancel(true);
                    bar.hide();
                    DialogUtil.errorDialog(mContext,"Error","File corrupt");
                    Utility.DEBUG_LOG(TAG,"checksum not matched");
                    flag = false;
                }

            } catch (Exception e) {
                Utility.DEBUG_LOG("TAG", "Update Error: " + e.getMessage());
                dn.cancel(true);
                bar.hide();
                DialogUtil.errorDialog(mContext,"Update Error:" ,e.getMessage());
                e.printStackTrace();
                flag = false;
            }
            return flag;
        }
    }

    //////////////////////////////////////////////INSTALL APK///////////////////////////////////////////////////////
    public void installApk() {

        final int[] counter = {
                0
        };
        Utility.DEBUG_LOG(TAG, "before loop" + code);
        while (code != 0 && counter[0] < 3) {
            Utility.DEBUG_LOG(TAG, "code" + code + "| count" + counter[0]);
            if (systemManager != null) {
                try {
                    //systemManager.installApp("/sdcard/Download/1.0.7.210414-Prod.apk", new IAppInstallObserver.Stub() {
                    systemManager.installApp("/sdcard/Download/baf.apk", new IAppInstallObserver.Stub() {

                        @Override
                        public void onInstallFinished(final String packageName, final int returnCode) throws RemoteException {
                            ((Activity)mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (returnCode == -1) {
                                        Utility.DEBUG_LOG(TAG, packageName + " : " + returnCode);
                                        code = returnCode;
                                    } else {
                                        Utility.DEBUG_LOG(TAG, packageName + " : " + returnCode);
                                        code = returnCode;
                                    }

                                    //Toast.makeText(mContext, packageName + " : " + returnCode, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }, "baf.apk");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            counter[0]++;
        }

    }

    //////////////////////////////////////////////PROFILE DOWNLOAD///////////////////////////////////////////////////////
    public void profileDownload() {
        try {
            Utility.DEBUG_LOG(TAG, "Downloading profile...");
            Utility.DEBUG_LOG(TAG, "+ sendGetRequest +");
            String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
            String currentDate = new SimpleDateFormat("MMddyy", Locale.getDefault()).format(new Date());
            String POSDateTime = currentTime + currentDate;

            RequestQueue queue = Volley.newRequestQueue(mContext);

            String url = PortalUrl +  "/baflserviceprofile?MID="+ MERCHANT_ID +"&TID=" + TERMINAL_SERIAL + "&Model=X990&APPNAME=BAF&SerialNumber="+TERMINAL_SERIAL+"&PosDateTime=" + POSDateTime + "";
            Utility.DEBUG_LOG(TAG, "url: " + url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Utility.DEBUG_LOG(TAG, "+ onResponse +");
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                Utility.DEBUG_LOG(TAG, "response:" + jsonObject.getString("Reponsemessage"));

                                if (jsonObject.getString("Reponsemessage").equals("Not available")) {
                                    Utility.DEBUG_LOG(TAG,jsonObject.getString("Reponsemessage"));
                                    dn.cancel(true);
                                    bar.hide();
                                    DialogUtil.errorDialog(mContext,"Error!","Profile Not Available");

                                } else {
                                    // it will add the response and create the json file

                                    mCreateAndSaveFile("jsonfile.json", response);
                                    // after downloding the profile it will request the acknowledgement
                                    ProfileAck.makeJsonObjReq(mContext, systemManager);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                Utility.DEBUG_LOG("RTSG", "Failed to Parse Json");
                                dn.cancel(true);
                                bar.hide();
                                DialogUtil.errorDialog(mContext,"Error!","Failed to Parse Json");

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext, "Error! :" + error.toString(), Toast.LENGTH_LONG).show();
                    dn.cancel(true);
                    bar.hide();
                    DialogUtil.errorDialog(mContext,"Error!",error.toString());

                }
            }) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            ;
            new GetRequest.NukeSSLCerts().nuke();
            queue.add(stringRequest);
        }catch (Exception e){
            dn.cancel(true);
            bar.hide();
            DialogUtil.errorDialog(mContext,"Error!",e.toString());
            Toast.makeText(mContext, "Error! :" + e, Toast.LENGTH_LONG).show();
        }
    }

    //////////////////////////////////////////////SSL certificate///////////////////////////////////////////////////////
    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
            } catch (Exception e) {}
        }
    }



}

