package cn.access.group.android_all_banks_pos.usecase;

import android.annotation.SuppressLint;
import android.util.Log;
import android.util.SparseArray;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.basic.ISO8583;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.INVOICE_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;

//import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;

/**
 * on 10/15/2019.
 */
public class BatchUploadTransaction {
    //private static final String BATCH_NO = ;
    private List<TransactionDetail> batchUploadTransactionsList;
    private List<SparseArray<String>> data8583 = new ArrayList<>();
    private String TAG = "BatchUploadTransaction";
    private SparseArray<String> tagOfF55 = new SparseArray<>();


    @SuppressLint("CheckResult")
    public BatchUploadTransaction(AppDatabase appDatabase) {
        appDatabase.transactionDetailDao().findByStatusList("approved")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactionDetailList -> {
                    batchUploadTransactionsList = transactionDetailList;
                    Utility.DEBUG_LOG(TAG, "+ BatchUploadTransaction +");
                    for (TransactionDetail obj : batchUploadTransactionsList) {
                        Utility.DEBUG_LOG(TAG, "transaction:" + obj.toString());
                    }
                });

    }

    public SparseArray<String> createTagOf55(TransactionDetail transactionDetail)
    {
        if ( transactionDetail == null )
        {
            SparseArray<String> rv = new SparseArray<String>();
            Utility.DEBUG_LOG(TAG,"Reversal record not found, return");
            return rv;
        }
        return transactionDetail.getTag55();
    }

    public List<TransactionDetail> getbatchUploadTransactionDetails()
    {
        Utility.DEBUG_LOG(TAG, "+ getbatchUploadTransactionDetails +");
        return batchUploadTransactionsList;
    }

    public List<SparseArray<String>> getBatchUploadTransactionsList() {

        Utility.DEBUG_LOG(TAG, "+ getBatchUploadTransactionsList +");
        //+ taha 12-03-2021 below line causing same entry appearing everywhere, moved inside loop
//        SparseArray<String> data8583_u_purchase = new SparseArray<>();
        try {
            if (batchUploadTransactionsList.size() != 0) {
                Utility.DEBUG_LOG(TAG, "batchUploadTransactionsList:" + batchUploadTransactionsList);
                Utility.DEBUG_LOG(TAG, "batchUploadTransactionsList.size:" + batchUploadTransactionsList.size());
                for (int i = 0; i < batchUploadTransactionsList.size(); i++) {
                    StringBuilder processingCode = new StringBuilder(batchUploadTransactionsList.get(i).getProcessingCode());
                    Utility.DEBUG_LOG(TAG, "i:" + i);
                    if (i != batchUploadTransactionsList.size() - 1) {
                        Utility.DEBUG_LOG(TAG, "this is NOT last transaction, change proccessing code last digit to 1");
                        processingCode.setCharAt(5, '1');
                    }
                    Utility.DEBUG_LOG(TAG, "Processing code:" + processingCode.toString());
                    SparseArray<String> data8583_u_purchase = new SparseArray<>();
                    data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.BATCH_UPLOAD_MESSAGE_TYPE);
                    data8583_u_purchase.put(ISO8583u.F_AccountNumber_02, batchUploadTransactionsList.get(i).getCardNo());
                    if(batchUploadTransactionsList.get(i).getTxnType().equals("COMPLETION")){
                        data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03, "000000");
                    }
                    else{
                        data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03, processingCode.toString());
                    }

                    String amount = "";

                    if(batchUploadTransactionsList.get(i).getTxnType().equals("VOID") && (batchUploadTransactionsList.get(i).isCompletion() || batchUploadTransactionsList.get(i).getCancelTxnType().equals("ADJUST"))) {
                        amount = "000000000000";
                    }
                    else {
                        amount = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(batchUploadTransactionsList.get(i).getAmount()) * 100));
                    }
                    Utility.DEBUG_LOG(TAG,"Amount:"+amount);

//            data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03,  batchUploadTransactionsList.get(i).getProcessingCode());
                    data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
                    data8583_u_purchase.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
                    data8583_u_purchase.put(ISO8583u.F_TxTime_12, batchUploadTransactionsList.get(i).getTxnTime());
                    data8583_u_purchase.put(ISO8583u.F_TxDate_13, batchUploadTransactionsList.get(i).getTxnDate());


                    String dateOfExpiry = batchUploadTransactionsList.get(i).getCardexpiry().replaceAll("[\\/]", "");
                    String dateOfExpiry2 = dateOfExpiry.substring(2,4)  + dateOfExpiry.substring(0,2);


                    data8583_u_purchase.put(ISO8583u.F_DateOfExpired_14, dateOfExpiry2);


                    Utility.DEBUG_LOG("MA exp", dateOfExpiry);
                    Utility.DEBUG_LOG("MA exp", dateOfExpiry2);
                 //   Utility.DEBUG_LOG("MA exp", batchUploadTransactionsList.get(i).getCardexpiry());

                    if (batchUploadTransactionsList.get(i).getCardType().equals("Chip")) {
                        data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
                    } else {
                        data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
                    }
                    data8583_u_purchase.put(ISO8583u.F_ApplicationPANSequenceNumber_23, "001");
                    data8583_u_purchase.put(ISO8583u.F_NII_24, Constants.NII);
                    data8583_u_purchase.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
                    data8583_u_purchase.put(ISO8583u.F_RRN_37, batchUploadTransactionsList.get(i).getRrn());
                    data8583_u_purchase.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
                    data8583_u_purchase.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
                    Utility.DEBUG_LOG("SL txntype", batchUploadTransactionsList.get(i).getTxnType());
                    if (TIP_ENABLED.equals("Y")) {
                        if(batchUploadTransactionsList.get(i).getTxnType().equals("VOID") && (batchUploadTransactionsList.get(i).isCompletion() || batchUploadTransactionsList.get(i).getCancelTxnType().equals("ADJUST"))) {
                            data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, "000000000000");
                        }else {
                            if( batchUploadTransactionsList.get(i).getTxnType().equals("ADJUST") || batchUploadTransactionsList.get(i).getTxnType().equals("SALE") || (batchUploadTransactionsList.get(i).getTxnType().equals("VOID") && batchUploadTransactionsList.get(i).getCancelTxnType().equals("SALE"))) {
                                data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(batchUploadTransactionsList.get(i).getTipAmount()) * 100)));
                            }
                        }
                    }


//            SparseArray<String> tag55ForBatchUpload = batchUploadTransactionsList.get(i).getTag55();
//            if (tag55ForBatchUpload != null) {
//                ISO8583u iso8583u = new ISO8583u();
//                byte[] packet = null;
//                for (int j = 0; j < tag55ForBatchUpload.size(); j++) {
//                    int tag = tag55ForBatchUpload.keyAt(j);
//                    String value = tag55ForBatchUpload.valueAt(j);
//                    if (value.length() > 0) {
//                        byte[] tmp = iso8583u.appendF55(tag, value);
//                        if (tmp == null) {
//                            Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
//                        } else {
//                            Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
//                        }
//                    }
//                }
//                packet = iso8583u.makePacket(tag55ForBatchUpload, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
//                String str = new String(packet, StandardCharsets.UTF_8); // for UTF-8 encoding
//                Utility.DEBUG_LOG(TAG,"F55 packet:"+str);
//            }

                    if (batchUploadTransactionsList.get(i).getTxnType().equals("ADJUST")) {
                        data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, "0220" + batchUploadTransactionsList.get(i).getInvoiceNo() + batchUploadTransactionsList.get(i).getRrn());

                    } else {
                        data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, "0200" + batchUploadTransactionsList.get(i).getInvoiceNo() + batchUploadTransactionsList.get(i).getRrn());
                    }
                    data8583_u_purchase.put(ISO8583u.F_ServiceCode_61, "01");
                    // data8583_u_purchase.put(ISO8583u.F_55, String.valueOf(batchUploadTransactionsList.get(i).getTag55()));


                    //Constants cn = new Constants();
                    data8583_u_purchase.put(ISO8583u.F_KeyExchange_62, INVOICE_NO);//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
                    if (Constants.HOST_INDEX_FROM_DB == 0) {
                        data8583_u_purchase.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38, batchUploadTransactionsList.get(i).getAuthorizationIdentificationResponseCode());
                        data8583_u_purchase.put(ISO8583u.F_KeyExchange_62, batchUploadTransactionsList.get(i).getInvoiceNo());
                        ////////////////////////////////////////////////////////msgtype+transstan............................+12spaces...... 22total length
//              + taha 01-04-2012 make this in right format
//                data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, "0200"+ batchUploadTransactionsList.get(i).getStan()+"            ");
//                String procCode="0200";
//                String batchNumber=batchUploadTransactionsList.get(i).getBatchNo();
//                String rrn=batchUploadTransactionsList.get(i).getRrn();
//                if ( batchUploadTransactionsList.get(i).getStatus().equals("approved"))
//                {
//                    if ( false ) //TODO adjust processing
//                    {
//                        if (batchUploadTransactionsList.get(i).getIsVoid())
//                        {
//                            procCode = "0200";
//                        }
//                        else
//                        {
//                            procCode = "0220";
//                        }
//                    }
//                    else
//                    {
//                        procCode = "0200";
//                    }
//                }
//                else
//                {
//                    if( batchUploadTransactionsList.get(i).getIsVoid() )
//                    {
//                        procCode = "0200";
//                    }
//                    else
//                    {
//                        procCode = "0220";
//                    }
//                }
//                Utility.DEBUG_LOG(TAG,"procCode:"+procCode);
//                Utility.DEBUG_LOG(TAG,"batchNumber:"+batchNumber);
//                Utility.DEBUG_LOG(TAG,"rrn:"+rrn);
//                data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, procCode + batchNumber + rrn);
                    }

                    data8583.add(data8583_u_purchase);
                }
            } else {
                Utility.DEBUG_LOG(TAG, "batch upload problem");
            }
        }catch (Exception e){
            Utility.DEBUG_LOG(TAG, "batch upload problem");
            e.printStackTrace();
        }
        return data8583;
    }


    public SparseArray<String> getTopTransactionInStringFormat() {

        Utility.DEBUG_LOG(TAG, "+ getTopTransactionInStringFormat +");
        SparseArray<String> data8583_u_purchase = null;
        if (batchUploadTransactionsList.size() == 0) {
            Utility.DEBUG_LOG(TAG, "No record found");
            return data8583_u_purchase;
        }
        data8583_u_purchase = new SparseArray<>();
        data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.BATCH_UPLOAD_MESSAGE_TYPE);
        data8583_u_purchase.put(ISO8583u.F_AccountNumber_02, batchUploadTransactionsList.get(0).getCardNo());
        data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03, batchUploadTransactionsList.get(0).getProcessingCode());
        String amount = String.format(Locale.getDefault(), "%012d", (int) (Double.parseDouble(batchUploadTransactionsList.get(0).getAmount()) * 100));
        data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
        data8583_u_purchase.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_u_purchase.put(ISO8583u.F_TxTime_12, batchUploadTransactionsList.get(0).getTxnTime());
        data8583_u_purchase.put(ISO8583u.F_TxDate_13, batchUploadTransactionsList.get(0).getTxnDate());

        String dateOfExpiry = batchUploadTransactionsList.get(0).getCardexpiry().replaceAll("[\\/]", "");
        String dateOfExpiry2 = dateOfExpiry.substring(2,4)  + dateOfExpiry.substring(0,2);


        data8583_u_purchase.put(ISO8583u.F_DateOfExpired_14, dateOfExpiry2);


        Utility.DEBUG_LOG("MA exp", dateOfExpiry);
        Utility.DEBUG_LOG("MA exp", dateOfExpiry2);
        //14
//            if(batchUploadTransactionsList.get(0).getCardType().equals("chip")){
//                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
//            }
//            else {
//                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
//            }
        data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, batchUploadTransactionsList.get(0).getPosEntryMode());
        data8583_u_purchase.put(ISO8583u.F_NII_24, Constants.NII);
        data8583_u_purchase.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        Utility.DEBUG_LOG(TAG, "before put F_Track_2_Data_35 advice string");
        data8583_u_purchase.put(ISO8583u.F_Track_2_Data_35, batchUploadTransactionsList.get(0).getTrack2());
        data8583_u_purchase.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38, batchUploadTransactionsList.get(0).getAuthorizationIdentificationResponseCode());
        data8583_u_purchase.put(ISO8583u.F_TID_41, batchUploadTransactionsList.get(0).getTId());
        data8583_u_purchase.put(ISO8583u.F_MerchantID_42, batchUploadTransactionsList.get(0).getMId());
//            data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, Constants.RESERVED_NATIONAL);
        //Constants cn = new Constants();
        data8583_u_purchase.put(ISO8583u.F_ServiceCode_61, Constants.FIELD_61);//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
        data8583_u_purchase.put(ISO8583u.F_KeyExchange_62, batchUploadTransactionsList.get(0).getInvoiceNo());//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
//            if(Constants.HOST_INDEX_FROM_DB==0){
//                data8583_u_purchase.put(ISO8583u.F_KeyExchange_62,batchUploadTransactionsList.get(0).getInvoiceNo());
//                ////////////////////////////////////////////////////////msgtype+transstan............................+12spaces...... 22total length
////                data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, "0200"+batchUploadTransactionsList.get(0).getStan()+"            ");
//            }
        return data8583_u_purchase;
    }

    public boolean popTopTransaction() {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "+ popTopTransaction +");
        if (batchUploadTransactionsList.size() == 0) {
            Utility.DEBUG_LOG(TAG, "No record found");
//            rv = false;
        } else {
            TransactionDetail txn = batchUploadTransactionsList.get(0);
            batchUploadTransactionsList.remove(0);
            rv = true;
        }
        Utility.DEBUG_LOG(TAG, "return rv:" + rv);
        return rv;
    }

    public int getBatchUploadTransactionCount() {
        Utility.DEBUG_LOG(TAG, "+ getBatchUploadTransactionCount +");

        int a = batchUploadTransactionsList.size();
        Utility.DEBUG_LOG(TAG, "return :" + a);

        return a;
    }
}
