package cn.access.group.android_all_banks_pos.viewfragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.system_service.aidl.IAppInstallObserver;
import com.vfi.smartpos.system_service.aidl.ISystemManager;
import com.vfi.smartpos.system_service.aidl.settings.ISettingsManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.BuildConfig;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.GetRequest;
import cn.access.group.android_all_banks_pos.Utilities.ProfileAck;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CHECK_EXPIRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_ETHERNET;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_GPRS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_VISA;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HIGH_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_ICC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_MAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LOW_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY_ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MOD10;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PIN_BYPASS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MAX_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MIN_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_OFFLINE_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_SERIAL;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_UPI;

@SuppressLint("ValidFragment")
public class UpdateFragment extends Fragment implements TransactionContract {

    @BindView(R.id.buttonDownload)
    Button buttonDownload;

    @BindView(R.id.buttonInstall)
    Button buttonInstall;

    TransactionDetail td;
    Unbinder unbinder;
    public static IDeviceService idevice;
    private static final String TAG = "UPDATE FRAGMENT";
    Intent intent = new Intent();
    Dialog alertDialog;
    public String current = "";
    public boolean bool;
    private WeakReference<Context> mContext;
    public boolean isFallback;
    int code = -2;
    JSONObject jsonObject;
    ISystemManager systemManager = null;
    final String[] s = {""};
    ISettingsManager settingsManager = null;
    private static final String ACTION = "com.vfi.smartpos.system_service";
    private static final String PACKAGE = "com.vfi.smartpos.system_service";
    private static final String CLASSNAME = "com.vfi.smartpos.system_service.SystemService";
    public static final String ACTION_X9SERVICE = "com.vfi.smartpos.device_service";
    public static final String PACKAGE_X9SERVICE = "com.vfi.smartpos.deviceservice";
    public static final String CLASSNAME_X9SERVICE = "com.verifone.smartpos.service.VerifoneDeviceService";
    String Checksum = "";
    String tmsUrl = "";
    DownloadNewVersion dn;
    ProgressDialog bar;

    @SuppressLint("ValidFragment")
    public UpdateFragment(ISystemManager systemManager) {
        this.systemManager = systemManager;
    }
    public UpdateFragment(){

    }
    public UpdateFragment(Context context){

       // tmsInitialization();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.DEBUG_LOG(TAG, "+ FragmentTipAdjust:onCreateView() +");
        //Repository repo = new Repository(AppDatabase.getAppDatabase(Context));
        View rootView = inflater.inflate(R.layout.fragment_update, container, false);
       // requireActivity().registerReceiver(onDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        unbinder = ButterKnife.bind(this, rootView);
        dn = new DownloadNewVersion(mContext.get());
        return rootView;
    }

    private final ServiceConnection conn1 = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Utility.DEBUG_LOG("systemSetting", "system service bind success");
            systemManager = ISystemManager.Stub.asInterface(iBinder);
            try {
                settingsManager = systemManager.getSettingsManager();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Utility.DEBUG_LOG("systemSetting", "system service disconnected.");
            systemManager = null;
        }
    };





    @OnClick({
            R.id.buttonDownload,
            R.id.buttonInstall
    })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonDownload:

                //deleting old apk
               // deleteAPK();
                //delete old profile and download profile
                //deleteJSONFile();
                //downloading new version

                tmsInitialization();
                buttonDownload.setEnabled(false);
                Utility.DEBUG_LOG("TMS2","s");
               // Utility.DEBUG_LOG("TMS1",result[0]);

                break;

            case R.id.buttonInstall:
                Utility.DEBUG_LOG("TMS2","s");
               // Utility.DEBUG_LOG("TMS2",result[0]);
                //MainApplication.showProgressDialog(mContext.get(),"Installing...");
//                loadJSONFromAsset();
               // installApk();
                break;
        }
    }

    // get request for profile download it will download and create the json file into external memory


    public void deleteJSONFile() {
        String PATH = "/sdcard/Download/jsonfile.json";
        File file = new File(PATH);
        Utility.DEBUG_LOG("TAG", "file path " + file);
        if (file.exists()) {

            file.delete();
//            try {
//               // systemManager.reboot();
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            }

            Utility.DEBUG_LOG(TAG, "file found and deleted");
            // send the request to profile download...

        } else {
            Utility.DEBUG_LOG(TAG, "no such file found");
        }
    }



    public String loadJSONFromAsset() {
        String json = null;

        try {

            InputStream is = new FileInputStream("/sdcard/Download/jsonfile.json");

            int size = is.available();
            Utility.DEBUG_LOG("TAG", String.valueOf(size));

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
            Utility.DEBUG_LOG("TAG", json);

            try {

                jsonObject = new JSONObject(json);
                Utility.DEBUG_LOG("My App", jsonObject.toString());
                TerminalConfig terminalConfig = new TerminalConfig(0, jsonObject.getString("MERCHANTID"),
                        jsonObject.getString("TERMINALID"),
                        jsonObject.getString("MERCHANTNAME"),
                        jsonObject.getString("DESTADDTPDU"),
                        jsonObject.getString("TIPENABLED"),
                        COM_ETHERNET,
                        COM_GPRS,
                        jsonObject.getString("EMVLOGSENABLED"),
                        jsonObject.getString("MARKETSEGMENT"),
                        jsonObject.getString("TIPTHERSHOLD"),
                        jsonObject.getString("ALLOWFALLBACK"),
                        jsonObject.getString("MSGHEADERLENHEX"),
                        jsonObject.getString("ISSSLENABLE"),
                        jsonObject.getString("TOPUPENABLED"),
                        jsonObject.getString("TERMINALMANUALENTRY"),
                        jsonObject.getString("AUTOSETTLE"),
                        jsonObject.getString("EMVLOGSENABLED"),
                        jsonObject.getString("AUTOSETTLETIME"),
                        jsonObject.getString("AMEXID"),
                        PIN_BYPASS,
                        MOD10,
                        CHECK_EXPIRY,
                        MANUAL_ENTRY_ISSUER,
                        LAST_4DIGIT_MAG,
                        LAST_4DIGIT_ICC,
                        ACQUIRER,
                        LOW_BIN,
                        HIGH_BIN,
                        ISSUER,
                        jsonObject.getString("TERMINALCURRENCY1"),
                        jsonObject.getString("PASSWORDUSER"),
                        jsonObject.getString("PASSWORDMANAGEMENT"),
                        jsonObject.getString("PASSWORDUSER"),
                        jsonObject.getString("PASSWORDSUPERUSER"),
                        jsonObject.getString("MERCHANTADDRESS1"),
                        jsonObject.getString("MERCHANTADDRESS2"),
                        jsonObject.getString("MERCHANTADDRESS3"),
                        jsonObject.getString("MERCHANTADDRESS4"),
                        jsonObject.getString("RECEIPTFOOTER1"),
                        jsonObject.getString("RECEIPTFOOTER2"),
                        jsonObject.getString("PRIMARYMEDIUM"),
                        jsonObject.getString("SECONDARYMEDIUM"),
                        jsonObject.getString("TMSIP"),
                        jsonObject.getString("TMSPORT"),
                        BATCH_NO,
                        jsonObject.getString("PRIMARYIP"),
                        jsonObject.getString("PRIMARYPORT"),
                        jsonObject.getString("SECONDARYIP"),
                        jsonObject.getString("SECONDARYPORT"),
                        CVM_LIMIT_VISA,
                        CVM_LIMIT_MC,
                        CVM_LIMIT_UPI,
                        CVM_LIMIT_PAYPAK,
                        CVM_LIMIT_JCB,
                        CVM_LIMIT_AMEX,
                        SALE_OFFLINE_LIMIT,
                        SALE_MIN_AMOUNT_LIMIT,
                        SALE_MAX_AMOUNT_LIMIT,
                        VEPS_LIMIT,
                        VEPS_LIMIT_MC,
                        VEPS_LIMIT_UPI,
                        VEPS_LIMIT_PAYPAK,
                        VEPS_LIMIT_JCB,
                        VEPS_LIMIT_AMEX,
                        jsonObject.getString("PORTALURL"),
                        jsonObject.getString("PORTALUSERNAME"),
                        jsonObject.getString("PORTALPASSWORD"),
                        jsonObject.getString("PORTALTIMEOUT"),
                        jsonObject.getString("PORTALTXNUPLOAD"),
                        jsonObject.getInt("SETTLECOUNTER"),
                        jsonObject.getInt("SETTLERANGE"),
                        jsonObject.getInt("TIPNUMADJUST"),
                        jsonObject.getString("ECRENABLED"),
                        jsonObject.getString("ECRTYPE")
                        );

                Repository repository = new Repository(mContext.get());
                repository.updateTerminalConfiguration(terminalConfig);
                Utility.DEBUG_LOG("My App", "Config updated");
                repository.getTerminalConfiguration();

                //terminalConfig.setAcquirer(obj.getString("ac"));

            } catch (Exception t) {
                t.printStackTrace();
                Utility.DEBUG_LOG("My App", "Could not parse malformed JSON: \"" + json + "\"");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }




//    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context arg0, Intent arg1) {
//            // TODO Auto-generated method stub
//            DownloadManager.Query q = new DownloadManager.Query();
//            q.setFilterById(downloadid);
//            Cursor cursor = downloadManager.query(q);
//            if (cursor.moveToFirst()) {
//                String message = "";
//                int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
//                // if download success show message and install application after 2 sec
//
//                if (status == DownloadManager.STATUS_SUCCESSFUL) {
//                  hideProgress();
//                    showProgress("Installing...");
//                    message = "Download successful";
//                    ProfileAck.downloadAcknowledgement(mContext.get());
//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                          try{
//                              try{
//                                  String s1 = fileToMD5("/sdcard/Download/baf.apk");
//                                  String s2 = s1.toUpperCase();
//                                  Utility.DEBUG_LOG(TAG,"checksum = "+Checksum);
//                                  Utility.DEBUG_LOG(TAG,"checksum1 = "+s2);
//                                  if(s2.equals(Checksum)){
//                                      showProgress("Installing...");
//                                      installApk();
//                                  }
//                                  else {
//                                      DialogUtil.errorDialog(mContext.get(),"Error","File corrupt");
//                                      Utility.DEBUG_LOG(TAG,"checksum not matched");
//                                  }
//                              }
//                              catch (Exception e){
//                                  hideProgress();
//                                  Utility.DEBUG_LOG(TAG,"Error "+e);
//                                  e.printStackTrace();
//                              }
//
//                          }
//                          catch(Exception e){
//                                hideProgress();
//                          }
//
//                        }
//                    }, 3000);
//                }
//                // download failed
//                else if (status == DownloadManager.STATUS_FAILED) {
//                    hideProgress();
//                    String failedReason = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
//                    Utility.DEBUG_LOG(TAG, "Show: " + failedReason);
//                    message = "Download failed";
//                    DialogUtil.errorDialog(mContext.get(),"Error",message);
//                }
//                Utility.DEBUG_LOG(TAG, "Show: " + message);
//            }
//        }
//    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        //requireActivity().unregisterReceiver(onDownloadComplete);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        //isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        alertDialog = new Dialog(context);
        mContext = new WeakReference<>(context);
        Utility.DEBUG_LOG(TAG, "UpdateFragment:onAttach()");
        Intent intent1 = new Intent();
        intent1.setAction(ACTION_X9SERVICE);
        intent1.setPackage(PACKAGE);
        intent1.setClassName(PACKAGE, CLASSNAME);
        context.bindService(intent1, conn1, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
                if (getActivity() != null)
                    getActivity().unbindService(conn1);
    }

    public void deleteAPK() {
        showProgress("Connecting...");
        String PATH = "/sdcard/Download/baf.apk";
        File file = new File(PATH);
        Utility.DEBUG_LOG("TAG", "file path " + file);

        if (file.exists()) {
            file.delete();
            hideProgress();
            Utility.DEBUG_LOG(TAG, "file found and deleted");
        } else {
            hideProgress();
            Utility.DEBUG_LOG(TAG, "no such file found");
        }
    }






    ////////////////////////////////////////////TMS INITIALIZE////////////////////////////////////////////////
    public void  tmsInitialization() {
        // request for new version

        RequestQueue queue = Volley.newRequestQueue(mContext.get());
        String url = PortalUrl + "/BaflFileAuth?username=bafl.service&password=bafl.service!&vers="+BuildConfig.VERSION_NAME;
        //showProgress("Connecting...");
        Utility.DEBUG_LOG(TAG, "TMS Connecting...");
        Utility.DEBUG_LOG(TAG, "url: " + url);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener < JSONObject > () {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Utility.DEBUG_LOG("ResponseAPI 1", response.toString());
                        try {
                            Utility.DEBUG_LOG("ResponseAPI 1", response.getString("msg"));
                                    Utility.DEBUG_LOG("ResponseAPI 1", "running download query");
                                    String url = null;
                                    //if server has newer version it will give the new url otherwsie exception occours

                                        if (response.getString("msg").equals("NO ANY VERSION FOUND.")) {
                                            Utility.DEBUG_LOG("response success","NO ANY VERSION FOUND.");
                                            //2021 07 02 dialog not showing
                                            DialogUtil.errorDialog(mContext.get(),"Error!","No Updates Available!");
                                            buttonDownload.setEnabled(true);
                                            // show no version
                                        }
                                        else if (response.getString("msg").equals("Exception.")) {
                                            Utility.DEBUG_LOG("response success","Exception.");
                                            //2021 07 02 dialog not showing
                                           // DialogUtil.errorDialog(mContext.get(),"Error!",response.getString("msg"));
                                            buttonDownload.setEnabled(true);
                                            // show exception
                                        }
                                        else {
                                            profileDownload();
                                            Checksum = response.getString("checksum");
                                            tmsUrl = response.getString("url");
                                            dn.execute();
                                            Utility.DEBUG_LOG("response success",tmsUrl);
                                        }

                        } catch (JSONException e) {

                            dn.cancel(true);
                            buttonDownload.setEnabled(true);
                            Toast.makeText(mContext.get(), "ERROR:" + e, Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }

                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getCause();
                        DialogUtil.errorDialog(mContext.get(),"Error", String.valueOf(error));
                        buttonDownload.setEnabled(true);
                        Utility.DEBUG_LOG("Error.ResponseAPI", error.toString());
                    }
                }
        );

        new NukeSSLCerts().nuke();
        queue.add(getRequest);
    }

    ////////////////////////////////////////////FILE CREATOR////////////////////////////////////////////////
    public void mCreateAndSaveFile(String params, String mJsonResponse) {
        try {
            Utility.DEBUG_LOG(TAG, "Creating file...");
            FileWriter file = new FileWriter("/sdcard/Download/" + params);
            file.write(mJsonResponse);
            Utility.DEBUG_LOG(TAG, "making json file");
            file.flush();
            file.close();
            Utility.DEBUG_LOG(TAG, "hide Creating file...");
            Utility.DEBUG_LOG(TAG, "profile successfully downloaded");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    ////////////////////////////////////////////CHECKSUM CREATOR////////////////////////////////////////////////
    public String fileToMD5(String filePath) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filePath);
            byte[] buffer = new byte[1024];
            MessageDigest digest = MessageDigest.getInstance("MD5");
            int numRead = 0;
            while (numRead != -1) {
                numRead = inputStream.read(buffer);
                if (numRead > 0)
                    digest.update(buffer, 0, numRead);
            }
            byte [] md5Bytes = digest.digest();
            return convertHashToString(md5Bytes);
        } catch (Exception e) {
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) { }
            }
        }
    }

    private String convertHashToString(byte[] md5Bytes) {
        String returnVal = "";
        for (int i = 0; i < md5Bytes.length; i++) {
            returnVal += Integer.toString(( md5Bytes[i] & 0xff ) + 0x100, 16).substring(1);
        }
        return returnVal;
    }

    ////////////////////////////////////////////Downloading new Version APK////////////////////////////////////////////////
    class DownloadNewVersion extends AsyncTask<String,Integer,Boolean> {

        Context context;

        DownloadNewVersion(Context context){
            this.context = context;
        }

        //show the progress dialog
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bar = new ProgressDialog(context);
            bar.setCancelable(false);

            bar.setMessage("Downloading...");
            bar.setIndeterminate(true);
            bar.setCanceledOnTouchOutside(false);
            bar.show();
        }

        // show progress update
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            bar.setIndeterminate(false);
            bar.setMax(100);
            bar.setProgress(progress[0]);
            String msg = "";
            if(progress[0]>99){
                msg="Finishing... ";
            }else {
                msg="Downloading... "+progress[0]+"%";
            }
            bar.setMessage(msg);
        }
        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            bar.dismiss();
            if(result){
                Toast.makeText(context,"Done!!", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context,"Error: Try Again", Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag = false;
            try {

                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                            }

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                            }
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });

                URL url = new URL(PortalUrl + "/BaflFileDown?username=bafl.service&password=bafl.service!&checksum="+Checksum+"&url="+tmsUrl);
                Utility.DEBUG_LOG(TAG,"APK download:"+url);
                HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
                c.setUseCaches(false);
                c.setRequestMethod("GET");
                c.addRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");
                c.setDoOutput(false);
                c.connect();


                String PATH = "sdcard/Download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file,"baf.apk");
                if(outputFile.exists()){
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();
                int total_size = 4581692;//size of apk
                byte[] buffer = new byte[1024];
                int len1 = 0;
                int per = 0;
                int downloaded=0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    downloaded +=len1;
                    per = (int) (downloaded * 100 / total_size);
                    publishProgress(per);
                }
                fos.close();
                is.close();
                ProfileAck.downloadAcknowledgement(mContext.get());

                String s1 = fileToMD5("/sdcard/Download/baf.apk");
                String s2 = s1.toUpperCase();
                Utility.DEBUG_LOG(TAG,"checksum = "+Checksum);
                Utility.DEBUG_LOG(TAG,"checksum1 = "+s2);

                if(s2.equals(Checksum)){
                    installApk();
                    flag = true;
                }
                else {
                    dn.cancel(true);
                    bar.hide();
                    buttonDownload.setEnabled(true);
                    DialogUtil.errorDialog(mContext.get(),"Error","File corrupt");
                    Utility.DEBUG_LOG(TAG,"checksum not matched");
                    flag = false;
                }

            } catch (Exception e) {
                Utility.DEBUG_LOG("TAG", "Update Error: " + e.getMessage());
                dn.cancel(true);
                bar.hide();
                buttonDownload.setEnabled(true);
                DialogUtil.errorDialog(mContext.get(),"Update Error:" ,e.getMessage());
                e.printStackTrace();
                flag = false;
            }
            return flag;
        }
    }

    //////////////////////////////////////////////INSTALL APK///////////////////////////////////////////////////////
    public void installApk() {

        final int[] counter = {
                0
        };
        Utility.DEBUG_LOG(TAG, "before loop" + code);
        while (code != 0 && counter[0] < 3) {
            Utility.DEBUG_LOG(TAG, "code" + code + "| count" + counter[0]);
            if (systemManager != null) {
                try {
                    //systemManager.installApp("/sdcard/Download/1.0.7.210414-Prod.apk", new IAppInstallObserver.Stub() {
                    systemManager.installApp("/sdcard/Download/baf.apk", new IAppInstallObserver.Stub() {

                        @Override
                        public void onInstallFinished(final String packageName, final int returnCode) throws RemoteException {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (returnCode == -1) {
                                        Utility.DEBUG_LOG(TAG, packageName + " : " + returnCode);
                                        code = returnCode;
                                    } else {
                                        Utility.DEBUG_LOG(TAG, packageName + " : " + returnCode);
                                        code = returnCode;
                                    }

                                    //Toast.makeText(mContext.get(), packageName + " : " + returnCode, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }, "bafl-1.0.7.20210426.apk");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            counter[0]++;
        }

    }

    //////////////////////////////////////////////PROFILE DOWNLOAD///////////////////////////////////////////////////////
    public void profileDownload() {
        try {
            Utility.DEBUG_LOG(TAG, "Downloading profile...");
            Utility.DEBUG_LOG(TAG, "+ sendGetRequest +");
            String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
            String currentDate = new SimpleDateFormat("MMddyy", Locale.getDefault()).format(new Date());
            String POSDateTime = currentTime + currentDate;

            RequestQueue queue = Volley.newRequestQueue(mContext.get());

            String url = PortalUrl +  "/baflserviceprofile?MID="+ MERCHANT_ID +"&TID=" + TERMINAL_SERIAL + "&Model=X990&APPNAME=BAF&SerialNumber="+TERMINAL_SERIAL+"&PosDateTime=" + POSDateTime + "";
            Utility.DEBUG_LOG(TAG, "url: " + url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Utility.DEBUG_LOG(TAG, "+ onResponse +");
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                Utility.DEBUG_LOG(TAG, "response:" + jsonObject.getString("Reponsemessage"));

                                if (jsonObject.getString("Reponsemessage").equals("Not available")) {
                                    Utility.DEBUG_LOG(TAG,jsonObject.getString("Reponsemessage"));
                                    dn.cancel(true);
                                    bar.hide();
                                    DialogUtil.errorDialog(mContext.get(),"Error!","Profile Not Available");
                                    buttonDownload.setEnabled(true);

                                } else {
                                    // it will add the response and create the json file

                                    mCreateAndSaveFile("jsonfile.json", response);
                                    // after downloding the profile it will request the acknowledgement
                                    ProfileAck.makeJsonObjReq(mContext.get(), systemManager);
                                }

                            } catch (Exception e) {
                                buttonDownload.setEnabled(true);
                                e.printStackTrace();
                                Utility.DEBUG_LOG("RTSG", "Failed to Parse Json");
                                dn.cancel(true);
                                bar.hide();
                                DialogUtil.errorDialog(mContext.get(),"Error!","Failed to Parse Json");

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext.get(), "Error! :" + error.toString(), Toast.LENGTH_LONG).show();
                    dn.cancel(true);
                    buttonDownload.setEnabled(true);
                    bar.hide();
                    DialogUtil.errorDialog(mContext.get(),"Error!",error.toString());

                }
            }) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            ;
            new GetRequest.NukeSSLCerts().nuke();
            queue.add(stringRequest);
        }catch (Exception e){
            dn.cancel(true);
            bar.hide();
            DialogUtil.errorDialog(mContext.get(),"Error!",e.toString());
            Toast.makeText(mContext.get(), "Error! :" + e, Toast.LENGTH_LONG).show();
        }
    }

    //////////////////////////////////////////////SSL certificate///////////////////////////////////////////////////////
    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
            } catch (Exception e) {}
        }
    }



    @Override
    public void toastShow(String str) {
    }

    @Override
    public void showProgress(String title) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.showProgressDialog(mContext.get(), title);
                    Utility.DEBUG_LOG("showProgress", title);
                }
            });
        } else {
            Utility.DEBUG_LOG("showProgress", "Error Dialog null");
        }
    }

    @Override
    public void hideProgress() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.hideProgressDialog();
                }
            });
        } else {
            Utility.DEBUG_LOG("hideProgress", "Error Dialog null");
        }
    }
    @Override
    public void transactionFailed(String title, String error) {

    }
    @Override
    public void transactionSuccess(String content) {
    }
    @Override
    public void transactionSuccessCustomer(String content, TransactionDetail transactionDetail, String copy, String txnType, String balance, boolean redeemCheck) {
    }
    @Override
    public void rocEmpty(String content, TransactionDetail transactionDetail, String copy, String txnType) {
    }
    @Override
    public boolean last4Digits(String text) {
        return false;
    }
    @Override
    public boolean last4Digits2(TransactionDetail transactionDetail, String text) {
        return false;
    }
    @Override
    public boolean panManualEntry(TransactionDetail transactionDetail, String text) {
        return false;
    }

}