package cn.access.group.android_all_banks_pos.repository;

import android.arch.persistence.room.TypeConverter;
import android.util.SparseArray;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;

public class Converters {



    @TypeConverter
    public static String sparseArrayToString(SparseArray sparseArray) {
        if (sparseArray == null) {
            return null;
        }

        int size = sparseArray.size();
        if (size <= 0) {
            return "{}";
        }

        StringBuilder buffer = new StringBuilder(size * 28);
        buffer.append('{');
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                buffer.append("-,- ");
            }
            int key = sparseArray.keyAt(i);
            buffer.append(key);
            buffer.append("-=-");
            Object value = sparseArray.valueAt(i);
            buffer.append(value);
        }
        buffer.append('}');
        return buffer.toString();

    }

    @TypeConverter
    public static SparseArray stringToSparseArray(String string) {
        if (string == null) {
            return null;
        }

        String entrySeparator = "-=-";
        String elementSeparator = "-,-";
        SparseArray sparseArray = new SparseArray();

        String[] entries = StringUtils.splitByWholeSeparator(string, elementSeparator);
        for (String entry : entries) {
            String[] parts = StringUtils.splitByWholeSeparator(entry, entrySeparator);
            if (parts[0].contains("{")) {
                parts[0] = parts[0].replace("{", "");
            }
            if (parts[1].contains("}")) {
                parts[1] = parts[1].replace("}", "");
            }
            int key = Integer.parseInt(parts[0].trim());
            String text = parts[1].trim();
            sparseArray.append(key, text);
        }
        return sparseArray;
    }

    @TypeConverter
    public String fromArray(ArrayList<String> strings) {
        StringBuilder string = new StringBuilder();
        for(String s : strings) string.append(s).append(",");

        return string.toString();
    }

    @TypeConverter
    public ArrayList<String> toArray(String concatenatedStrings) {
        return new ArrayList<>(Arrays.asList(concatenatedStrings.split(",")));
    }
}