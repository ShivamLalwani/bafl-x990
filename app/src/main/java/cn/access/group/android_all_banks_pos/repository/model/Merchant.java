package cn.access.group.android_all_banks_pos.repository.model;

/**
 * on 9/24/2019.
 */
@Deprecated
public class Merchant {
    private String merchantID="200000005000510",terminalID="42017890",merchantName="ACCESS GROUP TEST MERCHANT",merchantCity="KARACHI",merchantArea="BAHRIA COMPLEX IV";

   /* public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }
*/
    public String getMerchantCity() {
        return merchantCity;
    }

    public void setMerchantCity(String merchantCity) {
        this.merchantCity = merchantCity;
    }

    public String getMerchantArea() {
        return merchantArea;
    }

    public void setMerchantArea(String merchantArea) {
        this.merchantArea = merchantArea;
    }
}
