package cn.access.group.android_all_banks_pos.usecase;

import android.content.Context;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;

import java.util.Objects;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.access.group.android_all_banks_pos.R;

/**
 * on 10/14/2019.
 */
public class DeleteReversal {


    public DeleteReversal(Context context) {


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                GenerateReversal generateReversal= new GenerateReversal(AppDatabase.getAppDatabase(context));

                ((DashboardContainer)context).runOnUiThread(new Runnable() {
                    public void run() {
                        if (generateReversal.isReversalPresentInDB()){
                                generateReversal.deleteReversalAsync();
                                DialogUtil.successDialog(context,"Reversal Transaction deleted Successfully!");

                        }
                        else {

                            DialogUtil.infoDialog(context,"No reversal Transaction!");
                        }
                    }
                });
            }
        });
        thread.start();



    }
}
