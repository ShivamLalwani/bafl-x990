package cn.access.group.android_all_banks_pos.repository.dao;



import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * on 8/29/2019.
 * DAO contains the methods to query transaction detail table
 */
@Dao
public interface TransactionDetailDao {

    @Query("SELECT * FROM transactiondetail ORDER BY invoiceNo DESC")
    List<TransactionDetail> getAllTransactions();

    @Query("SELECT * FROM transactiondetail WHERE txnType NOT LIKE 'PRE AUTH' ORDER BY invoiceNo DESC")
    List<TransactionDetail> getAllTransactionsNotPreAuth();

    @Query("SELECT * FROM transactiondetail WHERE txnType NOT LIKE 'PRE AUTH' ORDER BY invoiceNo DESC")
    Flowable<List<TransactionDetail>>getAllTransactionsLive();

    @Query("SELECT * FROM transactiondetail where invoiceNo LIKE  :invoiceNo and  txnType NOT LIKE 'PRE AUTH'")
    Maybe<TransactionDetail> findByInvoiceNo(String invoiceNo);

    @Query("SELECT * FROM transactiondetail where ecrRefNo LIKE  :ecrRefNo and  txnType NOT LIKE 'VOID'")
    Maybe<TransactionDetail> findByEcrRefNo(String ecrRefNo);

    @Query("SELECT * FROM transactiondetail where invoiceNo LIKE  :invoiceNo and  (txnType IN ('SALE','ADJUST','COMPLETION')) ")
    Maybe<TransactionDetail> findByInvoiceNoAdjust(String invoiceNo);

    @Query("SELECT * FROM transactiondetail where txnType LIKE 'PRE AUTH' and authorizationIdentificationResponseCode LIKE  :authorizationIdentificationResponseCode")
    Maybe<TransactionDetail> findByAuthNo(String authorizationIdentificationResponseCode);

    @Query("SELECT * FROM transactiondetail  WHERE txnType NOT LIKE 'PRE AUTH' ORDER BY tdid DESC Limit 1")
    Maybe<TransactionDetail> lastReceipt();

    @Query("SELECT * FROM transactiondetail where status LIKE  :status")
    Maybe<TransactionDetail> findByStatus(String status);

    @Insert
    void saveSettlement(List<TransactionDetail> transactionDetail);

//    @Query("SELECT * FROM transactiondetail where authorizationIdentificationResponseCode LIKE  :authorizationIdentificationResponseCode")
//    LiveData<TransactionDetail> findByAuthId(String authorizationIdentificationResponseCode);

    @Query("SELECT * FROM transactiondetail where status LIKE  :status")
    Maybe <List<TransactionDetail>> findByStatusList(String status);

    @Query("SELECT * FROM transactiondetail where isAdvice = 1")
    Maybe <List<TransactionDetail>> findAllAdviceTransactions();


    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateTransaction(TransactionDetail transactionDetail);

    @Query("UPDATE transactiondetail SET txnType =:txnType, cancelTxnType=:cancelTxnType,processingCode=:proc WHERE tdid =:tdid")
    void updateStatus(String txnType, String cancelTxnType,String proc, int tdid);

    @Query("UPDATE transactiondetail SET tipAmount =:tipAmount,txnType =:txnType,isAdvice =:isAdvice, amount=:amount, tipNumAdjustCounter=:tipNumAdjustCounter, processingCode=:processingCode WHERE invoiceNo =:invoice")
    void updateTip(String tipAmount, String invoice, String txnType, String amount, boolean isAdvice,int tipNumAdjustCounter, String processingCode);

    @Query("UPDATE transactiondetail SET transactionAmount =:transactionAmount,txnType =:txnType,isAdvice =:isAdvice, amount=:amount WHERE invoiceNo =:invoice")
    void updateAmount(String transactionAmount, String invoice, String txnType, String amount, boolean isAdvice);

    @Query("UPDATE transactiondetail SET txnType =:txnType,isAdvice =:isAdvice, amount=:amount WHERE authorizationIdentificationResponseCode =:authorizationIdentificationResponseCode")
    void updateCompletion(String txnType,  boolean isAdvice, String amount,String authorizationIdentificationResponseCode);

    @Query("SELECT txnType as txnType ,COUNT(txnType) as count, SUM(amount) as totalAmount, SUM(tipAmount) as totalTipAmount from transactiondetail WHERE txnType NOT LIKE 'PRE AUTH' GROUP BY txnType")
    @Ignore
    Maybe<List<CountedTransactionItem>> transactionSumCount();

//    @Query("SELECT txnType as txnType ,COUNT(txnType) as count, SUM(amount) as totalAmount, SUM(tipAmount) as totalTipAmount from transactiondetail, WHERE txnType NOT LIKE 'PRE AUTH' GROUP BY txnType")
//    @Ignore
//    Maybe<List<CountedTransactionItem>> transactionCount();

    @Query("SELECT txnType as txnType, cardScheme2 as cardScheme ,COUNT(txnType) as count, SUM(amount) as totalAmount,SUM(tipAmount) as totalTipAmount from transactiondetail WHERE txnType NOT LIKE 'PRE AUTH' GROUP BY cardScheme,txnType")
    @Ignore
    Maybe<List<CountedTransactionItem>> transactionCardSchemeCount();

    @Query("SELECT txnType as txnType ,COUNT(txnType) as count, SUM(amount) as totalAmount , SUM(tipAmount) as totalTipAmount  from transactiondetail WHERE txnType NOT LIKE 'PRE AUTH' GROUP BY txnType")

    @Ignore
    Maybe<List<CountedTransactionItem>> transactionSumTipAmount();

    @Insert
    void insertAll(TransactionDetail... transactionDetails);

    @Insert
    void insertTransaction(TransactionDetail transactionDetail);


    @Delete
    void delete(TransactionDetail transactionDetail);

    @Query("DELETE FROM transactiondetail where status LIKE  :status")
    void deleteReversal(String status);


    @Query("DELETE FROM transactiondetail")
    void nukeTable();

}
