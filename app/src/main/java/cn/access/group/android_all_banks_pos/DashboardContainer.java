package cn.access.group.android_all_banks_pos;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import cn.access.group.android_all_banks_pos.viewfragments.PaymentAmountFragment;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CHECK_EXPIRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_ETHERNET;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_GPRS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CVM_LIMIT_VISA;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HIGH_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_ICC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_MAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LOW_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY_ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MOD10;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PIN_BYPASS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MAX_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_MIN_AMOUNT_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SALE_OFFLINE_LIMIT;

/**
 * @author muhammad.humayun
 * on 8/19/2019.
 */
public class DashboardContainer extends AppCompatActivity {
    public static int index = 0; //0=ubl ,1=upi
    public static FragmentManager fragmentManager;
    Repository repository;
    String TAG = "DashboardContainer";
    private TransactionDetail transactionDetail;// = new TransactionDetail();


    public static void switchFragmentWithBackStack(Fragment fragment, String tag) {
        fragmentManager.beginTransaction().replace(R.id.mainContainer, fragment, tag).addToBackStack(null).commitAllowingStateLoss();
       // fragmentManager.beginTransaction().remove(fragment).commit();
    }
//    public void bindSystemService() {
//        Intent intent = new Intent();
//        intent.setAction(ACTION_X9SERVICE);
//        intent.setPackage(PACKAGE);
//        intent.setClassName(PACKAGE, CLASSNAME);
//
//        ServiceConnection mSystemServiceConnection = new ServiceConnection() {
//            @Override
//            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
//                Utility.DEBUG_LOG("systemSetting", "system service bind success");
//                systemManager = ISystemManager.Stub.asInterface(iBinder);
//                try {
//                    settingsManager = systemManager.getSettingsManager();
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    systemManager.reboot();
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//            }
//            @Override
//            public void onServiceDisconnected(ComponentName componentName) {
//                Utility.DEBUG_LOG("systemSetting", "system service disconnected.");
//                systemManager = null;
//            }
//        };
//        boolean result = bindService(intent, mSystemServiceConnection, Context.BIND_AUTO_CREATE);
//        Utility.DEBUG_LOG("systemSetting", "bind system service " + result);
//    }
    /*
    this method uses the multihost config method
    this in now @Deprecated
     */
   /*   private void initializeConfig() {
        MultiHostsConfig.initialize();
        HostInformation hostInformation = MultiHostsConfig.get(index);
        SharedPref.write("hostIP",hostInformation.hostAddr);
        SharedPref.write("hostPort",hostInformation.hostPort);
        SharedPref.write("TERMINAL_ID",hostInformation.terminalID);
        SharedPref.write("MERCHANT_ID",hostInformation.merchantID);
        SharedPref.write("NII",hostInformation.nii);
        SharedPref.write("merchantName",hostInformation.merchantName);
        TERMINAL_ID= SharedPref.read("TERMINAL_ID","");
        MERCHANT_ID= SharedPref.read("MERCHANT_ID","");
        MERCHANT_NAME= SharedPref.read("merchantName","");
        hostIP= SharedPref.read("hostIP","");
        hostPort= SharedPref.read("hostPort");
        NII= SharedPref.read("NII","");
    }


 */

    public static void switchFragment(Fragment fragment, String tag) {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction().replace(R.id.mainContainer, fragment, tag).commitAllowingStateLoss();


//Percentage can be calculated for API 16+

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_container);
//        Constants.ecrOfflineCounter=0;
        fragmentManager = getSupportFragmentManager();


//shivam code start

//        //Disable Notification Bar,
//        Context context=this;
//        WindowManager manager = ((WindowManager) context.getApplicationContext()
//                .getSystemService(Context.WINDOW_SERVICE));
//
//        Activity activity = (Activity)context;
//        GetRequest.sendGetRequest(activity,appDatabase);
//        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
//        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
//        localLayoutParams.gravity = Gravity.TOP;
//        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|
//
//                // this is to enable the notification to recieve touch events
//                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
//
//                // Draws over status bar
//                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
//
//        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
//        int resId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
//        int result = 0;
//        if (resId > 0) {
//            result = activity.getResources().getDimensionPixelSize(resId);
//        }
//
//        localLayoutParams.height = result;
//
//        localLayoutParams.format = PixelFormat.TRANSPARENT;
//
//        customViewGroup view = new customViewGroup(context);
//
//        manager.addView(view, localLayoutParams); //not working for android 7.1

        //shivam code end


        SharedPref.init(getApplicationContext());
        setTerminalConfiguration();
        setCardRangesConfig();
        setTransactionDetail(new TransactionDetail());
        // Activity activity = (Activity)context;

        // initializeConfig();



//        mi = new ActivityManager.MemoryInfo();
//        activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//        activityManager.getMemoryInfo(mi);
        switchFragment(new HomeFragment(), "Home Fragment");

    }

    /*
    this is the latest and only method to set terminal configuration for host
    0 id -> represents the bank host Configuration
    1 id -> represents config for other host IF AND ONLY IF it is needed
    do not use any other config method or more than 2 config
     */
    private void setTerminalConfiguration() {
        Utility.DEBUG_LOG(TAG, "setTerminalConfiguration");
//        GetRequest.sendGetRequest(activity, appDatabase);
        repository = new Repository(this);


        String PATH = "/sdcard/Download/jsonfile.json";
        File file = new File(PATH);
        Utility.DEBUG_LOG("TAG", "file path " + file);
        if (file.exists()) {

            loadJSONFromAsset();
            Utility.DEBUG_LOG(TAG, "file found and deleted");
            // send the request to profile download...

        } else {
            Utility.DEBUG_LOG(TAG, "no such file found");
            repository.getTerminalConfiguration();
        }
        //repository.emptyTableTerminalConfig(); //clear terminal config table




//        }

    }

    public String loadJSONFromAsset() {
        String json = null;

        try {

            InputStream is = new FileInputStream("/sdcard/Download/jsonfile.json");
            JSONObject jsonObject;
            int size = is.available();
            Utility.DEBUG_LOG("TAG", String.valueOf(size));

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
            Utility.DEBUG_LOG("TAG", json);

            try {

                jsonObject = new JSONObject(json);
                Utility.DEBUG_LOG("My App", jsonObject.toString());
                TerminalConfig terminalConfig = new TerminalConfig(0, jsonObject.getString("MERCHANTID"),
                        jsonObject.getString("TERMINALID"),
                        jsonObject.getString("MERCHANTNAME"),
                        jsonObject.getString("DESTADDTPDU"),
                        jsonObject.getString("TIPENABLED"),
                        COM_ETHERNET,
                        COM_GPRS,
                        jsonObject.getString("EMVLOGSENABLED"),
                        jsonObject.getString("MARKETSEGMENT"),
                        jsonObject.getString("TIPTHERSHOLD"),
                        jsonObject.getString("ALLOWFALLBACK"),
                        jsonObject.getString("MSGHEADERLENHEX"),
                        jsonObject.getString("ISSSLENABLE"),
                        jsonObject.getString("TOPUPENABLED"),
                        jsonObject.getString("TERMINALMANUALENTRY"),
                        jsonObject.getString("AUTOSETTLE"),
                        jsonObject.getString("EMVLOGSENABLED"),
                        jsonObject.getString("AUTOSETTLETIME"),
                        jsonObject.getString("AMEXID"),
                        PIN_BYPASS,
                        MOD10,
                        CHECK_EXPIRY,
                        MANUAL_ENTRY_ISSUER,
                        LAST_4DIGIT_MAG,
                        LAST_4DIGIT_ICC,
                        ACQUIRER,
                        LOW_BIN,
                        HIGH_BIN,
                        ISSUER,
                        jsonObject.getString("TERMINALCURRENCY1"),
                        jsonObject.getString("PASSWORDUSER"),
                        jsonObject.getString("PASSWORDMANAGEMENT"),
                        jsonObject.getString("PASSWORDSYSTEM"),
                        jsonObject.getString("PASSWORDSUPERUSER"),
                        jsonObject.getString("MERCHANTADDRESS1"),
                        jsonObject.getString("MERCHANTADDRESS2"),
                        jsonObject.getString("MERCHANTADDRESS3"),
                        jsonObject.getString("MERCHANTADDRESS4"),
                        jsonObject.getString("RECEIPTFOOTER1"),
                        jsonObject.getString("RECEIPTFOOTER2"),
                        jsonObject.getString("PRIMARYMEDIUM"),
                        jsonObject.getString("SECONDARYMEDIUM"),
                        jsonObject.getString("TMSIP"),
                        jsonObject.getString("TMSPORT"),
                        BATCH_NO,
                        jsonObject.getString("PRIMARYIP"),
                        jsonObject.getString("PRIMARYPORT"),
                        jsonObject.getString("SECONDARYIP"),
                        jsonObject.getString("SECONDARYPORT"),
                        CVM_LIMIT_VISA,
                        CVM_LIMIT_MC,
                        CVM_LIMIT_UPI,
                        CVM_LIMIT_PAYPAK,
                        CVM_LIMIT_JCB,
                        CVM_LIMIT_AMEX,
                        SALE_OFFLINE_LIMIT,
                        SALE_MIN_AMOUNT_LIMIT,
                        SALE_MAX_AMOUNT_LIMIT,
                        jsonObject.getString("VEPSLIMIT"),
                        jsonObject.getString("MasterQPSLimit".toUpperCase()),
                        jsonObject.getString("UpiQPSLimit".toUpperCase()),
                        jsonObject.getString("PayPakQPSLimit".toUpperCase()),
                        jsonObject.getString("JcbQPSLimit".toUpperCase()),
                        jsonObject.getString("AmexQPSLimit".toUpperCase()),
                        jsonObject.getString("PORTALURL"),
                        jsonObject.getString("PORTALUSERNAME"),
                        jsonObject.getString("PORTALPASSWORD"),
                        jsonObject.getString("PORTALTIMEOUT"),
                        jsonObject.getString("PORTALTXNUPLOAD"),
                        jsonObject.getInt("SETTLECOUNTER"),
                        jsonObject.getInt("SETTLERANGE"),
                        jsonObject.getInt("TIPNUMADJUST"),
                        jsonObject.getString("ECRENABLED"),
                        jsonObject.getString("ECRTYPE")
                        );

                        Utility.DEBUG_LOG("ECR ENABLED",jsonObject.getString("ECRENABLED"));


                Repository repository = new Repository(this);
                repository.updateTerminalConfiguration(terminalConfig);
                Utility.DEBUG_LOG("My App", "Config updated");
                repository.getTerminalConfiguration();



                //terminalConfig.setAcquirer(obj.getString("ac"));

            } catch (Exception t) {
                t.printStackTrace();
                Utility.DEBUG_LOG("My App", "Could not parse malformed JSON: \"" + json + "\"");
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    private void setCardRangesConfig() {
        repository = new Repository(this);
        repository.getCardRangesConfig();//get card ranges config from db
        // 0th entry in db is ubl configuration
        // repository.emptyTableTerminalConfig(); //clear terminal config table

    }

    @Override
    public void onBackPressed() {
        if(Constants.ecrType.equals("M") && Constants.selectedECRMENU.equals("M"))
        {
            Utility.DEBUG_LOG("DASHBOARD","Switching Segments");
            Constants.MARKET_SEG="S";
            DashboardContainer.switchFragment(new HomeFragment(), "home fragment");
            Constants.selectedECRMENU = "S";

        }
        else  if(Constants.ecrType.equals("M") && Constants.selectedECRMENU.equals("S"))
        {
            Utility.DEBUG_LOG("DASHBOARD","Switching Segments");
            Constants.MARKET_SEG=".";
            DashboardContainer.switchFragment(new HomeFragment(), "home fragment");
            Constants.selectedECRMENU = "M";
        }
        Utility.DEBUG_LOG("DashboardContainer", "onBackPressed1");
       backStack();
    }
     public static void backStack(){


         HomeFragment homeFragment = (HomeFragment) fragmentManager.findFragmentByTag("Home Fragment");
         PaymentAmountFragment paymentAmountFragment = (PaymentAmountFragment) fragmentManager.findFragmentByTag("payment fragment");

         if (paymentAmountFragment != null && paymentAmountFragment.isVisible() && paymentAmountFragment.getFallbackOrignatorType() != PaymentAmountPresenter.FallbackOrignatorType.FT_NA) {
             Utility.DEBUG_LOG("DashboardContainer", "back press from fallback screen");
             if(homeFragment!=null) {
                 fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
             } else{
                 DashboardContainer.switchFragment(new HomeFragment(), "Home Fragment");
             }
            // DashboardContainer.switchFragment(new HomeFragment(), "home fragment");
             return;
         }

            if(homeFragment!=null) {
             fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
         } else{
             DashboardContainer.switchFragment(new HomeFragment(), "Home Fragment");
         }
     }

    // shivam code
//    @Override
//    public void onAttachedToWindow() {
//        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);//disable home button
//        super.onAttachedToWindow();
//    }

    public List<TransactionDetail> getAllBatchTransactions() throws ExecutionException, InterruptedException {
        return new GetTransactionsAsyncTaskAllTransactions().execute().get();
    }

    public TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    @SuppressLint("StaticFieldLeak")
    private class GetTransactionsAsyncTaskAllTransactions extends AsyncTask<Void, Void, List<TransactionDetail>> {
        @Override
        protected List<TransactionDetail> doInBackground(Void... voids) {
            return AppDatabase.getAppDatabase(DashboardContainer.this).transactionDetailDao().getAllTransactions();
        }
    }


    public List<TransactionDetail> getAllBatchTransactionsNotPreAuth() throws ExecutionException, InterruptedException {
        return new GetTransactionsAsyncTaskNotPreAuthTransactions().execute().get();
    }

    @SuppressLint("StaticFieldLeak")
    private class GetTransactionsAsyncTaskNotPreAuthTransactions extends AsyncTask<Void, Void, List<TransactionDetail>> {
        @Override
        protected List<TransactionDetail> doInBackground(Void... voids) {
            return AppDatabase.getAppDatabase(DashboardContainer.this).transactionDetailDao().getAllTransactionsNotPreAuth();
        }
    }



}
