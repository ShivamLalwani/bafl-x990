package cn.access.group.android_all_banks_pos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.Issuer;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;

/**
 * @author muhammad.humayun
 * on 8/19/2019.
 */
public class SplashScreen extends AppCompatActivity {
    @BindAnim(R.anim.slide_in_left)
    Animation slide_in_left;
    @BindAnim(R.anim.slide_in_right)
    Animation slide_in_right;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.digitalText)
    ImageView digitalText;
    private String TAG = "repository";
    Repository repository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        repository = new Repository(this);
        getTerminalConfiguration();
        getIssuerConfiguration();
        getCardRangeConfiguration();
        logo.startAnimation(slide_in_left);
        digitalText.startAnimation(slide_in_right);



        slide_in_right.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                Intent homeIntent = new Intent(SplashScreen.this, DashboardContainer.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(homeIntent);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
      //  getTerminalConfiguration();

    }





    public void getTerminalConfiguration() {
        try {
            List<TerminalConfig> terminalConfigList = new Repository(this).getAllTransactions();
            Utility.DEBUG_LOG(TAG, "config found");
                if (terminalConfigList.size() == 0) {
                    Utility.DEBUG_LOG(TAG, "insert config");
                    repository.setTerminalConfig();//insert config if not found
                    repository.setCardConfig();
                }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }





    public void getIssuerConfiguration() {
        try {
            List<Issuer> issuerList = new Repository(this).getAllIssuerConfig();
            Utility.DEBUG_LOG(TAG, "config found");
            if (issuerList.size() == 0) {
                Utility.DEBUG_LOG(TAG, "insert config");//insert config if not found
                repository.setIssuerConfig();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }
    public void getCardRangeConfiguration() {
        try {
            List<CardRanges> issuerList = new Repository(this).getAllCardRanges();
            Utility.DEBUG_LOG(TAG, "config found card ranges splash screen");
            if (issuerList.size() == 0) {
                Utility.DEBUG_LOG(TAG, "insert config");//insert config if not found
                repository.setCardConfig();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);//disable home button
        super.onAttachedToWindow();
    }
    @Override
    public void onBackPressed() {


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppDatabase.destroyInstance();
    }
}
