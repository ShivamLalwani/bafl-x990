package cn.access.group.android_all_banks_pos.presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.PrinterFonts;
import cn.access.group.android_all_banks_pos.Utilities.BatchUploadRequest;
import cn.access.group.android_all_banks_pos.Utilities.Comm;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.ErrorMap;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSettlement;
import cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator.SettlementPacketGenerator;
import cn.access.group.android_all_banks_pos.basic.ISO8583;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.Merchant;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.usecase.AdviceTransaction;
import cn.access.group.android_all_banks_pos.usecase.GenerateReversal;
import cn.access.group.android_all_banks_pos.usecase.BatchUploadTransaction;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.HOST_INDEX_FROM_DB;
import static java.lang.Thread.sleep;

/**
 * on 10/14/2019.
 */
public class SettlementPresenter {
    private static final String TAG = "SettlementPresenter";

    private IBeeper iBeeper;
    private IPrinter printer;
    private String headerType="";
    String printStatus;
    /**
     * field-value map to save iso data
     */
    private List<SparseArray<String>> batchUploadArray;
    private List<TransactionDetail> batchUploadTransactionDetails;
    private TransactionDetail currentBatchUploadTransaction;
    private List<SparseArray<String>> adviceArray;
    private SparseArray<String> data8583 = null, batchUploadData8583 =null, adviceData8583 = null;
    TransactionDetail currentAdviceTransaction;
    private AssetManager assetManager;
    private TransactionContract transactionContract;
    List<TransactionDetail> transactionDetailList = new ArrayList<>();
    private ReceiptPrinter receiptPrinter;
    private Merchant merchant = new Merchant();
    private ErrorMap errorMap;
    private String stan;
    private BatchUploadTransaction batchUploadTransaction;
    private SparseArray<String> tagOfF55 = null;
    private WeakReference<Context> context;
    TransPrinter transPrinter=null;
    BatchUploadRequest batchUploadRequest = new BatchUploadRequest();
    AdviceTransaction adviceTransaction;


    public SettlementPresenter(TransactionContract transactionContract,  Handler handler, boolean isSucc, AssetManager assetManager,
                                    IPrinter printer,List<TransactionDetail>  transactionDetailList, Context context){
        this.transactionContract = transactionContract;
        this.printer = printer;
        Utility.DEBUG_LOG(TAG, "MA"+String.valueOf(printer));
        this.assetManager = assetManager;
        this.transactionDetailList = transactionDetailList;
        //this.transactionDetailList1=transactionDetailList1;
        receiptPrinter = new ReceiptPrinter(handler);
        errorMap = new ErrorMap();
        this.context = new WeakReference<>(context) ;
        Utility.DEBUG_LOG(TAG,"Before new AdviceTransaction");
        this.adviceTransaction = new AdviceTransaction(AppDatabase.getAppDatabase(context));
        Utility.DEBUG_LOG(TAG,"After new AdviceTransaction");


        loadConfiguration();
        ISO8583u.SimulatorType st;
        sumAndCountTransactions();
        cardAndCountTransactions();
        switch (headerType)
        {
            case "SimulatorHost":
                st=ISO8583u.SimulatorType.SimulatorHost;
                break;
            case "ActualBank":
                st=ISO8583u.SimulatorType.ActualBank;
                break;
            default:
                st=ISO8583u.SimulatorType.AuthAll;
                break;
        }
        initialize8583data(st);
        try {
            printStatus = String.valueOf(printer.getStatus());
            Utility.DEBUG_LOG("2",printStatus);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        // check assets fonts and copy to file system for Service
        InitializeFontFiles();
        if (!isSucc) {
            Utility.DEBUG_LOG("TAG", "deviceService connect fail!");
        } else {
            Utility.DEBUG_LOG("TAG", "deviceService connect success");

        }

    }
    private void loadConfiguration()
    {

//        stan=String.format(Locale.getDefault(),"%06d", SharedPref.read("stan"));
//        int stan1 =  SharedPref.read("stan");
//        stan1=stan1+1;
//        SharedPref.write("stan",stan1);

        try {

            JSONObject obj = new JSONObject(loadJSONFromAsset());
         //   this.hostIP = obj.getString("Host IP");
         //   this.hostPort = obj.getInt("Host Port");
            this.headerType = obj.getString("Header Type");
            SharedPref.write("ListReport","Y");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = assetManager.open("config.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void initialize8583data(ISO8583u.SimulatorType st) {
        // build up the iso data
        ISO8583u.simType = st;
    }

    // check assets fonts and copy to file system for Service -- start
    private void InitializeFontFiles() {
        PrinterFonts.initialize(assetManager);
    }

    @SuppressLint("HandlerLeak")
    private
    Handler onlineResponse = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG,"before getData");
            Bundle data = msg.getData();
            String val = data.getString("value");
            Utility.DEBUG_LOG(TAG, "handle message:" + val);
        }
    };

    private ISO8583u isoResponse = null;

    private Thread isoReq = new Thread(new Runnable() {
        @Override
        public void run() {

            ISO8583u iso8583u;
            GenerateReversal generateReversal = new GenerateReversal(AppDatabase.getAppDatabase(context.get()));
            batchUploadTransaction = new BatchUploadTransaction(AppDatabase.getAppDatabase(context.get()));
            adviceTransaction = new AdviceTransaction(AppDatabase.getAppDatabase(context.get()));

            byte[] packet;
            if(generateReversal.isReversalPresentInDB()) {
                if(HOST_INDEX_FROM_DB==0){
                    iso8583u= new ISO8583u();
                }
                else {
                    iso8583u = new ISO8583u(16);
                }
                SparseArray<String> reversal = generateReversal.generateReversalPacket();
                if (reversal.get(ISO8583u.F_55) != null) {
                    SparseArray<String> tagOfF55Reversal = generateReversal.createTagOf55();
                    if (tagOfF55Reversal != null) {
                        for (int i = 0; i < tagOfF55Reversal.size(); i++) {
                            int tag = tagOfF55Reversal.keyAt(i);
                            String value = tagOfF55Reversal.valueAt(i);
                            if (value.length() > 0) {
                                byte[] tmp = iso8583u.appendF55(tag, value);
                                if (tmp == null) {
                                    Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                                } else {
                                    Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                                }
                            }
                        }
                    }
                }
                packet = iso8583u.makePacket(reversal, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
            }
            else if ( adviceTransaction.getAdviceCount() > 0 )
            {
                Utility.DEBUG_LOG(TAG,"Process advice first");
                //below just to test
                iso8583u = new ISO8583u();
                adviceData8583 = adviceTransaction.getTopAdviceInStringFormat();
                currentAdviceTransaction = adviceTransaction.getTopAdviceTransaction();
//                adviceData8583 = adviceTransaction.getFirstAdviceRecord();
                if(adviceData8583 !=null){
                    Utility.DEBUG_LOG(TAG,"Process advice upload");

                    if (currentAdviceTransaction.getTag55()  != null) {
                        Utility.DEBUG_LOG(TAG, "F55 found in advice");
                        Utility.DEBUG_LOG(TAG, "currentAdviceTransaction.getTag55():" + currentAdviceTransaction.getTag55().toString());
                        SparseArray<String> tagOfF55BatchUpload = currentAdviceTransaction.getTag55();
                        if (tagOfF55BatchUpload != null) {
                            for (int i = 0; i < tagOfF55BatchUpload.size(); i++) {
                                int tag = tagOfF55BatchUpload.keyAt(i);
                                String value = tagOfF55BatchUpload.valueAt(i);
                                if (value.length() > 0) {
                                    byte[] tmp = iso8583u.appendF55(tag, value);
                                    if (tmp == null) {
                                        Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                                    } else {
                                        Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                                    }
                                }
                            }
                        }
                    }

//                    packet = iso8583u.makePacket(adviceData8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
                    packet = iso8583u.makePacket(adviceData8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
                }
                else {
                    Utility.DEBUG_LOG(TAG,"added for compatibility, but this code should never be executed if logic is correct");
                    iso8583u = new ISO8583u();

                    if(batchUploadData8583 !=null){
                        Utility.DEBUG_LOG(TAG,"Process batch upload 1");

                        packet = iso8583u.makePacket(batchUploadData8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
                    }
                    else {
                        Utility.DEBUG_LOG(TAG,"Process settle packet 1...?");
                        Utility.DEBUG_LOG(TAG, "data8583:" + String.valueOf(data8583));
                        packet = iso8583u.makePacket(data8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
                    }
                }
            }
            else {
                iso8583u = new ISO8583u();
                if(batchUploadData8583 !=null){
                    Utility.DEBUG_LOG(TAG,"Process batch upload 2");

                    if (currentBatchUploadTransaction.getTag55()  != null) {
                        Utility.DEBUG_LOG(TAG,"F55 found in batch upload");
//                        if (batchUploadData8583.get(ISO8583u.F_55) != null) {
                        Utility.DEBUG_LOG(TAG,"batchUploadTransactionDetails:"+batchUploadTransactionDetails);
                        Utility.DEBUG_LOG(TAG,"batchUploadTransactionDetails.size:"+batchUploadTransactionDetails.size());
                        Utility.DEBUG_LOG(TAG,"currentBatchUploadTransaction:"+currentBatchUploadTransaction);
                        Utility.DEBUG_LOG(TAG,"currentBatchUploadTransaction.getTag55():"+currentBatchUploadTransaction.getTag55().toString());
                        SparseArray<String> tagOfF55BatchUpload = currentBatchUploadTransaction.getTag55();
                        if (tagOfF55BatchUpload != null) {
                            for (int i = 0; i < tagOfF55BatchUpload.size(); i++) {
                                int tag = tagOfF55BatchUpload.keyAt(i);
                                String value = tagOfF55BatchUpload.valueAt(i);
                                if (value.length() > 0) {
                                    byte[] tmp = iso8583u.appendF55(tag, value);
                                    if (tmp == null) {
                                        Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                                    } else {
                                        Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Utility.DEBUG_LOG(TAG,"F55 not found");
                    }
                    packet = iso8583u.makePacket(batchUploadData8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
                }
                else {
                    Utility.DEBUG_LOG(TAG,"Process settle packet 2...?");
                    Utility.DEBUG_LOG(TAG, "data8583:"+ String.valueOf(data8583));
                    packet = iso8583u.makePacket(data8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
                }
            }

            Comm comm = new Comm(Constants.hostIP, Constants.hostPort, context.get());
            Comm comm1 = new Comm(Constants.SEC_IP, Constants.SEC_PORT,context.get());
            transactionContract.showProgress("sending...");
            if (!comm.connect()) {
                if(comm1.connect()){
                    int packetLength= packet.length;
                    String packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2,Utility.byte2HexStr(packet),' ');
                    Utility.DEBUG_LOG(TAG,packetToPrint);
                    // receiptPrinter.printPacket(printer,"Request  ",packetLength,packetToPrint);
                    transactionContract.hideProgress();
                    comm1.send(packet);
                }
                else {
                    Utility.DEBUG_LOG(TAG, "Unable to establish connection with server.");
                    transactionContract.hideProgress();
                    hideProgressMessage();
                    transactionContract.transactionFailed("Error", "Unable to establish connection with server");

                    return;
                }
            }

            comm.send(packet);
            transactionContract.hideProgress();


            byte[] response = new byte[0];
            try {
                transactionContract.showProgress("receiving...");
                if(!comm.connect()){
                    response = comm1.receive(1024, 40);
                }else{
                    response = comm.receive(1024, 40);
                }
            } catch (IOException e) {
               transactionContract.hideProgress();

                transactionContract.transactionFailed("Error",e.getMessage());
                if(generateReversal.isReversalPresentInDB()) return;

               }
            if (null == response ) {
                Utility.DEBUG_LOG(TAG, "receive error");
                hideProgressMessage();
            }
            comm.disconnect();
            comm1.disconnect();
//           transactionContract.hideProgress();

            if (response == null ||response.length==0) {
                Utility.DEBUG_LOG(TAG, "response is null");
                Utility.DEBUG_LOG(TAG, "Test fails");
                Utility.DEBUG_LOG(TAG, "receive error");
                isoResponse=null;
                //transactionContract.transactionFailed("Error","Transaction Failed");
                hideProgressMessage();
            }
            else
                {
                Utility.DEBUG_LOG(TAG, "response.length:" + response.length);
                Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(response));
                isoResponse = new ISO8583u();
                if (isoResponse.unpack(response, 2)) {
                    String message = "";
                    String s;
                    String type = "";

                    s = isoResponse.getUnpack(0);
                    if (null != s) {
                        type = s;
                        message += "Message Type:";
                        message += s;
                        message += "\n";
                    }

                    s = isoResponse.getUnpack(39);
                    if (null != s) {
                        message += "Response(39):";
                        message += s;
                        message += "\n";
                        Utility.DEBUG_LOG(TAG,"F39 value:"+s);
                    }
                    else if (type.equals("0210")) {
                        Utility.DEBUG_LOG(TAG,"type equals 0210");
                        s = isoResponse.getUnpack(54);
                        if (null != s) {
                            message += "Balance(54):";
                            message += s.substring(0, 2) + "," + s.substring(2, 4) + "," + s.substring(4, 7) + "," + s.substring(7, 8);
                            message += "\n" + Integer.valueOf(s.substring(8, s.length() - 1));
                            message += "\n";
                        }
                    }
                    transactionContract.hideProgress();
                    transactionContract.toastShow(message);
                }
            }
            Message msg = new Message();
            Bundle data = new Bundle();
            data.putString("value", "receive finished");
            msg.setData(data);
            onlineResponse.sendMessage(msg);
            if ( isoResponse == null )
            {
                hideProgressMessage();
                Utility.DEBUG_LOG(TAG,"Response is Null Transaction failed");
                transactionContract.transactionFailed("Response is Null","Transaction failed");
            }
            else {
                String responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                String messageType= isoResponse.getUnpack(ISO8583u.F_MessageType_00);
                //String processingCode = isoResponse.getUnpack(ISO8583u.F_ProcessingCode_03);
                if (responseCode.equals("00"))//success
                {
                    if(messageType.equals("0410")) {
                        Utility.DEBUG_LOG(TAG,"before deleteReversal after getting response (done for reversal only)");
                        generateReversal.deleteReversal();
                        isoReq.run();
                    }
                    else if(messageType.equals("0330")){
                        Utility.DEBUG_LOG(TAG,"process batch upload...");
                        sendBatchUploadTransactionOrCloseSettle();
                    }
                    else if(messageType.equals("0230")){
                        Utility.DEBUG_LOG(TAG,"post process after advice upload...");
                        //adviceTransaction.popTopAdviceTransaction();
                        postAdviceProcessing();
                    }
                    else {
                        Utility.DEBUG_LOG(TAG, "transactionDetailList.size:"+ String.valueOf(transactionDetailList.size()));
                       // transactionContract.transactionSuccess("settlement successful!");
//                        transactionContract.showProgress("Printing");
                        //masood comment this on 04-12-20
                        //insertSaveSettlement(appDatabase, transactionDetailList);

                        //savesettlement
                        SharedPref.countedTransactionWrite(context.get(), cardTransactionItemList);

                        try {
                            Utility.DEBUG_LOG(TAG, "before getAllTransactions() 2");
                            SharedPref.writeList(context.get(), transactionDetailList);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }



                        hideProgressMessage();
                        Utility.DEBUG_LOG(TAG,"before emptyTable");
                        //delete batch
                        emptyTable();

                        //increase batch
                        int tempBatchNo = SharedPref.read("batch_no");
                        SharedPref.write("old_batch_no",SharedPref.read("batch_no"));
                        tempBatchNo= tempBatchNo+1;
                        SharedPref.write("batch_no",tempBatchNo);
                        Utility.DEBUG_LOG(TAG,"wrote batch_no:"+tempBatchNo);
                        Constants.BATCH_NO = String.format(Locale.getDefault(),"%06d", SharedPref.read("batch_no"));
                        Repository repository = new Repository(context.get());
                        TerminalConfig terminalConfig = new TerminalConfig();
                        terminalConfig.setBatchNo(String.format(Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
                        Utility.DEBUG_LOG(TAG,"batch updated in db"+terminalConfig.getBatchNo());
                        repository.updateBatchNumber(terminalConfig.getBatchNo(),0);

                        SharedPref.write("isSettleRequiured","N");
                       // transactionContract.transactionSuccess("settlement successful!");
//                        transactionContract.showProgress("Printing");
                        try
                        {

                            //receiptPrinter.printSettlementReceipt(printer, assetManager, "Settlement Report", transactionDetailList, cardTransactionItemList, false);
                            Utility.DEBUG_LOG(TAG,"before new PrintRecpSettlement");

                            //PrinterCanvas pc = new PrinterCanvas(transactionDetailList,cardTransactionItemList);


                            new Thread(new Runnable() {
                                public void run() {
                                    Utility.DEBUG_LOG(TAG, "in detail thread");
                                    transPrinter = new PrintRecpSettlement(context.get(),transactionDetailList, cardTransactionItemList);
                                    transPrinter.initializeData(transactionDetailList, cardTransactionItemList);
                                    Utility.DEBUG_LOG(TAG,"before transPrinter.print");
                                    transPrinter.print(transactionDetailList);
                                    Utility.DEBUG_LOG(TAG, "after print detail thread");
                                   // transactionContract.hideProgress();
                                   // MainApplication.successPrintingShow(mcontext.get(),"Print Finished..");
                                }
                            }).start();



//
//                            transPrinter = new PrintRecpSettlement(context.get());
//                            transPrinter.initializeData(transactionDetailList, cardTransactionItemList);
//                            transPrinter.print();

//                            Utility.DEBUG_LOG(TAG,"go back to home fragment");
//                            DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            if(transactionDetailList!=null && cardTransactionItemList!=null) {
                                batchUploadRequest.makeJsonObjReq(context.get(), transactionDetailList, cardTransactionItemList);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        Handler handler = new Handler();
//                        Runnable runnable = new Runnable() {
//                            @Override
//                            public void run() {
//
//                                handler.postDelayed(this, 3000);
//                            }
//                        };
//                        handler.postDelayed(runnable, 3000);
//                        emptyTable(appDatabase);
//                        transactionContract.transactionSuccess("settlement successful!");
//
//                        int tempBatchNo = SharedPref.read("batch_no");
//                        int  testBat = tempBatchNo;
//                        SharedPref.write("temp_batch_no",testBat);
//                        tempBatchNo = tempBatchNo + 1;
//                        //Constants.BATCH_NO_TEST = tempBatchNo ;
//                        SharedPref.write("batch_no",tempBatchNo);
//                        Utility.DEBUG_LOG("batch_settle", String.valueOf(tempBatchNo));
//
//
//
//                        receiptPrinter.printSettlementReceipt(printer, assetManager, "Settlement Report",sumTransactionItemList,cardTransactionItemList,false);
                    }
                } else {

                    if(responseCode.equals("95")){
                        transactionContract.toastShow("BATCH UPLOAD");
                        Utility.DEBUG_LOG(TAG,"before getBatchUploadTransactions");
                        getBatchUploadTransactions();
                        Utility.DEBUG_LOG(TAG,"before sendBatchUploadTransactionOrCloseSettle");
                        sendBatchUploadTransactionOrCloseSettle();
                    }
                    else {
                        transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
                        DashboardContainer.backStack();
                    }
                }

            }
            isoResponse = null;
        }
    });

    public boolean isPrintFinished()
    {
        Utility.DEBUG_LOG(TAG,"+ isPrintFinished +");
        boolean rv = false;
        Utility.DEBUG_LOG(TAG,"transPrinter:"+transPrinter);
        if ( transPrinter != null )
        {
            rv =  transPrinter.isPrintFinished();
        }
        Utility.DEBUG_LOG(TAG,"transPrinter.isPrintFinished:"+rv);
        return rv;
    }

    private void makeSettlementPacket(String ProcessingCode) {
        //  invoiceNoToVoid
        ///00,03, 11 ,24 ,41 42 60 62 63
        SettlementPacketGenerator settlementPacketGenerator= new SettlementPacketGenerator();
        SparseArray<String> data8583_u_settlement = new SparseArray<>();

      /*  data8583_u_settlement.put(ISO8583u.F_MessageType_00, Constants.SETTLEMENT_MESSAGE_TYPE);
        data8583_u_settlement.put(ISO8583u.F_ProcessingCode_03, ProcessingCode);
        data8583_u_settlement.put(ISO8583u.F_STAN_11, stan);
        data8583_u_settlement.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_u_settlement.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
        data8583_u_settlement.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
        data8583_u_settlement.put( ISO8583u.F_ReservedNational_60 , Constants.RESERVED_NATIONAL );
        if(Constants.HOST_INDEX_FROM_DB!=0)
        { data8583_u_settlement.put( ISO8583u.F_KeyExchange_62 , Constants.BATCH_NO);}
          data8583_u_settlement.put( ISO8583u.F_SettlementData_63 , Field63 );
*/
        String Field63 = getTransactionsTypeAndCount(sumTransactionItemList);
        Field63=Utility.rightPadding(Field63,58,'0');
        Utility.DEBUG_LOG(TAG,"field63:"+Field63);

//        data8583 = settlementPacketGenerator.getSettlementPacket(ProcessingCode,Field63);
        data8583 = settlementPacketGenerator.getSettlementPacket(ProcessingCode,Field63);
    }


    /**
     * \Brief make settlement fields
     */
    public void doSettlement() {
//        showProgressMessage("Processing...");
        Utility.DEBUG_LOG(TAG,"before doSettlementData");
        makeSettlementPacket(Constants.SETTLEMENT_PROCESSING_CODE);
        Utility.DEBUG_LOG(TAG,"before doTransaction");
        doTransaction();
    }

    @SuppressLint("DefaultLocale")
    private void doTransaction() {

        TransactionDetail transactionDetail= new TransactionDetail();
        transactionDetail.setMessageType(Constants.SETTLEMENT_MESSAGE_TYPE);
        transactionDetail.setProcessingCode(Constants.SETTLEMENT_PROCESSING_CODE);
        transactionDetail.setStan(stan);
        transactionDetail.setStatus("settlement");

        //insertTransaction(appDatabase,transactionDetail);

        isoReq.start();
   }
    @SuppressLint("CheckResult")
    private  void emptyTable(){
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).transactionDetailDao().nukeTable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ }// completed with success
                        ,throwable ->{}// there was an error
                );
    }

 private void getBatchUploadTransactions(){
     Utility.DEBUG_LOG(TAG,"+ getBatchUploadTransactions +");
     batchUploadArray = batchUploadTransaction.getBatchUploadTransactionsList();
     batchUploadTransactionDetails = batchUploadTransaction.getbatchUploadTransactionDetails();
     Utility.DEBUG_LOG(TAG,"batchUploadTransactionDetails:"+batchUploadTransactionDetails);
//     Utility.DEBUG_LOG(TAG,"batchUploadArray:"+batchUploadArray.toString());
 }

 private void logRemainingBatchUploadTransactions()
 {
     Utility.DEBUG_LOG(TAG,"+ logRemainingBatchUploadTransactions +");
     for ( SparseArray<String> obj : batchUploadArray) {
        Utility.DEBUG_LOG(TAG,"transaction:"+obj.toString());
     }
 }

 private void sendBatchUploadTransactionOrCloseSettle(){

//     if (  batchUploadTransaction.popTopTransaction() )
//     {
//         batchUploadTransaction.getBatchUploadTransactionCount();
//         batchUploadData8583 = batchUploadTransaction.getTopTransactionInStringFormat();
////         batchUploadData8583 = batchUploadArray.get(0);
//         Utility.DEBUG_LOG(TAG,"batchUploadData8583:"+batchUploadData8583.toString());
//         Utility.DEBUG_LOG(TAG,"before isoReq.run()");
//         isoReq.run();
//     }
//     else{
//         Utility.DEBUG_LOG(TAG,"batchUploadArray size is zero, proceed with closing settlement process");
//         batchUploadData8583 =null;
//         doSettlementData(Constants.SETTLEMENT_PROCESSING_CODE_CLOSE);
//         isoReq.run();
//     }

     if(batchUploadArray.size()!=0) {
         Utility.DEBUG_LOG(TAG,"batchUploadArray.size:"+batchUploadArray.size());
         batchUploadData8583 = batchUploadArray.get(0);
         currentBatchUploadTransaction = batchUploadTransactionDetails.get(0);
         Utility.DEBUG_LOG(TAG,"batchUploadArray.get(0):"+batchUploadArray.get(0).toString());
         batchUploadArray.remove(0);
         batchUploadTransactionDetails.remove(0);
         logRemainingBatchUploadTransactions();
         isoReq.run();
     }
     else{
         Utility.DEBUG_LOG(TAG,"batchUploadArray size is zero, proceed with closing settlement process");
         batchUploadData8583 =null;
         makeSettlementPacket(Constants.SETTLEMENT_PROCESSING_CODE_CLOSE);
         isoReq.run();
     }

 }


private void postAdviceProcessing(){
    if (  adviceTransaction.popTopAdviceTransaction() )
    {
        Utility.DEBUG_LOG(TAG,"before isoReq.run()");
        isoReq.run();

    }
}

 private String getTransactionsTypeAndCount(List<CountedTransactionItem> countedTransactionItemList){
     int size = 0;
     try{
         size = countedTransactionItemList.size();
     }
     catch (Exception e){
      e.printStackTrace();
     }
     String totalAmountStr;
     String totalTransactionsStr;
     int totalTransactionsCount = 0;
     double totalAmount = 0.0f;

     StringBuilder tempfield63 = new StringBuilder();
     int tempTotalAmount;
     Utility.DEBUG_LOG(TAG,"+ getTransactionsTypeAndCount +");

     // for sale and redeem [to check for void]
     try {
         for (int i = 0; i < size; i++) {
             if (
                     countedTransactionItemList.get(i).getTxnType().equals("SALE")
                             || countedTransactionItemList.get(i).getTxnType().equals("REDEEM")
                             || countedTransactionItemList.get(i).getTxnType().equals("ADJUST")
             ) {
                 totalAmount += countedTransactionItemList.get(i).getTotalAmount();
                 totalTransactionsCount += countedTransactionItemList.get(i).getCount();
             }
         }
     }catch (Exception e){
         e.printStackTrace();
     }
     totalTransactionsStr = String.format(Locale.getDefault(), "%03d", totalTransactionsCount);
     totalAmountStr = String.format(Locale.getDefault(), "%012d", (long) (totalAmount * 100));
     tempfield63.append(totalTransactionsStr);
     tempfield63.append(totalAmountStr);
     Utility.DEBUG_LOG(TAG,"totalTransactionsStr:"+totalTransactionsStr);
     Utility.DEBUG_LOG(TAG,"totalAmountStr:"+totalAmountStr);
     Utility.DEBUG_LOG(TAG,"tempfield63:"+tempfield63);

     totalAmount = 0.0f;
     totalTransactionsCount = 0;
     // for refund
     for (int i = 0; i < size; i++) {
         if (
                 countedTransactionItemList.get(i).getTxnType().equals("REFUND")
         )
         {
             totalAmount += countedTransactionItemList.get(i).getTotalAmount();
             totalTransactionsCount += countedTransactionItemList.get(i).getCount();
         }
     }
     totalTransactionsStr = String.format(Locale.getDefault(), "%03d", totalTransactionsCount);
     totalAmountStr = String.format(Locale.getDefault(), "%012d", (int) (totalAmount * 100));
     tempfield63.append(totalTransactionsStr);
     tempfield63.append(totalAmountStr);
     Utility.DEBUG_LOG(TAG,"totalTransactionsStr:"+totalTransactionsStr);
     Utility.DEBUG_LOG(TAG,"totalAmountStr:"+totalAmountStr);
     Utility.DEBUG_LOG(TAG,"tempfield63:"+tempfield63);

//     for (int i = 0; i < size; i++) {
//         if (
//                 countedTransactionItemList.get(i).getTxnType().equals("SALE")
//                 || countedTransactionItemList.get(i).getTxnType().equals("REDEEM")
//            )
//         {
//             Utility.DEBUG_LOG(TAG, "sale check:" + String.valueOf(countedTransactionItemList.get(i).getTxnType().equals("SALE")));
//             totalTransactionsStr = String.format(Locale.getDefault(), "%03d", countedTransactionItemList.get(i).getCount());
//             //       tempTotalAmount= Integer.parseInt(countedTransactionItemList.get(i).getTotalAmount());
//             totalAmountStr = String.format(Locale.getDefault(), "%012d", (int) (Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString()) * 100));
//             //         totalAmount= String.format(Locale.getDefault(),"%012d", tempTotalAmount);
//             tempfield63.append(totalTransactionsStr);
//             tempfield63.append(totalAmountStr);
//         }
//     }


     return tempfield63.toString();
 }

    public void showProgressMessage(String msg)
    {
        Utility.DEBUG_LOG(TAG,"+ showProgressMessage +");
        transactionContract.showProgress(msg);
    }

    public void hideProgressMessage()
    {
        Utility.DEBUG_LOG(TAG,"+ hideProgressMessage +");
        transactionContract.hideProgress();
        try
        {
            sleep(1*1000);
        }
        catch (Exception e)
        {
            Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString());
        }
    }


    private List <CountedTransactionItem> sumTransactionItemList;
    @SuppressLint("CheckResult")
    private void sumAndCountTransactions(){
        AppDatabase.getAppDatabase(context.get()).transactionDetailDao().transactionSumCount()
                .subscribeOn(Schedulers.newThread())
                .subscribe(count ->
                        { sumTransactionItemList=count;
                        }
                );
    }



    private List <CountedTransactionItem> cardTransactionItemList;
    @SuppressLint("CheckResult")
    private void cardAndCountTransactions(){
        AppDatabase.getAppDatabase(context.get()).transactionDetailDao().transactionCardSchemeCount()
                .subscribeOn(Schedulers.newThread())
                .subscribe(count ->
                        { cardTransactionItemList=count;
                        }
                );
    }

    //masood commented on 04-12-20

    private void getAdviceTransactionsArray(){
        adviceArray = adviceTransaction.getAdviceTransactionsList();
    }


}
