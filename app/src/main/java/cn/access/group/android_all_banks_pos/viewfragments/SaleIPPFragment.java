
package cn.access.group.android_all_banks_pos.viewfragments;



import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSale;
import cn.access.group.android_all_banks_pos.adapter.aidListAdapter;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_Ctls;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_Dip;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_NA;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_SeeYourPhone;
import static cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter.FallbackOrignatorType.FT_Swipe;

/**
 * @author muhammad.humayun
 * on 8/20/2019.
 * this is a view call which is reponsible for starting the hardware service and invoking the presenter of this view
 */
@SuppressLint("ValidFragment")
public class SaleIPPFragment extends Fragment implements TransactionContract,PaymentContract {

    @BindView(R.id.threeMonths)
    CardView threeMonths;

    @BindView(R.id.sixMonths)
    CardView sixMonths;

    @BindView(R.id.nineMonths)
    CardView nineMonths;

    @BindView(R.id.twelveMonths)
    CardView twelveMonths;

    @BindView(R.id.fifteenMonths)
    CardView fifteenMonths;

    @BindView(R.id.eighteenMonths)
    CardView eighteenMonths;

    @BindView(R.id.twentyoneMonths)
    CardView twentyoneMonths;

    @BindView(R.id.twentyfourMonths)
    CardView twentyfourMonths;

    @BindView(R.id.twentysevenMonths)
    CardView twentysevenMonths;

    @BindView(R.id.thirtytMonths)
    CardView thirtytMonths;

    @BindView(R.id.thirtythreeMonths)
    CardView thirtythreeMonths;

    @BindView(R.id.thirtysixMonths)
    CardView thirtysixMonths;

    @BindView(R.id.saleipp_layout)
    TableLayout saleipp_layout;

    @BindView(R.id.cardBoxLayout)
    ConstraintLayout cardBoxLayout;

    @BindView(R.id.selectMonths)
    TextView selectMonths;


    @BindView(R.id.imageView9)
    ImageView imageView9;
    @BindView(R.id.imageView11)
    ImageView imageView11;

    String transactionAmount;
    String tipAmount="0.00";
    PaymentAmountPresenter paymentAmountPresenter;
    Unbinder unbinder;
    IEMV iemv;
    IPinpad ipinpad;
    IBeeper iBeeper;
    IPrinter printer;
    private ReceiptPrinter receiptPrinter;
    public static IDeviceService idevice;

    IRFCardReader irfCardReader;
    boolean isSucc;
    AssetManager assetManager;
    ISerialPort serialPort;
    private static final String TAG = "EMVDemo";
    Animation animSlideRight;
    Intent intent = new Intent();
    Dialog alertDialog;
    ListView aidListView;
    public String current = "";
    public boolean bool;
    public boolean isFallback;
    private WeakReference<Context> mContext;
//    public boolean isFallback;
    PaymentAmountPresenter.FallbackOrignatorType fallbackOrignatorType = FT_NA;
    IDeviceInfo iDeviceInfo;
    TransPrinter transPrinter;
    String saleippMonths = "";


    @SuppressLint("ValidFragment")
    public SaleIPPFragment(IEMV iemv, IPinpad ipinpad, IBeeper iBeeper,
                           IPrinter printer, AppDatabase appDatabase, String transactionAmount, ISerialPort serialPort, IDeviceInfo iDeviceInfo, AssetManager assetManager) {
        receiptPrinter = new ReceiptPrinter(handler);
        this.iemv = iemv;
        this.ipinpad = ipinpad;
        this.iBeeper = iBeeper;
        this.printer = printer;
        this.transactionAmount = transactionAmount;
        this.serialPort = serialPort;
        this.iDeviceInfo = iDeviceInfo;
        this.assetManager = assetManager;
    }

    @SuppressLint("ValidFragment")
    public SaleIPPFragment(PaymentAmountPresenter.FallbackOrignatorType fallbackType, String transactionAmount,String saleippMonths) {
        Utility.DEBUG_LOG(TAG,"+ SaleIPPFragment(FallbackType) +");

        // Required empty public constructor
        receiptPrinter = new ReceiptPrinter(handler);
        this.fallbackOrignatorType = fallbackType;
        this.transactionAmount = transactionAmount;
        this.saleippMonths = saleippMonths;
        //+ taha 23-02-2021 logic to handle zero padded amount or decimal type amount
        if( this.transactionAmount.indexOf(".") > 0 )//actual amount, do nothing
        {

        }
        else//make it decimal type
        {
            double d = Double.parseDouble(this.transactionAmount);
            Utility.DEBUG_LOG(TAG,"d org=" + d);
            d = d / 100.0f;
            Utility.DEBUG_LOG(TAG,"d new=" + d);
            String s = String.valueOf(d);
            Utility.DEBUG_LOG(TAG,"s=" + s );
            this.transactionAmount = s;
            Utility.DEBUG_LOG(TAG,"new this.transactionAmount="+this.transactionAmount);
        }
        Utility.DEBUG_LOG(TAG,"fallbackType: "+ fallbackType + "; transactionAmount: " + transactionAmount);

    }


    public SaleIPPFragment(PaymentAmountPresenter.FallbackOrignatorType fallbackType, String transactionAmount, String saleippMonths,String tipAmount) {
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountFragment(FallbackType) +");

        // Required empty public constructor
        receiptPrinter = new ReceiptPrinter(handler);
        this.fallbackOrignatorType = fallbackType;
        this.transactionAmount = transactionAmount;
        if(TIP_ENABLED.equals("Y")) {
            this.tipAmount = DataFormatterUtil.formattedAmount(tipAmount);
            Utility.DEBUG_LOG(TAG,"tipamount"+ DataFormatterUtil.formattedAmount(tipAmount));
        }
        //+ taha 23-02-2021 logic to handle zero padded amount or decimal type amount
        if (this.transactionAmount.indexOf(".") > 0)//actual amount, do nothing
        {

        } else {
            double d = Double.parseDouble(this.transactionAmount);
            Utility.DEBUG_LOG(TAG, "d org=" + d);
            d = d / 100.0f;
            Utility.DEBUG_LOG(TAG, "d new=" + d);
            String s = String.valueOf(d);
            Utility.DEBUG_LOG(TAG, "s=" + s);
            this.transactionAmount = s;
            Utility.DEBUG_LOG(TAG, "new this.transactionAmount=" + this.transactionAmount);
        }
        Utility.DEBUG_LOG(TAG, "fallbackType: " + fallbackType + "; transactionAmount: " + transactionAmount);

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.DEBUG_LOG("SALEIPP", "+ FragmentTipAdjust:onCreateView() +");
        //Repository repo = new Repository(AppDatabase.getAppDatabase(Context));
        View rootView = inflater.inflate(R.layout.fragment_saleipp, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        if (this.fallbackOrignatorType != FT_NA) {
            if (fallbackOrignatorType == FT_Dip) {
                selectMonths.setText("Swipe");
                saleipp_layout.setVisibility(View.GONE);
                animationsFlow();
            } else if (fallbackOrignatorType == FT_Swipe) {
                selectMonths.setText("Tap / Insert / Swipe");
                saleipp_layout.setVisibility(View.GONE);
                animationsFlow();
            } else if (fallbackOrignatorType == FT_Ctls) {
                selectMonths.setText("Insert / Swipe");
                saleipp_layout.setVisibility(View.GONE);
                animationsFlow();
            } else if (fallbackOrignatorType == FT_SeeYourPhone) {
                selectMonths.setText("Tap / Insert / Swipe");
                saleipp_layout.setVisibility(View.GONE);
                animationsFlow();
            }
        }else{
            selectMonths.setText("Select Months");
        }

        cardBoxLayout = (ConstraintLayout) rootView.findViewById(R.id.cardBoxLayout);
        receiptPrinter = new ReceiptPrinter(handler);

        assetManager = getResources().getAssets();



        return rootView;
    }



    @OnClick({R.id.threeMonths,R.id.sixMonths,R.id.nineMonths,R.id.twelveMonths,R.id.fifteenMonths,R.id.eighteenMonths,R.id.twentyoneMonths,R.id.twentyfourMonths,R.id.twentysevenMonths,R.id.thirtytMonths,R.id.thirtythreeMonths,R.id.thirtysixMonths})
    @Nullable

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.threeMonths:
                saleippMonths = "03";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;

            case R.id.sixMonths:
                saleippMonths = "06";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;

            case R.id.nineMonths:
                saleippMonths = "09";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.twelveMonths:
                saleippMonths = "12";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.fifteenMonths:
                saleippMonths = "15";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.eighteenMonths:
                saleippMonths = "18";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.twentyoneMonths:
                saleippMonths = "21";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.twentyfourMonths:
                saleippMonths = "24";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.twentysevenMonths:
                saleippMonths = "27";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.thirtytMonths:
                saleippMonths = "30";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.thirtythreeMonths:
                saleippMonths = "33";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
            case R.id.thirtysixMonths:
                saleippMonths = "36";
                paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                animationsFlow();
                break;
        }
    }


    private void animationsFlow() {

        Animation animSlideLeft = AnimationUtils.loadAnimation(mContext.get(), R.anim.slide_out_left);
        saleipp_layout.startAnimation(animSlideLeft);
        animSlideRight = AnimationUtils.loadAnimation(mContext.get(), R.anim.slide_in_faster);
        saleipp_layout.setVisibility(View.GONE);
        if ( fallbackOrignatorType == FT_NA )
        {
            selectMonths.setText("Tap / Insert / Swipe");
        }
        animSlideLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")){
                    Utility.DEBUG_LOG(TAG, "SL TIP AMOUNT:"+ tipAmount);
                    Utility.DEBUG_LOG(TAG, "before paymentAmountPresenter.doPurchase");
                    paymentAmountPresenter.doPurchase(transactionAmount,tipAmount);
                }
                else {

                    Utility.DEBUG_LOG(TAG,"transactionAmount:"+transactionAmount);
                    if ( fallbackOrignatorType != FT_NA )
                    {
                        paymentAmountPresenter = new PaymentAmountPresenter(SaleIPPFragment.this, SaleIPPFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, transactionAmount,"SALEIPP",mContext.get(),serialPort, iDeviceInfo,saleippMonths);
                        Utility.DEBUG_LOG(TAG,"transactionAmount:"+transactionAmount);
                        Utility.DEBUG_LOG(TAG,"before doPurchase 4 fallback from [sale ipp]:" + fallbackOrignatorType);
                        Utility.DEBUG_LOG(TAG,"before doPurchase 4 fallback from [sale ipp months]:" + saleippMonths);

                        paymentAmountPresenter.doPurchase(transactionAmount,saleippMonths,"0", fallbackOrignatorType);
                    }
                    else
                    {
                        Utility.DEBUG_LOG(TAG,"before paymentAmountPresenter.doPurchase SaleIpp");
                        paymentAmountPresenter.doPurchase(transactionAmount,saleippMonths,"0");
                    }

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        //masood commneted this
//        assetManager = getResources().getAssets();
//        appDatabase = AppDatabase.getAppDatabase(context);
        alertDialog=new Dialog(context);
        mContext = new WeakReference<>(context);

        Utility.DEBUG_LOG("SALEIPP","PaymentAmountFragment:onAttach()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if ( getActivity() != null)
            getActivity().unbindService(conn);
    }

//masood commeneted this
    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private final ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            new CountDownTimer(01, 01) {
                @SuppressLint("LongLogTag")
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);
                    try {
                        iemv = idevice.getEMV();
                        ipinpad = idevice.getPinpad(1);
                        iBeeper = idevice.getBeeper();
                        printer = idevice.getPrinter();
                        irfCardReader = idevice.getRFCardReader();
                        iDeviceInfo = idevice.getDeviceInfo();
                        Utility.DEBUG_LOG("Total Ram Payment", String.valueOf(iDeviceInfo.getRamTotal()));
                        Utility.DEBUG_LOG("Available Ram Payment", String.valueOf(iDeviceInfo.getRamAvailable()));
                        Utility.DEBUG_LOG("Battery Level Payment", String.valueOf(iDeviceInfo.getBatteryLevel()));
                        Utility.DEBUG_LOG("Battery Temperature Payment", String.valueOf(iDeviceInfo.getBatteryTemperature()));
                        Utility.DEBUG_LOG("Data Usage Level Payment", String.valueOf(iDeviceInfo.getMobileDataUsageTotal()));
                        // Utility.DEBUG_LOG("IdeviceSystemTime", String.valueOf(iDeviceInfo.updateSystemTime("20210205","130000")));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }

                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    /**
     * this method animates the view, and executes the purchase function of the presenter which takes amount as parameter
     */

    // mmasood: countdown timer in sales screen


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Utility.DEBUG_LOG("SALEIPP","+ PaymentFragment:Handler:handleMessage()+ ");
            Utility.DEBUG_LOG(TAG,"before getData");

            String str = msg.getData().toString();
            Utility.DEBUG_LOG("SALEIPP","Message:" + str);
            super.handleMessage(msg);
//            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void toastShow(String str) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (str.contentEquals("show")) {
                        cardBoxLayout.setVisibility(View.VISIBLE);
                        cardBoxLayout.startAnimation(animSlideRight);
                    } else {
                        Message msg = new Message();
                        Utility.DEBUG_LOG(TAG,"before getData");

                        msg.getData().putString("msg", str);
                        handler.sendMessage(msg);
                        // Toast.makeText(getContext(), str, Toast.LENGTH_SHORT).show();
                        Utility.DEBUG_LOG(TAG, str);
                    }
                }
            });
        }
        else{
            Utility.DEBUG_LOG("showProgress","Error Dialog null");
        }
    }

    @Override
    public void showProgress(String title) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    saleipp_layout.setVisibility(View.GONE);
                    cardBoxLayout.setVisibility(View.GONE);
                    MainApplication.showProgressDialog(mContext.get(), title);
                }
            });
        }
        else{
            Utility.DEBUG_LOG("showProgress","Error Dialog null");
        }

    }

    @Override
    public void hideProgress() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.hideProgressDialog();
                }
            });
        }else{
            Utility.DEBUG_LOG("hideProgress","Error Dialog null");
        }
    }

    @Override
    public void showPaymentSuccessDialog(TransactionDetail transactionDetail) {
        DialogPaymentSuccessFragment newFragment = new DialogPaymentSuccessFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("transactionDetail", transactionDetail);
        newFragment.setArguments(bundle);
        DashboardContainer.switchFragmentWithBackStack(newFragment, "success fragment");
    }

    @Override
    public void showMultipleAid(List<String> aidList) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final aidListAdapter myadapter = new aidListAdapter(mContext.get(), R.layout.row_item, aidList);
                    aidListView.setAdapter(myadapter);
                    aidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            //PASS POSITION TO PRESENTER
                            position = position + 1;
                            paymentAmountPresenter.selectedAidIndex(position);
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
            });
        }
        else{
            Utility.DEBUG_LOG("showMultipleAid","Error Dialog null");
        }
    }

    @Override
    public void transactionFailed(String title, String error) {
        if(getActivity() != null ) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.errorDialog(mContext.get(), title, error);
                }
            });
        }
        else{
            Utility.DEBUG_LOG("transactionFailed","Error Dialog null");
        }
    }

    @Override
    public void rocEmpty(String content, TransactionDetail transactionDetail, String copy, String txnType){
//        getActivity().runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                DialogUtil.confirmDialogCustomer(getContext(), content, sweetAlertDialog1 -> {
//                    receiptPrinter.printSaleReceipt(printer, assetManager,transactionDetail , "aid",copy,txnType);
//                    sweetAlertDialog1.dismissWithAnimation();
//                    SharedPref.countedTransactionWrite(getContext(),null);
//                    SharedPref.writeList(getContext(),null);
//                });
//            }
//        });
    }

    @Override
    public boolean last4Digits(String text1) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.inputDialog(mContext.get(), "Enter Last 4 Digits", "Enter Last 4 Digits", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                            //show confirmation
                            if (text1.equals(text.getText().toString())) {
                                bool = true;
                                sweetAlertDialog.dismissWithAnimation();
                            } else {
                                bool = false;
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        }
                    }, InputType.TYPE_CLASS_NUMBER, null);

                }
            });
            return bool;
        }
        else{
            Utility.DEBUG_LOG("Last$didgits", String.valueOf(bool));
            return bool;

        }
    }

    @Override
    public boolean last4Digits2(TransactionDetail transactionDetail, String text) {
//        String rv = "";
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "+ PaymentAmountFragment::last4Digits_taha +");
//        last4DigitsStr = "";
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.inputDialog(mContext.get(), "Enter Last 4 Digits", "Enter Last 4 Digits", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                            transactionDetail.setLast4Digits(text.getText().toString());
                            Utility.DEBUG_LOG(TAG, "last4DigitsStr:" + transactionDetail.getLast4Digits());
//                            rv = text.getText().toString();
                            //show confirmation
                            if (text.equals(text.getText().toString())) {
//                                bool = true;
//                                rv = true;
                                sweetAlertDialog.dismissWithAnimation();
                            } else {
//                                bool = false;
//                                rv = false;
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        }
                    }, InputType.TYPE_CLASS_NUMBER, null);

                }
            });
//            return bool;
        } else {
            Utility.DEBUG_LOG("Last$didgits", String.valueOf(bool));
//            return bool;

        }
        return rv;
    }

    @Override
    public boolean panManualEntry(TransactionDetail transactionDetail, String text){
        return false;
    }

    @Override
    public void transactionSuccess(String content) {

    }

    @Override
    public void transactionSuccessCustomer(String content, TransactionDetail transactionDetail,String copy, String txnType, String balance, boolean redeemCheck) {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!(txnType.equals("ORBIT INQUIRY"))) {
                    try {
                        Thread.sleep(3000);
                    }
                    catch (Exception e)
                    {
                        Utility.DEBUG_LOG(TAG,"Exception: "+e.getMessage().toString());
                    }
                    Utility.DEBUG_LOG(TAG,"before showing dialog of customer copy");
                    DialogUtil.confirmDialogCustomer(mContext.get(), content, "Print Customer Copy", "", sweetAlertDialog1 -> {
                        receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail,transactionDetail.getAidCode() , "CUSTOMER COPY", txnType, "", balance,redeemCheck);

                        //receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "Customer Copy", txnType, "", balance,redeemCheck);
                        sweetAlertDialog1.dismissWithAnimation();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                        SharedPref.countedTransactionWrite(getContext(),null);
//                        SharedPref.writeList(getContext(),null);
//                        Constants.BANK_COPY = true;
//                            if(Constants.BANK_COPY) {
//                                DialogUtil.confirmDialogCustomer(getContext(), content, "Print Bank Copy", "", sweetAlertDialog1 -> {
//                                    receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "Bank Copy", txnType);
//                                    sweetAlertDialog1.dismissWithAnimation();
//                                });
//                            }
//                            Constants.BANK_COPY=false;
//                        }
//                    }, 1000);
                    });


                }
                else{
                    receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, "aid", "Customer Copy", txnType, "", balance,redeemCheck);
                }
            }
        });
    }

    // log & display handler
//    @SuppressLint("HandlerLeak")
//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            Utility.DEBUG_LOG(TAG,"+ PaymentFragment:handleMessage()+ ");
//            String string = msg.getData().getString("string");
//            super.handleMessage(msg);
//            Utility.DEBUG_LOG(TAG, msg.getData().getString("msg"));
////            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
//
//        }
//    };
    public boolean getIsFallback() {
        return isFallback;
    }

    public PaymentAmountPresenter.FallbackOrignatorType getFallbackOrignatorType() {
        return fallbackOrignatorType;
    }


}


