package cn.access.group.android_all_banks_pos.repository.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;

/**
 * on 10/29/2019.
 */
@Dao
public interface CardRangesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConfig(List<CardRanges> cardRanges);
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertManyConfig(List<TerminalConfig> terminalConfig);
//
    @Query("SELECT * FROM CardRanges ")
    List<CardRanges> cardRangesConfig();


//
//    @Query("SELECT * FROM terminalconfig WHERE tcid LIKE  :tcid ")
//    TerminalConfig terminalConfigurationByID(int tcid);
//
    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateCardRanges(CardRanges cardRanges);
//
    @Query("DELETE FROM cardranges")
    void nukeTable();


    @Query("SELECT * FROM CardRanges WHERE :pan BETWEEN LowerRange AND HigherRange")
    List<CardRanges> cardRangeCheck(int pan);
}
