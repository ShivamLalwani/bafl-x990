package cn.access.group.android_all_banks_pos.Utilities;


import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;


/**
 * Created by Simon on 2019/4/29.
 */

public class SSLComm {

    private static final String TAG = "SSLComm";
    private static final String CLIENT_KEY_PASSWORD = "password";
    private static final String TRUST_KEY_PASSWORD = "password";
    static SSLContext ssl_ctx;
    private WeakReference<Context> context;
    InputStream keyStream = null;  // for cert
    SSLSocket socket;
    String host;
    int port;
    SSLClient.ACTION action = SSLClient.ACTION.connect;

    ;

    public SSLComm(Context context, String host, int port) {
        try {
            this.context =new WeakReference<>(context);
            this.host = host;
            this.port = port;
            keyStream = context.getAssets().open("keystore/TLSkeystore.bks");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private SSLContext getSSLContext(int timeOutMillisecond, InputStream... certificates) {
//        long timeout = timeOutMillisecond - 1;
//        try {
//            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
//            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            keyStore.load(null);
//            int index = 0;
//            for (InputStream certificate : certificates) {
//                String certificateAlias = Integer.toString(index++);
//                keyStore.setCertificateEntry(certificateAlias, certificateFactory.generateCertificate(certificate));
//                try {
//                    if (certificate != null)
//                        certificate.close();
//                } catch (IOException e) {
//                }
//            }
//            SSLContext sslContext = SSLContext.getInstance("TLS");
//            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory
//                    .getDefaultAlgorithm());
//            trustManagerFactory.init(keyStore);
//            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
//            return sslContext;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
        //取得TLS协议的SSLContext实例
        try {
//            SSLContext sslContext = SSLContext.getInstance("TLS");
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");

            KeyStore clientKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            clientKeyStore.load(context.get().getResources().getAssets().open("keystore/TLSkeystore.bks"), CLIENT_KEY_PASSWORD.toCharArray());
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("X509");
            keyManagerFactory.init(clientKeyStore, CLIENT_KEY_PASSWORD.toCharArray());

            //取得BKS类型的密钥库实例，这里特别注意：手机只支持BKS密钥库，不支持Java默认的JKS密钥库 Get keystore instance of BKS type, if you just
            KeyStore trustKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustKeyStore.load(context.get().getResources().getAssets().open("keystore/TLSkeystore.bks"), TRUST_KEY_PASSWORD.toCharArray());
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("X509");
            trustManagerFactory.init(trustKeyStore);

            //初始化SSLContext实例
            sslContext.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), null);
            return sslContext;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean connect() {

        Utility.DEBUG_LOG(TAG, "call connect");
        SSLSocketFactory socketFactory = (SSLSocketFactory) getSSLContext(20000, keyStream).getSocketFactory();

        try {
            socket = (SSLSocket) socketFactory.createSocket(host, port);
            Utility.DEBUG_LOG(TAG, "HostIP SSL:" + host);
            Utility.DEBUG_LOG(TAG, "PORT SSL:" + String.valueOf(port));
//            this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            Handler.output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            //masood commented

            socket.startHandshake();
            Utility.DEBUG_LOG(TAG, "SSL Created the socket, input, and output!!");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        // Utility.DEBUG_LOG(TAG, "connect return");


    }

    public int send(byte[] data) {
        Utility.DEBUG_LOG(TAG, "call send");
        OutputStream outputStream = null;
        Utility.DEBUG_LOG(TAG, "SSL data:" + String.valueOf(data.length));
        try {
            outputStream = socket.getOutputStream();
            if (null == outputStream) {
                return 0;
            }
            outputStream.write(data);
            if (null != outputStream) {
                outputStream.flush();
            }
            Utility.DEBUG_LOG(TAG, "SSL Created the socket, input, and output!!");
            Utility.DEBUG_LOG(TAG, "send return");
            return data.length;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public byte[] receive(int wantLength, int timeoutSecond) throws IOException {
        // sslComm.receive();
//        if (status <= 0) {
//            return null;
//        }
//
        Utility.DEBUG_LOG(TAG, "revieve:");
        socket.setSoTimeout(timeoutSecond * 1000);
        InputStream inputStream = socket.getInputStream();
//
//
        Utility.DEBUG_LOG(TAG, "revieve:");
//
        if (null == inputStream) {
            return null;
        }
        byte[] tmp = new byte[wantLength];
        int recvLen = inputStream.read(tmp);
        if (recvLen > 0) {
            Utility.DEBUG_LOG(TAG, "SSL recvLen:" + String.valueOf(recvLen));
            byte[] ret = new byte[recvLen];
            System.arraycopy(tmp, 0, ret, 0, recvLen);
            String packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2, Utility.byte2HexStr(ret), ' ');
            Utility.DEBUG_LOG(TAG, "SSL received:" + packetToPrint);
            return ret;
        } else if (recvLen == 0) {
            return null;
        }

        return null;
    }

    public int receive() {
        Utility.DEBUG_LOG(TAG, "call receive");
        BufferedReader input = null;
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line;
            do {
                line = input.readLine();
                while (line == null) {
                    line = input.readLine();
                }
                Utility.DEBUG_LOG(TAG, "SSL receive:" + line);

                // Parse the message and do something with it
                // Done in a different class
//                OtherClass.parseMessageString(line);
            }
            while (!line.equals("exit|"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (null != input) {
                input.close();
            }
        } catch (IOException ioe) {
        } finally {

        }

        Utility.DEBUG_LOG(TAG, "receive return");

        return 0;

    }

//    public int send(byte[] data) {
//        Utility.DEBUG_LOG(TAG, "call send");
//        PrintWriter output = null;
//        try {
//            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
//            output.write("This is a SSL test\n");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Utility.DEBUG_LOG("SSL", "Created the socket, input, and output!!");
//        try {
//            if (null != output) {
//                output.close();
//            }
//        } finally {
//        }
//        Utility.DEBUG_LOG(TAG, "send return");
//        return 0;
//    }

//    public int receive() {
//        Utility.DEBUG_LOG(TAG, "call receive");
//        BufferedReader input = null;
//        try {
////            socket.setSoTimeout(timeoutSecond * 1000);
////            inputStream = socket.getInputStream();
//            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            String line;
//            do {
//                line = input.readLine();
//                while (line == null) {
//                    line = input.readLine();
//                }
//                Utility.DEBUG_LOG("SSL receive:", line);
//
//                // Parse the message and do something with it
//                // Done in a different class
////                OtherClass.parseMessageString(line);
//            }
//            while (!line.equals("exit|"));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            if (null != input) {
//                input.close();
//            }
//        } catch (IOException ioe) {
//        } finally {
//
//        }
//
//        Utility.DEBUG_LOG(TAG, "receive return");
//
//        return 0;
//
//    }

    public int close() {
        Utility.DEBUG_LOG(TAG, "call close");
        try {
            socket.close();
        } catch (IOException ioe) {
        } finally {

        }

        Utility.DEBUG_LOG(TAG, "close return");
        return 0;

    }


    enum ACTION {
        connect,
        send,
        receive
    }

}

